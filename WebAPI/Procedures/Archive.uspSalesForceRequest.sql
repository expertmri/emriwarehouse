USE WebAPI
GO

CREATE PROCEDURE Archive.uspSalesForceRequest 
	@SalesForceRequestId bigint
   
AS 
BEGIN
	SET NOCOUNT ON;

	BEGIN
		INSERT INTO WebAPI.Archive.SalesForceRequest(
				SalesForceRequestId
				, SalesForceRequestToken
				, RequestAction
				, RequestPayload
				, RequestInsertDate
				, Activity				
				, InsertDate
			)
		SELECT	SalesForceRequestId
				, SalesForceRequestToken
				, RequestAction
				, RequestPayload
				, InsertDate
				, 'DELETE'
				, GETDATE()
		FROM WebAPI.dbo.SalesForceRequest 
		WHERE SalesForceRequestId = @SalesForceRequestId
	END

END
	
GO




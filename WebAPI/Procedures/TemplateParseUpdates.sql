USE EMRIWarehouse
GO

ALTER PROCEDURE SF.uspAccountMerge
	@jsonRequest nvarchar(max)
AS
--------NEED TO ADD UPDATE FOR ACCOUNT CONTACT TABLE
--DROP TABLE #src
--DECLARE @jsonRequest nvarchar(max) = '{"SalesForceRequestToken":"1234ABCD","RequestAction":"AccountMerge","OriginalSFAccountId":"001f40000081QaSAAU","MergedSFAccountId":"001f40000081QaTAAU","AccountName":"Dr Feelgood Practice","Phone":"813-685-6723","Fax":"813-685-6799","Website":"www.abc123.com","Facility":"Torrance","Address1":"123 Sesame Street","Address2":"Suite B","City":"Scott","StateName":"California","Zip":"91001","ParentId":"a04f4000004dYibAAE","AccountManager":"Jim Jones","Territory":"West LA"}'
DECLARE @jsonRequest nvarchar(max) = '{"SalesForceRequestToken":"1234ABCD","RequestAction":"AccountMerge","OriginalSFAccountId":"001f40000081QaSAAU","MergedSFAccountId":"001f40000081QaTAAU","SFUser":"John Doe"}'


--Merge to '001f40000081QaTAAU' --19
--Merge from '001f40000081QaSAAU' --16,17,18

--BEGIN TRY
	
	DECLARE @MergedAccountId varchar(255)
			, @OriginalAccountId varchar(255)
			, @SalesForceRequestToken varchar(255)
			, @SFUser varchar(255)
	--SET @MergedAccountId = (SELECT MergedSFAccountId FROM #src)
	--SET @OriginalAccountId = (SELECT OriginalSFAccountId FROM #src)
	--SET @SalesForceRequestToken = (SELECT SalesForceRequestToken FROM #src)


	--CREATE TABLE #src (
	--	SalesForceRequestToken varchar(255)
	--	, OriginalSFAccountId varchar(255)
	--	, MergedSFAccountId varchar(255)
	--	, SFUser varchar(255)
		--, AccountName varchar(255)
		--, Phone varchar(255)
		--, Fax varchar(255)
		--, Website varchar(255)
		--, Facility varchar(255)
		--, Address1 varchar(255)
		--, Address2 varchar(255)
		--, City varchar(255)
		--, StateName varchar(255)
		--, Zip varchar(255)
		--, AccountManager varchar(255)
		--, Territory varchar(255)
		--, ParentId varchar(255)
		--)

	--INSERT INTO #src (
	--	SalesForceRequestToken
	--	, OriginalSFAccountId
	--	, MergedSFAccountId
	--	, SFUser 
		--, AccountName
		--, Phone
		--, Fax
		--, Website
		--, Facility
		--, Address1
		--, Address2
		--, City
		--, StateName
		--, Zip
		--, AccountManager
		--, Territory
		--, ParentId
		--)


	SELECT	@SalesForceRequestToken = SalesForceRequestToken
			, @OriginalAccountId = OriginalSFAccountId
			, @MergedAccountId = MergedSFAccountId
			, @SFUser = SFUser
			--, AccountName
			--, Phone
			--, Fax
			--, Website
			--, Facility
			--, Address1
			--, Address2
			--, City
			--, StateName
			--, Zip
			--, AccountManager
			--, Territory
			--, ParentId
	FROM OPENJSON(@jsonRequest)
	WITH (
			SalesForceRequestToken  nvarchar(255) 'strict $.SalesForceRequestToken'
			, OriginalSFAccountId nvarchar(255) '$.OriginalSFAccountId'
			, MergedSFAccountId nvarchar(255) '$.MergedSFAccountId'
			, SFUser nvarchar(255) '$.SFUser' 
			--, AccountName nvarchar(255) '$.AccountName'
			--, Phone nvarchar(255) '$.Phone'
			--, Fax nvarchar(255) '$.Fax'
			--, Website nvarchar(255) '$.Website'
			--, Facility nvarchar(255) '$.Facility'
			--, Address1 nvarchar(255) '$.Address1'
			--, Address2 nvarchar(255) '$.Address2'
			--, City nvarchar(255) '$.City'
			--, StateName nvarchar(255) '$.StateName'
			--, Zip nvarchar(255) '$.Zip'
			--, AccountManager nvarchar(255) '$.AccountManager'
			--, Territory nvarchar(255) '$.Territory'
			--, ParentId nvarchar(255) '$.ParentId'
		) 
	--SELECT * FROM #src	
	SELECT @SalesForceRequestToken, @OriginalAccountId, @MergedAccountId, @SFUser
	
	--PERFORM CHECK FOR BOTH ACCOUNTS AND FAIL THE ROW
	IF NOT EXISTS (SELECT * FROM Integration.dbo.Accounts WHERE SF_AccountId = @OriginalAccountId)
	BEGIN
		UPDATE WebAPI.dbo.SalesForceRequest
		SET IsError =1
			, ErrorReason = 'SF_AccountId ' + @OriginalAccountId + 'is not in Integration.dbo.Accounts'
		WHERE SalesForceRequestToken = @SalesForceRequestToken

		RETURN;
	END

	IF NOT EXISTS (SELECT * FROM EMRIWarehouse.SF.Accounts WHERE SF_AccountId = @OriginalAccountId)
	BEGIN
		UPDATE WebAPI.dbo.SalesForceRequest
		SET IsError =1
			, ErrorReason = 'SF_AccountId ' + @OriginalAccountId + 'is not in EMRIWarehouse.SF.Accounts'
		WHERE SalesForceRequestToken = @SalesForceRequestToken

		RETURN;
	END

	IF NOT EXISTS (SELECT * FROM Integration.dbo.Accounts WHERE SF_AccountId = @MergedAccountId)
	BEGIN
		UPDATE WebAPI.dbo.SalesForceRequest
		SET IsError =1
			, ErrorReason = 'SF_AccountId ' + @MergedAccountId + 'is not in Integration.dbo.Accounts'
		WHERE SalesForceRequestToken = @SalesForceRequestToken

		RETURN;
	END

	IF NOT EXISTS (SELECT * FROM EMRIWarehouse.SF.Accounts WHERE SF_AccountId = @MergedAccountId)
	BEGIN
		UPDATE WebAPI.dbo.SalesForceRequest
		SET IsError =1
			, ErrorReason = 'SF_AccountId ' + @MergedAccountId + 'is not in EMRIWarehouse.SF.Accounts'
		WHERE SalesForceRequestToken = @SalesForceRequestToken

		RETURN;
	END

	BEGIN TRAN 
	
		DELETE 
		FROM EMRIWarehouse.SF.Accounts_test
		WHERE SF_AccountId = @OriginalAccountId
	
		--Prob don't want to update this table unless we are getting back updated fields as well
		/*
		UPDATE dest
		SET
			dest.SF_AccountId = src.MergedSFAccountId
			, dest.AccountName = src.AccountName
			, dest.Phone = src.Phone	
			, dest.Fax = src.Fax			
			, dest.Website = src.Website
			, dest.Address1 = src.Address1
			, dest.Address2 = src.Address2
			, dest.City = src.City
			, dest.StateName = src.StateName
			, dest.Zip = src.Zip
			--, ParentId
			--, AccountManager
			--, Facility
			--, Territory
			, ModifiedDate = GETDATE()
		FROM EMRIWarehouse.SF.Accounts_test dest
		JOIN #src src ON dest.SF_AccountId = src.OriginalSFAccountId
		*/

		UPDATE dest
		SET dest.SF_AccountId = src.MergedSFAccountId
			, ModifiedDate = GETDATE()
		FROM Integration.dbo.Accounts_test dest
		JOIN #src src ON dest.SF_AccountId = src.OriginalSFAccountId
	
		--There is a unque index on the AccountId and ContactId so we can't just update all or the statement might violate the key. Instead we update all unique records and then delete the remaining would be duplicates
		;WITH orig AS (
			SELECT	AccountContactId
					, SF_AccountId
					, SF_ContactId
					, ModifiedDate
			FROM Integration.dbo.AccountContact_test dest
			JOIN #src src ON dest.SF_AccountId = src.OriginalSFAccountId
		)
	
		, new AS (
			SELECT	AccountContactId
					, SF_AccountId
					, SF_ContactId
			FROM Integration.dbo.AccountContact_test dest
			JOIN #src src ON dest.SF_AccountId = src.MergedSFAccountId
		)

		UPDATE	orig
		SET SF_AccountId = @MergedAccountId
			, ModifiedDate = GETDATE()
		FROM orig 
		LEFT JOIN new ON new.SF_ContactId = orig.SF_ContactId
		WHERE new.AccountContactId is null

		DELETE dest
		FROM Integration.dbo.AccountContact_test dest
		JOIN #src src ON dest.SF_AccountId = src.OriginalSFAccountID

	COMMIT
END TRY

BEGIN CATCH	
	UPDATE WebAPI.dbo.SalesForceRequest
	SET IsError =1
		, ErrorReason = 'SF.uspAccountMerge Procedure level error'
	WHERE SalesForceRequestToken = @SalesForceRequestToken
	
	IF @@TRANCOUNT > 0
	ROLLBACK
END CATCH

	
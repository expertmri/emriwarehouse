USE [WebAPI]
GO

/****** Object:  StoredProcedure [Archive].[uspWebFormRequest]    Script Date: 12/3/2018 10:36:01 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE Archive.uspWebFormRequest 
	@WebFormRequestId bigint
   
AS 
SET NOCOUNT ON;

BEGIN TRANSACTION

		INSERT INTO WebAPI.Archive.WebFormRequest(
				WebFormRequestId
				, RequestAction
				, RequestPayload
				, RequestInsertDate
				, Activity				
				, InsertDate
			)
		SELECT	WebFormRequestId
				, RequestAction
				, RequestPayload
				, InsertDate
				, 'DELETE'
				, GETDATE()
		FROM WebAPI.dbo.WebFormRequest
		WHERE WebFormRequestId <= @WebFormRequestId

		DELETE FROM WebAPI.dbo.WebFormRequest 
		WHERE WebFormRequestId = @WebFormRequestId

COMMIT
	
GO



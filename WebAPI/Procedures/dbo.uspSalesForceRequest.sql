USE WebAPI
GO

ALTER PROCEDURE dbo.uspSalesForceRequest 
--'{"SalesForceRequestToken":"Other Fail","RequestAction":"AccountMerge","OriginalSFAccountId":"001f40000081QaSAAU","MergedSFAccountId":"001f40000081QaTAAU","SFUser":"John Doe"}'
@postArray nvarchar(max)

AS

BEGIN TRY
	INSERT INTO dbo.SalesForceRequest (
		SalesForceRequestToken
		, RequestAction
		, RequestPayload
		)

	SELECT SalesForceRequestToken
			, RequestAction
			, @postArray
	FROM OPENJSON(@postArray)
	WITH (SalesForceRequestToken nvarchar(max) 'strict $.SalesForceRequestToken',  
			RequestAction nvarchar(max) '$.RequestAction'
			) 
END TRY

BEGIN CATCH
	IF ISJSON(@postArray) = 0
		BEGIN
			INSERT INTO dbo.SalesForceRequest (
					RequestPayload
					, IsError
					, ErrorReason
				)

			SELECT	@postArray
					, 1
					, 'Invalid JSON Format'
		END
	ELSE
		BEGIN
			INSERT INTO dbo.SalesForceRequest (
					RequestPayload
					, IsError
					, ErrorReason
				)

			SELECT	@postArray
					, 1
					, ERROR_MESSAGE()
		END
END CATCH	
	
GO

GRANT EXECUTE ON dbo.uspSalesForceRequest TO SalesForceAPI;
GO

USE [WebAPI]
GO

/****** Object:  Table [dbo].[SalesForceRequest]    Script Date: 10/2/2018 9:30:48 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE Archive.SalesForceRequest(
	ID bigint IDENTITY(1,1),
	SalesForceRequestId bigint NOT NULL,
	SalesForceRequestToken varchar(32) NULL,
	RequestAction varchar(255) NULL,
	RequestPayload nvarchar(max) NULL,
	RequestInsertDate datetime,
	Activity varchar(40),
	InsertDate datetime NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO




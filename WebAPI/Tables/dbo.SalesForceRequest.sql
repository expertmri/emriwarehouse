USE WebAPI
GO

ALTER TABLE dbo.SalesForceRequest DROP CONSTRAINT DF_SalesForceRequest_InsertDate
GO

/****** Object:  Table dbo.SalesForceRequest    Script Date: 10/1/2018 4:46:48 PM ******/
DROP TABLE dbo.SalesForceRequest
GO

/****** Object:  Table dbo.SalesForceRequest    Script Date: 10/1/2018 4:46:48 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE dbo.SalesForceRequest(
	SalesForceRequestId bigint IDENTITY(1,1) NOT NULL
	, SalesForceRequestToken varchar(255) NULL
	, RequestAction varchar(255) NULL
	, RequestPayload nvarchar(max) NULL
	, IsError bit CONSTRAINT DF_SalesForceRequest_IsError DEFAULT (0)
	, ErrorReason varchar(255)
	, IsErrorSent bit CONSTRAINT DF_SalesForceRequest_IsErrorSent DEFAULT (0)
	, InsertDate datetime NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE dbo.SalesForceRequest ADD  CONSTRAINT DF_SalesForceRequest_InsertDate  DEFAULT (GETDATE()) FOR InsertDate
GO



USE [WebAPI]
GO

/****** Object:  Table [Archive].[WebFormRequest]    Script Date: 12/3/2018 10:22:23 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Archive].[WebFormRequest](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[WebFormRequestId] [bigint] NOT NULL,
	--[WebFormRequestToken] [varchar](255) NULL,
	[RequestAction] [varchar](255) NULL,
	[RequestPayload] [nvarchar](max) NULL,
	[RequestInsertDate] [datetime] NULL,
	[Activity] [varchar](40) NULL,
	[InsertDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO



USE [WebAPI]
GO

/****** Object:  Table [dbo].[WebFormRequest]    Script Date: 11/30/2018 3:26:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[WebFormRequest](
	[WebFormRequestId] [bigint] IDENTITY(1,1) NOT NULL,
	[WebFormRequestToken] [varchar](255) NULL,
	[RequestAction] [varchar](255) NULL,
	[RequestPayload] [nvarchar](max) NULL,
	[IsError] [bit] NULL,
	[ErrorReason] [varchar](255) NULL,
	[IsErrorSent] [bit] NULL,
	[InsertDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[WebFormRequest] ADD  CONSTRAINT [DF_WebFormRequest_IsError]  DEFAULT ((0)) FOR [IsError]
GO

ALTER TABLE [dbo].[WebFormRequest] ADD  CONSTRAINT [DF_WebFormRequest_IsErrorSent]  DEFAULT ((0)) FOR [IsErrorSent]
GO

ALTER TABLE [dbo].[WebFormRequest] ADD  CONSTRAINT [DF_WebFormRequest_InsertDate]  DEFAULT (getdate()) FOR [InsertDate]
GO



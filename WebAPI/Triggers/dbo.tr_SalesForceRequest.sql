USE WebAPI
GO

CREATE TRIGGER dbo.tr_SalesForceRequest
   ON  dbo.SalesForceRequest
   AFTER DELETE
AS 
BEGIN
	SET NOCOUNT ON;

	IF EXISTS(SELECT * FROM deleted) AND NOT EXISTS(SELECT * FROM inserted)
	
	BEGIN
		INSERT INTO Archive.SalesForceRequest(
				SalesForceRequestId
				, SalesForceRequestToken
				, RequestAction
				, RequestPayload
				, RequestInsertDate
				, Activity				
				, InsertDate
			)
		SELECT	deleted.SalesForceRequestId
				, deleted.SalesForceRequestToken
				, deleted.RequestAction
				, deleted.RequestPayload
				, deleted.InsertDate
				, 'DELETE'
				, GETDATE()
		FROM deleted
	END

END
	
GO




use AbbaDoxRISMigration
go
select * from 
(
Select count(*) as [Count],'Patients_Notes'     as tableName,'Missing PatientID in Patients'        as [Note/Issue/Error] from Patients_Notes where PatientID not in (select PatientID from patients) union all
select count(*) as [Count],'ImgPatientNote'     as tableName,'Missing PatientID in Patients'        as [Note/Issue/Error] from ImgPatientNote where PatientID not in (select PatientID from patients) union all
select count(*) as [Count],'ImgPatientPayment'  as tableName,'Missing PatientID in Patients'        as [Note/Issue/Error] from ImgPatientPayment where PatientID not in (select PatientID from Patients) union all
select count(*) as [Count],'ImgPatientPayment'  as tableName,'Missing ChargeID in ImgPatientCharge' as [Note/Issue/Error] from ImgPatientPayment where ChargeID not in (select ChargeID from ImgPatientCharge) union all
select count(*) as [Count],'ImgPatientPayment'  as tableName,'Missing Plan# on Insurance Payment'   as [Note/Issue/Error] from ImgPatientPayment where PaymentType = 'INSURANCE' and InsurancePlanNumber in ('','0') union all
select count(*) as [Count],'ImgPatientCharge'   as tableName,'Missing/Invalid Referring Doctor'     as [Note/Issue/Error] from ImgPatientCharge where ReferringDoctorID not in (select UserID from dbo.Users) union all 
select count(*) as [Count],'ImgPatientCharge'   as tableName,'Missing/Invalid Location'             as [Note/Issue/Error] from ImgPatientCharge where LocationID not in (select LocationID from Locations) union all
select count(*) as [Count],'ImgPatientCharge'   as tableName,'Missing/Invalid Doctor'               as [Note/Issue/Error] from ImgPatientCharge where DoctorID not in (select UserID from dbo.users where UserType='R') union all
select count(*) as [Count],'ImgPatientCharge'   as tableName,'Orphaned Charges not tied to Patient' as [Note/Issue/Error] from ImgPatientCharge where PatientID not in (select PatientID from Patients) union all
select count(*) as [Count],'ImgPatientCharge'	as tableName,'Accounts without charges'             as [NoteIssue/Error] from Patients where patientid not in (select patientid from ImgPatientCharge)
) x

--DUPLICATE INSURANCE

with exams as (
select count(examid) as rcdCnt, ExamId
from AbbaDoxRISMigration.dbo.Exams_Insurances
group by ExamId
having count(examid) > 1
)

select * 
from AbbaDoxRISMigration.dbo.Exams_Insurances i
join exams e on e.examId = i.examId
order by e.examId

select a.* 
from EMRIWarehouse.dbo.Appointments a
join exams e on a.ApptId = e.ExamId
order by 1



--select * from ImgPatientCharge where PatientID not in (select PatientID from Patients)
--with refDoc as (
--select VisitNumber 
--from ImgPatientCharge 
--where ReferringDoctorID not in (select UserID from dbo.Users where UserType='P') 
--)

--select a.ReferringDr, a.ReferringDrId, a.*
--from EMRIWarehouse.dbo.Appointments a
--join refDoc r on a.ApptId = r.VisitNumber

--with pats as (
--select * from EMRIWarehouse.dbo.appointments 
--)

--select p.* 
--from pats p
--left join AbbaDoxRISMigration.dbo.patients p2 ON p.Account = p2.PatientID
--where p2.PatientID is null and p.Account not in (

--select * 
--from AbbaDoxRISMigration.dbo.ImgPatientPayment p
--where p.ChargeID is null

--with pats as (
--select * from EMRIWarehouse.dbo.appointments a where a.AppointmentStatusId = 4
--)

--select p.*
--from pats p
--left join  Integration.Stg.Charges c ON c.AptId = p.ApptId
--where c.TransactionNo is null




--with pats as (
--select p.* 
--from AbbaDoxRISMigration.dbo.Patients p
--left join AbbaDoxRISMigration.dbo.ImgPatientCharge c on p.PatientID = c.PatientID
--where c.PatientID is null
--)

--select distinct a.ApptId 
--from pats p
--join EMRIWarehouse.dbo.Appointments a on p.PatientId = a.account





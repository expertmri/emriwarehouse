/****************************************************************************************************
dbo.Users
PASSED
11,289 records in source and dest
****************************************************************************************************/
SELECT COUNT(*) FROM AbbaDoxRISMigration.dbo.Users
SELECT COUNT(*) FROM Integration.Stg.Operators
SELECT COUNT(*) FROM Integration.Stg.ReferringPhysicians
SELECT COUNT(*) FROM Integration.Stg.Attorneys

/****************************************************************************************************
dbo.ExamCodes
PASSED
514 src and dest
****************************************************************************************************/
SELECT COUNT(*) FROM AbbaDoxRISMigration.dbo.ExamCodes
SELECT COUNT(*) FROM Integration.Stg.AppointmentType

/****************************************************************************************************
dbo.Insurances
PASSED
4212 src and dest
****************************************************************************************************/
SELECT COUNT(*) FROM AbbaDoxRISMigration.dbo.Insurances
SELECT COUNT(*) FROM Integration.Stg.Insurances

/****************************************************************************************************
dbo.Locations
PASSED
32 src and dest
****************************************************************************************************/
SELECT COUNT(*) FROM AbbaDoxRISMigration.dbo.Locations
SELECT COUNT(*) FROM Integration.Stg.Locations l WHERE l.Code <> '~#'

/****************************************************************************************************
dbo.Locations_Resources
PASSED
69 src and dest
****************************************************************************************************/
SELECT COUNT(*) FROM AbbaDoxRISMigration.dbo.Locations_Resources
SELECT COUNT(*) FROM AbbaDoxRISMigration.import.LocationResources

/****************************************************************************************************
dbo.Patients
PASSED
77054 dest and 90470 source diff = 13,416
13,415 Patients No Appts or Cancelled and Patient info is basically all null
2 records in Appointments table and not in Abbadox (Slot and 10021)
1 record of these is not in Patients table in ADS (slot)
Total Patients matches and cancelled appointments are not coming over since they lack data in required fields
****************************************************************************************************/
SELECT COUNT(*) FROM AbbaDoxRISMigration.dbo.Patients
SELECT COUNT(*) FROM Integration.Stg.Patients

;WITH pats AS (
SELECT p.Account FROM Integration.Stg.Patients p EXCEPT
SELECT p.PatientID AS Account FROM AbbaDoxRISMigration.dbo.Patients p
)
SELECT * FROM Integration.Stg.Patients p
JOIN pats p2 ON p.Account = p2.Account

--All patients with no record in Appointments table so either cancelled or never scheduled
SELECT p.Account
FROM Integration.Stg.Patients p 
LEFT JOIN Integration.Stg.Appointments a2 ON a2.Account = p.Account
WHERE a2.Account IS NULL

--2 records in Appointments table and not in Abbadox (Slot and 10021)
SELECT a2.Account FROM Integration.Stg.Appointments a2
EXCEPT
SELECT p.PatientID AS Account FROM AbbaDoxRISMigration.dbo.Patients p

SELECT *
FROM Integration.Stg.Patients p 
JOIN Integration.Stg.Appointments a2 ON a2.Account = p.Account
WHERE a2.Account IN ('Slot', '10021')

--Slot doesn't exist in patients table only in Appts
SELECT * FROM Integration.Stg.Patients p WHERE p.Account = ('Slot')

/****************************************************************************************************
dbo.Patients_Insurances
PASSED
src has 90,470 and dest has 77,052
77,054 Patients so 2 missing insurance (72276, 89664)

****************************************************************************************************/
SELECT COUNT(*) FROM AbbaDoxRISMigration.dbo.Patients_Insurances
SELECT COUNT(*) FROM Integration.Stg.GuarantorInsuranceOrder
SELECT COUNT(DISTINCT AccountNumber) FROM Integration.Stg.GuarantorInsuranceOrder

-- 2 Patients without Patient_Insurances records
SELECT p.PatientID
FROM AbbaDoxRISMigration.dbo.Patients p
LEFT JOIN Integration.Stg.GuarantorInsuranceOrder gio ON p.PatientID = gio.AccountNumber
EXCEPT
SELECT i.PatientID FROM AbbaDoxRISMigration.dbo.Patients_Insurances i

--Both Accounts have null insurance data
SELECT * FROM Integration.Stg.GuarantorInsuranceOrder g WHERE g.AccountNumber IN ('72276', '89664')

/****************************************************************************************************
dbo.Appointments
PASSED
src 188,723 and dest 165,202 diff 23,521
23,521 Appointments not migrated
23,521 Appointments not migrated due to missing patient

****************************************************************************************************/
SELECT COUNT(*) FROM AbbaDoxRISMigration.dbo.Appointments
SELECT COUNT(*) FROM EMRIWarehouse.dbo.Appointments a

--Appointments whose Patient did not get migrated
;WITH cte AS (
	SELECT a.ApptId FROM EMRIWarehouse.dbo.Appointments a
	EXCEPT
	SELECT a.AppointmentID AS ApptId FROM AbbaDoxRISMigration.dbo.Appointments a
)
SELECT * 
FROM EMRIWarehouse.dbo.Appointments a
JOIN cte c ON c.ApptId = a.ApptId
LEFT JOIN AbbaDoxRISMigration.dbo.Patients p on p.PatientID = a.Account
WHERE p.PatientID IS NULL

--Appointments whose Patient did not get migrated
;WITH cte AS (
	SELECT a.ApptId FROM EMRIWarehouse.dbo.Appointments a
	EXCEPT
	SELECT a.AppointmentID AS ApptId FROM AbbaDoxRISMigration.dbo.Appointments a
)
SELECT p.*, a.* 
FROM EMRIWarehouse.dbo.Appointments a
JOIN cte c ON c.ApptId = a.ApptId
JOIN Integration.Stg.Patients p ON a.Account = p.Account
LEFT JOIN AbbaDoxRISMigration.dbo.Patients p2 on p2.PatientID = a.Account
WHERE p2.PatientID IS NULL

/****************************************************************************************************
dbo.Appointments_Comments
dbo.Exams_Comments
NOT VALIDATED
Not all appts have comments so only can validate against query that was used to migrate
****************************************************************************************************/

/****************************************************************************************************
dbo.Appointments_Distributions
PASSED
src and dest 165202
Same as appointments, and must have a patient record
****************************************************************************************************/
SELECT COUNT(*) FROM AbbaDoxRISMigration.dbo.Appointments_Distributions
SELECT COUNT(*) FROM EMRIWarehouse.dbo.Appointments a

/****************************************************************************************************
dbo.Appointments_Insurances
PASSED
165,113 records in Appointments_Insurances
89 appointments without insurance at patient level
****************************************************************************************************/
SELECT COUNT(*) FROM AbbaDoxRISMigration.dbo.Appointments_Insurances i
SELECT a.AppointmentID, a.PatientID FROM AbbaDoxRISMigration.dbo.Appointments a
LEFT JOIN AbbaDoxRISMigration.dbo.Appointments_Insurances p ON a.AppointmentID = p.AppointmentID
WHERE p.AppointmentID is null

--Example of Appt without a PatientInsuranceID (ie no record in GuarantorInsuranceOrder)
SELECT * FROM EMRIWarehouse.dbo.Appointments a where a.Account = '3470'
SELECT * FROM Integration.Stg.GuarantorInsuranceOrder gi where gi.AccountNumber = '3470'


/****************************************************************************************************
dbo.Exams
PASSED
154,235 dest and 189,006 src rcds diff = 34,771 minus appt no patient (23,521) = 11,250
- TechnologistUserID spreadsheet says use study.technologist but that table is empty in ADS
- 327 null modalities, all mapped to Other (OT)
- 11,020 not brought over due to missing exam code which is required (all cancelled appts)
****************************************************************************************************/
SELECT COUNT(*) FROM AbbaDoxRISMigration.dbo.Exams
SELECT COUNT(*) FROM EMRIWarehouse.dbo.Appointments a

--11,020 missing remaining Appts have no ApptointmentCode
SELECT * FROM EMRIWarehouse.dbo.Appointments a WHERE a.ApptId IN (
SELECT a.ApptId FROM EMRIWarehouse.dbo.Appointments a
LEFT JOIN AbbaDoxRISMigration.dbo.Patients p ON a.Account = p.PatientID
WHERE p.PatientID IS NOT NULL
EXCEPT
SELECT e.AppointmentID
FROM AbbaDoxRISMigration.dbo.Exams e)

/****************************************************************************************************
dbo.Exams_Insurances
NOT VALIDATED - any issues are same as Appointments_Insurances
****************************************************************************************************/

/****************************************************************************************************
dbo.Exams_Distributions
PASSED
Src 189,006 dest 165,255 Diff 23,751
Missing records are not in the patients table for same reasons as other queries
****************************************************************************************************/
SELECT COUNT(*) FROM AbbaDoxRISMigration.dbo.Exams_Distributions
SELECT COUNT(*) FROM EMRIWarehouse.dbo.Appointments

SELECT * FROM EMRIWarehouse.dbo.Appointments a WHERE a.ApptId IN (
SELECT a.ApptId FROM EMRIWarehouse.dbo.Appointments a
LEFT JOIN AbbaDoxRISMigration.dbo.Patients p ON a.Account = p.PatientID
WHERE p.PatientID IS NULL
EXCEPT
SELECT e.ExamID
FROM AbbaDoxRISMigration.dbo.Exams_Distributions e)









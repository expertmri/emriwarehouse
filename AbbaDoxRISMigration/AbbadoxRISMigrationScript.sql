USE Integration;
GO

--ALTER PROCEDURE dbo.AbbodoxRISMigration AS


/****************************************************************************************************
dbo.Users
- 3 tables merged into one, need to update UserId at the end
- Attorneys currently mapped to 'A', need correct user type 


****************************************************************************************************/
INSERT INTO AbbaDoxRISMigration.dbo.Users (
	 ADSKey
	 , SourceTable
	 , UserType
	 , UserTypeDescrip
	 , FirstName
	 , LastName
	 )
	 
SELECT	'ZZZZ' AS ADSKey
		, 'CONVERSION'
		, 'Z'
		, 'Migration Account'
		, 'CONVERSION' AS FirstName
		, 'User'

--Operators
INSERT INTO AbbaDoxRISMigration.dbo.Users (
	 ADSKey
	 , SourceTable
	 , UserType
	 , UserTypeDescrip
	 , FirstName
	 , LastName
	 , Credentials
	 )
	 
SELECT	Code AS ADSKey
		, 'Operators'
		, 'C'
		, 'Customer support representative, scheduler, medical records'
		, SUBSTRING(Name, 1, CHARINDEX(' ', Name)) AS FirstName
		, REPLACE(SUBSTRING(Name, CHARINDEX(' ', Name) +1, LEN(Name)), ', MD', '') AS LastName
		, CASE WHEN CHARINDEX(',', Name) > 0 THEN RIGHT(Name, 2) ELSE NULL END AS Credentials
FROM Integration.stg.operators;

--Providers
INSERT INTO AbbaDoxRISMigration.dbo.Users (
	 ADSKey
	 , SourceTable
	 , UserType
	 , UserTypeDescrip
	 , Prefix
	 , FirstName
	 , LastName
	 , Credentials
	 , Signature
	 , NPI
	 , ExternalID1
	 )
	 
SELECT	Code AS ADSKey
		, 'Providers'
		, 'R'
		, 'Providers'
		, 'Dr.'
		, REPLACE(SUBSTRING(Name, CHARINDEX(' ', Name) +1, LEN(Name)), ' MD', '') AS FirstName
		, SUBSTRING(Name, 1, CHARINDEX(',', Name) -1) AS LastName
		, p.Title AS Credentials
		, p.InsuranceName
		, NPI
		, MedicalLicense
FROM Integration.stg.Providers p;

--ReferringPhysicians
INSERT INTO AbbaDoxRISMigration.dbo.Users (
	 ADSKey
	 , SourceTable
	 , UserType
	 , UserTypeDescrip
	 , FirstName
	 , LastName
	 , MiddleName
	 , Signature
	 , WorkPhone
	 , Email
	 , Fax
	 , Address1
	 , Address2
	 , City
	 , State
	 , Zip
	 , NPI
	 )
	 
SELECT	Code AS ADSKey
		, 'RefPhys'
		, 'P'
		, 'Referring physician'
		, CASE WHEN CHARINDEX(' ', LTRIM(SUBSTRING(Name, CHARINDEX(',', Name) +1, LEN(Name)))) > 0 
			   THEN LEFT(LTRIM(SUBSTRING(Name, CHARINDEX(',', Name) +1, LEN(Name))), CHARINDEX(' ', LTRIM(SUBSTRING(Name, CHARINDEX(',', Name) +1, LEN(Name)))))
			   ELSE LTRIM(SUBSTRING(Name, CHARINDEX(',', Name) +1, LEN(Name))) END AS FirstName
		, LTRIM(RTRIM(REPLACE(SUBSTRING(Name, 1, CHARINDEX(',', Name)), ',', ''))) AS LastName
		, CASE WHEN CHARINDEX(' ', LTRIM(SUBSTRING(Name, CHARINDEX(',', Name) +1, LEN(Name)))) > 0 
			   THEN RIGHT(SUBSTRING(Name, CHARINDEX(',', Name) +1, LEN(Name)), 1) 
			   ELSE NULL END AS MiddletName
		, LabelName
		, Phone
		, Email
		, FaxNumber
		, Address1
		, Address2
		, City
		, State
		, ZipCode
		, NPI
FROM Integration.stg.ReferringPhysicians;

--Attorneys

INSERT INTO AbbaDoxRISMigration.dbo.Users (
	 ADSKey
	 , SourceTable
	 , UserType
	 , UserTypeDescrip
	 , FirstName
	 , LastName
	 , WorkPhone
	 , Email
	 , Fax
	 , Address1
	 , Address2
	 , City
	 , State
	 , Zip
	 )
	 
SELECT	Code AS ADSKey
		, 'Atty'
		, 'A'
		, 'Referring Attorney'
		, '' AS FirstName
		, PracticeName AS LastName
		, Phone
		, Email
		, Fax
		, Address1
		, Address2
		, City
		, State
		, ZipCode
FROM Integration.stg.Attorneys;

UPDATE AbbaDoxRISMigration.dbo.Users
SET UserId = UniqueRowID;


/****************************************************************************************************
dbo.ExamCodes
--Made Modality varchar(20) to accomodate Radio Fluoroscopy

****************************************************************************************************/
INSERT INTO AbbaDoxRISMigration.dbo.ExamCodes (
	ExamCode
    , CPTCode
    , ModalityCode
)

SELECT a.AppTypeCode AS ExamCode
		, a.SchedulingReminder
		, a.Modality
FROM Integration.Stg.AppointmentType a



/****************************************************************************************************
dbo.Insurances
--Using ADSKey for InsuranceId and changed datatype to varchar

****************************************************************************************************/

INSERT INTO AbbaDoxRISMigration.dbo.Insurances(
	InsuranceID
    , CarrierName
    , PlanName
    , GlobalCarrierID
    , GlobalPlanID
    , BillingCarrierID
    , BillingPlanID
    , Address1
    , Address2
    , City
    , State
    , ZIP
    , Phone
    , Fax
    , Website
    , MainContactName
    , ContractStartDate
    , ContractEndDate
    , ExamTypesRequiringAuthorization
	, FinancialClass

)

SELECT	i.Code AS InsuranceId
		, i.Name AS CarrierName
		, i.Name AS PlanName
		, NULL AS GlobalCarrierID
		, NULL AS GlobalPlanID
		, NULL AS BillingCarrierID
		, NULL AS BillingPlanID
		, i.Address1
		, i.Address2
		, i.City
		, i.State
		, i.ZipCode
		, i.Phone
		, i.Fax
		, NULL AS Website
		, i.ContactName AS MainContactName
		, NULL AS ContractStartDate
		, NULL AS ContractEndDate
		, NULL AS ExamTypesRequiringAuthorization
		, COALESCE(p.FinancialClass, 'Migration Mapping Error')
FROM Integration.Stg.Insurances i
LEFT JOIN AbbaDoxRISMigration.import.Payer p ON p.ClientCode = i.Code

--update i
--set FinancialClass = p.FinancialClass
--from AbbaDoxRISMigration.dbo.Insurances i
--JOIN AbbaDoxRISMigration.import.Payer p ON p.ClientCode = i.InsuranceId

/****************************************************************************************************
dbo.Locations
--Saba also provided a list already, below is my script plus default values she set in her table
-- COMMENTED OUT FOR GO-LIVE, STATIC DATA READY TO MIGRATE
****************************************************************************************************/
--INSERT INTO AbbaDoxRISMigration.dbo.Locations ( [LocationID]
--      ,[LocationOrganizationalID]
--      ,[Name]
--      ,[Address1]
--      ,[Address2]
--      ,[City]
--      ,[State]
--      ,[ZIP]
--      ,[Phone]
--      ,[Fax]
--      ,[Email]
--      ,[MainContactName]
--      ,[IsLocationActive]
--      ,[IsLocationVisibleInScheduler]
--      ,[IsOutsideRead]
--      ,[NPI]
--      ,[TimeZone]
--      ,[isSafvi]
--	)

--SELECT	l.Code AS LocationId
--		, l.Code AS LocationOrganizationalID
--		, l.Name
--		, l.Address AS Address1
--		, NULL AS Address2
--		, l.City
--		, l.State
--		, l.Zip
--		, '877-674-8888' AS Phone
--		, '877-370-5458' AS Fax
--		, NULL AS Email
--		, 'Luciana Espinoza' AS MainContactName
--		, 1 AS IsLocationActive
--		, 1 AS IsLocationVisibleInScheduler
--		, 0 AS IsOutsideRead
--		, NULL AS NPI
--		, NULL AS TimeZone
--		, 0 AS isSafvi
--FROM Integration.Stg.Locations l
--WHERE l.Code <> '~#'

/****************************************************************************************************
dbo.Locations_Resources
--Imported spreadsheet from Saba and inserted
-- COMMENTED OUT FOR GO-LIVE, STATIC DATA READY TO MIGRATE
****************************************************************************************************/
--INSERT INTO AbbaDoxRISMigration.dbo.Locations_Resources	(
--	LocationResourceID
--    , LocationID
--    , RoomName
--    , ModalityCode
--    , AETitle
--    , EstimatedMinsPerProcedure
--    , IsMammographyResource
--    , IsResourceActive

--)

--SELECT	r.Resource1
--		, r.Location
--		, r.RoomName
--		, r.Modality
--		, NULL AS AETiitle
--		, 0 AS EstimatedMinsPerProcedure
--		, 0 AS IsMammographyResource
--		, 1 AS IsResourceActive
--FROM AbbaDoxRISMigration.import.LocationResources r
--UNION
--SELECT	'CONVERSION'
--		, 'BF'
--		, NULL
--		, NULL
--		, NULL AS AETiitle
--		, 0 AS EstimatedMinsPerProcedure
--		, 0 AS IsMammographyResource
--		, 0 AS IsResourceActive



/****************************************************************************************************
dbo.Patients
- Using MAX to eliminate multiple rows from the same table for an account where one is NULL and one is not
- Latest DOS facility to use for a patient that has more than one

****************************************************************************************************/

;WITH cte AS(
SELECT DISTINCT
		p.Account AS PatientId
		--, COALESCE(a2.PatMedicalRecord, p.Account) AS MedicalRecordNumber -- removed due to length issue on insert
		, p.Account AS MedicalRecordNumber
		, LTRIM(RTRIM(CASE WHEN CHARINDEX(',', a3.PatName) > 0 THEN LEFT( a3.PatName, CHARINDEX(',', a3.PatName) - 1) ELSE a3.PatNAme END )) AS LastName
		, LTRIM(RTRIM(CASE WHEN RIGHT(SUBSTRING(a3.PatName, CHARINDEX(',', a3.PatName) + 1, LEN(a3.PatName)), 3) = ' JR' THEN REPLACE(SUBSTRING(a3.PatName, CHARINDEX(',', a3.PatName) + 1, LEN(a3.PatName)), ' JR', '') --Remove JR
			   WHEN RIGHT(SUBSTRING(a3.PatName, CHARINDEX(',', a3.PatName) + 1, LEN(a3.PatName)), 3) = ' SR' THEN REPLACE(SUBSTRING(a3.PatName, CHARINDEX(',', a3.PatName) + 1, LEN(a3.PatName)), ' SR', '') --Remove SR
			   ELSE SUBSTRING(a3.PatName, CHARINDEX(',', a3.PatName) + 1, LEN(a3.PatName)) END)) AS FirstName
		, NULL AS MiddleName
		, NULL AS MaidenName
		, NULL AS Prefix
		, CASE WHEN RIGHT(SUBSTRING(a3.PatName, CHARINDEX(',', a3.PatName) + 1, LEN(a3.PatName)), 3) IN (' JR', ' SR') THEN RIGHT(SUBSTRING(a3.PatName, CHARINDEX(',', a3.PatName) + 1, LEN(a3.PatName)), 2) 
			   ELSE NULL END AS Suffix
		, NULL AS Title
		, NULL AS Degree
		, p.Sex
		, CAST(p.Dob as datetime) AS DOB
		, CASE WHEN LEFT(p.Ssn, 4) = '0000' THEN NULL ELSE p.SSN END AS SSN
		, REPLACE(COALESCE(a2.PatHomePhone, p.HomePhone), '/', '-') AS HomePhone
		, REPLACE(COALESCE(a2.PatWorkPhone, a3.PatWorkPhone), '/', '-') AS WorkPhone
		, REPLACE(COALESCE(a2.PatientMobilePhone, a3.PatientMobilePhone), '/', '-') AS CellPhone
		--, REPLACE(a2.PatientEmail, 'TEST', NULL) AS Email
		, a2.PatientEmail AS Email
		, REPLACE(a2.PatientFax, '/', '-') AS Fax
		, COALESCE(a2.Address1, a3.address1) AS Address1
		, COALESCE(a2.Address2, a3.address2) AS Address2
		, COALESCE(a2.City, a3.City) AS City
		, COALESCE(a2.State, a3.State) AS State
		, COALESCE(a2.Zip, a3.Zip) AS Zip
		, 1 AS IsPatientActive
		, 0 AS IsPatientDeceased
		, NULL AS DateOfDEath
		, 0 AS IsPatientVIP
		, NULL AS IsTranslatorRequired
		, NULL AS IsWheelchairRequired
		, NULL AS AllowCommunicationViaPhone
		, NULL AS AllowCommunicationViaEmail
		, NULL AS AllowCommunicationViaMail
		, NULL AS AllowCommunicationViaSMS
		, p.Notes1 AS Comments
		, NULL AS Languages
		, NULL AS Race
		, NULL AS Ethnicity
		, NULL AS SmokingStatus
		, u.UserID AS MainReferringPhysicianUserID --FK to Users Table, must insert first and link
		, NULL AS LocationId --Using latest DOS facility
FROM Integration.Stg.Patients p
LEFT JOIN Integration.Stg.Appointments a2 ON a2.Account = p.Account AND a2.PatName <> 'DOWNTIME,BLOCK'
LEFT JOIN Integration.Stg.AppointmentsList a3 ON a3.Account = p.Account AND a3.PatName <> 'DOWNTIME,BLOCK'
LEFT JOIN AbbaDoxRISMigration.dbo.Users u ON u.SourceTable = 'RefPhys' AND u.ADSKey = p.ReferringDocCode
WHERE a3.Account is not null

)

--select MAX(LEN(MedicalRecordNumber)) FROM Cte


INSERT INTO AbbaDoxRISMigration.dbo.Patients

SELECT  PatientID 
      , MAX(PatientID) AS MedicalRecordNumber
      , MAX(FirstName) AS FirstName
      , MAX(LastName) AS LastName
      , MiddleName
      , MaidenName
      , Prefix
      , MAX(Suffix) AS Suffix
      , Title
      , Degree
      , MAX(Sex) AS Sex
      , MAX(DOB)AS DOB
      , MAX(SSN) AS SSN
      , MAX(HomePhone) AS HomePhone
      , MAX(WorkPhone) AS WorkPhone
      , MAX(CellPhone) AS CellPhone
      , MAX(Email) AS Email
      , MAX(Fax) AS Fax
      , MAX(Address1) AS Address1
      , MAX(Address2) AS Address2
      , MAX(City) AS City
      , MAX(State) AS State
      , MAX(Zip) AS Zip
      , MAX(IsPatientActive) AS IsPatientActive
      , MAX(IsPatientDeceased) AS IsPatientDeceased
      , DateOfDeath
      , IsPatientVIP
      , IsTranslatorRequired
      , IsWheelchairRequired
      , AllowCommunicationViaPhone
      , AllowCommunicationViaEmail
      , AllowCommunicationViaMail
      , AllowCommunicationViaSMS
      , MAX(Comments) AS Comments
      , Languages
      , Race 
      , Ethnicity 
      , SmokingStatus
      , MainReferringPhysicianUserID
      , LocationID
FROM cte 
GROUP BY PatientId
		 , MiddleName
		 , MaidenName
		 , Prefix
		 , Title
		 , Degree
		 , DateOfDeath
		 , IsPatientVIP
		 , IsTranslatorRequired
		 , IsWheelchairRequired
		 , AllowCommunicationViaPhone
		 , AllowCommunicationViaEmail
		 , AllowCommunicationViaMail
		 , AllowCommunicationViaSMS
		 , Languages
		 , Race 
		 , Ethnicity 
		 , SmokingStatus
		 , MainReferringPhysicianUserID
		 , LocationID

/*********SET LocationId to latest Facility based on DOS *******************************/
 ;WITH facilities AS (
	 SELECT	COUNT(DISTINCT Facility) AS RcdCnt
			 , Account
	 FROM EMRIWarehouse.dbo.Appointments
	 GROUP BY Account
	 HAVING COUNT(DISTINCT Facility) > 1
 )
 , orderedFacilities AS (
	 SELECT a.Account
			 , a.Facility
			 , a.ApptId
			 , a.DOS
			 , ROW_NUMBER() OVER (PARTITION BY a.Account ORDER BY a.DOS, a.Facility desc) AS RN
	 FROM facilities f
	 JOIN EMRIWarehouse.dbo.Appointments a on a.Account = f.Account
)

UPDATE p
SET p.LocationID = l.LocationID
FROM AbbaDoxRISMigration.dbo.Patients p
JOIN orderedFacilities o ON o.Account = p.PatientId
JOIN AbbaDoxRISMigration.dbo.Locations l ON l.Name = o.Facility
WHERE RN = 1


--dedupe check
--select PatientId from CTE2 group by PatientId having count(PatientId) > 1

-- temp email fix for length issue
--update Integration.stg.Appointments  set PatientEmail = 'CURTCAYLOR@HOTMAIL.COM' where account = '77781'
--second length issue in a2.MedicalRecord

-- more than one location with completed exam
--;with cte as (select distinct Account, a.facility
--from EMRIWarehouse.dbo.Appointments a
--where a.AppointmentStatusId = 4)

--select account, count(facility)
--from cte
--group by Account
--order by 2 desc
----having count(a.Facility) > 1

--select * from EMRIWarehouse.dbo.Appointments a where a.Account = '23885'

-- ;WITH facilities AS (
	-- SELECT	COUNT(DISTINCT Facility) AS RcdCnt
			-- , Account
	-- FROM EMRIWarehouse.dbo.Appointments
	-- GROUP BY Account
	-- HAVING COUNT(DISTINCT Facility) > 1
-- )
-- SELECT a.Account
		-- , a.Facility
		-- , a.ApptId
-- FROM facilities f
-- JOIN EMRIWarehouse.dbo.Appointments a on a.Account = f.Account
-- ORDER BY Account


/****************************************************************************************************
dbo.Cases
New table


****************************************************************************************************/

INSERT INTO AbbaDoxRISMigration.dbo.Cases (
    ExternalCaseId
    , CaseName
    , InsuranceId
    , InsuranceLevel
    , DocketNo
    , ARCategory
    , DOI
    , PICaseStlmtAmnt
    , CaseStlmtExpDate
    , TotalMedicals
    , AttorneyPracticeName
    , CaseManagerId
    , CaseManager
    , CaseManagerEmail
    , CaseStatus
    , LastStatusUpdated
)

SELECT DISTINCT CONCAT(a.Account,  '_' , a.ARClass , ' _' , COALESCE(CAST(a.DOI AS varchar), CAST(a.DOS AS varchar))) AS ExternalCaseId
		, a.ARClass + ' _' + COALESCE(CAST(a.DOI AS varchar), CAST(a.DOS AS varchar))  AS CaseName
		, a.InsCode AS InsuranceId
		, 1 AS InsuranceLevel
		, gi.PolicyNumber AS DocketNo
		, a.ARClass AS ARCategory
		, a.DOI
		, null AS PICaseStlmtAmnt
		, null AS CaseStlmtExpDate
		, null AS TotalMedicals
		, null AS AttorneyPracticeName
		, null AS CaseManagerId
		, null AS CaseManager
		, null AS CaseManagerEmail
		, null AS CaseStatus
		, null AS LastStatusUpdated
FROM EMRIWarehouse.dbo.Appointments a
LEFT JOIN Integration.Stg.GuarantorInsurances gi ON a.Account = gi.AccountNumber AND a.InsCode = gi.InsuranceCode
--LEFT JOIN Integration.Stg.UserForm uf ON uf.Account = a.Account
--WHERE uf.UserFormName = 'PICST'


INSERT INTO Integration.abdx.CaseMapping (ExternalCaseId, CaseId, Account)

SELECT CONCAT(a.Account,  '_' , a.ARClass , ' _' , COALESCE(CAST(a.DOI AS varchar), CAST(a.DOS AS varchar))) 
		, c.CaseId
		, a.Account
FROM EMRIWarehouse.dbo.Appointments a
JOIN AbbaDoxRISMigration.dbo.Cases c ON CONCAT(a.Account,  '_' , a.ARClass , ' _' , COALESCE(CAST(a.DOI AS varchar), CAST(a.DOS AS varchar)))  = c.ExternalCaseId

/****************************************************************************************************
dbo.Patients_Notes
--new added by saba

****************************************************************************************************/

INSERT INTO AbbaDoxRISMigration.dbo.Patients_Notes( 
	PatientID
	, NoteText
	, CreatedDateTime
	, CreatedByUserId
)

SELECT p.Account
		, p.Notes1 AS Note
		, GETDATE() AS CreateDateTime
		, u2.UserId AS Operator
FROM Integration.Stg.Patients p
LEFT JOIN  AbbaDoxRISMigration.dbo.Users u2 ON u2.ADSKey = 'ZZZZ' AND u2.SourceTable = 'CONVERSION'
JOIN AbbaDoxRISMigration.dbo.Patients p2 ON p2.PatientID = p.Account
WHERE p.Notes1 IS NOT NULL
UNION
SELECT uf.Account
		, uf.Field90
		, CAST( CONVERT(varchar(10),LEFT(uf.Field84, CHARINDEX(' ', uf.Field84)), 101) + ' ' +
		  CONVERT(varchar(10), REPLACE(SUBSTRING(uf.Field84, 1+ CHARINDEX(' ', uf.Field84 ), 6), '.', ':') + 'M', 100) AS datetime) AS CreateDateTime
		, COALESCE(u.userId, u2.userId) AS Operator
		--, REPLACE(LEFT(uf.EnteredBy, CHARINDEX(' ', uf.EnteredBy, 1+ CHARINDEX(' ', uf.EnteredBy))), '.', ':') AS CreatedDate
		--, RTRIM(LTRIM(SUBSTRING(uf.EnteredBy, 1+ CHARINDEX(' ', uf.EnteredBy, CHARINDEX('by', uf.EnteredBy)), LEN(uf.EnteredBy)))) AS Operator
FROM Integration.Stg.UserForm uf
LEFT JOIN  AbbaDoxRISMigration.dbo.Users u ON u.ADSKey = RTRIM(LTRIM(SUBSTRING(uf.Field84, 1+ CHARINDEX(' ', uf.Field84, CHARINDEX('by', uf.Field84)), LEN(uf.Field84)))) 
			AND u.SourceTable = 'Operators'
LEFT JOIN  AbbaDoxRISMigration.dbo.Users u2 ON u2.ADSKey = 'ZZZZ' AND u2.SourceTable = 'CONVERSION'
JOIN AbbaDoxRISMigration.dbo.Patients p2 ON p2.PatientID = uf.Account
WHERE uf.UserFormName IN ('NOTES', 'COLLNOT')


/****************************************************************************************************
dbo.Patients_Insurances
--Changed InsuranceID to varchar

****************************************************************************************************/
--truncate table AbbaDoxRISMigration.dbo.Patients_Insurances
INSERT INTO AbbaDoxRISMigration.dbo.Patients_Insurances( 
	PatientInsuranceID
    , PatientID
    , InsuranceID
    , InsuranceLevel
    , GroupNumber
    , PolicyNumber
    --, ExternalMemberID
    , InsuredFirstName
    , InsuredLastName
    --, InsuredMiddleName
	, InsuredGender
    , InsuredDOB
    , InsuredSSN
    , InsuredHomePhone
    , InsuredCellPhone
    --, InsuredFax
    , InsuredEmail
    , InsuredAddress1
    , InsuredAddress2
    , InsuredCity
    , InsuredState
    , InsuredZIP
    --, InsuredEmployerName
    --, InsuredEmployerAddress
    --, InsuredEmployerPhone
    --, PatientRelationshipToInsured
    , GuarantorFirstName
    , GuarantorLastName
    , GuarantorGender
    , GuarantorDOB
    , GuarantorSSN
    , GuarantorHomePhone
    , GuarantorCellPhone
    --, GuarantorFax
    --, GuarantorEmail
    , GuarantorAddress1
    , GuarantorAddress2
    , GuarantorCity
    , GuarantorState
    , GuarantorZIP
    --, GuarantorEmployerName
    --, GuarantorEmployerAddress
    --, GuarantorEmployerPhone
    --, PolicyExpirationDate
    --, PatientRelationshipToGuarantor
    --, IsSelfPay
)

SELECT DISTINCT 1 AS PatientInsuranceId
		, p.PatientID
		, i.InsuranceID
		, 1 AS InsuranceLevel
		, gi.GroupNumber
		, gi.PolicyNumber
		, LTRIM(RTRIM(CASE WHEN RIGHT(SUBSTRING(g.Name, CHARINDEX(',', g.Name) + 1, LEN(g.Name)), 3) = ' JR' THEN REPLACE(SUBSTRING(g.Name, CHARINDEX(',', g.Name) + 1, LEN(g.Name)), ' JR', '') --Remove JR
			   WHEN RIGHT(SUBSTRING(g.Name, CHARINDEX(',', g.Name) + 1, LEN(g.Name)), 3) = ' SR' THEN REPLACE(SUBSTRING(g.Name, CHARINDEX(',', g.Name) + 1, LEN(g.Name)), ' SR', '') --Remove SR
			   ELSE SUBSTRING(g.Name, CHARINDEX(',', g.Name) + 1, LEN(g.Name)) END)) AS InsuredFirstName
		, LTRIM(RTRIM(CASE WHEN CHARINDEX(',', g.Name) > 0 THEN LEFT( g.Name, CHARINDEX(',', g.Name) - 1) ELSE g.Name END )) AS InsuredLastName
		, g.Sex AS InsuredGender
		, g.DOB AS InsuredDOB
		, g.SSN AS InsuredSSN
		, g.HomePhone AS InsuredHomePhone
		, g.MobilePhone AS InsuredCellPhone
		, g.Email AS InsuredEmail
		, g.Address1 AS InsuredAddress1
		, g.Address2 AS InsuredAddress2
		, CASE WHEN LEN(g.CityState) < 3 THEN NULL ELSE LTRIM(RTRIM(SUBSTRING(g.CityState, 0, CHARINDEX(',', g.CityState)))) END AS InsuredCity
		, CASE WHEN LEN(g.CityState) < 3 THEN NULL ELSE RIGHT(LTRIM(RTRIM(g.CityState)), 2) END AS InsuredState
		--, InsuredCity
		--, InsuredState
		, g.Zip AS InsuredZIP
		, LTRIM(RTRIM(CASE WHEN RIGHT(SUBSTRING(g.Name, CHARINDEX(',', g.Name) + 1, LEN(g.Name)), 3) = ' JR' THEN REPLACE(SUBSTRING(g.Name, CHARINDEX(',', g.Name) + 1, LEN(g.Name)), ' JR', '') --Remove JR
			   WHEN RIGHT(SUBSTRING(g.Name, CHARINDEX(',', g.Name) + 1, LEN(g.Name)), 3) = ' SR' THEN REPLACE(SUBSTRING(g.Name, CHARINDEX(',', g.Name) + 1, LEN(g.Name)), ' SR', '') --Remove SR
			   ELSE SUBSTRING(g.Name, CHARINDEX(',', g.Name) + 1, LEN(g.Name)) END)) AS GuarantorFirstName
		, LTRIM(RTRIM(CASE WHEN CHARINDEX(',', g.Name) > 0 THEN LEFT( g.Name, CHARINDEX(',', g.Name) - 1) ELSE g.Name END )) AS GuarantorLastName
		, g.Sex AS GuarantorGender
		, g.DOB AS GuarantorDOB
		, g.SSN AS GuarantorSSN
		, g.HomePhone AS GuarantorHomePhone
		, g.MobilePhone AS GuarantorCellPhone
		, g.Address1 AS GuarantorAddress1
		, g.Address2 AS GuarantorAddress2
		, CASE WHEN LEN(g.CityState) < 3 THEN NULL ELSE LTRIM(RTRIM(SUBSTRING(g.CityState, 0, CHARINDEX(',', g.CityState)))) END AS GuarantorCity
		, CASE WHEN LEN(g.CityState) < 3 THEN NULL ELSE RIGHT(LTRIM(RTRIM(g.CityState)), 2) END AS GuarantorState
		, g.Zip AS GuarantorZIP
FROM Integration.Stg.GuarantorInsurances gi 
JOIN AbbaDoxRISMigration.dbo.Patients p ON p.PatientId = gi.AccountNumber
JOIN AbbaDoxRISMigration.dbo.Insurances i ON gi.InsuranceCode = I.InsuranceID
JOIN Integration.Stg.Guarantors g ON g.Account = gi.AccountNumber

UPDATE AbbaDoxRISMigration.dbo.Patients_Insurances
SET PatientInsuranceID = UniqueRowID

/* TEMPORARY 3/25/2021 ONLY TO REMOVE DUPES FOR MIGRATION TEST */
;with pats as (
select PatientInsuranceID, PolicyNumber,ROW_NUMBER() OVER (PARTITION BY PatientId ORDER BY PolicyNumber desc) AS RN
from AbbaDoxRISMigration.dbo.Patients_Insurances p 
)

delete from pats where RN > 1

/****************************************************************************************************
dbo.Appointments

****************************************************************************************************/

--SELECT	a.ApptId AS AppointmentId
--		, a.Account AS PatientId
--		, Facility AS LocationResourceID --*******Need to creater table and map
--		, stat.AbbadoxStatus AS AppointmentStatus
--		, CAST(CONVERT(varchar(20),(CONVERT(varchar(10),a.DOS, 101) + ' ' + a.ApptTime), 0) AS datetime) AS AppointmentDateTime
--FROM EMRIWarehouse.dbo.Appointments a
--JOIN EMRIWarehouse.XRef.AppointmentStatus stat ON a.AppointmentStatusId = stat.AppointmentStatusId


INSERT INTO AbbaDoxRISMigration.dbo.Appointments (
	AppointmentID
    , PatientID
    , LocationResourceID
    , AppointmentStatus
    , AppointmentDateTime
    , AppointmentStartDateTime
    , AppointmentEndDateTime
    , AppointmentCreatedDateTime
    , AppointmentCreatedByUserID
	, BillingEntity
	, TaxId
	, Img_LocationResourceId
)

SELECT	a.ApptId
		, a.Account AS PatientId
		, COALESCE(r.LocationResourceID, r2.LocationResourceID)
		, s.AbbadoxStatus
		, CASE WHEN a.AppointmentStatusId = '7' THEN CAST(a.DOS AS datetime) ELSE CAST(a.DOS AS datetime) + CAST(CAST(a.ApptTime AS time) AS datetime) END AS AppointmentDateTime
		, CASE WHEN a.AppointmentStatusId = '7' THEN CAST(a.DOS AS datetime) ELSE CAST(a.DOS AS datetime) + CAST(CAST(a.ApptTime AS time) AS datetime) END AS AppointmentStartDateTime --SAME AS DOS
		, CASE WHEN a.AppointmentStatusId = '7' THEN DATEADD(mi, at.Duration , CAST(a.DOS AS datetime)) ELSE DATEADD(mi, at.Duration , CAST(a.DOS AS datetime) + CAST(CAST(a.ApptTime AS time) AS datetime)) END AS AppointmentEndDateTime --SAME AS DOS
		, CAST(a.BookedDate AS datetime) + CAST(CAST(a.BookedTime AS time) AS datetime) AS ApptCreatedDate
		, COALESCE(u.UserID, u2.UserID) AS ApptCreateByUser
		, null --a.BillingEntity
		, null --a.TaxId
		, null AS Img_LocationResourceId
FROM EMRIWarehouse.dbo.Appointments a
LEFT JOIN EMRIWarehouse.XRef.AppointmentStatus s ON a.AppointmentStatusId = s.AppointmentStatusId
LEFT JOIN AbbaDoxRISMigration.dbo.Locations_Resources r ON a.Resource1 = r.LocationResourceID
LEFT JOIN  AbbaDoxRISMigration.dbo.Users u ON u.ADSKey = a.OperatorCode AND u.SourceTable = 'Operators'
LEFT JOIN  AbbaDoxRISMigration.dbo.Users u2 ON u2.ADSKey = 'ZZZZ' AND u2.SourceTable = 'CONVERSION'
LEFT JOIN  AbbaDoxRISMigration.dbo.Locations_Resources r2 ON r2.LocationResourceID = 'CONVERSION'
JOIN AbbaDoxRISMigration.dbo.Patients p ON p.PatientID = a.Account
LEFT JOIN Integration.Stg.AppointmentType at ON at.AppTypeCode = a.AppointmentCode


UPDATE c
SET Img_LocationResourceID = COALESCE(cv.Img_LocationResourceID, c.LocationResourceID)
	, BillingEntity = COALESCE(cv.BillingEntity, 'EXPERT MRI, PC')
	, TaxId = COALESCE(cv.TaxId, '82-0713584')
FROM AbbaDoxRISMigration.dbo.Appointments c
LEFT JOIN AbbaDoxRISMigration.import.appointments cv  ON cv.appointmentId = c.appointmentId

/****************************************************************************************************
dbo.Appointments_Comments
-- 3/19/21 Added Dedupe of notes per Isaac

****************************************************************************************************/


 ;WITH notes AS (
	 SELECT a.SequenceNo AS ApptId
			, a.ApptNote 
			, a.UserCreated
			, a.SqlDateCreated AS CreatedDateTime
	 FROM Integration.stg.AppointmentsList a
	 UNION 
	 SELECT a.ApptId
			, a.ApptNote
			, null AS userCreated
			, null AS CreatedDateTime
	 FROM Integration.stg.Appointments a
 )
 , dedupeNotes AS (
	
		select * 
				, ROW_NUMBER() OVER (PARTITION BY ApptId, ApptNote order by CreatedDateTime desc) AS RN
		from notes
		)

INSERT INTO AbbaDoxRISMigration.dbo.Appointments_Comments (
	AppointmentID
	, CommentType
	, Comments
	, IsPresentedAsAlert
	, CreatedDateTime
	, CreatedByUserID
)
SELECT n.ApptId
      , 10 AS CommentType --Migration
      , ApptNote
      , 0 AS IsPresentedAsAlert
      , CAST(n.CreatedDateTime AS datetime) AS CreatedDateTime
      --, COALESCE(u.UserID, u2.UserID) AS CreatedByUserID --changed 1/26 per Saba, see below
	  , u2.UserID AS CreatedByUserID
  FROM dedupeNotes n
  --JOIN AbbaDoxRISMigration.dbo.Users u ON n.UserCreated = u.ADSKey AND u.SourceTable = 'Operators' --Removed per Saba, set all users to Migration since the user in Appts table is who create the appt not the note
  LEFT JOIN  AbbaDoxRISMigration.dbo.Users u2 ON u2.ADSKey = 'ZZZZ' AND u2.SourceTable = 'CONVERSION'
  WHERE ApptNote is not null
		AND RN = 1

/****************************************************************************************************
dbo.Exams_Comments
New table
Inserted same data as Appointments_comments for migration
****************************************************************************************************/

INSERT INTO AbbaDoxRISMigration.dbo.Exams_Comments (
	 ExamId
     , CommentType
     , Comments
     , IsPresentedAsAlert
     , CreatedDateTime
     , CreatedByUserID

)

SELECT a.AppointmentID
	   , a.CommentType
	   , a.Comments
	   , 0
	   , a.CreatedDateTime
	   , a.CreatedByUserID
FROM AbbaDoxRISMigration.dbo.Appointments_Comments a

/****************************************************************************************************
dbo.Appointments_Distributions
Per Saba spreadsheet, these are all referring physicians and not attorney data
****************************************************************************************************/

INSERT INTO AbbaDoxRISMigration.dbo.Appointments_Distributions (
	  AppointmentID
      , ReferringPhysicianUserID
      , ReferringCategorization
      , FaxResultDeliveredDateTime
      , EmailResultDeliveredDateTime
      , PortalResultDeliveredDateTime
      , InterfaceResultDeliveredDateTime
)

SELECT	a.ApptId
		, COALESCE(u.UserID, u2.UserID) 
		, 'R' --referringPhys code
		, NULL
		, NULL
		, NULL
		, NULL
FROM EMRIWarehouse.dbo.Appointments a
JOIN AbbaDoxRISMigration.dbo.Patients p ON p.PatientID = a.Account
LEFT JOIN AbbaDoxRISMigration.dbo.Users u ON a.ReferringDrId = u.ADSKey AND u.SourceTable = 'RefPhys'
LEFT JOIN  AbbaDoxRISMigration.dbo.Users u2 ON u2.ADSKey = 'ZZZZ' AND u2.SourceTable = 'CONVERSION'

/****************************************************************************************************
dbo.Appointments_Insurances

****************************************************************************************************/
--truncate table AbbaDoxRISMigration.dbo.Appointments_Insurances 
INSERT INTO AbbaDoxRISMigration.dbo.Appointments_Insurances (
	  AppointmentID
      , PatientInsuranceID
      , InsuranceLevel
)

SELECT	a.ApptId
		, i.PatientInsuranceID
		, 1 AS InsLevel --Primary
FROM EMRIWarehouse.dbo.Appointments a
JOIN AbbaDoxRISMigration.dbo.Patients_Insurances i ON a.Account = i.PatientID AND a.InsCode = i.InsuranceID


/****************************************************************************************************
dbo.Exams
- TechnologistUserID spreadsheet says use study.technologist but that table is empty in ADS
- 327 null modalities, all mapped to Other (OT)
****************************************************************************************************/

INSERT INTO AbbaDoxRISMigration.dbo.Exams (
	  ExamId
      , AppointmentID
      , AccessionNumber
      , ExamCode
      , ModalityCode
      , ReasonForExam
      , ReasonsForExamICD10
      , Priority
      , ExamStatus
      , StudyUID
      , ExamOrderedForDateTime
      , ExamStartDateTime
      , ExamEndDateTime
      , ExamCompletedDateTime
      , TechnologistUserID
      , TechnologistNotes
      , InterpretationDateTime
      , InterpretedByUserID
      , TranscriptionDateTime
      , TranscribedByUserID
      , SignedDateTime
      , SignedByUserID
      , CoSignedDateTime
      , CoSignedByUserID
      , InterpretationResultFormattedTextAsHTML
      , InterpretationResultAsClearText
      , InterpretationResultLinkToExternalFile
      , ExamExternalD1
      , ExamExternalID2
      , ExamEnternalID3
	  , CaseId
	  , LienStatus
	  , LienType
	  , LienOwner
	  , SaleDate
	  , SaleAmount
	  , CollectionAgency
)

SELECT DISTINCT	a.ApptId AS ExamId
		, a.ApptId AS AppointmentId
		, a.ApptId AS AccessionNumber
		, at.AppTypeCode AS ExamCode
		, COALESCE(ac.ADSApptCode, 'OT') AS ModalityCode  --all nulls mapped to Other (OT)
		, NULL AS ReasonForExam
		, NULL AS ReasonsForExamICD10
		, 4 AS Priority --Set all to Routine
		, stat.AbbadoxStatus AS ExamStatus
		, NULL AS StudyUID
		, CASE WHEN a.AppointmentStatusId = '7' THEN CAST(a.DOS AS datetime) ELSE CAST(a.DOS AS datetime) + CAST(CAST(a.ApptTime AS time) AS datetime) END AS ExamOrderedForDateTime
		, CASE WHEN a.AppointmentStatusId = '7' THEN CAST(a.DOS AS datetime) ELSE CAST(a.DOS AS datetime) + CAST(CAST(a.ApptTime AS time) AS datetime) END AS ExamStartDateTime
		, CASE WHEN a.AppointmentStatusId = '7' THEN DATEADD(mi, at.Duration , CAST(a.DOS AS datetime)) ELSE DATEADD(mi, at.Duration , CAST(a.DOS AS datetime) + CAST(CAST(a.ApptTime AS time) AS datetime)) END AS ExamEndDateTime
		, CASE WHEN a.AppointmentStatusId = '7' THEN CAST(a.DOS AS datetime) ELSE CAST(a.DOS AS datetime) + CAST(CAST(a.ApptTime AS time) AS datetime) END AS ExamCompletedDateTime
		, NULL AS TechnologistUserID --study table in ADS empty study.technologist
		, NULL AS TechnologistNotes
		, NULL AS InterpretationDateTime
		, NULL AS InterpretedByUserID
		, NULL AS TranscriptionDateTime
		, NULL AS TranscribedByUserID
		, NULL AS SignedDateTime
		, NULL AS SignedByUserID
		, NULL AS CoSignedDateTime
		, NULL AS CoSignedByUserID
		, NULL AS InterpretationResultFormattedTextAsHTML
		, NULL AS InterpretationResultAsClearText
		, NULL AS InterpretationResultLinkToExternalFile
		, NULL AS ExamExternalD1
		, NULL AS ExamExternalID2
		, NULL AS ExamEnternalID3
		, CONCAT(a.Account,  '_' , a.ARClass , ' _' , COALESCE(CAST(a.DOI AS varchar), CAST(a.DOS AS varchar))) AS CaseId
		, CASE WHEN a.ARClass = 'PILATT' THEN 'Lien Sold'
			   WHEN a.ARClass = 'PIFAC' AND a.InsCode = '195' THEN 'Lien Sold'
			   WHEN a.ARClass = 'PIFAC' AND a.InsCode LIKE 'Key%' THEN 'Lien Sold'
			   ELSE 'CONVERSION'
			  END AS LienStatus
		, 'Written' AS LienType
		, CASE WHEN a.ARClass = 'PILATT' THEN 'Atticus'
			   WHEN a.ARClass = 'PIINCP' THEN 'Carepoint'
			   WHEN a.ARClass IN ('LRWCCP', 'MLWCCP', 'LRWC') THEN 'Creditpoint'
			   WHEN a.ARClass = 'PIFAC' AND a.InsCode = '195' THEN 'HFS'
			   WHEN a.ARClass = 'PIFAC' AND a.InsCode LIKE 'Key%' THEN 'KeyHealth'
			   ELSE 'OneSource'
			END AS LienOwnwer 
		, null AS SaleDate
		, null AS SaleAmount
		, CASE WHEN a.ARClass = 'PILATT' THEN 'Atticus'
			   WHEN a.ARClass = 'PIINCP' THEN 'OneSource'
			   WHEN a.ARClass IN ('LRWCCP', 'MLWCCP', 'LRWC') THEN 'Creditpoint'
			   WHEN a.ARClass = 'PIFAC' AND a.InsCode = '195' THEN 'HFS'
			   WHEN a.ARClass = 'PIFAC' AND a.InsCode LIKE 'Key%' THEN 'KeyHealth'
			   ELSE 'OneSource'
			END AS CollectionAgency
FROM EMRIWarehouse.dbo.Appointments a
JOIN EMRIWarehouse.XRef.AppointmentStatus stat ON a.AppointmentStatusId = stat.AppointmentStatusId
LEFT JOIN EMRIWarehouse.XRef.ApptCodes ac ON a.Modality = ac.Modality AND ac.ADSApptCode <> 'XR'
JOIN Integration.Stg.AppointmentType at ON a.AppointmentCode = at.AppTypeCode
JOIN AbbaDoxRISMigration.dbo.Patients p ON a.Account = p.PatientID
LEFT JOIN AbbaDoxRISMigration.dbo.Cases c ON CONCAT(a.Account,  '_' , a.ARClass , ' _' , COALESCE(CAST(a.DOI AS varchar), CAST(a.DOS AS varchar))) = c.ExternalCaseId



/****************************************************************************************************
dbo.Exams_Insurances
Per Saba spreadsheet, new table and I just copied over from Appt_Ins
****************************************************************************************************/
--truncate table AbbaDoxRISMigration.dbo.Exams_Insurances 
INSERT INTO AbbaDoxRISMigration.dbo.Exams_Insurances (
	  ExamId
      , PatientInsuranceID
      , InsuranceLevel
	  , ARClass
)

SELECT	a.ApptId
		, i.PatientInsuranceID
		, 1 AS InsLevel --Primary
		, a.ARClass
FROM EMRIWarehouse.dbo.Appointments a
JOIN AbbaDoxRISMigration.dbo.Patients_Insurances i ON a.Account = i.PatientID AND a.InsCode = i.InsuranceID

/****************************************************************************************************
dbo.Exams_Distributions
Per Saba spreadsheet, new table and I just copied over from Appt_Distrib and addedd RefAtty
****************************************************************************************************/

INSERT INTO AbbaDoxRISMigration.dbo.Exams_Distributions (
	  ExamId
      , ReferringPhysicianUserID
      , ReferringCategorization
      , FaxResultDeliveredDateTime
      , EmailResultDeliveredDateTime
      , PortalResultDeliveredDateTime
      , InterfaceResultDeliveredDateTime
	  , ReferringAttorneyUserID
)

SELECT	a.ApptId
		, COALESCE(u.UserID, u3.UserID) AS RefDr
		, 'R' --referringPhys code
		, NULL
		, NULL
		, NULL
		, NULL
		, COALESCE(u2.UserID, u3.UserID) AS RefAtty
FROM EMRIWarehouse.dbo.Appointments a
LEFT JOIN AbbaDoxRISMigration.dbo.Users u ON a.ReferringDrId = u.ADSKey AND u.SourceTable = 'RefPhys'
LEFT JOIN AbbaDoxRISMigration.dbo.Users u2 ON a.RefAttyCode = u2.ADSKey AND u2.SourceTable = 'Atty'
LEFT JOIN AbbaDoxRISMigration.dbo.Users u3 ON u3.ADSKey = 'ZZZZ' AND u3.SourceTable = 'CONVERSION'
JOIN AbbaDoxRISMigration.dbo.Patients p ON a.Account = p.PatientID


/****************************************************************************************************
dbo.Attorneys
New table
Relies on import table 
****************************************************************************************************/

INSERT INTO AbbaDoxRISMigration.dbo.Attorneys (
	ExternalId
    , LastName
    , FirstName
    , Signature
    , FullName
    , Address1
    , Address2
    , City
    , State
    , ZipCode
    , Phone
    , Fax
    , Email
    , credentials
    , title
    , specialty
    , PracticeId
    , PracticeName
    , LogicallyDeleted
)

SELECT	ia.ExternalId
		, ia.LastName
		, ia.FirstName
		, ia.Signature
		, ia."Full Name"
		, ia.Address
		, ia.Address2
		, ia.City
		, ia.State
		, ia.ZipCode
		, ia.Phone
		, ia.Fax
		, ia.Email
		, ia.credentials
		, ia.title
		, ia.specialty
		, ia.[PracticeID/Attorney Insurance]
		, ia.[Practice name]
		, a.LogicallyDeleted
FROM AbbaDoxRISMigration.import.Attorneys ia
LEFT JOIN Integration.Stg.Attorneys a ON ia.ExternalID = a.Code

/****************************************************************************************************
dbo.PICaseStatus
New table
Use for mapping to Cases to get the Abbadox_casestatus
****************************************************************************************************/
DROP TABLE Integration.abdx.PICaseStatus;

SELECT 	
	uf.Account
	, uf.Patient
	, uf.UserFormName
	, uf.Field2 AS [PAYOR]
	, uf.Field18 AS [AR CLASS]
	, uf.Field10 AS [DATE OF LOSS]
	, CASE WHEN uf.Field20 = '1^0' THEN 'Yes' ELSE 'No' END AS [SIGNED PI LIEN ON FILE] 
	, COALESCE(cs.adsStatus, 'ADS INVALID STATUS') AS [ADS CASE STATUS] 
	, COALESCE(cs.abbadoxStatus, 'ADS INVALID STATUS') AS [ABBADOX CASE STATUS] 
	, uf.Field4 AS [PI CASE STLMT AMT]
	, uf.Field5 AS [CASE STLMT/EXP DATE]
	, uf.Field6 AS [REASON FOR CASE STATUS]
	, uf.Field7 AS [TOTAL MEDICALS]
	, uf.Field8 AS [EXPERT LIEN AMOUNT]
	, uf.Field15 AS [PRORATA FOR EXPERT]
	, uf.Field11 AS [LIEN REDUCTION OFFER]
	, LEFT(uf.Field9, 17) AS [LAST STATUS UPDATE ON]
	, uf.Field28 AS [CASE MANAGER]
	, uf.Field30 AS [CASE MANGER EMAIL]
INTO Integration.abdx.PICaseStatus
FROM Integration.Stg.[userform]	uf
LEFT JOIN AbbaDoxRISMigration.xref.CaseStatus cs ON uf.Field16  = cs.StatusCode
WHERE [UserFormName] = 'PICST'	


/****************************************************************************************************
xref.CaseStatus
New table
Pending Info from Saba/Richard
****************************************************************************************************/
/*
create table xref.CaseStatus (statusCode char(2), adsStatus varchar(50), abbadoxStatus varchar(100))

insert into xref.CaseStatus (statusCode , adsStatus, abbadoxStatus )
values ('DR', 'DROPPED', 'Dropped')
, ('DS', 'DO NOT STATUS', 'DO NOT STATUS')
, ('ES', 'EMAILED CASE STATUS REQUEST', 'Pending Case Status')
, ('FS', 'FAXED CASE STATUS REQUEST', 'Pending Case Status')
, ('IL', 'IN LITIGATION', 'In Litigation')
, ('LV', 'LEFT VOICEMAIL', 'Pending Case Status')
, ('OT', 'ONGOING PT STILL TREATING', 'Ongoing Patient Treatment')
, ('PL', 'PRE-LITIGATION', 'Pre-Litigation')
, ('SO', 'SETTLEMENT OFFER MADE', 'Settlement offer made')
, ('ST', 'SETTLED', 'Settled')
, (null, 'ADS INVALID STATUS', 'ADS INVALID STATUS')
*/

/****************************************************************************************************
dbo.PISettlement
New table
****************************************************************************************************/

DROP TABLE Integration.abdx.PISettlement;

SELECT Account
		, Patient
		, UserFormName
		, Field2 AS [PAYOR]
		, Field18 AS [AR CLASS]
		, Field26 AS [DOL]
		, Field10 AS [SETTLED WITH]
		, Field27 AS [NEGOTIATORS EMAIL]
		, Field28 AS [NEGOTIATORS PHONE]
		, Field16 AS [TOTAL BILLED]
		, Field23 AS [NO OF MULTI MRIS]
		, Field24 AS [NO OF SINGLE MRIS]
		, Field25 AS [NO OF CT]
		, Field1 AS [NO OF CR] 
		, Field3 AS [DEMAND] 
		, Field4 AS [SETTLEMENT AMOUNT] 
		, Field7 AS [SETTLEMENT DATE]
		, Field6 AS [SIGNED CONFIRMATION RCVD] 
		, Field7 AS [SIGNED CONFIRMATION DATE] 
		, Field8 AS [SETTLED BY] 
		, Field15 AS [APPROVED BY]
		, Field9 AS [ENTERED ON] 
		, Field11 AS [PAYMENT RCVD] 
		, Field12 AS [PAYMENT DATE]
		, Field13 AS [PAYMENT AMOUNT]
		, Field20 AS [FOLLOWUP DATE]
		, Field21 AS [FOLLOWUP NOTES] 
INTO Integration.abdx.PISettlement
FROM Integration.Stg.userform 
WHERE UserFormName = 'STLMT'

INSERT INTO AbbaDoxRISMigration.dbo.PISettlement (
      CaseId
      , SettledWith
      , NegotiatorEmail
      , NegotiatorPhone
      , LienReductionOffer
      , SettlementAmount
      , SettlementDate
      , SettledBy
      , SettlementEnteredOn
)

SELECT	DISTINCT c.CaseId
		, p.[SETTLED WITH]
		, p.[NEGOTIATORS EMAIL]
		, p.[NEGOTIATORS PHONE]
		, p.DEMAND/100
		, p.[SETTLEMENT AMOUNT]/100
		, p.[SETTLEMENT DATE]
		, p.[SETTLED BY]
		, p.[ENTERED ON]
FROM AbbaDoxRISMigration.dbo.Cases c
JOIN Integration.abdx.CaseMapping cm ON c.CaseId = cm.CaseId
JOIN Integration.abdx.PISettlement p ON p.Account = cm.Account
WHERE UserFormName = 'STLMT'


/****************************************************************************************************
Cases table update once dependencies are in place

****************************************************************************************************/
--select top 100000
--	c.ExternalCaseId
--    , c.CaseName
--    , c.InsuranceId
--    , c.InsuranceLevel
--    , c.DocketNo
--    , c.ARCategory
--    , c.DOI
--    , [PI CASE STLMT AMT] AS PICaseStlmtAmnt
--    , [CASE STLMT/EXP DATE] AS CaseStlmtExpDate
--    , [TOTAL MEDICALS] AS TotalMedicals
--    , c.AttorneyPracticeName
--    , null AS CaseManagerId
--    , [CASE MANAGER] AS CaseManager
--    , [CASE MANGER EMAIL] AS CaseManagerEmail
--    , [ABBADOX CASE STATUS] AS CaseStatus
--    , [LAST STATUS UPDATE ON] AS LastStatusUpdated
--FROM AbbaDoxRISMigration.dbo.cases c
--LEFT JOIN Integration.abdx.CaseMapping cm ON c.CaseId = cm.CaseId
--LEFT JOIN Integration.abdx.PICaseStatus cs ON cs.Account = cm.Account

--select [LAST STATUS UPDATE ON],
--CAST( CONVERT(varchar(10),LEFT([LAST STATUS UPDATE ON], CHARINDEX(' ', [LAST STATUS UPDATE ON])), 101) + ' ' +
--CONVERT(varchar(10), REPLACE(SUBSTRING([LAST STATUS UPDATE ON], 1+ CHARINDEX(' ', [LAST STATUS UPDATE ON] ), 6), '.', ':') + 'M', 100) AS datetime) 
-- from  Integration.abdx.PICaseStatus 

UPDATE c
SET	PICaseStlmtAmnt = [PI CASE STLMT AMT] /100
    , CaseStlmtExpDate = [CASE STLMT/EXP DATE]
    , TotalMedicals = [TOTAL MEDICALS]/100
    , CaseManager = [CASE MANAGER] 
    , CaseManagerEmail = [CASE MANGER EMAIL] 
    , CaseStatus = [ABBADOX CASE STATUS] 
    , LastStatusUpdated = CAST( CONVERT(varchar(10),LEFT([LAST STATUS UPDATE ON], CHARINDEX(' ', [LAST STATUS UPDATE ON])), 101) + ' ' +
						  CONVERT(varchar(10), REPLACE(SUBSTRING([LAST STATUS UPDATE ON], 1+ CHARINDEX(' ', [LAST STATUS UPDATE ON] ), 6), '.', ':') + 'M', 100) AS datetime) 
FROM AbbaDoxRISMigration.dbo.cases c
LEFT JOIN Integration.abdx.CaseMapping cm ON c.CaseId = cm.CaseId
LEFT JOIN Integration.abdx.PICaseStatus cs ON cs.Account = cm.Account






/********************************************************************************************************
Imagine Tables moved into Abbadox database per Isaac 3/11/21

********************************************************************************************************/


/********************************************************************************************************
dbo.ImgPatientCharge
192,731 inserted and charges table has same 
********************************************************************************************************/
--truncate table AbbaDoxRISMigration.dbo.ImgPatientCharge
INSERT INTO AbbaDoxRISMigration.dbo.ImgPatientCharge (
	ChargeID
    , PatientID
    , VisitNumber
    , LocationID
    , DoctorID
    , ReferringDoctorID
    , ReferringDoctorFullName
    , DepartmentCode
    , CPTCode
    , CPTCodeDescription
    , Modifier
    , Unit
    , ServiceDate
    , ServiceDate_to
    , OrderNumber
    , ICD1
    , ICD2
    , ICD3
    , ICD4
    , POSCode
    , HCFA19_Note
    , HCFA24_Note
    , ChargeAmount
    , Balance
    , PostDate
    , BatchNumber
    , UserName
    , BillingStatus
	, LienOwnwer
	, SaleDate
	, SaleAmount
	, CollectionAgency
    , FirstStatementDate
    , LastStatementDate
    , FollowupAction
    , FollowUpNextDate
    , isMailReturn
    , isOnHold
    , isVoided
    , VoidedDate
    , LastClaimDate
    , ReportingUnitCount
    , Type
    , AnesthesiaTechnique
    , BillingProcedureCode
    , AuthorizationNumber
    , InterpretationDate
    , InterpretationLocationID
	, BillingEntity
)

SELECT	c.TransactionNo AS ChargeId
		, c.AccountNo AS PatientId
		, c.AptId AS VisitNumber
		, c.POS AS LocationId
		, COALESCE(prov.UserID, u2.UserId) AS DoctorId --c.Provider 
		, COALESCE(ref.UserID, u2.UserId)AS ReferringDocId --c.Referral
		, ref.Signature-- rp.LabelName
		, null AS DepartmentCode
		, c.Cpt AS CPTCode
		, c.CptDescr
		, c.Modifiers -- Need parsing info from Saba, notes just say they should be separated by spaces
		, null AS Unit
		, CAST(c.FromDate AS datetime) AS ServiceDate
		, CAST(c.ThruDate AS datetime) AS ServiceDate
		, c.AptId AS OrderNumber
		, c.Dx1 AS ICD1
		, c.Dx2 AS ICD2
		, c.Dx3 AS ICD3
		, c.Dx4 AS ICD4
		, 11 AS POSCode--c.POS AS POSCode
		, null AS HCFA19_Note
		, null AS HCFA24_Note
		, c.SqlAmount AS ChargeAmount --divide per spec
		, null AS Balance
		, CAST(c.PostingDate AS datetime) AS PostDate
		, null AS BatchNumber
		, COALESCE(u.UserID, u2.UserID) AS UserName
		, COALESCE(bs.ImagBillingStatus, 'STMTEVAL') AS BillingStatus
		, CASE WHEN c.ARClass = 'PILATT' THEN 'Atticus'
			   WHEN c.ARClass = 'PIINCP' THEN 'Carepoint'
			   WHEN c.ARClass IN ('LRWCCP', 'MLWCCP', 'LRWC') THEN 'Creditpoint'
			   WHEN c.ARClass = 'PIFAC' AND c.InsCode1 = '195' THEN 'HFS'
			   WHEN c.ARClass = 'PIFAC' AND c.InsCode1 LIKE 'Key%' THEN 'KeyHealth'
			   ELSE 'OneSource'
			END AS LienOwnwer 
		, null AS SaleDate
		, null AS SaleAmount
		, CASE WHEN c.ARClass = 'PILATT' THEN 'Atticus'
			   WHEN c.ARClass = 'PIINCP' THEN 'OneSource'
			   WHEN c.ARClass IN ('LRWCCP', 'MLWCCP', 'LRWC') THEN 'Creditpoint'
			   WHEN c.ARClass = 'PIFAC' AND c.InsCode1 = '195' THEN 'HFS'
			   WHEN c.ARClass = 'PIFAC' AND c.InsCode1 LIKE 'Key%' THEN 'KeyHealth'
			   ELSE 'OneSource'
			END AS CollectionAgency
		, CAST(c.PostingDate AS datetime) AS FirstStatementDate
		, CAST(c.PostingDate AS datetime) AS LastStatementDate
		, null AS FollowupAction
		, null AS FollowUpNextDate
		, 0 AS isMailReturn
		, CASE WHEN c.ARClass IN ('ACR','AWCMIS','CCH','CPR','CTC','FOH','LRWC','LRWCCP','MIS','MLWC','NAWC','PIEXP','PIFAC','PIINCP','PIINT','PILATT','PILIEN','PIMEDP','PPO','RE','SG','SOH','SP','UN') THEN 1 
			   WHEN c.ARClass IN ('AWC','BS','CWC','LOA','MC','VC') AND DOS <= '3/25/2021' THEN 1
			   WHEN c.ARClass IS NULL THEN 1
			   ELSE 0 END AS isOnHold 
		, 0 AS isVoided
		, null AS VoidedDate
		, '1/1/1900' AS LastClaimDate --date value provided by spec
		, 1 AS ReportingUnitCount
		, 'R' AS BillingType
		, null AS AnesthiaTech
		, null AS BilingProdCode
		, null AS AuthNumb
		, null AS InterpDate
		, null AS InterpLocation
		, e.CompanyName
FROM Integration.Stg.Charges c
LEFT JOIN EMRIWarehouse.dbo.Appointments a on c.AptId = a.ApptId
--LEFT JOIN Integration.Stg.ReferringPhysicians rp ON c.Provider = rp.Code
LEFT JOIN  AbbaDoxRISMigration.dbo.Users u ON u.ADSKey = c.Operator AND u.SourceTable = 'Operators'
LEFT JOIN  AbbaDoxRISMigration.dbo.Users u2 ON u2.ADSKey = 'ZZZZ' AND u2.SourceTable = 'Conversion'
LEFT JOIN AbbaDoxRISMigration.xref.ImagBillingStatus bs ON c.InsFlag = bs.ADSInsFlag
LEFT JOIN AbbaDoxRISMigration.dbo.Users prov ON c.Provider = prov.ADSKey AND prov.SourceTable = 'Providers'
LEFT JOIN AbbaDoxRISMigration.dbo.Users ref ON COALESCE(c.Referral, a.ReferringDrId) = ref.ADSKey AND ref.SourceTable = 'RefPhys'
LEFT JOIN Integration.Stg.Entities e ON c.Entity = e.code


UPDATE c
SET VisitNumber = a.ApptId
FROM AbbaDoxRISMigration.dbo.ImgPatientCharge c
JOIN EMRIWarehouse.dbo.appointments a  ON a.CptCode = c.CPTCode and a.Account = c.PatientID and a.DOS = c.ServiceDate
WHERE c.VisitNumber IS NULL

UPDATE c
SET VisitNumber = cv.ApptId
FROM AbbaDoxRISMigration.dbo.ImgPatientCharge c
JOIN AbbaDoxRISMigration.import.ChargesVisit cv  ON cv.chargeId = c.ChargeID
WHERE c.VisitNumber IS NULL

UPDATE c
SET ImgLocationId= COALESCE(cv.ImgLocationId, c.LocationId)
FROM AbbaDoxRISMigration.dbo.ImgPatientCharge c
LEFT JOIN AbbaDoxRISMigration.import.ImgPatientCharge cv  ON cv.chargeId = c.ChargeID

 

/********************************************************************************************************
dbo.PatientNote
- straightforward, validated pending response on PaymentId value
********************************************************************************************************/
INSERT INTO AbbaDoxRISMigration.dbo.ImgPatientNote (
	NoteID
	, UserId
	, Username
	, PatientID
	, PaymentID
	, ChargeID
	, Subject
	, Note
	, DateCreated
)

SELECT	tm.TransId AS NoteId
		, u.UserID
		, tm.Operator
		, tm.AccountNo AS PatientId
		, null AS PaymentId
		, tm.RelatesToTransNo AS ChargeId
		, null AS Subject
		, tm.Message AS Note
		, CAST(tm.PostingDate AS datetime) AS DateCreated
FROM Integration.Stg.TransactionMessages tm
LEFT JOIN  AbbaDoxRISMigration.dbo.Users u ON u.ADSKey = tm.Operator AND u.SourceTable = 'Operators'

/********************************************************************************************************
dbo.PatientPayment

********************************************************************************************************/

--truncate table AbbaDoxRISMigration.dbo.ImgPatientPayment
INSERT INTO AbbaDoxRISMigration.dbo.ImgPatientPayment (
	PaymentID
	, PatientID
	, PaymentTransactionNo
	, ChargeID
	, SystemDate
	, PaymentDate
	, BatchNumber
	, Username
	, PaymentAmount
	, AdjustmentAmount
	, ApprovedAmount
	, AllowedAmount
	, CoInsuranceAmount
	, DeductibleAmount
	, PaymentType
	, InsurancePlanNumber
	, InsurancePlanPolicy
	, AdjustmentType
	, CheckNumber
	, isDenial
	, DenialCode
	, ElectronicPaymentClaimICN
	, ElectronicPaymentCodes
)

SELECT DISTINCT	cc.ChargesCreditsId AS PaymentID
		, c.AccountNo AS PatientId
		, cc.CreditTransactionNo AS PaymentTransactionNo
		, cc.ChargeTransactionNo AS ChargeId
		, c.PostingDate AS SystemDate
		, c.PostingDate AS PaymentDate
		, null AS BatchNumber
		, COALESCE(u.UserID, u2.UserID) AS UserName
		, CASE WHEN pt.TransType = 'Payment' THEN cc.Amount/100  ELSE 0 END AS PaymentAmount
		, CASE WHEN pt.TransType = 'Adjustment' THEN cc.Amount/100  ELSE 0 END AS AdjustedAmount --if pt.[TransType] is adjustment then populate
		, 0 AS ApprovedAmount
		, 0 AS AllowedAmount
		, 0 AS CoInsuranceAmount
		, 0 AS DeductibleAmount
		, CASE WHEN pt.TransType = 'Payment' THEN COALESCE(pt.ImagPaymentType ,'MIGRATION ERROR') ELSE '' END AS PaymentType 
		, cc2.InsCode1 AS InsurancePlanNumber -- U
		, null AS InsurancePlanPolicy
		, CASE WHEN pt.TransType = 'Adjustment' THEN COALESCE(pt.ImagPaymentType ,'MIGRATION ERROR') ELSE '' END AS AdjustmentType --if pt.[TransType] is adjustment
		, c.CheckNo AS CheckNumber
		, 0 AS isDenial
		, null AS DenialCode
		, null AS ElectronicPaymentClaimICN
		, null AS ElectronicPaymentCodes
FROM Integration.Stg.Credits c
JOIN EMRIWarehouse.dbo.ChargesCredits cc ON c.TransactionNo = cc.CreditTransactionNo
LEFT JOIN  (
		SELECT DISTINCT c.TransactionNo , c.InsCode1 
		FROM Integration.Stg.Charges c 
		--JOIN EMRIWarehouse.dbo.ChargesCredits cc on c.TransactionNo = cc.ChargeTransactionNo 
	  ) cc2 ON cc.ChargeTransactionNo = cc2.TransactionNo
LEFT JOIN  AbbaDoxRISMigration.dbo.Users u ON u.ADSKey = c.Operator AND u.SourceTable = 'Operators'
LEFT JOIN  AbbaDoxRISMigration.dbo.Users u2 ON u2.ADSKey = 'ZZZZ' AND u2.SourceTable = 'Conversion'
JOIN AbbaDoxRISMigration.xref.ImagPaymentType pt ON c.FinancialCode = pt.FinancialCode
 
--Fix for missing insuranceCodes 
UPDATE ip
SET InsurancePlanNumber = a.InsCode
FROM AbbaDoxRISMigration.dbo.ImgPatientPayment ip
JOIN Integration.Stg.Charges c ON ip.ChargeID = c.TransactionNo
JOIN EMRIWarehouse.dbo.Appointments a ON c.AptId = a.ApptId
WHERE ip.InsurancePlanNumber is null


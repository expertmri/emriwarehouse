USE ImagineMigration;
GO

/********************************************************************************************************
dbo.Patient

********************************************************************************************************/

INSERT INTO ImagineMigration.dbo.Patient (
	PatientID
    , FirstName
    , MiddleName
    , LastName
    , Address
    , Address2
    , City
    , State
    , Zip
    , Zip2
    , Country
    , Phone
    , SSN
    , DateofBirth
    , Gender
    , Email
)
SELECT	p.PatientID
		, p.FirstName
		, p.MaidenName
		, p.LastName
		, p.Address1
		, p.Address2
		, p.City
		, LTRIM(RTRIM(p.state))
		, p.ZIP
		, null AS Zip2
		, 'UNITED STATES' AS Country
		, p.HomePhone
		, p.SSN
		, CONVERT(varchar(10), p.DOB ,101)
		, null as Gender
		, p.Email
FROM AbbaDoxRISMigration.dbo.Patients p




/********************************************************************************************************
dbo.PatientCharge
192,731 inserted and charges table has same 
********************************************************************************************************/

INSERT INTO ImagineMigration.dbo.PatientCharge (
	ChargeID
    , PatientID
    , VisitNumber
    , LocationID
    , DoctorID
    , ReferringDoctorID
    , ReferringDoctorFullName
    , DepartmentCode
    , CPTCode
    , CPTCodeDescription
    , Modifier
    , Unit
    , ServiceDate
    , ServiceDate_to
    , OrderNumber
    , ICD1
    , ICD2
    , ICD3
    , ICD4
    , POSCode
    , HCFA19_Note
    , HCFA24_Note
    , ChargeAmount
    , Balance
    , PostDate
    , BatchNumber
    , UserName
    , BillingStatus
	, LienOwnwer
	, SaleDate
	, SaleAmount
	, CollectionAgency
    , FirstStatementDate
    , LastStatementDate
    , FollowupAction
    , FollowUpNextDate
    , isMailReturn
    , isOnHold
    , isVoided
    , VoidedDate
    , LastClaimDate
    , ReportingUnitCount
    , Type
    , AnesthesiaTechnique
    , BillingProcedureCode
    , AuthorizationNumber
    , InterpretationDate
    , InterpretationLocationID
)

SELECT	c.TransactionNo AS ChargeId
		, c.AccountNo AS PatientId
		, c.AptId AS VisitNumber
		, c.POS AS LocationId
		, COALESCE(prov.UserID, u2.UserId) AS DoctorId --c.Provider 
		, COALESCE(ref.UserID, u2.UserId)AS ReferringDocId --c.Referral
		, rp.LabelName
		, null AS DepartmentCode
		, c.Cpt AS CPTCode
		, c.CptDescr
		, c.Modifiers -- Need parsing info from Saba, notes just say they should be separated by spaces
		, null AS Unit
		, CAST(c.FromDate AS datetime) AS ServiceDate
		, CAST(c.ThruDate AS datetime) AS ServiceDate
		, c.AptId AS OrderNumber
		, c.Dx1 AS ICD1
		, c.Dx2 AS ICD2
		, c.Dx3 AS ICD3
		, c.Dx4 AS ICD4
		, c.POS AS POSCode
		, null AS HCFA19_Note
		, null AS HCFA24_Note
		, c.SqlAmount AS ChargeAmount --divide per spec
		, null AS Balance
		, CAST(c.PostingDate AS datetime) AS PostDate
		, null AS BatchNumber
		, COALESCE(u.UserID, u2.UserID) AS UserName
		, COALESCE(bs.ImagBillingStatus, 'Migration Mapping Error') AS BillingStatus
		, CASE WHEN c.ARClass = 'PILATT' THEN 'Atticus'
			   WHEN c.ARClass = 'PIINCP' THEN 'Carepoint'
			   WHEN c.ARClass IN ('LRWCCP', 'MLWCCP', 'LRWC') THEN 'Creditpoint'
			   WHEN c.ARClass = 'PIFAC' AND c.InsCode1 = '195' THEN 'HFS'
			   WHEN c.ARClass = 'PIFAC' AND c.InsCode1 LIKE 'Key%' THEN 'KeyHealth'
			   ELSE 'OneSource'
			END AS LienOwnwer 
		, null AS SaleDate
		, null AS SaleAmount
		, CASE WHEN c.ARClass = 'PILATT' THEN 'Atticus'
			   WHEN c.ARClass = 'PIINCP' THEN 'OneSource'
			   WHEN c.ARClass IN ('LRWCCP', 'MLWCCP', 'LRWC') THEN 'Creditpoint'
			   WHEN c.ARClass = 'PIFAC' AND c.InsCode1 = '195' THEN 'HFS'
			   WHEN c.ARClass = 'PIFAC' AND c.InsCode1 LIKE 'Key%' THEN 'KeyHealth'
			   ELSE 'OneSource'
			END AS CollectionAgency
		, CAST(c.PostingDate AS datetime) AS FirstStatementDate
		, CAST(c.PostingDate AS datetime) AS LastStatementDate
		, null AS FollowupAction
		, null AS FollowUpNextDate
		, 0 AS isMailReturn
		, 0 AS isOnHold
		, 0 AS isVoided
		, null AS VoidedDate
		, '1/1/1900' AS LastClaimDate --date value provided by spec
		, 1 AS ReportingUnitCount
		, 'R' AS BillingType
		, null AS AnesthiaTech
		, null AS BilingProdCode
		, null AS AuthNumb
		, null AS InterpDate
		, null AS InterpLocation
FROM Integration.Stg.Charges c
LEFT JOIN Integration.Stg.ReferringPhysicians rp ON c.Provider = rp.Code
LEFT JOIN  AbbaDoxRISMigration.dbo.Users u ON u.ADSKey = c.Operator AND u.SourceTable = 'Operators'
LEFT JOIN  AbbaDoxRISMigration.dbo.Users u2 ON u2.ADSKey = 'ZZZZ' AND u2.SourceTable = 'Conversion'
LEFT JOIN ImagineMigration.xref.ImagBillingStatus bs ON c.InsFlag = bs.ADSInsFlag
LEFT JOIN AbbaDoxRISMigration.dbo.Users prov ON c.Provider = prov.ADSKey AND prov.SourceTable = 'RefPhys'
LEFT JOIN AbbaDoxRISMigration.dbo.Users ref ON c.Referral = ref.ADSKey AND ref.SourceTable = 'RefPhys'

UPDATE c
SET VisitNumber = a.ApptId
FROM ImagineMigration.dbo.PatientCharge c
JOIN EMRIWarehouse.dbo.appointments a  ON a.CptCode = c.CPTCode and a.Account = c.PatientID and a.DOS = c.ServiceDate
WHERE c.VisitNumber IS NULL


/********************************************************************************************************
dbo.PatientNote
- straightforward, validated pending response on PaymentId value
********************************************************************************************************/
INSERT INTO ImagineMigration.dbo.PatientNote (
	NoteID
	, UserId
	, Username
	, PatientID
	, PaymentID
	, ChargeID
	, Subject
	, Note
	, DateCreated
)

SELECT	tm.TransId AS NoteId
		, u.UserID
		, tm.Operator
		, tm.AccountNo AS PatientId
		, null AS PaymentId
		, tm.RelatesToTransNo AS ChargeId
		, null AS Subject
		, tm.Message AS Note
		, CAST(tm.PostingDate AS datetime) AS DateCreated
FROM Integration.Stg.TransactionMessages tm
LEFT JOIN  AbbaDoxRISMigration.dbo.Users u ON u.ADSKey = tm.Operator AND u.SourceTable = 'Operators'

/********************************************************************************************************
dbo.PatientPayment

********************************************************************************************************/

--truncate table ImagineMigration.dbo.PatientPayment 
INSERT INTO ImagineMigration.dbo.PatientPayment (
	PaymentID
	, PatientID
	, PaymentTransactionNo
	, ChargeID
	, SystemDate
	, PaymentDate
	, BatchNumber
	, Username
	, PaymentAmount
	, AdjustmentAmount
	, ApprovedAmount
	, AllowedAmount
	, CoInsuranceAmount
	, DeductibleAmount
	, PaymentType
	, InsurancePlanNumber
	, InsurancePlanPolicy
	, AdjustmentType
	, CheckNumber
	, isDenial
	, DenialCode
	, ElectronicPaymentClaimICN
	, ElectronicPaymentCodes
)

SELECT DISTINCT	cc.ChargesCreditsId AS PaymentID
		, c.AccountNo AS PatientId
		, cc.CreditTransactionNo AS PaymentTransactionNo
		, cc.ChargeTransactionNo AS ChargeId
		, c.PostingDate AS SystemDate
		, c.PostingDate AS PaymentDate
		, null AS BatchNumber
		, COALESCE(u.UserID, u2.UserID) AS UserName
		, cc.Amount/100 AS PaymentAmount
		, 0 AS AdjustedAmount
		, 0 AS ApprovedAmount
		, 0 AS AllowedAmount
		, 0 AS CoInsuranceAmount
		, 0 AS DeductibleAmount
		, COALESCE(pt.ImagPaymentType ,'MIGRATION ERROR') AS PaymentType 
		, null AS InsurancePlanNumber
		, null AS InsurancePlanPolicy
		, COALESCE(pt.ImagPaymentType ,'MIGRATION ERROR') AS AdjustmentType
		, c.CheckNo AS CheckNumber
		, 0 AS isDenial
		, null AS DenialCode
		, null AS ElectronicPaymentClaimICN
		, null AS ElectronicPaymentCodes
--FROM EMRIWarehouse.dbo.BilledTransactions bt
FROM Integration.Stg.Credits c
JOIN EMRIWarehouse.dbo.ChargesCredits cc ON c.TransactionNo = cc.CreditTransactionNo
--LEFT JOIN Integration.Stg.Payments p ON bt.TransactionNumber = p.TransactionNo
LEFT JOIN  AbbaDoxRISMigration.dbo.Users u ON u.ADSKey = c.Operator AND u.SourceTable = 'Operators'
LEFT JOIN  AbbaDoxRISMigration.dbo.Users u2 ON u2.ADSKey = 'ZZZZ' AND u2.SourceTable = 'Conversion'
JOIN ImagineMigration.xref.ImagPaymentType pt ON c.FinancialCode = pt.FinancialCode
--WHERE bt.IsCharge = 0 and cc.ChargesCreditsId = 7 bt.Account





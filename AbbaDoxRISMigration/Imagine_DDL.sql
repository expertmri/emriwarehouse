USE ImagineMigration;
GO

CREATE SCHEMA xref;
GO

DROP TABLE dbo.Patient;

CREATE TABLE dbo.Patient (
	PatientID int PRIMARY KEY
	, FirstName varchar(255)
	, MiddleName varchar(255)
	, LastName varchar(255)
	, Address varchar(255)
	, Address2 varchar(255)
	, City varchar(255)
	, State varchar(2)
	, Zip varchar(10)
	, Zip2 varchar(10)
	, Country varchar(100)
	, Phone varchar(13)
	, SSN varchar(11)
	, DateofBirth varchar(10)
	, Gender varchar(1)
	, Email varchar(255)
)

DROP TABLE dbo.PatientCharge;

CREATE TABLE dbo.PatientCharge (
	ChargeID numeric(9)
	, PatientID numeric(9)
	, VisitNumber varchar(255)
	, LocationID varchar(255)
	, DoctorID numeric(9)
	, ReferringDoctorID numeric(9)
	, ReferringDoctorFullName varchar(255)
	, DepartmentCode varchar(255)
	, CPTCode varchar(255)
	, CPTCodeDescription varchar(255)
	, Modifier varchar(255)
	, Unit numeric(9)
	, ServiceDate datetime
	, ServiceDate_to datetime
	, OrderNumber varchar(255)
	, ICD1 varchar(255)
	, ICD2 varchar(255)
	, ICD3 varchar(255)
	, ICD4 varchar(255)
	, POSCode varchar(255)
	, HCFA19_Note varchar(255)
	, HCFA24_Note varchar(255)
	, ChargeAmount money
	, Balance money
	, PostDate datetime
	, BatchNumber varchar(255)
	, UserName varchar(255)
	, BillingStatus varchar(255)
	, LienOwnwer varchar(255)
	, SaleDate varchar(255)
	, SaleAmount varchar(255)
	, CollectionAgency varchar(255)
	, FirstStatementDate datetime
	, LastStatementDate datetime
	, FollowupAction varchar(255)
	, FollowUpNextDate varchar(255)
	, isMailReturn int
	, isOnHold int
	, isVoided int
	, VoidedDate datetime
	, LastClaimDate datetime
	, ReportingUnitCount numeric
	, Type varchar(255)
	, AnesthesiaTechnique varchar(255)
	, BillingProcedureCode varchar(255)
	, AuthorizationNumber varchar(255)
	, InterpretationDate varchar(255)
	, InterpretationLocationID varchar(255)
	, UniqueRowID int IDENTITY(1,1)
	, CONSTRAINT PK_PatientCharge_ChargeId PRIMARY KEY (ChargeID)
);

DROP TABLE dbo.PatientNote;

CREATE TABLE dbo.PatientNote (
	NoteID numeric(9)
	, UserID int
	, Username varchar(255)
	, PatientID numeric(9)
	, PaymentID numeric(9)
	, ChargeID numeric(9)
	, Subject varchar(1024)
	, Note varchar(4000)
	, DateCreated datetime
	, CONSTRAINT PK_PatientNote_NoteID PRIMARY KEY (NoteID)
);

DROP TABLE dbo.PatientPayment;

CREATE TABLE dbo.PatientPayment (
	PaymentID numeric(9)
	, PatientID numeric(9)
	, PaymentTransactionNo int
	, ChargeID int
	, SystemDate varchar(255)
	, PaymentDate varchar(255)
	, BatchNumber varchar(255)
	, Username varchar(255)
	, PaymentAmount varchar(255)
	, AdjustmentAmount varchar(255)
	, ApprovedAmount varchar(255)
	, AllowedAmount varchar(255)
	, CoInsuranceAmount varchar(255)
	, DeductibleAmount varchar(255)
	, PaymentType varchar(255)
	, InsurancePlanNumber varchar(255)
	, InsurancePlanPolicy varchar(255)
	, AdjustmentType varchar(255)
	, CheckNumber varchar(255)
	, isDenial int
	, DenialCode varchar(255)
	, ElectronicPaymentClaimICN varchar(255)
	, ElectronicPaymentCodes varchar(255)
	, CONSTRAINT PK_PatientPayment_PaymentId PRIMARY KEY (PaymentID)
);

CREATE TABLE xref.ImagBillingStatus (
	ADSInsFlag varchar(255)
	, ImagBillingStatus varchar(255)
);

INSERT INTO xref.ImagBillingStatus
VALUES ('A5', 'STMTEVAL')
, ('A3', 'PRIM')
, ('AT~AT', 'STMTEVAL')
, ('A1~AT', 'STMTEVAL')
, ('A0', 'STMTEVAL')
, ('NULL', 'PRIM')
, ('AF', 'STMTEVAL')
, ('A2~AT', 'PRIM')
, ('AT', 'STMTEVAL')
, ('AT~A2', 'STMTEVAL')
, ('A2', 'PRIM')
, ('AT~A5', 'STMTEVAL')
, ('A1', 'PRIM')
, ('N6', 'PRIM')
;

CREATE TABLE xref.ImagPaymentType (
	FinancialCode varchar(255)
	, TransType varchar(255)
	, ImagPaymentType varchar(255)
);

INSERT INTO xref.ImagPaymentType
VALUES('CASH','Payment','PATIENT')
, ('CC','Payment','INSURANCE')
, ('CCOP','Payment','INSURANCE')
, ('GRP','Payment','INSURANCE')
, ('GRWO','Adjustment','INSURANCE WRITEOFF')
, ('HFSP','Payment','INSURANCE')
, ('HFSWO','Adjustment','INSURANCE WRITEOFF')
, ('INCK','Payment','INSURANCE')
, ('INDD','Adjustment','INSURANCE WRITEOFF')
, ('INDR','Adjustment','INSURANCE WRITEOFF')
, ('INWO','Adjustment','INSURANCE WRITEOFF')
, ('KHP','Payment','INSURANCE')
, ('KHWO','Adjustment','INSURANCE WRITEOFF')
, ('MCCK','Payment','INSURANCE')
, ('MCWO','Adjustment','INSURANCE WRITEOFF')
, ('MISP','Payment','INSURANCE')
, ('MISWO','Adjustment','INSURANCE WRITEOFF')
, ('MLP','Payment','INSURANCE')
, ('MLWO','Adjustment','INSURANCE WRITEOFF')
, ('PATRF','Adjustment','REF-PATIENT')
, ('PRP','Payment','INSURANCE')
, ('PRWO','Adjustment','INSURANCE WRITEOFF')
, ('PTCK','Payment','PATIENT')
, ('PTRF','Adjustment','REF-PATIENT')
, ('PTWO','Adjustment','PATIENT WRITEOFF')

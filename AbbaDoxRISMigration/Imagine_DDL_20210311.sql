
USE AbbaDoxRISMigration
GO
/****** Object:  User [imagineUser]    Script Date: 3/12/2021 9:48:57 AM ******/
CREATE USER [imagineUser] FOR LOGIN [imagineUser] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [imagineUser]
GO
GRANT CONNECT TO [imagineUser] AS [dbo]
GO
GRANT VIEW ANY COLUMN ENCRYPTION KEY DEFINITION TO [public] AS [dbo]
GO
GRANT VIEW ANY COLUMN MASTER KEY DEFINITION TO [public] AS [dbo]
GO
/****** Object:  Schema [xref]    Script Date: 3/12/2021 9:49:01 AM ******/
CREATE SCHEMA [xref]
GO
/****** Object:  Table [dbo].[Patient]    Script Date: 3/12/2021 9:49:01 AM ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
--CREATE TABLE [dbo].[Patient](
--	[PatientID] [int] NOT NULL,
--	[FirstName] [varchar](255) NULL,
--	[MiddleName] [varchar](255) NULL,
--	[LastName] [varchar](255) NULL,
--	[Address] [varchar](255) NULL,
--	[Address2] [varchar](255) NULL,
--	[City] [varchar](255) NULL,
--	[State] [varchar](2) NULL,
--	[Zip] [varchar](10) NULL,
--	[Zip2] [varchar](10) NULL,
--	[Country] [varchar](100) NULL,
--	[Phone] [varchar](13) NULL,
--	[SSN] [varchar](11) NULL,
--	[DateofBirth] [varchar](10) NULL,
--	[Gender] [varchar](1) NULL,
--	[Email] [varchar](255) NULL,
--PRIMARY KEY CLUSTERED 
--(
--	[PatientID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]
--GO
/****** Object:  Table [dbo].[PatientCharge]    Script Date: 3/12/2021 9:49:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ImgPatientCharge](
	[ChargeID] [numeric](9, 0) NOT NULL,
	[PatientID] [numeric](9, 0) NULL,
	[VisitNumber] [varchar](255) NULL,
	[LocationID] [varchar](255) NULL,
	[DoctorID] [numeric](9, 0) NULL,
	[ReferringDoctorID] [numeric](9, 0) NULL,
	[ReferringDoctorFullName] [varchar](255) NULL,
	[DepartmentCode] [varchar](255) NULL,
	[CPTCode] [varchar](255) NULL,
	[CPTCodeDescription] [varchar](255) NULL,
	[Modifier] [varchar](255) NULL,
	[Unit] [numeric](9, 0) NULL,
	[ServiceDate] [datetime] NULL,
	[ServiceDate_to] [datetime] NULL,
	[OrderNumber] [varchar](255) NULL,
	[ICD1] [varchar](255) NULL,
	[ICD2] [varchar](255) NULL,
	[ICD3] [varchar](255) NULL,
	[ICD4] [varchar](255) NULL,
	[POSCode] [varchar](255) NULL,
	[HCFA19_Note] [varchar](255) NULL,
	[HCFA24_Note] [varchar](255) NULL,
	[ChargeAmount] [money] NULL,
	[Balance] [money] NULL,
	[PostDate] [datetime] NULL,
	[BatchNumber] [varchar](255) NULL,
	[UserName] [varchar](255) NULL,
	[BillingStatus] [varchar](255) NULL,
	[LienOwnwer] [varchar](255) NULL,
	[SaleDate] [varchar](255) NULL,
	[SaleAmount] [varchar](255) NULL,
	[CollectionAgency] [varchar](255) NULL,
	[FirstStatementDate] [datetime] NULL,
	[LastStatementDate] [datetime] NULL,
	[FollowupAction] [varchar](255) NULL,
	[FollowUpNextDate] [varchar](255) NULL,
	[isMailReturn] [int] NULL,
	[isOnHold] [int] NULL,
	[isVoided] [int] NULL,
	[VoidedDate] [datetime] NULL,
	[LastClaimDate] [datetime] NULL,
	[ReportingUnitCount] [numeric](18, 0) NULL,
	[Type] [varchar](255) NULL,
	[AnesthesiaTechnique] [varchar](255) NULL,
	[BillingProcedureCode] [varchar](255) NULL,
	[AuthorizationNumber] [varchar](255) NULL,
	[InterpretationDate] [varchar](255) NULL,
	[InterpretationLocationID] [varchar](255) NULL,
	[UniqueRowID] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_ImgPatientCharge_ChargeId] PRIMARY KEY CLUSTERED 
(
	[ChargeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PatientNote]    Script Date: 3/12/2021 9:49:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ImgPatientNote](
	[NoteID] [numeric](9, 0) NOT NULL,
	[UserID] [int] NULL,
	[Username] [varchar](255) NULL,
	[PatientID] [numeric](9, 0) NULL,
	[PaymentID] [numeric](9, 0) NULL,
	[ChargeID] [numeric](9, 0) NULL,
	[Subject] [varchar](1024) NULL,
	[Note] [varchar](4000) NULL,
	[DateCreated] [datetime] NULL,
 CONSTRAINT [PK_PatientNote_NoteID] PRIMARY KEY CLUSTERED 
(
	[NoteID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PatientPayment]    Script Date: 3/12/2021 9:49:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ImgPatientPayment](
	[PaymentID] [numeric](9, 0) NOT NULL,
	[PatientID] [numeric](9, 0) NULL,
	[PaymentTransactionNo] [int] NULL,
	[ChargeID] [int] NULL,
	[SystemDate] [varchar](255) NULL,
	[PaymentDate] [varchar](255) NULL,
	[BatchNumber] [varchar](255) NULL,
	[Username] [varchar](255) NULL,
	[PaymentAmount] [varchar](255) NULL,
	[AdjustmentAmount] [varchar](255) NULL,
	[ApprovedAmount] [varchar](255) NULL,
	[AllowedAmount] [varchar](255) NULL,
	[CoInsuranceAmount] [varchar](255) NULL,
	[DeductibleAmount] [varchar](255) NULL,
	[PaymentType] [varchar](255) NULL,
	[InsurancePlanNumber] [varchar](255) NULL,
	[InsurancePlanPolicy] [varchar](255) NULL,
	[AdjustmentType] [varchar](255) NULL,
	[CheckNumber] [varchar](255) NULL,
	[isDenial] [int] NULL,
	[DenialCode] [varchar](255) NULL,
	[ElectronicPaymentClaimICN] [varchar](255) NULL,
	[ElectronicPaymentCodes] [varchar](255) NULL,
 CONSTRAINT [PK_PatientPayment_PaymentId] PRIMARY KEY CLUSTERED 
(
	[PaymentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [xref].[ImagBillingStatus]    Script Date: 3/12/2021 9:49:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [xref].[ImagBillingStatus](
	[ADSInsFlag] [varchar](255) NULL,
	[ImagBillingStatus] [varchar](255) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [xref].[ImagPaymentType]    Script Date: 3/12/2021 9:49:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [xref].[ImagPaymentType](
	[FinancialCode] [varchar](255) NULL,
	[TransType] [varchar](255) NULL,
	[ImagPaymentType] [varchar](255) NULL
) ON [PRIMARY]
GO

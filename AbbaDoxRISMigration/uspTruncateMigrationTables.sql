USE AbbaDoxRISMigration;
GO

ALTER PROCEDURE uspTruncateMigrationTables AS
TRUNCATE TABLE AbbaDoxRISMigration.dbo.Appointments
TRUNCATE TABLE AbbaDoxRISMigration.dbo.Appointments_Comments
TRUNCATE TABLE AbbaDoxRISMigration.dbo.Appointments_Distributions
TRUNCATE TABLE AbbaDoxRISMigration.dbo.Appointments_Insurances
TRUNCATE TABLE AbbaDoxRISMigration.dbo.Attorneys 
TRUNCATE TABLE AbbaDoxRISMigration.dbo.Cases
TRUNCATE TABLE AbbaDoxRISMigration.dbo.ExamCodes
TRUNCATE TABLE AbbaDoxRISMigration.dbo.Exams
TRUNCATE TABLE AbbaDoxRISMigration.dbo.Exams_Comments
TRUNCATE TABLE AbbaDoxRISMigration.dbo.Exams_Distributions
TRUNCATE TABLE AbbaDoxRISMigration.dbo.Exams_Insurances
TRUNCATE TABLE AbbaDoxRISMigration.dbo.ImgPatientCharge
TRUNCATE TABLE AbbaDoxRISMigration.dbo.ImgPatientNote
TRUNCATE TABLE AbbaDoxRISMigration.dbo.ImgPatientPayment
TRUNCATE TABLE AbbaDoxRISMigration.dbo.Insurances
--TRUNCATE TABLE AbbaDoxRISMigration.dbo.Locations
--TRUNCATE TABLE AbbaDoxRISMigration.dbo.Locations_Resources
TRUNCATE TABLE AbbaDoxRISMigration.dbo.Patients
TRUNCATE TABLE AbbaDoxRISMigration.dbo.Patients_Notes
TRUNCATE TABLE AbbaDoxRISMigration.dbo.Patients_Insurances
TRUNCATE TABLE AbbaDoxRISMigration.dbo.PISettlement 
TRUNCATE TABLE AbbaDoxRISMigration.dbo.Users
TRUNCATE TABLE Integration.abdx.CaseMapping;
--Imagine Tables


/* ONLY RUN DURING GO LIVE*/
TRUNCATE TABLE AbbaDoxRISMigration.dbo.ScannedDocuments


--Imagine Tables
--TRUNCATE TABLE ImagineMigration.dbo.Patient
--TRUNCATE TABLE ImagineMigration.dbo.PatientCharge
--TRUNCATE TABLE ImagineMigration.dbo.PatientNote
--TRUNCATE TABLE ImagineMigration.dbo.PatientPayment

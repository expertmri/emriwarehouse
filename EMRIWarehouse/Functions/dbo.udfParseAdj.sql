USE [EMRIWarehouse]
GO
/****** Object:  UserDefinedFunction [dbo].[udfParseAdj]    Script Date: 10/24/2017 8:53:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[udfParseAdj] (@ApptId varchar(100))
	RETURNS varchar(100)
AS

BEGIN
DECLARE @Adj varchar(100)
		, @InsPolicy varchar(100)

SET @InsPolicy = (

					SELECT DISTINCT tt.InsPolicy
					FROM Integration.Stg.AppointmentsList apt
					JOIN Integration.Stg.Cases c ON c.Account = apt.Account
										 AND c.CaseNumber = CASE WHEN RTRIM(LEFT(apt.CaseId, 2)) IN ('1','2','3') THEN RTRIM(LEFT(apt.CaseId, 2)) ELSE null END
					LEFT JOIN (
									SELECT apt2.Account
										   , c2.CaseNumber
										   , apt2.InsPolicy
									FROM Integration.Stg.AppointmentsList apt2
									JOIN Integration.Stg.Cases c2 ON c2.Account = apt2.Account
												 AND c2.CaseNumber = CASE WHEN RTRIM(LEFT(apt2.CaseId, 2)) IN ('1','2','3') THEN RTRIM(LEFT(apt2.CaseId, 2)) ELSE null END
											AND apt2.InsPolicy like '%ADJ%'
											AND apt2.Account = (SELECT DISTINCT Account FROM Integration.Stg.AppointmentsList WHERE SequenceNo = @ApptId)
								) tt ON tt.Account = c.Account AND tt.CaseNumber = c.CaseNumber
					WHERE apt.SequenceNo = @ApptId
				)


SET @Adj = (
				SELECT SUBSTRING(
									@InsPolicy 
									, PATINDEX('%ADJ%', @InsPolicy) 
									, (LEN(@InsPolicy) - (PATINDEX('%ADJ%', @InsPolicy) - 1))
								)
				WHERE @InsPolicy LIKE '%ADJ%'
			)

RETURN @Adj;
END;
GO

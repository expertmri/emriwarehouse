USE [EMRIWarehouse]
GO
/****** Object:  UserDefinedFunction [dbo].[udfGetMonthName]   Script Date: 11/7/2017 8:53:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[udfGetMonthName] (@Date date)
	RETURNS varchar(9)
AS

BEGIN

DECLARE @MonthName varchar(9)

SET @MonthName =	(
						
						SELECT DATENAME(MONTH, DATEADD(MONTH, (MONTH(@Date)), -1))

					)			

RETURN @MonthName ;
END;
GO

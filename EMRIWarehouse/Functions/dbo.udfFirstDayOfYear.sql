USE [EMRIWarehouse]
GO
/****** Object:  UserDefinedFunction [dbo].[udfFirstDayOfYear]    Script Date: 1/9/2020 9:57:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER FUNCTION [dbo].[udfFirstDayOfYear]
(
	-- Add the parameters for the function here
	@Date datetime
)
RETURNS datetime
AS
BEGIN
	
	DECLARE @Return datetime

	SET @Return = (SELECT CAST('1/1/' + CAST(DATEPART(YEAR, @Date) AS varchar(10)) AS date))

	RETURN @Return

END

USE EMRIWarehouse
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION dbo.udfFirstDayLastMonth
(
	-- Add the parameters for the function here
	@Date datetime
)
RETURNS datetime
AS
BEGIN
	
	DECLARE @Return datetime

	SET @Return = (SELECT DATEADD(MONTH, DATEDIFF(MONTH, 0, @Date)-1, 0))

	RETURN @Return

END
GO


USE [EMRIWarehouse]
GO
/****** Object:  UserDefinedFunction [dbo].[udfParseInsPolicy]    Script Date: 10/24/2017 8:53:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[udfParseInsPolicy] (@InsPolicy varchar(100))
	RETURNS varchar(100)
AS

BEGIN
DECLARE @Policy varchar(100)

IF @InsPolicy LIKE '%ADJ%'

		SET @Policy = (
						SELECT SUBSTRING(
											@InsPolicy 
											, 1
											, (PATINDEX('%ADJ%', @InsPolicy) - 1)
										)
						)

ELSE

	SET @Policy = @InsPolicy
	SET @Policy = (SELECT REPLACE(@Policy, CHAR(01), ''))

RETURN @Policy;
END;
GO

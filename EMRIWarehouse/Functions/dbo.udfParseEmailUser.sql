USE [EMRIWarehouse]
GO

/****** Object:  UserDefinedFunction [dbo].[udfParseEmailUser]    Script Date: 9/27/2019 3:01:24 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE FUNCTION dbo.udfParseEmailUser (@email varchar(255))
	RETURNS varchar(255)
AS

BEGIN
DECLARE @return varchar(255)

	SET @return = (SELECT SUBSTRING(@email, 1, CHARINDEX('@', @email) - 1))
	

RETURN @return;
END;

GO



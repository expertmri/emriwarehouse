USE EMRIWarehouse
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION dbo.udfEndDayLastMonth
(
	-- Add the parameters for the function here
	@Date datetime
)
RETURNS datetime
AS
BEGIN
	
	DECLARE @Return datetime

	SET @Return = (SELECT DATEADD(MONTH, DATEDIFF(MONTH, -1, @Date)-1, -1))

	RETURN @Return

END
GO


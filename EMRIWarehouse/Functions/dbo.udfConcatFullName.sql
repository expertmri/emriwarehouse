USE [EMRIWarehouse]
GO

/****** Object:  UserDefinedFunction [dbo].[udfConcatFullName]    Script Date: 4/9/2018 3:01:24 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE FUNCTION dbo.udfConcatFullName (@FirstName varchar(255), @MiddleInitial varchar(255), @LastName varchar(255), @Suffix varchar(255))
	RETURNS varchar(100)
AS

BEGIN
DECLARE @FullName varchar(255)

	SET @FullName = (
						SELECT	COALESCE(@FirstName + ' ','') 
								+ COALESCE(@MiddleInitial + ' ', '')
								+ COALESCE(@LastName + ' ', '')
								+ COALESCE(@Suffix, '')
					)
	SET @FullName = LTRIM(RTRIM(@FullName))

RETURN @FullName;
END;

GO



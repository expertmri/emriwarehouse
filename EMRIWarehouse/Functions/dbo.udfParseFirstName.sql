USE [EMRIWarehouse]
GO
/****** Object:  UserDefinedFunction [dbo].[udfParseFirstName]    Script Date: 10/24/2017 8:53:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[udfParseFirstName] (@Name varchar(255))
	RETURNS varchar(100)
AS

BEGIN
DECLARE @FirstName varchar(255)

	SET @FirstName = (
					SELECT SUBSTRING(
										@Name 
										, CHARINDEX(',', @Name) + 1
										, LEN(@Name) - CHARINDEX(',', @Name)
										
									)
					)
	SET @FirstName = LTRIM(RTRIM(@FirstName))

RETURN @FirstName;
END;

GO

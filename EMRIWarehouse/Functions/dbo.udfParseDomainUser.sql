USE [EMRIWarehouse]
GO

/****** Object:  UserDefinedFunction [dbo].[udfParseDomainUser]    Script Date: 9/27/2019 3:01:24 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE FUNCTION dbo.udfParseDomainUser (@user varchar(255))
	RETURNS varchar(255)
AS

BEGIN
DECLARE @return varchar(255)

	SET @return = (SELECT SUBSTRING(@user, CHARINDEX('\', @user) + 1, LEN(@user)))
	

RETURN @return;
END;

GO



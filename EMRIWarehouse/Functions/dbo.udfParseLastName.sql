USE [EMRIWarehouse]
GO
/****** Object:  UserDefinedFunction [dbo].[udfParseLastName]    Script Date: 10/24/2017 8:53:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[udfParseLastName] (@Name varchar(255))
	RETURNS varchar(100)
AS

BEGIN
DECLARE @LastName varchar(255)

	SET @LastName = (
					SELECT SUBSTRING(
										@Name 
										, 1
										, (CHARINDEX(',', @Name) - 1)
									)
					)

	SET @LastName = LTRIM(RTRIM(@LastName))
	
RETURN @LastName;
END;

GO

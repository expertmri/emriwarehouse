USE EMRIWarehouse
GO

ALTER PROCEDURE Rpt.SSRSCompletedByMLCDoctor
@Range varchar(20) = null

AS

DECLARE @StartDate date
		, @FirstOfMonth date
		, @EndDate date
		, @YearStartDate date

SET @YearStartDate = (SELECT CAST('1/1/' + CAST(DATEPART(YEAR, GETDATE()) AS varchar(10)) AS date)) 

SELECT @FirstOfMonth = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)
	   , @EndDate = DATEADD(DAY, -1, @FirstOfMonth)
	   , @StartDate = DATEADD(MONTH, -3, @FirstOfMonth)

;WITH AvgCTE AS
	(
		SELECT  apt.ReferringDr
				, CAST(COUNT (CASE WHEN DOS BETWEEN @StartDate AND @EndDate THEN apt.ApptId ELSE NULL END) as decimal)/DATEDIFF(DAY, @StartDate, @EndDate) AS QtrAvg
				, CAST(COUNT(CASE WHEN DOS >= @FirstOfMonth THEN apt.ApptId ELSE NULL END) AS decimal)/ DATEDIFF(DAY, @FirstOfMonth , CAST(GETDATE() AS date)) AS MthAvg
		FROM dbo.Appointments apt
		JOIN XRef.AppointmentStatus stat ON apt.AppointmentStatusId = stat.AppointmentStatusId
		JOIN XRef.MLCDoctor mlc ON mlc.ADSReferringPhysicianCode = apt.ReferralCode
		LEFT JOIN XRef.ARClassGroup ar ON apt.ARClass = ar.ARClass
		WHERE apt.Modality = 'MRI'
			  AND apt.AppointmentStatusId = 4 --Exam Complete
			  AND ar.IsNonRevenue = 0
		GROUP BY ReferringDr
	)

, AllData_CTE AS
	(	
		SELECT	apt.ReferringDr
				, dr.Address1 
					+ RTRIM(LTRIM(+ ' ' + COALESCE(dr.Address2,''))) 
					+ ' ' + REPLACE( dr.City, ' ', '') 
					+ ' ' + dr.State 
					+ ' ' + dr.ZipCode  AS DrAddress
				, DATEADD(MONTH, DATEDIFF(MONTH, 0, apt.DOS), 0) AS MnthNmbr
				, dbo.udfGetMonthName(apt.DOS) AS DOS
				, COUNT(apt.ApptId) AS Completed
				, CASE WHEN cte.QtrAvg = 0 THEN 'NEW'
					   WHEN cte.MthAvg >= cte.QtrAvg THEN 'GOOD'
					   WHEN cte.MthAvg < cte.QtrAvg THEN 'DROPPED'
				  END AS AccountStatus
				, CASE WHEN cte.MthAvg < cte.QtrAvg THEN CEILING(((cte.QtrAvg - cte.MthAvg)/cte.QtrAvg) * 100)
				  END AS PercentDrop
		FROM dbo.Appointments apt
		JOIN Integration.Stg.ReferringPhysicians dr ON dr.Code = apt.ReferralCode
		JOIN XRef.MLCDoctor mlc ON mlc.ADSReferringPhysicianCode = apt.ReferralCode
		JOIN XRef.AppointmentStatus stat ON apt.AppointmentStatusId = stat.AppointmentStatusId
		LEFT JOIN XRef.ARClassGroup ar ON apt.ARClass = ar.ARClass
		LEFT JOIN AvgCTE cte ON apt.ReferringDr = cte.ReferringDr
		WHERE apt.Modality = 'MRI'
			  AND apt.AppointmentStatusId = 4 --Exam Complete
			  AND ar.IsNonRevenue = 0
			  AND apt.DOS >= @YearStartDate
		GROUP BY apt.ReferringDr
				, dr.Address1 
					+ RTRIM(LTRIM(+ ' ' + COALESCE(dr.Address2,''))) 
					+ ' ' + REPLACE( dr.City, ' ', '') 
					+ ' ' + dr.State 
					+ ' ' + dr.ZipCode
				, DATEADD(MONTH, DATEDIFF(MONTH, 0, apt.DOS), 0)
				, dbo.udfGetMonthName(apt.DOS)
				, cte.QtrAvg
				, cte.MthAvg
		)

, FinalData AS 
	(
		SELECT	tt.ReferringDr
				, tt.DrAddress
				, tt.MnthNmbr
				, tt.DOS
				, tt.Completed
				, tt.AccountStatus
				, CASE WHEN tt.PercentDrop > 100 THEN 100 ELSE tt.PercentDrop END AS PercentDrop
				, CASE WHEN tt.AccountStatus = 'DROPPED' THEN
						CASE WHEN	tt.PercentDrop <= 10 THEN '0-10'
							 WHEN tt.PercentDrop BETWEEN 11 AND 20 THEN '11-20'
							 WHEN tt.PercentDrop BETWEEN 21 AND 40 THEN '21-40'
							 WHEN tt.PercentDrop BETWEEN 41 AND 60 THEN '41-60'
							 WHEN tt.PercentDrop BETWEEN 61 AND 80 THEN '61-80'
							 WHEN tt.PercentDrop BETWEEN 81 AND 99 THEN '81-99'
							 ELSE '100' END
				END AS DropRange
		FROM AllData_CTE tt
	)

SELECT	ReferringDr
		, DrAddress
		, MnthNmbr
		, DOS
		, Completed
		, AccountStatus
		, PercentDrop
		, DropRange
FROM FinalData
WHERE @Range IS NULL OR DropRange = @Range

GO

GRANT EXECUTE ON Rpt.SSRSCompletedByMLCDoctor TO PUBLIC;

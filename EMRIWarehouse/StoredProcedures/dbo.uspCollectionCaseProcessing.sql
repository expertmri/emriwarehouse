USE EMRIWarehouse
GO

ALTER PROCEDURE dbo.uspCollectionCaseProcessing
	@DEBUG int = 0
AS

SET NOCOUNT ON;

DECLARE @i int = 1
		, @dayInterval int
		, @runningInterval int = 90

CREATE TABLE #steps (StepId int, EventName varchar(30), CalcDate date)

WHILE @i < 9
BEGIN

	IF @i in (1,6,7,8)
	SET @dayInterval = 30

	IF @i in (2,3,4)
	SET @dayInterval = 90

	IF @i = 5
	SET @dayInterval = 60

	IF @i in (1,2)
	BEGIN
		INSERT INTO #steps (StepId, EventName, CalcDate)
		SELECT	@i
				, CASE WHEN @i < 3 THEN CAST(@dayInterval as varchar) + ' days from DOS'
					   ELSE CAST(@dayInterval as varchar) + ' days from Step ' + CAST((@i - 1) as varchar)
				  END
				, DATEADD(DAY, - @dayInterval, GETDATE())
	END

	IF @i > 2
	BEGIN
		SET @runningInterval = @runningInterval + @dayInterval

		INSERT INTO #steps (StepId, EventName, CalcDate)
		SELECT	@i
				, CASE WHEN @i < 3 THEN CAST(@dayInterval as varchar) + ' days from DOS'
					   ELSE CAST(@dayInterval as varchar) + ' days from Step ' + CAST((@i - 1) as varchar)
				  END
				, DATEADD(DAY, - @runningInterval, GETDATE())
	END

	SET @i = @i + 1
END

IF @DEBUG = 1
SELECT * FROM #steps

IF @DEBUG = 0
BEGIN
	SELECT	cc.Account
		   , MIN(a.DOS) AS DOS
		   , MAX(cc.LastUpdated) AS LastUpdated
	INTO #accountDOS
	FROM dbo.CollectionCases cc
	LEFT JOIN dbo.Appointments a ON cc.Account = a.Account
	GROUP BY cc.Account

	INSERT INTO dbo.CollectionCaseProcessing (
		EventName
		, Account
		, PatientName
		, PayorName
		, PayorPhone
		, CaseManager
		, CaseMangerEmail
		, DOS
		, DateOfLoss
		, TotalMedicals
		, LastUpdated
	)

	SELECT DISTINCT s.EventName
			, cc.Account
			, p.PatName AS PatientName
			, ins.Name AS PayorName
			, ins.Phone AS PayorPhone
			, cc.CaseManager
			, cc.CaseMangerEmail
			, dates.DOS
			, cc.DateOfLoss
			, cc.TotalMedicals
			, dates.LastUpdated
	FROM #accountDOS dates
	JOIN dbo.CollectionCases cc ON dates.Account = cc.Account AND dates.LastUpdated = cc.LastUpdated
	JOIN Integration.Stg.AppointmentsList p ON dates.Account = p.Account
	JOIN Integration.Stg.Insurances ins ON cc.Payor = ins.Code
	JOIN #steps s ON s.CalcDate = dates.DOS
END

USE EMRIWarehouse
GO

ALTER PROCEDURE dbo.uspCollectionCases
AS

SET NOCOUNT ON

BEGIN TRAN

	TRUNCATE TABLE dbo.CollectionCases

	INSERT INTO dbo.CollectionCases (
		Account
		, Payor
		, PISettlementAmnt
		, CaseSettlementDate
		, CaseReason
		, TotalMedicals
		, ExpertLienAmount
		, LastUpdated
		, DateOfLoss
		, LienReductionOffer
		, Prorata
		, CaseStatus
		, ARClass
		, SignedLienOnFile
		, CaseManager
		, CaseMangerEmail
	)

	SELECT	Account
			, Field2
			, Field4/100
			, Field5
			, Field6
			, Field7
			, Field8/100
			, LEFT(Field9, 10)
			, Field10
			, Field11/100
			, Field15/100
			, CASE WHEN SUBSTRING(Field16, 1, 1) = '1' THEN 'IN LITIGATION'
				   WHEN SUBSTRING(Field16, 3, 1) = '1' THEN 'SETTLED'
				   WHEN SUBSTRING(Field16, 5, 1) = '1' THEN 'DROPPED'
				   WHEN SUBSTRING(Field16, 7, 1) = '1' THEN 'SETTLEMENT OFFER MADE'
				   WHEN SUBSTRING(Field16, 9, 1) = '1' THEN 'ONGOING PT STILL TREATING'
				   WHEN SUBSTRING(Field16, 11, 1) = '1' THEN 'DO NOT STATUS'
				   WHEN SUBSTRING(Field16, 13, 1) = '1' THEN 'LEFT VOICEMAIL'
				   WHEN SUBSTRING(Field16, 15, 1) = '1' THEN 'PRE-LITIGATION'
			  END AS CaseStatus
			, Field18
			, CASE WHEN SUBSTRING(Field20, 1, 1) = '1' THEN 'Yes'
				   WHEN	SUBSTRING(Field20, 3, 1) = '1' THEN 'No'
			  END AS SignedLienOnFile
			, Field27
			, Field29
	FROM Integration.Stg.CollectionCase

COMMIT


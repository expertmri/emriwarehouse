USE EMRIWarehouse;
GO

/******************************************************************************************************
8/17/2018 slundy	User doesn't really know what they want just yet so leaving the flexibility
					of requesting different dateblocks by parameter for now. SSRS already is hooked
					in to use it so simplifies future requests.

*******************************************************************************************************/

ALTER PROCEDURE Rpt.SSRSRTAWorkflowStageAnalysis
	@ReportSection int = 2
AS

DECLARE @cutOffDate datetime 
		, @yesterday datetime
		, @lastWeek  datetime
		, @lastMonth datetime
		, @LatestStudy date

SET	@cutOffDate = (SELECT CAST(CAST(GETDATE() AS date) AS varchar) + ' 07:00')
SET @yesterday = (SELECT DATEADD(DAY, -1, @cutOffDate))
SET @lastWeek = (SELECT DATEADD(WEEK, -1, @cutOffDate))
SET @lastMonth = (SELECT DATEADD(MONTH, -1, @cutOffDate))
SET @LatestStudy = (SELECT MAX(TimeInFInal) FROM EMRIWarehouse.dbo.RTAReportMetrics)

SELECT	DATEDIFF(HOUR, ReceivedDate, COALESCE(TimeInSubmitted, TimeInVerified, TimeInDictated, TimeInPreliminary, TimeInFinal)) AS RcdToSbmt
		, DATEDIFF(HOUR, TimeInSubmitted, COALESCE(TimeInVerified, TimeInDictated, TimeInPreliminary, TimeInFinal)) AS SbmtToVfy
		, DATEDIFF(HOUR, TimeInSubmitted, COALESCE(TimeInDictated, TimeInPreliminary, TimeInFinal)) AS SbmtToDictated
		, DATEDIFF(HOUR, TimeInVerified, COALESCE(TimeInDictated, TimeInPreliminary, TimeInFinal)) AS VfyToDict
		, DATEDIFF(HOUR, TimeInDictated, COALESCE(TimeInPreliminary, TimeInFinal)) AS DictToPrelim
		, DATEDIFF(HOUR, TimeInPreliminary, TimeInFinal) AS PrelimToFinal
		, TimeInFinal
		, RA
		, AccessionNumber
INTO #minutes
FROM EMRIWarehouse.dbo.RTAReportMetrics 
WHERE TimeInFinal >= @lastWeek AND TimeInFinal <= @cutOffDate


--IF @ReportSection = 1
--BEGIN
--	SELECT	AVG(RcdToSbmt) AS RcdToSbmt
--			, AVG(SbmtToVfy)	AS SbmtToVfy
--			, AVG(VfyToDict)	AS VfyToDict
--			, AVG(DictToPrelim) AS DictToPrelim
--			, AVG(PrelimToFinal)	AS PrelimToFinal
--			, RA
--			, COUNT(AccessionNumber) AS TotalExams
--			, CAST(TimeInFinal AS date) AS TimeInFinal
--	FROM #minutes
--	WHERE TimeInFinal > @yesterday AND TimeInFinal <= @cutOffDate
--	GROUP BY RA
--			 , CAST(TimeInFinal AS date) 

--END

IF @ReportSection = 2
BEGIN
	--SELECT	AVG(RcdToSbmt) AS RcdToSbmt
	--		, AVG(SbmtToVfy)	AS SbmtToVfy
	--		, AVG(VfyToDict)	AS VfyToDict
	--		, AVG(DictToPrelim) AS DictToPrelim
	--		, AVG(PrelimToFinal)	AS PrelimToFinal
	--		, RA
	--		, COUNT(AccessionNumber) AS TotalExams
	--		, MIN(CAST(TimeInFinal AS date)) AS StartDate
	--		, MAX(CAST(TimeInFinal AS date)) AS EndDate
	--FROM #minutes
	--WHERE TimeInFinal > @lastWeek AND TimeInFinal <= @cutOffDate
	--GROUP BY RA

	
	SELECT CASE WHEN SbmtToDictated <= 24 THEN '0-24'
				WHEN SbmtToDictated BETWEEN 25 AND 36 THEN '25-36'
				WHEN SbmtToDictated BETWEEN 37 AND 48 THEN '37-48'
				WHEN SbmtToDictated > 48 THEN '+48'
				ELSE NULL
			END AS SbmtToDictated
			, CASE WHEN DictToPrelim <= 24 THEN '0-24'
				   WHEN DictToPrelim BETWEEN 25 AND 36 THEN '25-36'
				   WHEN DictToPrelim BETWEEN 37 AND 48 THEN '37-48'
				   WHEN DictToPrelim > 48 THEN '+48'
				   ELSE NULL
			END AS DictToPrelim
			, CASE WHEN PrelimToFinal <= 24 THEN '0-24'
				   WHEN PrelimToFinal BETWEEN 25 AND 36 THEN '25-36'
				   WHEN PrelimToFinal BETWEEN 37 AND 48 THEN '37-48'
				   WHEN PrelimToFinal > 48 THEN '+48'
				   ELSE NULL
			END AS PrelimToFinal
			--, CASE WHEN RcdToSbmt <= 24 THEN '0-24'
			--	WHEN RcdToSbmt BETWEEN 25 AND 36 THEN '25-36'
			--	WHEN RcdToSbmt BETWEEN 37 AND 48 THEN '37-48'
			--	WHEN RcdToSbmt > 48 THEN '+48'
			--END AS RcdToSbmt
			, RA
			, CAST(@lastWeek AS date) AS StartDate
			, CAST(@cutOffDate AS date) AS EndDate
			, AccessionNumber
			, @LatestStudy as LatestStudy
	FROM #minutes
	WHERE TimeInFinal >= @lastWeek AND TimeInFinal <= @cutOffDate

END

--IF @ReportSection = 3
--BEGIN
--	SELECT	AVG(RcdToSbmt) AS RcdToSbmt
--			, AVG(SbmtToVfy)	AS SbmtToVfy
--			, AVG(VfyToDict)	AS VfyToDict
--			, AVG(DictToPrelim) AS DictToPrelim
--			, AVG(PrelimToFinal)	AS PrelimToFinal
--			, RA
--			, COUNT(AccessionNumber) AS TotalExams
--			, MIN(CAST(TimeInFinal AS date)) AS StartDate
--			, MAX(CAST(TimeInFinal AS date)) AS EndDate
--	FROM #minutes
--	WHERE TimeInFinal > @lastMonth AND TimeInFinal <= @cutOffDate
--	GROUP BY RA

--END

GO

GRANT EXECUTE ON Rpt.SSRSRTAWorkflowStageAnalysis TO PUBLIC;
USE [EMRIWarehouse]
GO
/****** Object:  StoredProcedure [Rpt].[uspGetAccountZones]    Script Date: 10/10/2019 15:07:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE Rpt.uspGetAccountZones

AS

SET NOCOUNT ON

SELECT ZoneRegion
FROM (
		SELECT DISTINCT ZoneRegion
		FROM SF.Accounts
		--UNION ALL
		--SELECT 'ALL' AS ZoneRegion
	  )tt
ORDER BY CASE WHEN ZoneRegion <> 'Unassigned' THEN 0 ELSE 1 END
		 , ZoneRegion



GO	

GRANT EXECUTE ON Rpt.uspGetAccountZones TO [public] 

GO



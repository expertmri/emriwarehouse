USE [EMRIWarehouse]
GO
/****** Object:  StoredProcedure [Rpt].[SSRSCommissionReport]    Script Date: 10/11/2019 10:34:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [Rpt].[SSRSCommissionReport_V3] 
--'SATURN\scott.lundy', '8/1/2019'
@UserName varchar(255)
, @StartDate date


AS
-- just for testing
DECLARE @StartDate date = '11/1/2019'

DECLARE @EndDate date
		, @LastQtrStartDate date
		, @LastQtrEndDate date

SET @EndDate = DATEADD(DAY, -1, DATEADD(MONTH, 3, @StartDate))
SET @LastQtrStartDate = DATEADD(MONTH, -3, @StartDate)
SET @LastQtrEndDate = DATEADD(DAY, -1, @StartDate)

--SELECT @StartDate, @EndDate, @LastQtrStartDate, @LastQtrEndDate

DECLARE @AccountsReq int = 15
DECLARE @ScansReqQtr int = 3
DECLARE @ScansReqMonth int = 3
DECLARE @ActiveVisits float = .80
DECLARE @WeeklyVisits int = 40
DECLARE @LeadPayout int = 20

/****** REMOVED 1/13/2020 EM-606 ******
DECLARE @OfficePresentations int = 6
**************************************/

;WITH accounts AS (
		SELECT	m.SF_AccountId
				, a.AccountName
				, a.SalesRepresentative
				, m.AccountStatusDetailed
				, wf.ScanCountMonthly
				, wf.MonthYear AS MonthDate
				, 1 AS isStatusChanged
				, m.MonthDate AS StatusChangedDate
		FROM ScansByAccountWorkflow wf 
		JOIN SF.Accounts a ON wf.SF_AccountId = a.SF_AccountId
		JOIN SF.AccountsByMonth m ON wf.SF_AccountId = m.SF_AccountId
		WHERE m.AccountStatusDetailed IN ('Restart', 'New' )
			  AND wf.MonthYear BETWEEN @StartDate AND @EndDate
			  AND wf.MonthYear = m.MonthDate
			  --AND wf.SF_AccountId = '001f4000019Mu8ZAAS'
	)
, lastScan AS (
		SELECT MIN(DOS) AS lastScanDate
			   , s.SF_AccountId
		FROM dbo.Scans s 
		JOIN accounts a ON a.SF_AccountId = s.SF_AccountId
		WHERE DOS > a.MonthDate
		GROUP BY s.SF_AccountId
	)
, completedTask AS ( 
	SELECT	u.SF_AccountId
			, u.CompletedBy
			, MAX(completedDate) AS LastCompletedDate 
	FROM SF.UserTaskProductivity u
	JOIN lastScan l ON l.SF_AccountId = u.SF_AccountId
	WHERE TaskStatus = 'Completed'
		  AND u.CompletedDate < l.lastScanDate
	GROUP BY u.SF_AccountId
			 , u.CompletedBy
	)

SELECT	a.SF_AccountId
		, a.AccountName
		, a.SalesRepresentative
		, a.AccountStatusDetailed
		, a.ScanCountMonthly
		, a.MonthDate
		, c.LastCompletedDate
		, lastScanDate AS StatusChangedDate
		, 1 as isStatusChanged
INTO #accounts
FROM accounts a
LEFT JOIN completedTask c ON a.SF_AccountId = c.SF_AccountId AND a.SalesRepresentative = c.CompletedBy
JOIN lastScan l ON a.SF_AccountId = l.SF_AccountId

INSERT INTO #accounts
SELECT	wf.SF_AccountId
		, a.AccountName
		, a.SalesRepresentative
		, a.AccountStatusDetailed
		, wf.ScanCountMonthly
		, wf.MonthYear AS MonthDate
		, a.LastCompletedDate
		, a.StatusChangedDate
		, 0 AS isStatusChanged
FROM ScansByAccountWorkflow wf 
LEFT JOIN #accounts a ON wf.SF_AccountId = a.SF_AccountId 
WHERE wf.MonthYear > a.MonthDate 
	  AND wf.MonthYear <= @EndDate

SELECT * FROM #accounts



/********************** TERRITORY GROWTH *************************/
--DECLARE @StartDate date = '11/1/2019'
--DECLARE @EndDate date
--		, @LastQtrStartDate date
--		, @LastQtrEndDate date
--SET @EndDate = DATEADD(DAY, -1, DATEADD(MONTH, 3, @StartDate))
--SET @LastQtrStartDate = DATEADD(MONTH, -3, @StartDate)
--SET @LastQtrEndDate = DATEADD(DAY, -1, @StartDate)

;WITH currentQtr AS (
	SELECT	SUM(ScanCountMonthly) AS ScanTotal
			, a.SalesRepresentative
			, a.ZoneRegion
	FROM ScansByAccountWorkflow s
	JOIN SF.Accounts a ON s.SF_AccountId = a.SF_AccountId
	--WHERE s.MonthYear BETWEEN '10/1/2019' AND '12/31/2019'
	WHERE s.MonthYear BETWEEN @StartDate AND @EndDate
	GROUP BY a.SalesRepresentative
			 , a.ZoneRegion
)
, lastQtr AS (
	SELECT	SUM(ScanCountMonthly) AS ScanTotal
			, a.SalesRepresentative
			, a.ZoneRegion
	FROM ScansByAccountWorkflow s
	JOIN SF.Accounts a ON s.SF_AccountId = a.SF_AccountId
	--WHERE s.MonthYear BETWEEN '8/1/2019' AND '10/31/2019'
	WHERE s.MonthYear BETWEEN @LastQtrStartDate AND @LastQtrEndDate
	GROUP BY a.SalesRepresentative
			 , a.ZoneRegion
)
, baseData AS (
	SELECT SUM(c.ScanTotal) AS currentScans
			, SUM(l.ScanTotal) AS lastScans
			, SUM(t.BaseScans) AS totalBase
			, c.SalesRepresentative
	FROM currentQtr c
	JOIN lastQtr l ON c.SalesRepresentative = l.SalesRepresentative
	JOIN XRef.TerritoryScans t ON t.ZoneRegion = c.ZoneRegion
	GROUP BY c.SalesRepresentative
)
, finalTerritory AS (
	SELECT	b.SalesRepresentative
			, CASE WHEN b.currentScans >= b.totalBase AND b.currentScans > b.lastScans THEN CAST(((CAST(b.currentScans AS float) - CAST(b.lastScans AS float))/CAST(b.lastScans AS float)) AS float)
				ELSE '0'
			END AS percentGrowth
	FROM baseData b
)
SELECT f.SalesRepresentative
	   , f.percentGrowth
	   , CASE WHEN f.percentGrowth >= .05 AND f.percentGrowth < 10 THEN CEILING(f.percentGrowth* 100) * 100
			  WHEN f.percentGrowth >= .10 AND f.percentGrowth < 15 THEN CEILING(f.percentGrowth* 100) * 105
			  WHEN f.percentGrowth >= .15 AND f.percentGrowth < 20 THEN CEILING(f.percentGrowth* 100) * 110
			  WHEN f.percentGrowth >= .20 AND f.percentGrowth < 25 THEN CEILING(f.percentGrowth* 100) * 115
			  WHEN f.percentGrowth >= .25 AND f.percentGrowth < 30 THEN CEILING(f.percentGrowth* 100) * 120
			  WHEN f.percentGrowth > .30 THEN CEILING(f.percentGrowth * 100) * 162
			  ELSE 0 END AS territoryCommission
INTO #territoryGrowth
FROM finalTerritory f

/********************** END TERRITORY GROWTH *************************/


/********************** LEAD CONVERSION *************************/
--DECLARE @StartDate date = '11/1/2019'
--DECLARE @EndDate date
--DECLARE @LeadPayout int = 20
--SET @EndDate = DATEADD(DAY, -1, DATEADD(MONTH, 3, @StartDate))
--drop table #leadConv
--drop table #leadCommission


CREATE TABLE #leadConv (SF_AccountId varchar(255), AccountName varchar(255), AccountAddress varchar(255), ConvDate date, FirstReferral date, ConvTime int, CreatedBy varchar(255))

INSERT INTO #leadConv
EXEC Rpt.SSRSLeadConversion @StartDate

-- we only want leads converted during the Qtr and the First Exam must be no more than one day before the lead is entered
DELETE FROM #leadConv
WHERE ConvDate NOT BETWEEN @StartDate AND @EndDate
	  OR ConvTime < -1

--SELECT a.SalesRepresentative
--	   , COUNT(l.SF_AccountId) AS TotalAccounts
--	   , COUNT(l.SF_AccountId) * @LeadPayout AS LeadCommission
--INTO #leadCommission
--FROM #leadConv l
--JOIN SF.Accounts a ON l.SF_AccountId = a.SF_AccountId
--GROUP BY a.SalesRepresentative

SELECT a.SalesRepresentative
	   , l.SF_AccountId
	   , 'Yes' AS IsLeadCommission
	   , lc.TotalLeadsConv
	   --, dbo.udfFirstDayOfMonth(l.ConvDate) AS MonthDate
INTO #leadCommission
FROM #leadConv l
JOIN SF.Accounts a ON l.CreatedBy = a.SalesRepresentative
JOIN (
		SELECT	COUNT(l.SF_AccountId) AS TotalLeadsConv 
				, a.SalesRepresentative
		FROM #leadConv l
		JOIN SF.Accounts a ON l.SF_AccountId = a.SF_AccountId
		GROUP BY a.SalesRepresentative
	) lc ON lc.SalesRepresentative = a.SalesRepresentative


/********************** END LEAD CONVERSION *************************/

/*********************** 80% ACTIVE ACCOUNTS VISITED*****************/
--DECLARE @StartDate date = '11/1/2019'
--DECLARE @EndDate date
----drop table #activeAcct
--SET @EndDate = DATEADD(DAY, -1, DATEADD(MONTH, 3, @StartDate))

--;WITH activeAcct AS (
	SELECT m.SF_AccountId
		   , a.SalesRepresentative
	INTO #activeAcct
	FROM SF.AccountsByMonth m
	JOIN SF.Accounts a ON m.SF_AccountId = a.SF_AccountId
	WHERE m.MonthDate = @StartDate
		  AND m.IsActive = 1
--)
;WITH visitedAcct AS (
	SELECT	u.SF_AccountId
			, u.CompletedBy
			, MIN(completedDate) AS visitDate
	FROM SF.UserTaskProductivity u
	WHERE u.TaskStatus = 'Completed'
		  AND u.CompletedDate BETWEEN @StartDate AND @EndDate
	GROUP BY u.SF_AccountId
			, u.CompletedBy
)
, activeVists AS (
	SELECT a.SF_AccountId
		   , a.SalesRepresentative
		   , v.visitDate
		   , CASE WHEN v.visitDate IS NOT NULL THEN 1 ELSE 0 END AS isVisited
	FROM #activeAcct a
	LEFT JOIN visitedAcct v ON a.SF_AccountId = v.SF_AccountId
)
, totalActive AS (
	SELECT	COUNT(a.SF_AccountId) AS totalAccounts
			, a.SalesRepresentative
	FROM #activeAcct a
	GROUP BY a.SalesRepresentative
)

SELECT	v.SalesRepresentative
		, SUM(v.isVisited) AS TotalActiveVisit
		, CAST(SUM(v.isVisited) AS float) / CAST(COUNT(t.totalAccounts) AS float) AS PercentActiveVisited
		--, v.SF_AccountId
	   --, t.totalAccounts
	   --, dbo.udfFirstDayOfMonth(v.visitDate) AS MonthDate
	   --, v2.totalVisited AS TotalActiveVisit
	   --, v2.percentVisited
INTO #activeVisits
FROM activeVists v
JOIN totalActive t ON t.SalesRepresentative = v.SalesRepresentative
GROUP BY v.SalesRepresentative
--JOIN (
--		SELECT	SUM(v.isVisited) AS totalVisited
--				, CAST(SUM(v.isVisited) AS float) / CAST(COUNT(t.totalAccounts) AS float) AS percentVisited
--				, v.SalesRepresentative
--		FROM activeVists v
--		JOIN totalActive t ON t.SalesRepresentative = v.SalesRepresentative
--		GROUP BY v.SalesRepresentative
--	) v2 ON v.SalesRepresentative = v2.SalesRepresentative
--GROUP BY v.SF_AccountId
--		 , v.visitDate

/*********************** END 80% ACTIVE ACCOUNTS VISITED*************/

/*********************** 40 UNIQUE ACCOUNTS VISITED WEEKLY *************/
--DECLARE @StartDate date = '11/1/2019'
--DECLARE @EndDate date
--DECLARE @WeeklyVisits int = 40
--SET @EndDate = DATEADD(DAY, -1, DATEADD(MONTH, 3, @StartDate))

DECLARE @StartDOW int
		, @EndDOW int
		, @StartProrated int
		, @EndProrated int
		, @TotalVisits int
		, @StartWeekNo int
		, @EndWeekNo int

SET @StartDOW = DATEPART(dw, @StartDate)
SET @EndDOW = DATEPART(dw, @EndDate)
SET @StartWeekNo = DATEPART(wk, @StartDate)
SET @EndWeekNo = DATEPART(wk, @EndDate)

-- get how many days left in the week for partial weeks and prorate the @weekly visits for those weeks
SET @StartProrated = (SELECT CASE WHEN @StartDOW = 1 THEN @WeeklyVisits ELSE ((8 - CAST(DATEPART(dw, @StartDate) AS float)) / 7) * @WeeklyVisits END)
SET @EndProrated = (SELECT CASE WHEN @EndDOW = 7 THEN @WeeklyVisits ELSE (CAST(DATEPART(dw, @StartDate) AS float) / 7) * @WeeklyVisits END)
SET @TotalVisits = (SELECT ((DATEDIFF(wk, @StartDate, @EndDate) - 2) * @WeeklyVisits) + @StartProrated + @EndProrated)

--SELECT @StartProrated, @EndProrated, @TotalVisits

;WITH visitedAcct AS (
	SELECT DISTINCT u.SF_AccountId
					, u.CompletedBy AS SalesRepresentative
					, MIN(completedDate) AS visitDate
					--, completedDate AS visitDate
					, DATEPART(wk, u.CompletedDate) AS WeekCompleted
	FROM SF.UserTaskProductivity u
	WHERE u.TaskStatus = 'Completed'
		  AND u.CompletedDate BETWEEN @StartDate AND @EndDate
	GROUP BY u.SF_AccountId
			, u.CompletedBy
			, DATEPART(wk, u.CompletedDate) 
)
SELECT	v.SalesRepresentative
		--, dbo.udfFirstDayOfMonth(v.visitDate) AS MonthDate
		--, v.SF_AccountId
		--, v2.visits AS TotalVisits
		, COUNT(v.SF_AccountId) AS UniqueVisitWeekly
		, CASE WHEN @TotalVisits <= COUNT(v.SF_AccountId) THEN 1 ELSE 0 END AS isCommission
		--, v.WeekCompleted
		--, CASE WHEN v.WeekCompleted = @StartWeekNo THEN COUNT(v.SF_AccountID) - @StartProrated
		--	   WHEN v.WeekCompleted = @EndWeekNo THEN COUNT(v.SF_AccountID) - @EndProrated
		--	   ELSE COUNT(v.SF_AccountID) - @WeeklyVisits
		--  END AS WeeklyVisitGoal --this is the net difference by week of total scans that week vs goal for the week
INTO #fortyWeeks
FROM visitedAcct v
GROUP BY v.SalesRepresentative


--JOIN (
--		SELECT	COUNT(v.SF_AccountID) AS visits
--				, v.SalesRepresentative
--		FROM visitedAcct v
--		GROUP BY v.SalesRepresentative
--	) v2 ON v.SalesRepresentative = v2.SalesRepresentative



--ORDER BY 1,2

/*********************** END 40 UNIQUE ACCOUNTS VISITED WEEKLY *************/

/******************** REMOVED SLUNDY 1/13/2020 EM-606********************/
--;WITH presentations AS (
--		SELECT	a.SalesRepresentative
--				--, AccountStatusDetailed
--				, COUNT(SF_AccountId) AS TotalOfficePresent
--		FROM Integration.SF.Events e 
--		JOIN SF.Accounts a ON a.SF_AccountId = e.AccountId
--		WHERE e.ActivityDate BETWEEN @StartDate AND @EndDate
--				AND Activity_Type__c = 'Office Presentation'
--		GROUP BY SalesRepresentative
--)
/***********************************************************************/

;WITH finalData AS (
		SELECT 	a.SF_AccountId
				, a.AccountName
				, a.SalesRepresentative
				, a.AccountStatusDetailed
				, a.ScanCountMonthly
				--, av.ActiveVisitMonthly
				, a.MonthDate
				, a.LastCompletedDate
				, a.isStatusChanged
				, a.StatusChangedDate
				, tg.percentGrowth AS TerritoryGrowthRate
				, av.PercentActiveVisited AS PercentVisitedQtr
				, fw.UniqueVisitWeekly
				, lc.TotalLeadsConv
				, tg.territoryCommission AS TerritoryCommissionAmnt
				, COALESCE(lc.IsLeadCommission, 'No') AS IsLeadCommission
				, CASE WHEN ts.TotalScans >= @ScansReqQtr OR ms.MaxMonthlyScans >= @ScansReqMonth  THEN 'Yes'
						ELSE 'No' END AS IsScanCommission
				, COALESCE(tsk.TaskCommission, 'No') AS IsTaskCommission
				, CASE WHEN ta.TotalAccounts >= @AccountsReq THEN 'Yes' ELSE 'No' END AS IsAccountsCommission
				, CASE WHEN av.PercentActiveVisited >= @ActiveVisits THEN 'Yes' ELSE 'No' END AS IsActiveVisitCommission
				, CASE WHEN @TotalVisits <= fw.UniqueVisitWeekly THEN 'Yes' ELSE 'No' END AS IsUniqueVisitCommission
				, CASE WHEN (ts.TotalScans >= @ScansReqQtr OR ms.MaxMonthlyScans >= @ScansReqMonth) 
								AND COALESCE(tsk.TaskCommission, 'No') = 'Yes' 
								AND ta.TotalAccounts >= @AccountsReq
								AND av.PercentActiveVisited >= @ActiveVisits
								AND @TotalVisits <= fw.UniqueVisitWeekly
							THEN 'Yes' 
							ELSE 'No' END AS IsPayCommission
		FROM #accounts a
		JOIN (
				SELECT	SF_AccountId
						, MAX(ScanCountMonthly) AS MaxMonthlyScans
				FROM #accounts
				GROUP BY SF_AccountId
			) ms ON a.SF_AccountId = ms.SF_AccountId
		JOIN (
				SELECT	SF_AccountId
						, SUM(ScanCountMonthly) AS TotalScans
				FROM #accounts
				GROUP BY SF_AccountId
			) ts ON a.SF_AccountId = ts.SF_AccountId
		LEFT JOIN	(
						SELECT	SF_AccountId
								, 'Yes' AS TaskCommission
						FROM #accounts a
						WHERE isStatusChanged = 1 
								AND a.StatusChangedDate <= DATEADD(DAY, 120, a.LastCompletedDate)
					) tsk ON a.SF_AccountId = tsk.SF_AccountId
		JOIN (
				SELECT	SalesRepresentative
						, AccountStatusDetailed
						, COUNT(SF_AccountId) AS TotalAccounts
				FROM #accounts
				GROUP BY SalesRepresentative
							, AccountStatusDetailed
			) ta ON a.SalesRepresentative = ta.SalesRepresentative AND ta.AccountStatusDetailed = a.AccountStatusDetailed
		LEFT JOIN #territoryGrowth tg ON a.SalesRepresentative = tg.SalesRepresentative
		LEFT JOIN #leadCommission lc ON a.SF_AccountId = lc.SF_AccountId
		LEFT JOIN #activeVisits av ON a.SalesRepresentative = av.SalesRepresentative
		LEFT JOIN #fortyWeeks fw ON a.SalesRepresentative = fw.SalesRepresentative
		--LEFT JOIN	(
		--				SELECT	lc2.SF_AccountId
		--						, lc2.MonthDate
		--						, 'Yes' AS IsLeadCommission
		--				FROM #leadCommission lc2
		--			) lc ON a.SF_AccountId = lc.SF_AccountId AND a.MonthDate = lc.MonthDate
		--LEFT JOIN (
		--				SELECT	av.SF_AccountId
		--						, av.MonthDate
		--						, av.percentVisited
		--						, av.TotalActiveVisit
		--						, COUNT(av.SF_AccountId) AS ActiveVisitMonthly
		--				FROM #activeVisits av
		--				GROUP BY av.SF_AccountId
		--						, av.MonthDate
		--						, av.percentVisited
		--						, av.TotalActiveVisit
		--			) av ON a.SF_AccountId = av.SF_AccountId AND a.MonthDate = av.MonthDate
		--LEFT JOIN (
		--				SELECT	fw.
		--						, 'Yes' AS UniqueVisit
		--				FROM #fortyWeeks fw 
		--			) fw ON a.SF_AccountId = fw.SF_AccountId AND a.MonthDate = fw.MonthDate
)

--using Min date as a join condition otherwise the data returns dollars for every month which sums up in the report and we just want one commission for an account, not monthly
SELECT	DISTINCT fd.SF_AccountId
				, fd.AccountName
				, fd.SalesRepresentative
				, fd.AccountStatusDetailed
				, fd.ScanCountMonthly
				, fd.UniqueVisitWeekly
				, @TotalVisits AS GoalUniqueVisits
				, fd.TotalLeadsConv
				, fd.MonthDate
				, fd.LastCompletedDate
				, fd.isStatusChanged
				, fd.StatusChangedDate
				, fd.TerritoryGrowthRate
				, fd.PercentVisitedQtr
				, fd.IsLeadCommission
				, fd.IsScanCommission
				, fd.IsTaskCommission
				, fd.IsAccountsCommission
				, fd.IsActiveVisitCommission
				, fd.IsUniqueVisitCommission
				, fd.IsPayCommission
				, com.AccountsCommissionAmnt 
				, CASE WHEN fd.IsPayCommission = 'Yes' THEN com.LeadCommissionAmnt ELSE 0 END AS LeadCommissionAmnt
				, CASE WHEN fd.IsPayCommission = 'Yes' THEN fd.TerritoryCommissionAmnt ELSE 0 END AS TerritoryCommissionAmnt
				
FROM finalData fd
JOIN dbo.SSRSUsers s ON (fd.SalesRepresentative = s.UserName  OR s.isAdmin = 1)
LEFT JOIN (
		SELECT SF_AccountId
				, MAX(CASE WHEN AccountStatusDetailed = 'Restart' AND IsPayCommission = 'Yes' THEN 50
					   WHEN AccountStatusDetailed = 'New' AND IsPayCommission = 'Yes' THEN 100
					   ELSE 0 END) AS AccountsCommissionAmnt
				, MAX(CASE WHEN fd.IsLeadCommission = 'Yes' THEN @LeadPayout ELSE 0 END) AS LeadCommissionAmnt
				, MAX(fd.IsLeadCommission) AS IsLeadCommission
				, MIN(MonthDate) AS MonthDate
		FROM finalData fd
		GROUP BY SF_AccountId
	) com ON fd.SF_AccountId = com.SF_AccountId AND fd.MonthDate = com.MonthDate
WHERE s.domainUser = @UserName 

GO

GRANT EXECUTE ON Rpt.SSRSCommissionReport_V3 TO PUBLIC;


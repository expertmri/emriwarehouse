USE EMRIWarehouse
GO

ALTER PROCEDURE dbo.uspExceptionGoalsByFacility
AS

DECLARE @CurrentDate date = (SELECT dbo.udfFirstDayOfMonth(GETDATE()))
		, @LastDate date = (SELECT MAX(MonthDate) FROM XRef.GoalsByFacility)

IF @CurrentDate >= @LastDate

BEGIN
	DECLARE @subject1 nvarchar(255)
		   , @EmailBody nvarchar(max)
		   , @emailTo nvarchar(MAX)

	SET @subject1 = 'New Facility Goals need to be added'
	SET @emailTo = 'scott.lundy@expertmri.com; saba.ejaz@expertmri.com;'
	SET @EmailBody = 'This is the last month that we have goals for the facilities. Please provide an updated spreadsheet so that next month does not display zero. You can find the required fields in EMRIWarehouse.XRef.GoalsByFacility.'	

	EXEC msdb.dbo.sp_send_dbmail  
		  @recipients = @emailTo
		, @subject = @subject1
		, @body_format = 'HTML'
		, @importance = 'High'
		, @body = @EmailBody
END
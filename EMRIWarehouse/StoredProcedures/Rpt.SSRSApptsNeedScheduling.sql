USE [EMRIWarehouse]
GO
/****** Object:  StoredProcedure [Rpt].[SSRSPendingToBeScheduled]    Script Date: 12/19/2017 8:50:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [Rpt].[SSRSApptsNeedScheduling]
AS

SELECT * 
FROM dbo.Appointments appt
JOIN XREF.AppointmentStatus stat ON appt.AppointmentStatusId = stat.AppointmentStatusId
WHERE appt.AppointmentListType in ('Appointment never scheduled', 'Appointment needs to be rescheduled')
		OR (appt.Facility = 'Intake' AND appt.AppointmentListType <> 'Cancelled')  --added per Saba 12/19/2017
		OR appt.AppointmentStatusId = 5 --No Show //--added per Saba 12/19/2017

GO
GRANT EXECUTE ON [Rpt].[SSRSApptsNeedScheduling] TO [public]


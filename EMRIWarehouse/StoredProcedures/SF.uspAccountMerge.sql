USE EMRIWarehouse
GO

ALTER PROCEDURE SF.uspAccountMerge
	@jsonRequest nvarchar(max)
AS

--DECLARE @jsonRequest nvarchar(max) = '{"SalesForceRequestToken":"7df855e1-d2aa-f577-3d77-f39f23735ccc","RequestAction":"AccountMerge","OriginalSFAccountId":"001f400000Xi6QlAAJ","MergedSFAccountId":"001f400000DNMmWAAX","SFUser":"Ian Peters"}'
--DECLARE @jsonRequest nvarchar(max) = '{"SalesForceRequestToken":"1234ABCD","RequestAction":"AccountMerge","OriginalSFAccountId":"001f40000081QaSAAU","MergedSFAccountId":"001f40000081QaTAAU","SFUser":"John Doe"}'

BEGIN TRY


	
	DECLARE @MergedAccountId varchar(255)
			, @OriginalAccountId varchar(255)
			, @SalesForceRequestToken varchar(255)
			, @SFUser varchar(255)
	
	SELECT	@SalesForceRequestToken = SalesForceRequestToken
			, @OriginalAccountId = OriginalSFAccountId
			, @MergedAccountId = MergedSFAccountId
			, @SFUser = SFUser
	FROM OPENJSON(@jsonRequest)
	WITH (
			SalesForceRequestToken  nvarchar(255) 'strict $.SalesForceRequestToken'
			, OriginalSFAccountId nvarchar(255) '$.OriginalSFAccountId'
			, MergedSFAccountId nvarchar(255) '$.MergedSFAccountId'
			, SFUser nvarchar(255) '$.SFUser' 
			) 
	
	--PERFORM CHECK FOR BOTH ACCOUNTS AND FAIL THE ROW
	IF NOT EXISTS (SELECT * FROM Integration.dbo.Accounts WHERE SF_AccountId = @OriginalAccountId)
	BEGIN
		UPDATE WebAPI.dbo.SalesForceRequest
		SET IsError =1
			, ErrorReason = 'SF_AccountId ' + @OriginalAccountId + ' is not in Integration.dbo.Accounts'
		WHERE SalesForceRequestToken = @SalesForceRequestToken
	END
			
	--IF NOT EXISTS (SELECT * FROM EMRIWarehouse.SF.Accounts WHERE SF_AccountId = @OriginalAccountId)
	--BEGIN
	--	UPDATE WebAPI.dbo.SalesForceRequest
	--	SET IsError =1
	--		, ErrorReason = 'SF_AccountId ' + @OriginalAccountId + ' is not in EMRIWarehouse.SF.Accounts'
	--	WHERE SalesForceRequestToken = @SalesForceRequestToken
	--END

	--IF NOT EXISTS (SELECT * FROM Integration.dbo.Accounts WHERE SF_AccountId = @MergedAccountId)
	--BEGIN
	--	UPDATE WebAPI.dbo.SalesForceRequest
	--	SET IsError =1
	--		, ErrorReason = 'SF_AccountId ' + @MergedAccountId + ' is not in Integration.dbo.Accounts'
	--	WHERE SalesForceRequestToken = @SalesForceRequestToken

	--	RETURN;
	--END

	--IF NOT EXISTS (SELECT * FROM EMRIWarehouse.SF.Accounts WHERE SF_AccountId = @MergedAccountId)
	--BEGIN
	--	UPDATE WebAPI.dbo.SalesForceRequest
	--	SET IsError =1
	--		, ErrorReason = 'SF_AccountId ' + @MergedAccountId + ' is not in EMRIWarehouse.SF.Accounts'
	--	WHERE SalesForceRequestToken = @SalesForceRequestToken

	--	RETURN;
	--END

	--MERGE DATA
	BEGIN TRAN 

		--UPDATE EMRIWarehouse.SF.Accounts
		--SET ModifiedUser = @SFUser
		--WHERE SF_AccountId = @OriginalAccountId

		EXEC Archive.uspSFAccounts
		   @SF_AccountId = @OriginalAccountId
		   , @SFUser = @SFUser
		   , @Activity = 'DELETE'
		
		DELETE 
		FROM EMRIWarehouse.SF.Accounts
		WHERE SF_AccountId = @OriginalAccountId
	
		INSERT INTO Integration.Archive.Accounts (AccountId, OriginalSFAccountId, MergedSFAccountId, ModifiedUser)
		SELECT dest.AccountId
				, @OriginalAccountId
				, @MergedAccountId
				, @SFUser
		FROM Integration.dbo.Accounts dest
		WHERE dest.SF_AccountId = @OriginalAccountId
		
		UPDATE dest
		SET dest.SF_AccountId = @MergedAccountId
			, ModifiedDate = GETDATE()
		FROM Integration.dbo.Accounts dest
		WHERE dest.SF_AccountId = @OriginalAccountId

		--There is a unque index on the AccountId and ContactId so we can't just update all or the statement might violate the key. Instead we update all unique records and then delete the remaining would be duplicates
		;WITH orig AS (
			SELECT	AccountContactId
					, SF_AccountId
					, SF_ContactId
					, ModifiedDate
					, ModifiedUser
			FROM Integration.dbo.AccountContact dest
			WHERE dest.SF_AccountId = @OriginalAccountId
		)
	
		, new AS (
			SELECT	AccountContactId
					, SF_AccountId
					, SF_ContactId
			FROM Integration.dbo.AccountContact dest
			WHERE dest.SF_AccountId = @MergedAccountId
		)

		UPDATE	orig
		SET SF_AccountId = @MergedAccountId
			, ModifiedDate = GETDATE()
			, ModifiedUser = @SFUser
		FROM orig 
		LEFT JOIN new ON new.SF_ContactId = orig.SF_ContactId
		WHERE new.AccountContactId is null

		UPDATE Integration.dbo.AccountContact
		SET ModifiedUser = @SFUser
		WHERE SF_AccountId = @OriginalAccountId

		DELETE dest
		FROM Integration.dbo.AccountContact dest
		WHERE dest.SF_AccountId = @OriginalAccountId

	COMMIT
END TRY

BEGIN CATCH	
		
	IF @@TRANCOUNT > 0
	ROLLBACK

	UPDATE WebAPI.dbo.SalesForceRequest
	SET IsError =1
		, ErrorReason = 'SF.uspAccountMerge Procedure level error'
	WHERE SalesForceRequestToken = @SalesForceRequestToken

END CATCH

GO

GRANT EXECUTE ON SF.uspAccountMerge TO public
GO
	
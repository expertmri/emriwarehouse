USE EMRIWarehouse
GO

ALTER PROCEDURE dbo.uspChargesCredits
AS

SET NOCOUNT ON;

BEGIN TRAN

	TRUNCATE TABLE EMRIWarehouse.dbo.ChargesCredits

	DECLARE @SQLString nvarchar(max)
			, @Number int = 1
			, @ColNumber nvarchar(10)

	WHILE @Number < 58 --Total number columns is 57 although only up to about 10 or so have data

	BEGIN

		SET @ColNumber = (SELECT CASE WHEN LEN(@Number) = 1 THEN N'0' + CAST(@Number as nvarchar) ELSE CAST(@Number as nvarchar) END)

		SET @SQLString = 'INSERT INTO dbo.ChargesCredits (ChargeTransactionNo, CreditTransactionNo, Amount) ' +
						'SELECT TransNo' + @ColNumber + ', c.TransactionNo, Amount' + @ColNumber + ' ' +
						'FROM Integration.Stg.Credits c ' +
						'JOIN Integration.Stg.Charges cg ON cg.TransactionNo = c.TransNo' + @ColNumber

		EXECUTE sp_executesql  @SQLString

		SET @Number = @Number + 1

	END

COMMIT




USE [EMRIWarehouse]
GO
/****** Object:  StoredProcedure [Rpt].[SSRSPendingToBeScheduled]    Script Date: 10/24/2017 8:53:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [Rpt].[SSRSPendingToBeScheduled]
AS


SELECT	appt.BookedDate as DOS
		, appt.ReferringDr
		, appt.ARClass
		, appt.Account
	    , appt.Modality
		, CASE WHEN CEILING(DATEPART(DAY, appt.BookedDate)/15.0) > 4 THEN 4 
			   ELSE CEILING(DATEPART(DAY, appt.BookedDate)/15.0)
		  END AS Aging
		, COALESCE(appt.Resource1, 'INTAKE') AS Resource1
FROM dbo.Appointments appt
JOIN XRef.AppointmentStatus stat ON appt.AppointmentStatusId = stat.AppointmentStatusId
JOIN XRef.ARClassGroup arg ON appt.ARClass = arg.ARClass
WHERE (appt.AppointmentStatusId IN (1,2) --'Pending', 'Pending Scheduling'
		OR appt.AppointmentStatusId = 6 AND appt.Resource1 = 'PANDEMIC')
	  AND appt.Modality = 'MRI'
	  AND arg.IsNonRevenue = 0 



GO
GRANT EXECUTE ON [Rpt].[SSRSPendingToBeScheduled] TO [public] AS [dbo]
GO

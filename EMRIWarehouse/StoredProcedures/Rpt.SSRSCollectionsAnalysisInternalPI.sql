USE [EMRIWarehouse]
GO
/****** Object:  StoredProcedure [Rpt].[SSRSCollectionsAnalysisInternalPI]    Script Date: 3/1/2019 11:27:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE Rpt.SSRSCollectionsAnalysisInternalPI
	@Modality varchar(100) = 'ALL'
	, @Payor varchar(255) = 'ALL'

--AND (@Modality = bt.Modality OR @Modality = 'ALL')


AS

--;WITH openExams AS (
			SELECT	COUNT(ApptId) AS ExamCount
					, InsCode1
					, Modality
			INTO #openExams
			FROM (
					SELECT	bt.ApptId
							, bt.InsCode1
							, bt.Modality
					FROM dbo.BilledTransactions bt
					JOIN XRef.ARClassGroup arc ON bt.ARClass = arc.ARClass
					WHERE bt.ApptId IS NOT NULL
						  AND IsCharge = 1
						  --AND arc.IsPersonalInjury = 1
						  AND arc.ARClass IN ('PIINT', 'PIINCP')
						  AND bt.InsCode1 <> '1795'
					EXCEPT
					SELECT	bt.ApptId
							, bt.InsCode1
							, bt.Modality
					FROM dbo.BilledTransactions bt
					JOIN Integration.Stg.FinancialCodes fc ON bt.FinancialCode = fc.Code
					JOIN XRef.ARClassGroup arc ON bt.ARClass = arc.ARClass
					WHERE bt.ApptId IS NOT NULL
						  AND IsCharge = 0
						  --AND arc.IsPersonalInjury = 1
						  AND arc.ARClass IN ('PIINT', 'PIINCP')
						  AND bt.InsCode1 NOT IN ('1795', '952')
						  AND (bt.FinancialCode not like '%WO' OR fc.SpecialFieldA = 1)
				) oc
			GROUP BY InsCode1
					 , Modality
--	)

--, totalBilled AS (
			SELECT	COUNT(DISTINCT bt.ApptId) AS ExamCount
					, bt.InsCode1
					, bt.Modality
			INTO #totalBilled
			FROM dbo.BilledTransactions bt
			JOIN XRef.ARClassGroup arc ON bt.ARClass = arc.ARClass
			WHERE bt.ApptId IS NOT NULL
					AND IsCharge = 1
					--AND arc.IsPersonalInjury = 1
					AND arc.ARClass IN ('PIINT', 'PIINCP')
					AND bt.InsCode1 <> '1795'
			GROUP BY bt.InsCode1
					 , bt.Modality
--	)

--, droppedExams AS (
			SELECT	COUNT(ApptId) AS ExamCount
					, InsCode1
					, Modality
			INTO #droppedExams
			FROM	(
						SELECT	ApptId
								, bt.InsCode1
								, bt.Modality
						FROM dbo.BilledTransactions bt
						JOIN Integration.Stg.FinancialCodes fc ON bt.FinancialCode = fc.Code
						JOIN XRef.ARClassGroup arc ON bt.ARClass = arc.ARClass
						WHERE bt.ApptId IS NOT NULL
							  AND IsCharge = 0
							  --AND arc.IsPersonalInjury = 1
							  AND arc.ARClass IN ('PIINT', 'PIINCP')
							  AND bt.InsCode1 <> '1795'
							  AND (bt.FinancialCode LIKE '%WO' OR fc.SpecialFieldA = 1)
						EXCEPT
						SELECT	ApptId as ExamCount
								, bt.InsCode1
								, bt.Modality
						FROM dbo.BilledTransactions bt
						JOIN Integration.Stg.FinancialCodes fc ON bt.FinancialCode = fc.Code
						JOIN XRef.ARClassGroup arc ON bt.ARClass = arc.ARClass
						WHERE bt.ApptId IS NOT NULL
							  AND IsCharge = 0
							  --AND arc.IsPersonalInjury = 1
							  AND arc.ARClass IN ('PIINT', 'PIINCP')
							  AND bt.InsCode1 <> '1795'
							  AND (bt.FinancialCode NOT LIKE '%WO' OR fc.SpecialFieldA = 1)
					) tt
			GROUP BY InsCode1
					 , Modality
--	)

--, settledExams AS (
			SELECT	COUNT(DISTINCT ApptId) as ExamCount
					, bt.InsCode1
					, bt.Modality
			INTO #settledExams
			FROM dbo.BilledTransactions bt
			JOIN Integration.Stg.FinancialCodes fc ON bt.FinancialCode = fc.Code
			JOIN XRef.ARClassGroup arc ON bt.ARClass = arc.ARClass
			WHERE bt.ApptId IS NOT NULL
				  AND IsCharge = 0
				  --AND arc.IsPersonalInjury = 1
				  AND arc.ARClass IN ('PIINT', 'PIINCP')
				  AND bt.InsCode1 <> '1795'
				  AND (bt.FinancialCode not like '%WO' OR fc.SpecialFieldA = 1)
			GROUP BY bt.InsCode1
					 , bt.Modality
--	)
--, PIINT AS (
			SELECT	COUNT(DISTINCT bt.ApptId) AS ExamCount
					, bt.InsCode1
					, bt.Modality
			INTO #PIINT
			FROM dbo.BilledTransactions bt
			JOIN XRef.ARClassGroup arc ON bt.ARClass = arc.ARClass
			WHERE bt.ApptId IS NOT NULL
					AND IsCharge = 1
					AND bt.ARClass IN ('PIINT', 'PIINCP')
					AND bt.InsCode1 <> '1795'
			GROUP BY bt.InsCode1
					 , bt.Modality
--	)

--, totalCompleted AS (
			SELECT	COUNT(appt.ApptId) AS ExamCount
					, appt.InsCode AS InsCode1
					, appt.Modality
			INTO #totalCompleted
			FROM dbo.Appointments appt
			JOIN XRef.ARClassGroup arc ON appt.ARClass = arc.ARClass
			WHERE arc.IsPersonalInjury = 1
					AND appt.InsCode <> '1795'
					AND appt.AppointmentStatusId = 4 --Exam Complete
			GROUP BY appt.InsCode
					 , appt.Modality
--	)

--, threeMnthAvg AS (
			SELECT	COUNT(appt.ApptId) AS ExamCount
					, appt.InsCode AS InsCode1
					, appt.Modality
			INTO #threeMnthAvg
			FROM dbo.Appointments appt
			JOIN XRef.ARClassGroup arc ON appt.ARClass = arc.ARClass
			WHERE arc.IsPersonalInjury = 1
					AND appt.InsCode <> '1795'
					AND appt.AppointmentStatusId = 4 --Exam Complete
					AND appt.DOS >= CAST(DATEADD(DAY, -90, GETDATE()) AS date)
			GROUP BY appt.InsCode
					 , appt.Modality
	--)

SELECT DISTINCT appt.InsName AS Payor
		, appt.Modality
		, COALESCE(openExams.ExamCount, 0) AS OpenExams
		, COALESCE((openExams.ExamCount/CAST(totalBilled.ExamCount as float)), 0) AS PercentOpen
		, COALESCE(droppedExams.ExamCount, 0) AS DroppedExams
		, COALESCE((droppedExams.ExamCount/CAST(totalBilled.ExamCount as float)), 0) AS PercentDropped
		, COALESCE(settledExams.ExamCount, 0) AS SettledExams
		, COALESCE((settledExams.ExamCount/CAST(totalBilled.ExamCount as float)),0) AS PercentSettled
		, COALESCE(PIINT.ExamCount, 0) AS TotalPIINT
		, COALESCE(totalCompleted.ExamCount, 0) AS TotalCompleted
		, COALESCE(CAST(threeMnthAvg.ExamCount AS float)/3, 0)  AS ThreeMnthAvg
FROM dbo.Appointments appt
LEFT JOIN #openExams AS openExams  ON appt.InsCode = openExams.InsCode1 AND appt.Modality = openExams.Modality
LEFT JOIN #totalBilled AS totalBilled ON appt.InsCode = totalBilled.InsCode1 AND appt.Modality = totalBilled.Modality
LEFT JOIN #settledExams AS settledExams ON appt.InsCode = settledExams.InsCode1 AND appt.Modality = settledExams.Modality
LEFT JOIN #droppedExams AS droppedExams ON appt.InsCode = droppedExams.InsCode1 AND appt.Modality = droppedExams.Modality
LEFT JOIN #PIINT AS PIINT ON appt.InsCode = PIINT.InsCode1 AND appt.Modality = PIINT.Modality
LEFT JOIN #totalCompleted AS totalCompleted ON appt.InsCode = totalCompleted.InsCode1 AND appt.Modality = totalCompleted.Modality
LEFT JOIN #threeMnthAvg AS threeMnthAvg ON appt.InsCode = threeMnthAvg.InsCode1 AND appt.Modality = threeMnthAvg.Modality
WHERE (@Modality = appt.Modality OR @Modality = 'ALL')
	  AND (@Payor = appt.InsName OR @Payor = 'ALL')


USE EMRIWarehouse
GO

ALTER PROCEDURE Rpt.SSRSSFAccountManagerMetrics
	@UserName varchar(255)

AS


DECLARE @FirstOfMonth date

SELECT @FirstOfMonth = dbo.udfFirstDayOfMonth(GETDATE())

SELECT	s.SF_AccountId
		, a.AccountManager
		, ScanCountMonthly
		, s.MonthYear
		, s.MonthName
		, GETDATE() AS InsertDate
INTO #ScansMRIComplete
FROM dbo.ScansByAccountWorkflow s
LEFT JOIN SF.Accounts a ON a.SF_AccountId = s.SF_AccountId
LEFT JOIN Integration.SF.Account a2 ON a.SF_AccountId = a2.Id
WHERE MonthYear >= DATEADD(MONTH, -6, @FirstOfMonth)

--VISITED ACCOUNTS BY MONTH
SELECT DISTINCT t.AccountId
		, CASE WHEN t.LastModifiedById = '005f4000000doagAAA' THEN dbo.udfFirstDayOfMonth(t.Date_Completed__c) ELSE dbo.udfFirstDayOfMonth(COALESCE(t.Date_Completed__c, t.LastModifiedDate)) END AS MonthYear
INTO #visited
FROM Integration.SF.Tasks t
WHERE t.Activity_Status__c = 'Completed'
	  AND t.AccountId IS NOT NULL

--NOT VISITED ACCOUNTS BY MONTH
SELECT DISTINCT t.AccountId
		, dbo.udfFirstDayOfMonth(t.Date_Completed__c) AS MonthYear
INTO #notVisited
FROM Integration.SF.Tasks t
LEFT JOIN #visited v ON v.AccountId = t.AccountId AND v.MonthYear = dbo.udfFirstDayOfMonth(t.Date_Completed__c)
WHERE t.Activity_Status__c = 'Open'
	  AND v.AccountId IS NULL

--SCANS FOR ACCOUNTS VISITED BY MONTH
SELECT s.AccountManager
		, s.MonthYear
		, SUM(s.ScanCountMonthly) AS ScanCount
INTO #visitedScans
FROM #ScansMRIComplete s
JOIN #visited v ON s.SF_AccountId = v.AccountId AND s.MonthYear = v.MonthYear
WHERE s.MonthYear > '12/1/2018'
GROUP BY s.AccountManager
		 , s.MonthYear

--SCANS FOR ACCOUNTS NOT VISITED BY MONTH
SELECT s.AccountManager
		, s.MonthYear AS MonthYear
		, SUM(s.ScanCountMonthly) AS ScanCount
INTO #notVisitedScans
FROM #ScansMRIComplete s
LEFT JOIN #notVisited v ON s.SF_AccountId = v.AccountId AND s.MonthYear = v.MonthYear
LEFT JOIN  #visited v2 ON s.SF_AccountId = v2.AccountId AND s.MonthYear = v2.MonthYear
WHERE s.MonthYear > '12/1/2018'
	  AND v2.AccountId IS NULL
GROUP BY s.AccountManager
		 , s.MonthYear

SELECT	a.AccountManager
		, COUNT(m.SF_AccountId) AS ActiveCount
		, m.MonthDate AS MonthYear
		--, CASE WHEN m.IsActive = 0 THEN 'Inactive' ELSE 'Active' END AS AccountStatus
INTO #Active
--INTO #Accounts
FROM SF.AccountsByMonth m
JOIN SF.Accounts a ON m.SF_AccountId = a.SF_AccountId
WHERE IsActive = 1
GROUP BY a.AccountManager
		 , m.MonthDate

SELECT	a.AccountManager
		, COUNT(m.SF_AccountId) AS InactiveCount
		, m.MonthDate  AS MonthYear
		--, CASE WHEN m.IsActive = 0 THEN 'Inactive' ELSE 'Active' END AS AccountStatus
INTO #Inactive
FROM SF.AccountsByMonth m
JOIN SF.Accounts a ON m.SF_AccountId = a.SF_AccountId
WHERE IsActive = 0
GROUP BY a.AccountManager
		 , m.MonthDate

SELECT COUNT(v.AccountId) AS ActiveVisit
		, a.AccountManager
		, v.MonthYear
INTO #visitedActive
FROM #visited v
JOIN SF.AccountsByMonth m ON v.AccountId = m.SF_AccountId AND m.MonthDate = v.MonthYear
JOIN SF.Accounts a ON m.SF_AccountId = a.SF_AccountId
WHERE IsActive = 1
GROUP BY a.AccountManager
		 , v.MonthYear

SELECT COUNT(v.AccountId) AS InactiveVisit
		, a.AccountManager
		, v.MonthYear
INTO #visitedInactive
FROM #visited v
JOIN SF.AccountsByMonth m ON v.AccountId = m.SF_AccountId AND m.MonthDate = v.MonthYear
JOIN SF.Accounts a ON m.SF_AccountId = a.SF_AccountId
WHERE IsActive = 0
GROUP BY a.AccountManager
		 , v.MonthYear


SELECT	t.Id
		, a.AccountManager
		, 'Open' AS TaskStatus
		--, t.Activity_Status__c
		--, LEFT(t.Task_Type__c, 2) AS Task
		, LEFT(t.Subject, 2) AS Task
		, dbo.udfFirstDayOfMonth(t.CreatedDate) AS MonthYear
		, DATEDIFF(day, t.CreatedDate, GETDATE()) AS Ageing
INTO #OpenAll
FROM Integration.SF.Tasks t
JOIN SF.Accounts a ON t.AccountId = a.SF_AccountId
WHERE Task_Type__c IS NOT NULL --AND Task_Type__c <> 'Other'
		--AND t.CreatedDate > '12/31/2018'
		AND t.Activity_Status__c = 'Open' 
		AND t.IsClosed = 0

SELECT	AVG(Ageing) AS Ageing
		, AccountManager
		, Task
INTO #Ageing
FROM #OpenAll
GROUP BY AccountManager
		 , Task


SELECT COUNT(t.Id) AS TaskCount
		, a.AccountManager
		, 'Assigned' AS TaskStatus
		, LEFT(t.Subject, 2) AS Task
		, dbo.udfFirstDayOfMonth(t.CreatedDate) AS MonthYear
		, NULL AS Ageing
INTO #Tasks
FROM Integration.SF.Tasks t
JOIN SF.Accounts a ON t.AccountId = a.SF_AccountId
WHERE Task_Type__c IS NOT NULL --AND Task_Type__c --<> 'Other'
	  AND t.CreatedDate > '12/31/2018'
GROUP BY a.AccountManager
		 , dbo.udfFirstDayOfMonth(t.CreatedDate) 
		 , LEFT(t.Subject, 2) 
UNION ALL
SELECT COUNT(t.Id) AS TaskCount
		, a.AccountManager
		, 'Complete' AS TaskStatus
		, LEFT(t.Subject, 2) AS Task
		, dbo.udfFirstDayOfMonth(t.Date_Completed__c) AS MonthYear
		, NULL AS Ageing
FROM Integration.SF.Tasks t
JOIN SF.Accounts a ON t.AccountId = a.SF_AccountId
WHERE Task_Type__c IS NOT NULL --AND Task_Type__c-- <> 'Other'
	  AND dbo.udfFirstDayOfMonth(t.Date_Completed__c) > '12/31/2018'
	  AND t.Activity_Status__c = 'Completed'
GROUP BY a.AccountManager
		 , dbo.udfFirstDayOfMonth(t.Date_Completed__c)
		 , LEFT(t.Subject, 2) 
UNION ALL 
SELECT	s.ScanCount
		, s.AccountManager
		, 'Visited'
		, 'Scan'
		, s.MonthYear
		, NULL AS Ageing
FROM #visitedScans s
UNION ALL 
SELECT	s.ScanCount
		, s.AccountManager
		, 'Not Visited'
		, 'Scan'
		, s.MonthYear
		, NULL AS Ageing
FROM #notVisitedScans s
UNION ALL
SELECT	COUNT(Id) AS TaskCount
		, o.AccountManager
		, TaskStatus
		, o.Task
		, MonthYear
		, a.Ageing
--INTO #Open
FROM #OpenAll o
JOIN #Ageing a ON a.AccountManager = o.AccountManager AND a.Task = o.Task
WHERE MonthYear > '12/31/2018'
GROUP BY o.AccountManager
		 , TaskStatus
		 , o.Task
		 , MonthYear
		 , a.Ageing


SELECT a.AccountManager
		, a.ActiveCount
		, i.InactiveCount
		, va.ActiveVisit
		, vi.InactiveVisit
		, a.MonthYear
		, t.Task
		, t.TaskCount
		, t.TaskStatus
		, t.Ageing
		--, s.ScanCount AS VisitedScans
		--, nv.ScanCount AS NotVisitedScans
FROM #Active a
LEFT JOIN #Inactive i ON a.AccountManager = i.AccountManager AND a.MonthYear = i.MonthYear
LEFT JOIN #Tasks t ON a.AccountManager = t.AccountManager AND t.Task IN ('OB', 'EL', 'AH', 'Scan') AND a.MonthYear  = t.MonthYear
LEFT JOIN #visitedInactive vi ON a.AccountManager = vi.AccountManager AND a.MonthYear = vi.MonthYear
LEFT JOIN #visitedActive va ON a.AccountManager = va.AccountManager AND a.MonthYear = va.MonthYear
JOIN dbo.SSRSUsers s ON (a.AccountManager = s.UserName  OR s.isAdmin = 1)
WHERE --(a.AccountManager = @AccountManager OR @AccountManager = 'ALL')
	  s.domainUser = @UserName 

GO

GRANT EXECUTE ON Rpt.SSRSSFAccountManagerMetrics TO PUBLIC;


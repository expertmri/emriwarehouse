USE EMRIWarehouse;
GO

ALTER PROCEDURE Rpt.SSRSEventSummary 
AS

SELECT COUNT(t.Id) AS DeadLeads, t.EventId
INTO #dead
FROM (
		SELECT DISTINCT	e.Event_Reference_Id__c AS EventId
				, e.id
				--, COUNT(e.id) AS DeadLeads
		FROM Integration.SF.Events e 
		JOIN Integration.SF.ADSQueue ads ON ads.Account_Id__c = e.AccountId
		LEFT JOIN Integration.SF.Reference ref ON e.Event_Reference_Id__c = ref.Id
				AND ads.CreatedDate < ref.Start_Date__c
		WHERE	Status__c = 'Dead Lead'
	) t
GROUP BY t.EventId

SELECT COUNT(tt.id) AS ProcessedLeads, tt.EventId
INTO #processed
FROM (
		SELECT DISTINCT	e.Event_Reference_Id__c AS EventId
				, e.id
		FROM Integration.SF.Events e 
		JOIN Integration.SF.ADSQueue ads ON ads.Account_Id__c = e.AccountId
		LEFT JOIN Integration.SF.Reference ref ON e.Event_Reference_Id__c = ref.Id
				AND (ads.CreatedDate < ref.Start_Date__c OR ref.Start_Date__c is null)
		WHERE	ads.Source__c = 'Lead' 
				AND ads.Is_Processed__c = 'True'
				--AND (ads.CreatedDate < ref.Start_Date__c OR ref.Start_Date__c is null)
	) tt
GROUP BY tt.EventId

SELECT COUNT(ttt.id) AS UnprocessedLeads, ttt.EventId
INTO #unprocessed
FROM (
		SELECT	e.Event_Reference_Id__c AS EventId
				, e.id
		FROM Integration.SF.Events e 
		JOIN Integration.SF.ADSQueue ads ON ads.Account_Id__c = e.AccountId
		LEFT JOIN Integration.SF.Reference ref ON e.Event_Reference_Id__c = ref.Id
				AND (ads.CreatedDate < ref.Start_Date__c OR ref.Start_Date__c is null)
		WHERE	ads.Source__c = 'Lead' 
				AND ads.Is_Processed__c = 'False'
				AND ads.Status__c = 'Normal'
	) ttt
GROUP BY ttt.EventId

SELECT	DISTINCT e.Subject AS EventName
		, CAST(COALESCE(ref.Start_Date__c, e.ActivityDate) AS date) AS EventDate
		, CASE WHEN e.Event_Reference_Id__c IS NULL THEN 'Manual' ELSE 'Imported' END AS 'Source'
		, 'Pending' AS LeadsImported
		, 'Pending' AS ExistingClients
		, COALESCE(p.ProcessedLeads, 0) AS ProcessedLeads
		, COALESCE(u.UnprocessedLeads, 0) AS UnprocessedLeads
		, COALESCE(d.DeadLeads, 0) AS DeadLeads
		, 'Pending' AS ConvertedClients
FROM Integration.SF.Events e
LEFT JOIN Integration.SF.Reference ref ON e.Event_Reference_Id__c = ref.Id
LEFT JOIN #dead d ON e.Event_Reference_Id__c = d.EventId
LEFT JOIN #processed p ON e.Event_Reference_Id__c = p.EventId
LEFT JOIN #unprocessed u ON e.Event_Reference_Id__c = u.EventId

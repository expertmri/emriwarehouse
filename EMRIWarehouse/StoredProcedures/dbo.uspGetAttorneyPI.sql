USE EMRIWarehouse
GO

ALTER PROCEDURE dbo.uspGetAttorneyPI
AS

SELECT Insurance1
FROM (
		SELECT DISTINCT bt.Insurance1
		FROM dbo.BilledTransactions bt
		JOIN XRef.ARClassGroup arc ON bt.ARClass = arc.ARClass
		WHERE bt.IsCharge = 0
			  --AND arc.IsPersonalInjury = 1
			  AND arc.ARClass IN ('PIINT', 'PIINCP')
			  AND bt.InsCode1 <> '1795'
		UNION 
		SELECT 'ALL'
	) tt
ORDER BY CASE WHEN Insurance1 = 'ALL' THEN 0 ELSE 1 END
		 , Insurance1

GO

GRANT EXECUTE ON dbo.uspGetAttorneyPI TO PUBLIC;
GO	  
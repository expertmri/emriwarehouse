USE EMRIWarehouse
GO

ALTER PROCEDURE Rpt.SSRSHawthorneDirectPhoneRecords
AS

DECLARE @TimeDiff int = (SELECT DATEDIFF(HOUR, GETUTCDATE(), GETDATE()))

SELECT	pr.SvcType
		, pr.SvcNum
		, DATEADD(HOUR, @TimeDiff, pr.CallDate) AS CallDate
		, pr.UsageTypeID
		, pr.OrigCC
		, pr.OrigNumber
		, pr.OrigDesc
		, pr.TermCC
		, pr.TermNumber
		, pr.TermDesc
		, pr.CallDur
		, pr.EU_RatePlan
		, pr.EU_BillDur
FROM Integration.INBOUND.PhoneRecords pr
WHERE InsertDate > CAST(GETDATE() AS date)

GO

GRANT EXECUTE ON Rpt.SSRSHawthorneDirectPhoneRecords TO PUBLIC;

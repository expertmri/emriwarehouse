USE EMRIWarehouse
GO

ALTER PROCEDURE Rpt.SSRSCurrentOnBooks
@Modality varchar(20)

AS

SELECT	COALESCE(ar.DirectorGroup, 'Unknown')  AS ARClass
		, apt.Modality
		, apt.DOS
		, COUNT(apt.ApptId) AS RcdCnt
FROM dbo.Appointments apt
JOIN XRef.AppointmentStatus stat ON apt.AppointmentStatusId = stat.AppointmentStatusId
LEFT JOIN XRef.ARClassGroup ar ON apt.ARClass = ar.ARClass
WHERE stat.IsOnBooks = 1
		AND Modality = @Modality
GROUP BY ar.DirectorGroup 
		, apt.Modality
		, apt.DOS
		
GO

GRANT EXECUTE ON Rpt.SSRSCurrentOnBooks TO PUBLIC;
USE [EMRIWarehouse]
GO
/****** Object:  StoredProcedure [dbo].[uspScansByAccountWorkflow]    Script Date: 7/5/2019 9:55:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE dbo.uspScansByAccountWorkflow

AS

SET NOCOUNT ON

DECLARE @FirstOfMonth date
		, @FirstDayLastMonth date
		, @EndDayLastMonth date
		, @calcDate date
		, @DEBUG bit = 0
		, @BeginCalendarDate date 
		--, @PriorRunDates date = '10/1/2019'
		, @LoopCounter int = 0

--Run for last month once then current month
SELECT @FirstOfMonth = DATEADD(MONTH, -1, dbo.udfFirstDayOfMonth(GETDATE()))


--Rerun for past months, need to also run EXEC SF.uspAccountsByMonth per month immediately after
--SELECT @FirstOfMonth = dbo.udfFirstDayOfMonth(@PriorRunDates)
--	   , @FirstDayLastMonth = dbo.udfFirstDayLastMonth(@PriorRunDates)
--	   , @EndDayLastMonth = dbo.udfEndDayLastMonth(@PriorRunDates)


WHILE @LoopCounter < 2
BEGIN
	
	SET @BeginCalendarDate = '1/1/2017'
	SELECT @FirstDayLastMonth = dbo.udfFirstDayLastMonth(@FirstOfMonth)
		   , @EndDayLastMonth = dbo.udfEndDayLastMonth(@FirstOfMonth)	
	
	--Must use set or returns null due to select setting reference variable
	SET @calcDate = (SELECT CASE WHEN EOMONTH(@FirstOfMonth) > GETDATE() THEN GETDATE() ELSE EOMONTH(@FirstOfMonth) END)

	IF OBJECT_ID('tempdb..#Scans') is not null
    DROP TABLE #Scans

	CREATE TABLE #Scans (
		SF_AccountId varchar(50)
		, ApptId int
		, DOS date
		, AccountType varchar(30)
		, IsWorkComp bit
		)
	
	IF OBJECT_ID('tempdb..#Monthly') is not null
    DROP TABLE #Monthly

	CREATE TABLE #Monthly (
		SF_AccountId varchar(50)
		, ReferralName varchar(255)
		, ScanCountMonthly int
		, ThreeMonthAvg float
		, FirstReferralDate date
		, LastReferralDate date
		, PercentAvg float
		, AccountCategory varchar(40)
		, AccountStatus varchar(40)
		, MonthYear date
		, MonthName varchar(20)
		, AccountType varchar(30)
		, BusinessType varchar(30)
		, RN int
		)

	IF OBJECT_ID('tempdb..#Calendar') is not null
    DROP TABLE #Calendar

	CREATE TABLE #Calendar (
		MonthYear date
		)

	WHILE @BeginCalendarDate <= @FirstOfMonth
	BEGIN
		INSERT INTO #Calendar
		SELECT @BeginCalendarDate

		SET @BeginCalendarDate = DATEADD(MONTH, 1, @BeginCalendarDate)
	END
		
	IF OBJECT_ID('tempdb..#ScansCalendar') is not null
    DROP TABLE #ScansCalendar
	
	CREATE TABLE #ScansCalendar (
		SF_AccountId varchar(50)
		, MonthYear date
		, AccountType varchar(30)
		)

	IF OBJECT_ID('tempdb..#Status') is not null
    DROP TABLE #Status
	
	CREATE TABLE #Status (
		LowerPercent float
		, UpperPercent float
		, StatusName varchar(20)
	)

	INSERT INTO #Status (LowerPercent, UpperPercent, StatusName)
	VALUES (0, .2, '1% to 19% Drop')
	, (.2, .4, '20% to 39% Drop')
	, (.4, .6, '40% to 59% Drop')
	, (.6, .8, '60% to 79% Drop')
	, (.8, 1, '80% to 100% Drop')
	, (1, 9999999, 'Good')

	INSERT INTO #Scans (SF_AccountId, ApptId, DOS, AccountType, IsWorkComp)
	SELECT  SF_AccountId AS SF_AccountId
			, ApptId
			, DOS
			, AccountType
			, IsWorkComp
	FROM dbo.Scans
	WHERE Modality = 'MRI'
		  AND IsNonRevenue = 0
		  AND DOS <= @calcDate -- ******* 11/4 ADDED FOR RERUN
		  --AND SF_AccountId = '001f400000wDbRsAAK'

	INSERT INTO #ScansCalendar (SF_AccountId, MonthYear, AccountType)
	SELECT	DISTINCT SF_AccountId
			, c.MonthYear
			, AccountType
	FROM #Scans
	JOIN #Calendar c ON 1=1
	WHERE SF_AccountId IS NOT NULL
	  
	  
	INSERT INTO #Monthly (SF_AccountId, ScanCountMonthly, MonthYear, MonthName, AccountType, RN)
	SELECT  sc.SF_AccountId
			, COUNT(ApptId) AS ScanCountMonthly
			, sc.MonthYear
			, dbo.udfGetMonthName(sc.MonthYear) AS MonthName
			, sc.AccountType
			, ROW_NUMBER() OVER(PARTITION BY sc.SF_AccountId ORDER BY sc.MonthYear desc) AS RN
	FROM #ScansCalendar sc
	LEFT JOIN #Scans s ON s.SF_AccountId = sc.SF_AccountId AND dbo.udfFirstDayOfMonth(DOS) = sc.MonthYear
	GROUP BY sc.SF_AccountId
			 , sc.MonthYear
			 , dbo.udfGetMonthName(DOS)
			 , sc.AccountType

	/*Get First and Last Referral Dates*/
	;WITH ReferralDates AS
	(
		SELECT SF_AccountId
				, MAX(DOS) AS LastReferralDate
				, MIN(DOS) AS FirstReferralDate
		FROM #Scans s
		--WHERE DOS < @calcDate -- ******11/4/2019 ADDED where clause
		GROUP BY SF_AccountId
	)

	UPDATE m
	SET LastReferralDate = ct.LastReferralDate
		, FirstReferralDate = ct.FirstReferralDate
	FROM #Monthly m
	LEFT JOIN ReferralDates ct ON m.SF_AccountId = ct.SF_AccountId

	/*Calculate 3 Month Average*/
	;WITH Average AS
	(
		SELECT SF_AccountId
			   , SUM(ScanCountMonthly) AS TotalScans
		FROM #Monthly
		--WHERE MonthYear >= DATEADD(MONTH, -3, @FirstDayLastMonth) AND MonthYear < @FirstDayLastMonth -- ******11/4/2019 NEED TO USE LAST 3 MONTHS NOT 3 MONTHS PRIOR TO LAST MONTH
		WHERE MonthYear >= DATEADD(MONTH, -3, @FirstOfMonth) AND MonthYear < @FirstOfMonth 
		GROUP BY SF_AccountId
	)

	UPDATE m
	SET ThreeMonthAvg = COALESCE(CAST(TotalScans AS float),0)/3.00
	FROM #Monthly m
	LEFT JOIN Average a ON m.SF_AccountId = a.SF_AccountId

	/*Calculate Current Month Drop*/
	UPDATE #Monthly
	SET PercentAvg = CASE WHEN ThreeMonthAvg = 0 OR ScanCountMonthly = 0 THEN 0 
						  WHEN ScanCountMonthly >= ThreeMonthAvg THEN CAST(ScanCountMonthly AS float)/ThreeMonthAvg
						  ELSE 1 - (COALESCE(CAST(ScanCountMonthly AS float)/ThreeMonthAvg, 0)) END --Get % drop so need to subtract from 1
	--WHERE MonthYear = @FirstDayLastMonth  -- ******11/4/2019 NEED TO USE LAST 3 MONTHS NOT 3 MONTHS PRIOR TO LAST MONTH
	WHERE MonthYear = @FirstOfMonth

	--8/21 EM-520 Added Update to set all other PercentAvg fields so Status logic works
	;WITH  percents AS (
							SELECT DISTINCT PercentAvg, SF_AccountId
							FROM #Monthly
							WHERE PercentAvg IS NOT NULL
						)

	UPDATE m
	SET PercentAvg = p.PercentAvg
	FROM #Monthly m
	JOIN percents p ON p.SF_AccountId = m.SF_AccountId
	WHERE m.PercentAvg IS NULL
 
 
	UPDATE #Monthly
	SET AccountCategory = 'Active'
		, AccountStatus = 'New'
	WHERE AccountStatus IS NULL
		  --AND FirstReferralDate >= @FirstDayLastMonth --SLUNDY 9/9 debugging bad statuses
		  AND FirstReferralDate >= @FirstOfMonth

	

	UPDATE m
	SET AccountCategory = 'Active'
		, AccountStatus = CASE WHEN PercentAvg = 0 THEN 'Good' ELSE StatusName END --8/21 EM-520 Added CASE instead of just status name lookup to add Good records
	FROM #Monthly m
	JOIN #Status s ON PercentAvg >= LowerPercent AND PercentAvg < UpperPercent
	WHERE AccountStatus IS NULL
		  AND (
				PercentAvg <> 0
				-- 8/21 EM-520 Added OR for Last completed month = 0, but 3 month Avg is positive and current month has scans again
				OR (PercentAvg = 0 AND ThreeMonthAvg > 0 AND ScanCountMonthly > 0)
				--OR (PercentAvg = 0 AND ThreeMonthAvg = 0 AND LastReferralDate BETWEEN @FirstDayLastMonth AND @FirstOfMonth ) 
				OR (PercentAvg = 0 AND ThreeMonthAvg = 0 AND LastReferralDate > @FirstDayLastMonth AND LastReferralDate < @FirstOfMonth )
				)

	--CHANGED 7/5/2019 JIRA EM-486
	UPDATE #Monthly
	SET AccountCategory = 'Active'
		, AccountStatus = 'Restart' 
	WHERE LastReferralDate >= @FirstDayLastMonth
		  AND ScanCountMonthly <> 0
		  AND ThreeMonthAvg = 0
		  --8/21 EM-520 
		  --AND RN = 2
		  AND AccountStatus IS NULL

	UPDATE #Monthly
	SET AccountCategory = 'Inactive'
		--, AccountStatus = CASE WHEN LastReferralDate >  DATEADD(MONTH, -3,@FirstOfMonth) THEN 'Less than 3 months' ELSE 'More than 3 months' END -- ******11/4/2019
		, AccountStatus = CASE WHEN LastReferralDate >  DATEADD(MONTH, -3, @calcDate) THEN 'Less than 3 months' ELSE 'More than 3 months' END
	WHERE AccountStatus IS NULL
		  --AND LastReferralDate < @FirstDayLastMonth   -- ******11/4/2019 NEED TO USE LAST 3 MONTHS NOT 3 MONTHS PRIOR TO LAST MONTH
	AND LastReferralDate < @FirstOfMonth
		  -- 8/21 EM-520 Added OR for Last completed month = 0, but 3 month Avg is positive and current month has 0 scans
		  --OR (ScanCountMonthly = 0 AND ThreeMonthAvg > 0) redundant i believe probably remove these last 2 lines

	-- ******11/4/2019 DUPLICATE AS ABOVE COMMENTED OUT
	--UPDATE #Monthly
	--SET AccountCategory = 'Inactive'
	--	, AccountStatus = CASE WHEN LastReferralDate >  DATEADD(MONTH, -3,@FirstOfMonth) THEN 'Less than 3 months' ELSE 'More than 3 months' END
	--WHERE AccountStatus IS NULL
	--	  AND LastReferralDate < @FirstDayLastMonth 


	--CHANGED 8/20/2019 JIRA EM-520 AND removed again
	--UPDATE #Monthly
	--SET AccountCategory = 'Active'
	--	, AccountStatus = 'Restart' 
	--WHERE LastReferralDate >= @FirstOfMonth
	--	  AND ScanCountMonthly <> 0
	--	  AND AccountStatus IS NULL


	/*
	--REMOVED 7/5/2019 per Saba JIRA EM-486
	--July has Scans but June has none
	UPDATE #Monthly
	SET AccountCategory = 'Active'
		, AccountStatus = 'Restart' 
	WHERE RN = 2
		  AND ScanCountMonthly = 0
		  AND LastReferralDate >= @FirstOfMonth
		  AND AccountStatus IS NULL

	--June or July has Scans but May does not
	UPDATE #Monthly
	SET AccountCategory = 'Active'
		, AccountStatus = 'Restart' 
	WHERE LastReferralDate >= @FirstDayLastMonth
		  AND ScanCountMonthly = 0
		  AND RN = 3
		  AND AccountStatus IS NULL

	*/

	-----Do this after all status logic is done
	;WITH MissingStatus AS
	(
		SELECT	SF_AccountId
				, AccountStatus
				, AccountCategory
				, PercentAvg
		FROM #Monthly
		WHERE AccountStatus IS NULL OR PercentAvg IS NULL
	)

	UPDATE m
	SET AccountCategory = m2.AccountCategory
		, AccountStatus = m2.AccountStatus
		, PercentAvg = m2.PercentAvg
	FROM MissingStatus m
	JOIN #Monthly m2 ON m.SF_AccountId = m2.SF_AccountId AND m2.AccountStatus IS NOT NULL
	--WHERE m.AccountStatus IS NOT NULL


	;WITH WorkComp AS
	(
		SELECT	SF_AccountId
				, COUNT(apptId) AS WCScans
		FROM #Scans
		WHERE DOS >= DATEADD(MONTH, -4, @FirstOfMonth) AND DOS < @FirstOfMonth
			  AND IsWorkComp = 1
		GROUP BY SF_AccountId
	)
	, TotalScans AS
	(
		SELECT	SF_AccountId
				, COUNT(apptId) AS TotalScans
		FROM #Scans
		WHERE DOS >= DATEADD(MONTH, -4, @FirstOfMonth) AND DOS < @FirstOfMonth
		GROUP BY SF_AccountId
	)

	UPDATE	m
	SET BusinessType = CASE WHEN CAST(COALESCE(WCScans, 0) AS float) / TotalScans >= .3 THEN 'WC' ELSE 'Hybrid' END
	FROM #Monthly m
	LEFT JOIN TotalScans ts ON ts.SF_AccountId = m.SF_AccountId
	LEFT JOIN WorkComp wc ON ts.SF_AccountId = wc.SF_AccountId

	IF @DEBUG = 1
	SELECT * FROM #Monthly

	ELSE

	BEGIN
		BEGIN TRAN

			TRUNCATE TABLE dbo.ScansByAccountWorkflow

			INSERT INTO dbo.ScansByAccountWorkflow (
				SF_AccountId
				, ReferralName
				, ScanCountMonthly
				, ThreeMonthAvg
				, FirstReferralDate
				, LastReferralDate
				, PercentAvg
				, AccountCategory
				, AccountStatus
				, MonthYear 
				, MonthName 
				, AccountType
				, BusinessType
			)

			SELECT SF_AccountId
				, ReferralName
				, ScanCountMonthly
				, ThreeMonthAvg
				, FirstReferralDate
				, LastReferralDate
				, PercentAvg
				, AccountCategory
				, AccountStatus
				, MonthYear 
				, MonthName 
				, AccountType
				, BusinessType
			FROM #Monthly
		
		COMMIT
END
	EXEC SF.uspAccountsByMonth @FirstOfMonth

	SELECT @FirstOfMonth = dbo.udfFirstDayOfMonth(GETDATE())

	SET @LoopCounter = @LoopCounter + 1


END


USE EMRIWarehouse
GO

ALTER PROCEDURE Rpt.uspGetEventNames
AS

SELECT *
FROM (
		SELECT DISTINCT e.Subject AS EventName
		FROM Integration.SF.Events e
		JOIN Integration.SF.Reference r ON e.Subject = r.Name
		WHERE r.RecordTypeId = '012f4000001Du3pAAC' --exclude user created events
		UNION ALL
		SELECT 'ALL' AS EventName
	) tt
ORDER BY CASE WHEN tt.EventName = 'ALL' THEN 0 ELSE 1 END, tt.EventName

GO

GRANT EXECUTE ON Rpt.uspGetEventNames TO PUBLIC;
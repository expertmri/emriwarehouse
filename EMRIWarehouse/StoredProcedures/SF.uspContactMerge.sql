USE EMRIWarehouse
GO

ALTER PROCEDURE SF.uspContactMerge 
	@jsonRequest nvarchar(max)
AS

--DECLARE @jsonRequest nvarchar(max) = '{"SalesForceRequestToken":"ef93498e-2eb8-373e-ca05-2df76c9711e8","RequestAction":"ContactMerge","OriginalSFContactId":"003f400000RXzXOAA1","MergedSFContactId":"003f4000003xADIAA2","SFUser":"Saba Ejaz"}'

--Merge to '001f40000081QaTAAU' --19
--Merge from '001f40000081QaSAAU' --16,17,18

BEGIN TRY

	DECLARE @MergedContactId varchar(255)
			, @OriginalContactId varchar(255)
			, @SalesForceRequestToken varchar(255)
			, @SFUser varchar(255)
	

	SELECT	@SalesForceRequestToken = SalesForceRequestToken
			, @OriginalContactId = OriginalSFContactId
			, @MergedContactId = MergedSFContactId
			, @SFUser = SFUser
	FROM OPENJSON(@jsonRequest)
	WITH (
			SalesForceRequestToken  nvarchar(255) 'strict $.SalesForceRequestToken'
			, OriginalSFContactId nvarchar(255) '$.OriginalSFContactId'
			, MergedSFContactId nvarchar(255) '$.MergedSFContactId'
			, SFUser nvarchar(255) '$.SFUser'
		) 

	--PERFORM CHECK FOR BOTH CONTACTS AND FAIL THE ROW
	--IF EXISTS (SELECT * FROM Integration.SF.Contact WHERE Id = @OriginalContactId AND Source__c = 'Lead')
	--BEGIN
	--	UPDATE WebAPI.dbo.SalesForceRequest
	--	SET IsError =1
	--		, ErrorReason = 'SF_ContactId ' + @OriginalContactId + ' is a Lead'
	--	WHERE SalesForceRequestToken = @SalesForceRequestToken

	--	RETURN;
	--END
	
	--IF EXISTS (SELECT * FROM Integration.SF.Contact WHERE Id = @MergedContactId AND Source__c = 'Lead')
	--BEGIN
	--	UPDATE WebAPI.dbo.SalesForceRequest
	--	SET IsError =1
	--		, ErrorReason = 'SF_ContactId ' + @OriginalContactId + ' is a Lead'
	--	WHERE SalesForceRequestToken = @SalesForceRequestToken

	--	RETURN;
	--END

	IF NOT EXISTS (SELECT * FROM Integration.dbo.Contacts WHERE SF_ContactId IN (@OriginalContactId, @MergedContactId))
	BEGIN
		UPDATE WebAPI.dbo.SalesForceRequest
		SET IsError =1
			, ErrorReason = 'SF_ContactId ' + @OriginalContactId + ' is not in Integration.dbo.Contacts'
		WHERE SalesForceRequestToken = @SalesForceRequestToken

		RETURN;
	END

	--IF NOT EXISTS (SELECT * FROM EMRIWarehouse.SF.Contacts WHERE SF_ContactId = @OriginalContactId)
	--BEGIN
	--	UPDATE WebAPI.dbo.SalesForceRequest
	--	SET IsError =1
	--		, ErrorReason = 'SF_ContactId ' + @OriginalContactId + ' is not in EMRIWarehouse.SF.Contacts'
	--	WHERE SalesForceRequestToken = @SalesForceRequestToken

	--	RETURN;
	--END

	IF NOT EXISTS (SELECT * FROM Integration.dbo.Contacts WHERE SF_ContactId IN (@OriginalContactId, @MergedContactId))
	BEGIN
		UPDATE WebAPI.dbo.SalesForceRequest
		SET IsError =1
			, ErrorReason = 'SF_ContactId ' + @MergedContactId + ' is not in Integration.dbo.Contacts'
		WHERE SalesForceRequestToken = @SalesForceRequestToken

		RETURN;
	END

	IF NOT EXISTS (SELECT * FROM EMRIWarehouse.SF.Contacts WHERE SF_ContactId = @MergedContactId)
	BEGIN
		INSERT INTO EMRIWarehouse.SF.Contacts(
				SF_ContactId
			  , FirstName
			  , LastName
			  , MiddleName
			  , Salutation
			  , Suffix
			  , JobTitle
			  , Phone
			  , Fax
			  , Email
			  , ADSKey
			  , ContactType
			  , CreateDate
			)
		SELECT 
				src.Id
				, src.FirstName
				, src.LastName
				, src.MiddleName
				, src.Salutation
				, src.Suffix
				, src.Title
				, src.Phone
				, src.Fax
				, src.Email
				, null
				, src.Contact_Type__c
				, GETDATE()
		FROM Integration.SF.Contact src
		WHERE src.Source__c = 'RIS'
			  AND src.Id = @MergedContactId
		
		--UPDATE WebAPI.dbo.SalesForceRequest
		--SET IsError =1
		--	, ErrorReason = 'SF_ContactId ' + @MergedContactId + ' is not in EMRIWarehouse.SF.Contacts'
		--WHERE SalesForceRequestToken = @SalesForceRequestToken

		
	END

	BEGIN TRAN 

		--UPDATE EMRIWarehouse.SF.Contacts
		--SET ModifiedUser = @SFUser
		--WHERE SF_ContactId = @OriginalContactId
	
		EXEC Archive.SFContacts
		   @SF_ContactId = @OriginalContactId
		   , @SFUser = @SFUser
		   , @Activity = 'DELETE'

		DELETE 
		FROM EMRIWarehouse.SF.Contacts
		WHERE SF_ContactId = @OriginalContactId

		INSERT INTO Integration.Archive.Contacts (ContactId, AccountId, OriginalSFContactId, MergedSFContactId, ModifiedUser)
		SELECT  dest.ContactId
				, dest.AccountId
				, @OriginalContactId
				, @MergedContactId
				, @SFUser
		FROM Integration.dbo.Contacts dest
		WHERE dest.SF_ContactId = @OriginalContactId

		UPDATE dest
		SET dest.SF_ContactId = @MergedContactId
			, ModifiedDate = GETDATE()
		FROM Integration.dbo.Contacts dest
		WHERE dest.SF_ContactId = @OriginalContactId

		--There is a unque index on the ContactId and ContactId so we can't just update all or the statement might violate the key. Instead we update all unique records and then delete the remaining would be duplicates
		;WITH orig AS (
			SELECT	AccountContactId
					, SF_AccountId
					, SF_ContactId
					, ModifiedDate
					, ModifiedUser
			FROM Integration.dbo.AccountContact dest
			WHERE dest.SF_ContactId = @OriginalContactId
		)
	
		, new AS (
			SELECT	AccountContactId
					, SF_AccountId
					, SF_ContactId
			FROM Integration.dbo.AccountContact dest
			WHERE dest.SF_ContactId = @MergedContactId
		)

		--select *
		UPDATE	orig
		SET SF_ContactId = @MergedContactId
			, ModifiedDate = GETDATE()
			, ModifiedUser = @SFUser
		FROM orig 
		LEFT JOIN new ON new.SF_AccountId = orig.SF_AccountId
		WHERE new.AccountContactId is null

		--Update the modified user on any contacts that did not get merged because they were duplicates so we can track this value when deleting
		UPDATE Integration.dbo.AccountContact
		SET ModifiedUser = @SFUser
		WHERE SF_ContactId = @OriginalContactID

		DELETE dest
		FROM Integration.dbo.AccountContact dest
		WHERE dest.SF_ContactId = @OriginalContactID

	COMMIT
END TRY

BEGIN CATCH	
	DECLARE @Error varchar(max) = ERROR_MESSAGE();

	IF @@TRANCOUNT > 0
	ROLLBACK

	UPDATE WebAPI.dbo.SalesForceRequest
	SET IsError =1
		, ErrorReason = 'SF.uspContactMerge ' + @Error
	WHERE SalesForceRequestToken = @SalesForceRequestToken

END CATCH

GO

GRANT EXECUTE ON SF.uspContactMerge TO public
GO
	
USE EMRIWarehouse
GO

ALTER PROCEDURE Rpt.SSRSReceivedByAttorneyWC
@Range varchar(20) = null

AS

DECLARE @StartDate date
		, @FirstOfMonth date
		, @EndDate date

SELECT @FirstOfMonth = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)
	   , @EndDate = DATEADD(DAY, -1, @FirstOfMonth)
	   , @StartDate = DATEADD(MONTH, -3, @FirstOfMonth)

;WITH AvgCTE AS
	(
		SELECT  aty.code AS AttyCode
				, CAST(COUNT (CASE WHEN BookedDate BETWEEN @StartDate AND @EndDate THEN apt.ApptId ELSE NULL END) as decimal)/DATEDIFF(DAY, @StartDate, @EndDate) AS QtrAvg
				, CAST(COUNT(CASE WHEN BookedDate >= @FirstOfMonth THEN apt.ApptId ELSE NULL END) AS decimal)/ DATEDIFF(DAY, @FirstOfMonth , CAST(GETDATE() AS date)) AS MthAvg
		FROM dbo.Appointments apt
		JOIN Integration.Stg.Patients p ON apt.Account = p.Account
		JOIN Integration.Stg.Attorneys aty ON p.Reserved = aty.Code
		LEFT JOIN XRef.ARClassGroup ar ON apt.ARClass = ar.ARClass
		WHERE apt.Modality = 'MRI'
			  AND ar.IsWorkComp = 1
		GROUP BY code  		
	)
	
, AllData_CTE AS
	(	
		SELECT	aty.PracticeName AS Attorney
				, aty.Address1 
					+ RTRIM(LTRIM(+ ' ' + COALESCE(aty.Address2,''))) 
					+ ' ' + REPLACE( aty.City, ' ', '') 
					+ ' ' + aty.State 
					+ ' ' + aty.ZipCode  AS AttyAddress
				, DATEADD(MONTH, DATEDIFF(MONTH, 0, apt.BookedDate), 0) AS MnthNmbr
				, dbo.udfGetMonthName(apt.BookedDate) AS DOS
				, COUNT(apt.ApptId) AS Completed
				, CASE WHEN cte.QtrAvg = 0 THEN 'NEW'
					   WHEN cte.MthAvg >= cte.QtrAvg THEN 'GOOD'
					   WHEN cte.MthAvg < cte.QtrAvg THEN 'DROPPED'
				  END AS AccountStatus
				, CASE WHEN cte.MthAvg < cte.QtrAvg THEN CEILING(((cte.QtrAvg - cte.MthAvg)/cte.QtrAvg) * 100)
				  END AS PercentDrop
		FROM dbo.Appointments apt
		JOIN Integration.Stg.Patients p ON apt.Account = p.Account
		JOIN Integration.Stg.Attorneys aty ON p.Reserved = aty.Code
		LEFT JOIN XRef.ARClassGroup ar ON apt.ARClass = ar.ARClass
		LEFT JOIN AvgCTE cte ON aty.Code = cte.AttyCode
		WHERE apt.Modality = 'MRI'
			  AND ar.IsWorkComp = 1
		GROUP BY aty.PracticeName
				, aty.Address1 
					+ RTRIM(LTRIM(+ ' ' + COALESCE(aty.Address2,''))) 
					+ ' ' + REPLACE( aty.City, ' ', '') 
					+ ' ' + aty.State 
					+ ' ' + aty.ZipCode
				, DATEADD(MONTH, DATEDIFF(MONTH, 0, apt.BookedDate), 0)
				, dbo.udfGetMonthName(apt.BookedDate)
				, cte.QtrAvg
				, cte.MthAvg
		)

, FinalData AS 
	(
		SELECT	tt.Attorney
				, tt.AttyAddress
				, tt.MnthNmbr
				, tt.DOS
				, tt.Completed
				, tt.AccountStatus
				, CASE WHEN tt.PercentDrop > 100 THEN 100 ELSE tt.PercentDrop END AS PercentDrop
				, CASE WHEN tt.AccountStatus = 'DROPPED' THEN
						CASE WHEN	tt.PercentDrop <= 10 THEN '0-10'
							 WHEN tt.PercentDrop BETWEEN 11 AND 20 THEN '11-20'
							 WHEN tt.PercentDrop BETWEEN 21 AND 40 THEN '21-40'
							 WHEN tt.PercentDrop BETWEEN 41 AND 60 THEN '41-60'
							 WHEN tt.PercentDrop BETWEEN 61 AND 80 THEN '61-80'
							 WHEN tt.PercentDrop BETWEEN 81 AND 99 THEN '81-99'
							 ELSE '100' END
				END AS DropRange
		FROM AllData_CTE tt
	)

SELECT	Attorney
		, AttyAddress
		, MnthNmbr
		, DOS
		, Completed
		, AccountStatus
		, PercentDrop
		, DropRange
FROM FinalData
WHERE @Range IS NULL OR DropRange = @Range

GO

GRANT EXECUTE ON Rpt.SSRSReceivedByAttorneyWC TO PUBLIC;

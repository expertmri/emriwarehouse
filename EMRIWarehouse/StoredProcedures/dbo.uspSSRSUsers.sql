USE EMRIWarehouse
GO

ALTER PROCEDURE dbo.uspSSRSUsers
AS

TRUNCATE TABLE dbo.SSRSUsers ;

INSERT INTO dbo.SSRSUsers (SF_UserName, domainUser, SF_UserId, UserName, isAdmin)
SELECT u.Username AS SF_UserName
		, CASE WHEN u.Username = 'michaela.bina@expertmri.com' THEN 'OSMD\michaela.robles'
			   ELSE 'OSMD\' + dbo.udfParseEmailUser(u.Username) END AS domainUser
		, u.Id AS SF_UserId
		, u.FirstName + ' ' + u.LastName AS UserName
		, CASE WHEN u.Id IN ('005f4000000eIY8AAM', '005f4000004HzHpAAK', '005f4000001l4C2AAI') THEN 1 ELSE 0 END AS isAdmin
FROM Integration.SF.Users u
UNION ALL 
SELECT 'Scott Lundy Dev' AS SF_UserName
		, 'SATURN\scott.lundy'
		, '9999ZZZscottLundy' AS SF_UserId
		, 'Scott Lundy Dev' AS UserName
		, 1 AS isAdmin

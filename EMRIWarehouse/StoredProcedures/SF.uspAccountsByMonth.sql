USE EMRIWarehouse
GO

ALTER PROCEDURE SF.uspAccountsByMonth
@FirstOfMonth date

AS

SET NOCOUNT ON;

--DECLARE @FirstOfMonth date = (SELECT dbo.udfFirstDayOfMonth(GETDATE()))
--DECLARE @FirstOfMonth date = (SELECT dbo.udfFirstDayOfMonth('11/1/2019'))

BEGIN TRAN
		
	UPDATE am
	SET IsActive = a.Active_Status__c
		, AccountStatus = a.Status__c
		, MonthDate = @FirstOfMonth
		, AccountStatusDetailed = sw.AccountStatus 
		, ScanCountMonthly = sw.ScanCountMonthly
		, InsertDate = GETDATE()
	FROM SF.AccountsByMonth am
	JOIN Integration.SF.Account a ON am.SF_AccountId = a.Id
	LEFT JOIN dbo.ScansByAccountWorkflow sw ON a.Id = sw.SF_AccountId AND @FirstOfMonth = sw.MonthYear
	WHERE am.MonthDate = @FirstOfMonth
			
	INSERT INTO SF.AccountsByMonth (SF_AccountId, IsActive, AccountStatus, MonthDate, AccountStatusDetailed, ScanCountMonthly)
		
	SELECT  Id
			, a.Active_Status__c
			, a.Status__c
			, @FirstOfMonth
			, sw.AccountStatus AS AccountStatusDetailed
			, sw.ScanCountMonthly
	FROM Integration.SF.Account a
	LEFT JOIN dbo.ScansByAccountWorkflow sw ON a.Id = sw.SF_AccountId AND @FirstOfMonth = sw.MonthYear
	LEFT JOIN SF.AccountsByMonth am ON am.SF_AccountId = a.Id AND am.MonthDate = @FirstOfMonth
	WHERE am.AccountsByMonthId IS NULL
		
COMMIT TRAN
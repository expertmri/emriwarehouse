USE EMRIWarehouse
GO

ALTER PROCEDURE Rpt.SSRSBonusConversionReport
AS

SET NOCOUNT ON;

DECLARE @StartDate date = '7/1/2019'

SELECT am.SF_AccountId
	   , a.AccountName
	   , a.AccountManager
	   , a.AccountHandler
	   , a.CurrentHandler
	   , am.AccountStatusDetailed AS AccountStatus
	   , am.ScanCountMonthly AS MonthlyScans
	   , a.LastActivityDate
	   , am.MonthDate
FROM SF.AccountsByMonth am
JOIN EMRIWarehouse.SF.Accounts a ON am.SF_AccountId = a.SF_AccountId
WHERE am.MonthDate >= @StartDate 
		AND am.AccountStatusDetailed in ('New', 'Restart')

GO

GRANT EXECUTE ON Rpt.SSRSBonusConversionReport TO PUBLIC;
		
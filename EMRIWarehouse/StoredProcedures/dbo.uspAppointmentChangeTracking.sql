USE EMRIWarehouse
GO

/*************************************************************************
	3/26/2019 MAJOR BUG: Must join on only the latest record
*************************************************************************/

ALTER PROCEDURE dbo.uspAppointmentChangeTracking
AS

INSERT INTO dbo.AppointmentChangeTracking
	(
		ApptId
      , CaseId
      , Account
      , AppointmentListType
      , BookedDate
      , DOS
      , DOI
      , Modality
      , Facility
      , ARClass
      , AppointmentStatusId
      , AppointmentStatus
      , MarketingReferral
      , ReferringDrId
      , ReferralCode
      , ReferringDr
      , ReferringGroup
      , InsCode
	  , InsName
      , RefAttyCode
      , ReferringAttorney
	  , OperatorCode
	  , Operator
	  , CancelDate
	  , CancelUser
	  , CancelReasonCode
	  , CancelReasonDescription
	  , RowCheckSum
	)

SELECT appt.ApptId
      , appt.CaseId
      , appt.Account
      , appt.AppointmentListType
      , appt.BookedDate
      , appt.DOS
      , appt.DOI
      , appt.Modality
      , appt.Facility
      , appt.ARClass
      , appt.AppointmentStatusId
      , appt.AppointmentStatus
      , appt.MarketingReferral
      , appt.ReferringDrId
      , appt.ReferralCode
      , appt.ReferringDr
      , appt.ReferringGroup
      , appt.InsCode
	  , appt.InsName
	  , appt.RefAttyCode
      , appt.ReferringAttorney
	  , appt.OperatorCode
	  , appt.Operator
	  , appt.CancelDate
	  , appt.CancelUser
	  , appt.CancelReasonCode
	  , appt.CancelReasonDescription
	  , CHECKSUM (	appt.CaseId
					, appt.Account
					, appt.AppointmentListType
					, appt.BookedDate
					, appt.DOS
					, appt.DOI
					, appt.Modality
					, appt.Facility
					, appt.ARClass
					, appt.AppointmentStatusId
					, appt.AppointmentStatus
					, appt.MarketingReferral
					, appt.ReferringDrId
					, appt.ReferralCode
					, appt.ReferringDr
					, appt.ReferringGroup
					, appt.InsCode
					, appt.InsName
					, appt.RefAttyCode
					, appt.ReferringAttorney
					, appt.OperatorCode
					, appt.Operator
					, appt.CancelDate
					, appt.CancelUser
					, appt.CancelReasonCode
					, appt.CancelReasonDescription
				)
FROM dbo.Appointments appt
LEFT JOIN (
			SELECT ApptId, RowCheckSum , AppointmentChangeTrackingID , ROW_NUMBER() OVER (PARTITION BY ApptId ORDER BY InsertDate desc) AS RN
			FROM dbo.AppointmentChangeTracking act 
			WHERE ApptId IS NOT NULL
			) act ON appt.ApptId = act.ApptId 
					AND RN = 1
					AND act.RowCheckSum = CHECKSUM (appt.CaseId
													, appt.Account
													, appt.AppointmentListType
													, appt.BookedDate
													, appt.DOS
													, appt.DOI
													, appt.Modality
													, appt.Facility
													, appt.ARClass
													, appt.AppointmentStatusId
													, appt.AppointmentStatus
													, appt.MarketingReferral
													, appt.ReferringDrId
													, appt.ReferralCode
													, appt.ReferringDr
													, appt.ReferringGroup
													, appt.InsCode
													, appt.InsName
													, appt.RefAttyCode
													, appt.ReferringAttorney
													, appt.OperatorCode
													, appt.Operator
													, appt.CancelDate
													, appt.CancelUser
													, appt.CancelReasonCode
													, appt.CancelReasonDescription
												)
WHERE act.AppointmentChangeTrackingID IS NULL
	  AND appt.OperatorCode IS NOT NULL

GO


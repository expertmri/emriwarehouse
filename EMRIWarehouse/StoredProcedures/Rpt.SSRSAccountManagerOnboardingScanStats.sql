USE EMRIWarehouse
GO

ALTER PROCEDURE Rpt.SSRSAccountManagerOnboardingScanStats
AS

CREATE TABLE #appointments (
		DOSMonthYear date
		--, DOSMonth int
		--, DOSYear int
		, ExamCount int
		--, SF_AccountId varchar(30)
		--, FirstExam date
		, ThirtyDays int
		, SixtyDays int
		, NinetyDays int
		, AccountManager varchar(255)
		, OnboardCount int
	)

--INSERT INTO #appointments (DOSMonthYear, DOSMonth, DOSYear, ExamCount, AccountManager, OnboardCount)
INSERT INTO #appointments (DOSMonthYear, ExamCount, AccountManager, OnboardCount)

SELECT	DOSMonthYear
		, SUM(ExamCount)
		, AccountManager
		, SUM(OnboardCount)
FROM (
		SELECT	CAST(DATEPART(MONTH, appt.DOS) AS varchar(2)) + '/1/'+ CAST(DATEPART(YEAR, appt.DOS) AS varchar(4)) AS DOSMonthYear
				, DATEPART(MONTH, appt.DOS) AS DOSMonth
				, DATEPART(YEAR, appt.DOS) AS DOSYear
				--, ar.ARGroup
				, COUNT(COALESCE (appt.ApptId, appt.Account)) as ExamCount
				--, c.SF_AccountId AS SF_AccountId
				--, ob.FirstExam
				, acct.AccountManager
				, SUM(CASE WHEN dbo.udfFirstDayOfMonth(ob.FirstExam) = CAST(DATEPART(MONTH, appt.DOS) AS varchar(2)) + '/1/'+ CAST(DATEPART(YEAR, appt.DOS) AS varchar(4))  THEN 1 ELSE 0 END) AS OnboardCount
		FROM EMRIWarehouse.dbo.Appointments appt
		LEFT JOIN Integration.dbo.Accounts c ON c.SourceSystemPk = appt.RefAttyCode and c.SourceTable = 'Attorney'
		JOIN (
				SELECT	c.SF_AccountId AS SF_AccountId
						, MIN(DOS) AS FirstExam
				FROM EMRIWarehouse.dbo.Appointments appt
				LEFT JOIN Integration.dbo.Accounts c ON c.SourceSystemPk = appt.RefAttyCode and c.SourceTable = 'Attorney'
				GROUP BY c.SF_AccountId
			  ) ob ON c.SF_AccountId = ob.SF_AccountId
		JOIN EMRIWarehouse.XRef.ARClassGroup ar ON appt.ARClass = ar.ARClass
		JOIN EMRIWarehouse.SF.Accounts acct ON acct.SF_AccountId = c.SF_AccountId
		WHERE AppointmentStatus = 'Exam Complete'
			  AND COALESCE(c.SF_AccountId, '') <> ''
			  --AND dbo.udfFirstDayOfMonth(appt.DOS) > DATEADD(MONTH, -6, dbo.udfFirstDayOfMonth(GETDATE()))
			  AND appt.Modality = 'MRI'
			  AND ar.IsNonRevenue = 0
			  AND ob.FirstExam >= DATEADD(MONTH, -6, dbo.udfFirstDayOfMonth(GETDATE()))
		GROUP BY CAST(DATEPART(MONTH, appt.DOS) AS varchar(2)) + '/1/'+ CAST(DATEPART(YEAR, appt.DOS) AS varchar(4))
				 , DATEPART(MONTH, appt.DOS)  
				 , DATEPART(YEAR, appt.DOS) 
				 --, ar.ARGroup
				 --, c.SF_AccountId
				 --, ob.FirstExam
				 , acct.AccountManager
		--HAVING ob.FirstExam >= DATEADD(MONTH, -6, dbo.udfFirstDayOfMonth(GETDATE()))
		UNION ALL
		SELECT	CAST(DATEPART(MONTH, appt.DOS) AS varchar(2)) + '/1/'+ CAST(DATEPART(YEAR, appt.DOS) AS varchar(4)) AS DOSMonthYear
				, DATEPART(MONTH, appt.DOS) AS DOSMonth
				, DATEPART(YEAR, appt.DOS) AS DOSYear
				--, ar.ARGroup
				, COUNT(COALESCE (appt.ApptId, appt.Account)) as ExamCount
				--, a.SF_AccountId AS SF_AccountId
				--, ob.FirstExam
				, acct.AccountManager
				, SUM(CASE WHEN dbo.udfFirstDayOfMonth(ob.FirstExam) = CAST(DATEPART(MONTH, appt.DOS) AS varchar(2)) + '/1/'+ CAST(DATEPART(YEAR, appt.DOS) AS varchar(4))  THEN 1 ELSE 0 END) AS OnboardCount
		FROM EMRIWarehouse.dbo.Appointments appt
		LEFT JOIN Integration.dbo.Accounts a ON a.SourceSystemPk = appt.ReferringDrId and a.SourceTable = 'Doctor'
		JOIN (
				SELECT	a.SF_AccountId AS SF_AccountId
						, MIN(DOS) AS FirstExam
				FROM EMRIWarehouse.dbo.Appointments appt
				--LEFT JOIN Integration.dbo.Accounts c ON c.SourceSystemPk = appt.RefAttyCode and c.SourceTable = 'Attorney'
				LEFT JOIN Integration.dbo.Accounts a ON a.SourceSystemPk = appt.ReferringDrId and a.SourceTable = 'Doctor'
				GROUP BY a.SF_AccountId
			  ) ob ON a.SF_AccountId = ob.SF_AccountId
		JOIN EMRIWarehouse.XRef.ARClassGroup ar ON appt.ARClass = ar.ARClass
		JOIN EMRIWarehouse.SF.Accounts acct ON acct.SF_AccountId = a.SF_AccountId
		WHERE AppointmentStatus = 'Exam Complete'
			  AND COALESCE(a.SF_AccountId, '') <> ''
			  --AND dbo.udfFirstDayOfMonth(appt.DOS) > DATEADD(MONTH, -6, dbo.udfFirstDayOfMonth(GETDATE()))
			  AND appt.Modality = 'MRI'
			  AND ar.IsNonRevenue = 0
			  AND ob.FirstExam >= DATEADD(MONTH, -6, dbo.udfFirstDayOfMonth(GETDATE()))
		GROUP BY CAST(DATEPART(MONTH, appt.DOS) AS varchar(2)) + '/1/'+ CAST(DATEPART(YEAR, appt.DOS) AS varchar(4))
				 , DATEPART(MONTH, appt.DOS)  
				 , DATEPART(YEAR, appt.DOS) 
				 --, ar.ARGroup
				 --, a.SF_AccountId
				 --, ob.FirstExam
				 , acct.AccountManager
		--HAVING ob.FirstExam >= DATEADD(MONTH, -6, dbo.udfFirstDayOfMonth(GETDATE()))
) t1
GROUP BY DOSMonthYear
		, AccountManager

;WITH pastExamsCTE AS (
						SELECT	appt.DOSMonthYear
								--, appt.SF_AccountId
								, appt.AccountManager
								, COALESCE(t.ExamCount, 0) AS ThirtyDays
								, COALESCE(tt.ExamCount, 0) + COALESCE(t.ExamCount, 0)  AS SixtyDays
								, COALESCE(ttt.ExamCount, 0) + COALESCE(tt.ExamCount, 0) + COALESCE(t.ExamCount, 0)  AS NinetyDays
						FROM #appointments appt
						LEFT JOIN	(
										SELECT	DOSMonthYear
												--, SF_AccountId
												, AccountManager
												, ExamCount
										FROM #appointments
									) t ON dbo.udfFirstDayLastMonth(appt.DOSMonthYear) = t.DOSMonthYear AND appt.AccountManager = t.AccountManager
						LEFT JOIN	(
									SELECT	DOSMonthYear
											--, SF_AccountId
											, AccountManager
											, ExamCount
									FROM #appointments
								) tt ON DATEADD(MONTH, -2, appt.DOSMonthYear) = tt.DOSMonthYear AND appt.AccountManager = tt.AccountManager
						LEFT JOIN	(
									SELECT	DOSMonthYear
											--, SF_AccountId
											, AccountManager
											, ExamCount
									FROM #appointments
								) ttt ON DATEADD(MONTH, -3, appt.DOSMonthYear) = ttt.DOSMonthYear AND appt.AccountManager = ttt.AccountManager
					)

UPDATE appt
SET ThirtyDays = t.ThirtyDays
	, SixtyDays = t.SixtyDays
	, NinetyDays = t.NinetyDays
FROM #appointments appt
JOIN pastExamsCTE t ON appt.AccountManager = t.AccountManager AND appt.DOSMonthYear = t.DOSMonthYear

--;WITH onboardCTE AS (
--		SELECT a.AccountManager
--				, a.DOSMonthYear
--				, COUNT(a.SF_AccountId) AS rcdCnt
--		FROM #appointments a
--		WHERE a.DOSMonthYear = dbo.udfFirstDayOfMonth(a.FirstExam)
--		GROUP BY a.AccountManager
--				, a.DOSMonthYear

--	)

--UPDATE appt
--SET OnboardCount = rcdCnt
--FROM #appointments appt
--JOIN onboardCTE t ON appt.AccountManager = t.AccountManager AND appt.DOSMonthYear = t.DOSMonthYear

SELECT * 
FROM #appointments appt
WHERE DOSMonthYear >= DATEADD(MONTH, -3, dbo.udfFirstDayOfMonth(GETDATE()))

GO

GRANT EXECUTE ON Rpt.SSRSAccountManagerOnboardingScanStats TO PUBLIC;

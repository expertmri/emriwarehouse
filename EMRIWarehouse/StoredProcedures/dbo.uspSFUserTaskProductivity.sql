USE EMRIWarehouse
GO

/* 1/28/2019 SLUNDY 
This proc is not being used currently, it was written due to an unclear report spec. Keeping for now because it may become relevant or have useful code.
*/

ALTER PROCEDURE dbo.uspSFUserTaskProductivity
AS

SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ COMMITTED;

TRUNCATE TABLE SF.UserTaskProductivity;

INSERT INTO SF.UserTaskProductivity (SF_AccountId, Task, TaskId, Activity, Reason, TaskStatus, CompletedBy, AssignedTo, Priority, CreatedDate, CompletedDate)
SELECT	t.AccountId
		, t.Task_Type__c
		, t.Id
		, t.Activity_Type__c
		, t.Reason__c
		, t.[Status]
		, u. FirstName + ' ' + u.LastName
		, u2. FirstName + ' ' + u2.LastName AS AssignedTo
		, t.Priority
		, t.CreatedDate
		, t.Date_Completed__c 
FROM Integration.SF.Tasks t
LEFT JOIN Integration.SF.Users u ON COALESCE(t.Completed_by__c, t.OwnerId) = u.Id
LEFT JOIN Integration.SF.Users u2 ON t.OwnerId = u2.Id
WHERE COALESCE(Outcome__c, 'None') <> 'Expired' --EM-505

/*removed where clause 8/14/2019 as it is no longer needed*/
--WHERE t.LastModifiedById NOT IN ('005f4000000e5liAAA', '005f4000000doagAAA') --AutomatedClean and Ian

USE EMRIWarehouse
GO

ALTER PROCEDURE Rpt.SSRSBadCaseID
AS

--TRUNCATE TABLE Integration.Stg.AppointmentsList

--INSERT INTO Integration.Stg.AppointmentsList
--	(
--		ID
--	   , Entity
--	   , Account
--	   , PatName
--	   , ListType
--	   , Resource
--	   , Slot
--	   , SubSlot
--	   , Address1
--	   , Address2
--	   , City
--	   , State
--	   , Zip
--	   , ApptTime
--	   , ApptDuration
--	   , Resource1
--	   , Resource2
--	   , Resource3
--	   , Resource4
--	   , Resource5
--	   , ApptCode
--	   , ApptNote
--	   , BCodeInfo
--	   , CancelCode
--	   , CancelUser
--	   , CaseId
--	   , CheckIn
--	   --,Comment
--	   , DateCreated
--	   , DateModified
--	   , InsCode
--	   , InsPolicy
--	   , Location
--	   , NoShow
--	   , Override
--	   , PatDob
--	   , PatHomePhone
--	   , PatMedicalRecord
--	   , PatSSN
--	   , PatSex
--	   , PatWorkPhone
--	   , PatientEmail
--	   , PatientFax
--	   , PatientMobilePhone
--	   , PatientNote
--	   , PlaceCode
--	   , ProcCode
--	   , ProcDesc
--	   --,Procedures
--	   , RefCode
--	   , RefName
--	   , RefPhone
--	   , ReschCancelDate
--	   , SequenceNo
--	   , SqlDateCreated
--	   , SqlDateModified
--	   , SqlReschCancelDate
--	   , Status
--	   , TimeCreated
--	   , TimeModified
--	   , UserCreated
--	   , UserFlags
--	   , UserModified
--	)

SELECT ID
	   , Entity
	   , CAST(Account as int) as Account
	   , PatName
	   , ListType
	   , Resource
	   , Slot
	   , SubSlot
	   , Address1
	   , Address2
	   , City
	   , State
	   , Zip
	   , ApptTime
	   , ApptDuration
	   , Resource1
	   , Resource2
	   , Resource3
	   , Resource4
	   , Resource5
	   , ApptCode
	   , ApptNote
	   , BCodeInfo
	   , CancelCode
	   , CancelUser
	   , CaseId
	   , CheckIn
	   --,Comment
	   , DateCreated
	   , DateModified
	   , InsCode
	   , InsPolicy
	   , Location
	   , NoShow
	   , Override
	   , PatDob
	   , PatHomePhone
	   , PatMedicalRecord
	   , PatSSN
	   , PatSex
	   , PatWorkPhone
	   , PatientEmail
	   , PatientFax
	   , PatientMobilePhone
	   , PatientNote
	   , PlaceCode
	   , ProcCode
	   , ProcDesc
	   --,Procedures
	   , RefCode
	   , RefName
	   , RefPhone
	   , ReschCancelDate
	   , SequenceNo
	   , SqlDateCreated
	   , SqlDateModified
	   , SqlReschCancelDate
	   , Status
	   , TimeCreated
	   , TimeModified
	   , UserCreated
	   , UserFlags
	   , UserModified
FROM ADS..Adsc_MedicsPremier.AppointmentsList
WHERE LEFT(CaseId,1) not in ('1','2','3')

--SELECT * 
--FROM Integration.Stg.AppointmentsList 
--WHERE LEFT(CaseId,1) not in ('1','2','3')

GO

GRANT EXECUTE ON Rpt.SSRSBadCaseID TO PUBLIC;
USE EMRIWarehouse
GO 

ALTER PROCEDURE Rpt.SSRSSFUserTaskAging
@UserName varchar(255)

AS

SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


SELECT	u.AssignedTo AS UserName
		, Task
		, Activity
		, CASE WHEN CreatedDate >= CAST(DATEADD(DAY, -30, GETDATE()) AS date) THEN 1 ELSE 0 END AS lessThanThirty
		, CASE WHEN CreatedDate < CAST(DATEADD(DAY, -30, GETDATE()) AS date) AND CreatedDate >= CAST(DATEADD(DAY, -60, GETDATE()) AS date) THEN 1 ELSE 0 END AS lessThanSixty
		, CASE WHEN CreatedDate < CAST(DATEADD(DAY, -60, GETDATE()) AS date) AND CreatedDate >= CAST(DATEADD(DAY, -90, GETDATE()) AS date) THEN 1 ELSE 0 END AS lessThanNinety
		, CASE WHEN CreatedDate < CAST(DATEADD(DAY, -90, GETDATE()) AS date) THEN 1 ELSE 0 END AS moreThanNinety
		, CreatedDate
FROM SF.UserTaskProductivity u 
JOIN dbo.SSRSUsers s ON (COALESCE(u.CompletedBy, u.AssignedTo) = s.UserName  OR s.isAdmin = 1)
WHERE TaskStatus = 'Open'
	  --AND (u.AssignedTo = @UserName OR @UserName = 'ALL')
	  AND s.domainUser = @UserName 

GO

GRANT EXECUTE ON Rpt.SSRSSFUserTaskAging TO PUBLIC;


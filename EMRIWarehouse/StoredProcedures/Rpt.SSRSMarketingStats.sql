USE [EMRIWarehouse]
GO
/****** Object:  StoredProcedure [Rpt].[SSRSMarketingStats]    Script Date: 10/24/2017 8:53:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE Rpt.SSRSMarketingStats
@Limited int = 0

--The parameter is now hardcoded due to change of user needs of returning different data depending upon the Daily Scans report, but left functionality in place for now

AS

DECLARE @StartDate date
SET @StartDate = (SELECT CAST('1/1/' + CAST(DATEPART(YEAR, GETDATE()) AS varchar(10)) AS date)) 

SELECT	MarketingReferral 
		, [1] AS Jan
		, [2] AS Feb
		, [3] AS Mar
		, [4] AS Apr
		, [5] AS May
		, [6] AS Jun
		, [7] AS Jul
		, [8] AS Aug
		, [9] AS Sep
		, [10] AS Oct
		, [11] AS Nov
		, [12] AS Dec
		, [1]+[2]+[3]+[4]+[5]+[6]+[7]+[8]+[9]+[10]+[11]+[12] AS GrandTotal
INTO #stats
FROM
		(
			SELECT	DATEPART(MONTH, DOS) AS DateMonth 
					, r.MarketingReferral
					, ApptId
			FROM dbo.Appointments r
			WHERE MarketingReferral IS NOT NULL
					AND Modality = 'MRI'
					AND AppointmentStatus = 'Exam Complete'
					AND DOS >= @StartDate
					AND MarketingReferral IN ('IS INJURY INSTITUTE', 'SM SOCIAL MEDIA', 'TV TV AD', 'NF NEW FLEX GROUP', 'S2 SOCIAL MEDIA GETEXPERT')
	  
		) tt
PIVOT (COUNT(ApptId) FOR DateMonth IN ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12])) as t

IF @Limited = 0
SELECT * FROM #stats

IF @Limited = 1
SELECT * FROM #stats WHERE MarketingReferral IN ('SM SOCIAL MEDIA', 'TV TV AD', 'S2 SOCIAL MEDIA GETEXPERT')

GO
GRANT EXECUTE ON [Rpt].[SSRSMarketingStats] TO [public] AS [dbo]
GO

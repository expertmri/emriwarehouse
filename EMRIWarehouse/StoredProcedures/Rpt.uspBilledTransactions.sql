USE [EMRIWarehouse]
GO
/****** Object:  StoredProcedure [Rpt].[uspBilledTransactions]    Script Date: 10/24/2017 8:53:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
*********************************************************************************************************
 Name:			Rpt.uspBilledTransactions
 Date:			6/25/2017           
 Author:		Scott Lundy
 Description:	Sum Balance and Fee Schedule by Factoring Company and Pledge Status
 Called by:
 Modified:
*********************************************************************************************************
*/

CREATE PROCEDURE [Rpt].[uspBilledTransactions]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED


--select * from BilledTransactions
--241 nationwide
--Key001, 002, 003 KeyHealth
--195 HFS, ARType PFAC
--367 MIS
--ALL Codes ARType %WC for WorkComp
--PILATT AND PILIEN Atticus

SELECT	tt.FactoringCompany
		, tt.PledgeStatus
		, tt.ServiceDate
		, SUM(tt.Balance) AS 'Sum of Balance'
		, SUM(tt.EstimatedFeeSchd) AS 'Sum of Est FeeSchd'
FROM (
		SELECT CASE WHEN bt.InsCode1 = '241' THEN 'NationWide'
					WHEN bt.InsCode1 in ('KEY001', 'KEY002', 'KEY003') THEN 'KEYHEALTH'
					WHEN bt.InsCode1 = '195' THEN 'HFS'
					WHEN bt.InsCode1 = '367' THEN 'MIS'
					WHEN bt.ARType LIKE '%WC' THEN 'WorkComp'
					WHEN bt.ARType IN ('PILATT', 'PILIEN') THEN 'ATTICUS'
					END AS 'FactoringCompany'
			   , bt.Balance
			   , bt.EstimatedFeeSchd
			   , ps.PledgeStatus
			   , CONVERT(varchar(3), bt.ServiceDate, 100) AS ServiceDate
			   , bt.insCode1 --testing only, remove for PROD
			   , bt.ARType --testing only, remove for PROD
		FROM dbo.BilledTransactions bt
		JOIN dbo.PledgeStatus ps ON bt.PledgeStatusId = ps.PledgeStatusId
	) tt
GROUP BY tt.FactoringCompany
		, tt.PledgeStatus
		, tt.ServiceDate
ORDER BY tt.FactoringCompany
		, tt.PledgeStatus
		, tt.ServiceDate
GO

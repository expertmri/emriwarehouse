USE EMRIWarehouse
GO

ALTER PROCEDURE Rpt.SSRSSchedulingRequest
	@SchedulingRequestId bigint = NULL
	, @SchedulingStatus varchar(255) = NULL
	, @Account varchar(20) = NULL
	, @Action varchar(20) = NULL
AS

SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ COMMITTED;

IF @Action = 'Update'
BEGIN
	IF @SchedulingStatus IN ('Created in ADS')
	BEGIN
		--Only archive if there is a previous status before setting as processed
		IF EXISTS(SELECT * FROM dbo.SchedulingRequest WHERE SchedulingRequestId = @SchedulingRequestId AND SchedulingStatus is null)
		BEGIN
			UPDATE dbo.SchedulingRequest
			SET SchedulingStatus = @SchedulingStatus
				, Account = @Account
				, ModifiedDate = GETDATE()
				, IsProcessed = 1
			WHERE SchedulingRequestId = @SchedulingRequestId
		END
		ELSE
		BEGIN
			INSERT INTO Archive.SchedulingRequest (
					SchedulingRequestId
					, WebFormRequestId
					, FirstName
					, LastName
					, DOB
					, Email
					, Phone
					, Modality
					, ReferralSource
					, CallSummary
					, Account
					, SchedulingStatus
					, IsProcessed
					, CreateDate
					, ModifiedDate
					, InsertDate
				)

			SELECT  SchedulingRequestId
					, WebFormRequestId
					, FirstName
					, LastName
					, DOB
					, Email
					, Phone
					, Modality
					, ReferralSource
					, CallSummary
					, Account
					, SchedulingStatus
					, IsProcessed
					, CreateDate
					, ModifiedDate
					, GETDATE()
			FROM dbo.SchedulingRequest
			WHERE SchedulingRequestId = @SchedulingRequestId
		END

	END
	ELSE 
	BEGIN
		INSERT INTO Archive.SchedulingRequest (
				SchedulingRequestId
				, WebFormRequestId
				, FirstName
				, LastName
				, DOB
				, Email
				, Phone
				, Modality
				, ReferralSource
				, CallSummary
				, Account
				, SchedulingStatus
				, IsProcessed
				, CreateDate
				, ModifiedDate
				, InsertDate
			)

		SELECT  SchedulingRequestId
				, WebFormRequestId
				, FirstName
				, LastName
				, DOB
				, Email
				, Phone
				, Modality
				, ReferralSource
				, CallSummary
				, Account
				, SchedulingStatus
				, IsProcessed
				, CreateDate
				, ModifiedDate
				, GETDATE()
		FROM dbo.SchedulingRequest
		WHERE SchedulingRequestId = @SchedulingRequestId
		
		UPDATE dbo.SchedulingRequest
		SET SchedulingStatus = @SchedulingStatus
			, Account = @Account
			, ModifiedDate = GETDATE()
			, IsProcessed = 0
		WHERE SchedulingRequestId = @SchedulingRequestId
	END

	SELECT	SchedulingRequestId
			, FirstName
			, LastName
			, DOB
			, Email
			, Phone
			, CallSummary
			, CreateDate
			, SchedulingStatus
			, ModifiedDate
	FROM dbo.SchedulingRequest
	WHERE IsProcessed = 0


END

SELECT	SchedulingRequestId
		, FirstName
		, LastName
		, DOB
		, Email
		, Phone
		, CallSummary
		, CreateDate
		, SchedulingStatus
		, ModifiedDate
FROM dbo.SchedulingRequest
WHERE IsProcessed = 0
	  AND (@SchedulingRequestId = SchedulingRequestId OR @SchedulingRequestId IS NULL)

GO

GRANT EXECUTE ON Rpt.SSRSSchedulingRequest TO PUBLIC;


USE EMRIWarehouse
GO

ALTER PROCEDURE Rpt.SSRSSFTaskProductivity
@StartDate date = null
, @EndDate date = null

AS

SELECT COUNT(t.SF_AccountId) RcdCount
	   , COALESCE(t.CompletedBy, t.AssignedTo) AS UserName
	   , t.Task
FROM SF.UserTaskProductivity t
WHERE (CompletedDate BETWEEN @StartDate AND @EndDate OR @StartDate IS NULL)
GROUP BY COALESCE(t.CompletedBy, t.AssignedTo)
		 , t.Task

GO

GRANT EXECUTE ON Rpt.SSRSSFTaskProductivity  TO PUBLIC

USE EMRIWarehouse
GO

ALTER PROCEDURE dbo.uspSchedulingRequestCreate
AS

SET NOCOUNT ON;

BEGIN TRY
	BEGIN TRANSACTION

		DECLARE @MaxRequestId bigint
				, @CurrentRequestId bigint
				, @jsonRequest nvarchar(max)
				, @FirstName varchar(255)
				, @LastName varchar(255)
				, @DOB date
				, @Email varchar(255)
				, @Phone varchar(255)
				, @Modality varchar(255)
				, @RefSource varchar(255)
				, @CallSummary varchar(4000)
		SET @CurrentRequestId = (SELECT MIN(WebFormRequestId) FROM WebAPI.dbo.WebFormRequest WHERE IsError = 0)
		SET @MaxRequestId = (SELECT MAX(WebFormRequestId) FROM WebAPI.dbo.WebFormRequest)

		WHILE @CurrentRequestId <= @MaxRequestId

		BEGIN

			SET @jsonRequest = (SELECT RequestPayload FROM WebAPI.dbo.WebFormRequest WHERE WebFormRequestId = @CurrentRequestId)
			SELECT	@FirstName = FirstName
					, @LastName = LastName
					, @DOB = CAST(DOB as date)
					, @EMail = EMail
					, @Phone = Phone
					, @Modality = Modality
					, @RefSource = RefSource
					, @CallSummary = CallSummary

			FROM OPENJSON(@jsonRequest)
			WITH (
					firstname  nvarchar(255) 'strict $.firstname'
					, lastname nvarchar(255) '$.lastname'
					, DOB nvarchar(255) '$.dob'
					, Email nvarchar(255) '$.email'
					, Phone nvarchar(255) '$.phone'
					, Modality nvarchar(255) '$.modality'
					, RefSource nvarchar(255) '$.refSource'
					, CallSummary nvarchar(255) '$.summary'

				) 

			INSERT INTO dbo.SchedulingRequest (
				 WebFormRequestId
				, FirstName
				, LastName
				, DOB
				, Email
				, Phone
				, Modality
				, ReferralSource
				, CallSummary
				, CreateDate
			)

			SELECT	@CurrentRequestId
					, @FirstName
					, @LastName
					, @DOB
					, @EMail
					, @Phone
					, @Modality
					, @RefSource
					, @CallSummary
					, GETDATE()

			EXEC WebAPI.Archive.uspWebFormRequest 
				@WebFormRequestId = @CurrentRequestId

			SET @CurrentRequestId = (SELECT MIN(WebFormRequestId) FROM WebAPI.dbo.WebFormRequest WHERE IsError = 0)
		END

	COMMIT

END TRY
BEGIN CATCH
	DECLARE @Error varchar(max) = ERROR_MESSAGE();

	IF @@TRANCOUNT > 0
	ROLLBACK

	UPDATE WebAPI.dbo.WebFormRequest
	SET IsError = 1
		, ErrorReason =  'dbo.uspSchedulingRequestCreate ' + @Error
	WHERE WebFormRequestId = @CurrentRequestId

END CATCH




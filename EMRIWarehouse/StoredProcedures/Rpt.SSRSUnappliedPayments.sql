USE EMRIWarehouse
GO

ALTER PROCEDURE Rpt.SSRSUnappliedPayments
AS

SELECT	PostingDate
		, Entity
		, AccountNo
		, PatientNo
		, Type
		, Department
		, InverseFinanceKey
		, Reference
		, TransactionAmount/100 AS TransactionAmount
		, UndistributedAmount/100 AS UndistributedAmount
		, Provider
		, Referral
		, PlaceOfService
		, ClassOfCode
		, Operator
		, Message
		, TransactionDate
		, MarkedAsDeleted
		, CheckNo
		, FinancialCode
		, TransactionNo
		, TransactionSqlDate
FROM Integration.Stg.Credits 
WHERE TransNo01 is null

GO

GRANT EXECUTE ON Rpt.SSRSUnappliedPayments TO PUBLIC;
GO



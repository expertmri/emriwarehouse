USE EMRIWarehouse
GO

ALTER PROCEDURE Rpt.SSRSCompletedMissingCase
AS

SELECT	Account
		, ApptId
		, BookedDate
		, DOS
		, Modality
		, AppointmentStatus
FROM dbo.Appointments
WHERE CaseId IS NULL
AND AppointmentStatusId = 4 --Exam Complete

GO

GRANT EXECUTE ON Rpt.SSRSCompletedMissingCase TO PUBLIC;
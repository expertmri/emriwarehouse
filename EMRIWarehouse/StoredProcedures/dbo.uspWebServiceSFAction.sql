USE EMRIWarehouse
GO

ALTER PROCEDURE dbo.uspWebServiceSFAction
AS


DECLARE @currentRecord bigint
		, @lastRecord bigint
		, @requestAction varchar(255)
		, @requestPayload nvarchar(max)
		, @SFAccountId varchar(255)
		, @SFContactId varchar(255)
	
SET @lastRecord = (SELECT MAX(SalesForceRequestId) + 1 FROM WebAPI.dbo.SalesForceRequest WHERE IsError = 0)
SET @currentRecord = (SELECT MIN(SalesForceRequestId) FROM WebAPI.dbo.SalesForceRequest WHERE IsError = 0)


WHILE @currentRecord <= @lastRecord
BEGIN
	BEGIN TRY
		SET @requestAction = (SELECT RequestAction FROM WebAPI.dbo.SalesForceRequest WHERE SalesForceRequestId = @currentRecord)
		SET @requestPayload = (SELECT RequestPayload FROM WebAPI.dbo.SalesForceRequest WHERE SalesForceRequestId = @currentRecord)

		IF @requestAction = 'AccountMerge'
		BEGIN
			SELECT @SFAccountId = SFAccountId
			FROM OPENJSON(@requestPayload)
			WITH (SFAccountId nvarchar(max) 'strict $.MergedSFAccountId'
					) 
			--Merge Accounts
			EXEC SF.uspAccountMerge	@jsonRequest = @requestPayload
			--This is in case the merge proc finds an error, we want to skip and move on
			IF (SELECT IsError FROM WebAPI.dbo.SalesForceRequest WHERE SalesForceRequestId = @currentRecord) = 1
				BEGIN
					SET @currentRecord = (SELECT MIN(SalesForceRequestId) FROM WebAPI.dbo.SalesForceRequest WHERE IsError = 0)
				END
			--ELSE
				/*
				BEGIN
					--Export on the scan data for only the newly merged account
					EXEC Integration.SF.uspScansByAccountExport @SFAccountId = @SFAccountId
					--Push the scan data to SalesForce
					EXEC xp_cmdshell 'DTEXEC.EXE /F "F:\SSIS\Integration\AccountScansExportToSalesForce.dtsx" /CONFIGFILE "F:\SSIS\Config\AccountScansExport.dtsConfig" /REPORTING V'
					--Update the Account Workflow in SF
					EXEC Integration.Outbound.uspAccountCategorizationWorkflow @SFAccountId = @SFAccountId
					EXEC xp_cmdshell 'DTEXEC.EXE /F "F:\SSIS\Integration\AccountCategorizationWorkflow.dtsx" /CONFIGFILE "F:\SSIS\Config\AccountCategorizationWorkflow.dtsConfig" /REPORTING V'
				END
				*/
		END
	
		IF @requestAction = 'ContactMerge'
		BEGIN
			SELECT @SFContactId = SFContactId
			FROM OPENJSON(@requestPayload)
			WITH (SFContactId nvarchar(max) 'strict $.MergedSFContactId'
					) 
			--Merge Contacts
			EXEC SF.uspContactMerge	@jsonRequest = @requestPayload
			--This is in case the merge proc finds an error, we want to skip and move on
			IF (SELECT IsError FROM WebAPI.dbo.SalesForceRequest WHERE SalesForceRequestId = @currentRecord) = 1
				BEGIN
					SET @currentRecord = (SELECT MIN(SalesForceRequestId) FROM WebAPI.dbo.SalesForceRequest WHERE IsError = 0)
				END
			--ELSE
				--BEGIN
				--	--Export on the scan data for only the newly merged contacts
				--	EXEC Integration.SF.uspScansByContactExport	@SFContactId = @SFContactId
				--	--Push the scan data to SalesForce
				--	EXEC xp_cmdshell 'DTEXEC.EXE /F "F:\SSIS\Integration\ContactScansExportToSalesForce.dtsx" /CONFIGFILE "F:\SSIS\Config\ContactScansExport.dtsConfig" /REPORTING V'
				--END
		END
		
		IF (SELECT IsError FROM WebAPI.dbo.SalesForceRequest WHERE SalesForceRequestId = @currentRecord) = 0
		BEGIN
			EXEC WebAPI.Archive.uspSalesForceRequest @SalesForceRequestId = @currentRecord

			DELETE FROM WebAPI.dbo.SalesForceRequest WHERE SalesForceRequestId = @currentRecord
		END

		SET @currentRecord = (SELECT MIN(SalesForceRequestId) FROM WebAPI.dbo.SalesForceRequest WHERE IsError = 0)
	END TRY

	BEGIN CATCH
		DECLARE @ErrorMessage varchar(max) = ERROR_MESSAGE();
		
		UPDATE WebAPI.dbo.SalesForceRequest
		SET IsError = 1
			, ErrorReason = 'dbo.uspWebServiceSFAction; SQL Error: ' + @ErrorMessage
		WHERE SalesForceRequestId = @currentRecord

		SET @currentRecord = (SELECT MIN(SalesForceRequestId) FROM WebAPI.dbo.SalesForceRequest WHERE IsError = 0)

	END CATCH

END

GO

GRANT EXECUTE ON dbo.uspWebServiceSFAction TO public
GO

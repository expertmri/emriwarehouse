USE [WebAPI]
GO

/****** Object:  StoredProcedure [dbo].[uspWebFormRequest]    Script Date: 12/3/2018 2:24:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE dbo.uspWebFormRequest

@postArray nvarchar(max)
AS

BEGIN TRY
	INSERT INTO dbo.WebFormRequest (
		RequestAction
		, RequestPayload
		)

	SELECT RequestAction
			, @postArray
	FROM OPENJSON(@postArray)
	WITH (--WebFormRequestToken nvarchar(max) 'strict $.WebFormRequestToken',  
			RequestAction nvarchar(max) 'strict $.RequestAction'
			) 
END TRY

BEGIN CATCH
	IF ISJSON(@postArray) = 0
		BEGIN
			INSERT INTO dbo.WebFormRequest (
					RequestPayload
					, IsError
					, ErrorReason
				)

			SELECT	@postArray
					, 1
					, 'Invalid JSON Format'
		END
	ELSE
		BEGIN
			INSERT INTO dbo.WebFormRequest (
					RequestPayload
					, IsError
					, ErrorReason
				)

			SELECT	@postArray
					, 1
					, ERROR_MESSAGE()
		END
END CATCH	
	
GO

GRANT EXECUTE ON [dbo].[uspWebFormRequest] TO [SalesForceAPI] AS [dbo]
GO



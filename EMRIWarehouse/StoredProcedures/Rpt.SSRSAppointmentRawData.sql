USE [EMRIWarehouse]
GO
/****** Object:  StoredProcedure [Rpt].[SSRSCompletedScansMarketingReport]    Script Date: 4/11/2018 12:07:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE Rpt.SSRSAppointmentRawData

AS

SELECT apt.ApptId
	   , al.PatName AS PatientName
       , apt.CaseId
       , apt.Account
       --, apt.AppointmentListType
       , apt.BookedDate
       , apt.DOS
       , apt.DOI
       , apt.Modality
       , apt.Facility
       , apt.ARClass
       --, apt.AppointmentStatusId
       , apt.AppointmentStatus
       --, apt.AppointmentCode
       --, apt.AppointmentDescription
       --, apt.MarketingReferral
       --, apt.ReferringDrId
       --, apt.ReferralCode
       , apt.ReferringDr
       , apt.ReferringGroup
	   , dr.Address1 AS PhysicianAddress1
       , dr.Address2 AS PhysicianAddress2
	   , dr.City AS PhysicianCity
       , dr.State AS PhysicianState
       , dr.ZipCode AS PhysicianZip
       , dr.Phone AS PhysicianPhone
	   , dr.FaxNumber AS PhysicianFax
	   , dr.NPI
       , apt.InsCode
       , apt.InsName
	   , ins.Address1 AS InsuranceAddress1
       , ins.Address2 AS InsuranceAddress2
       , ins.City AS InsuranceCity
       , ins.State AS InsuranceState
       , ins.ZipCode AS InsuranceZip
       , ins.Phone AS InsurancePhone
       , ins.Fax AS InsuranceFax
       --, apt.RefAttyCode
       , apt.ReferringAttorney
	   , att.Address1 AS AttorneyAddress1
       , att.Address2 AS AttorneyAddress2
	   , att.City AS AttorneyCity
       , att.State AS AttorneyState
       , att.ZipCode AS AttorneyZip
       , att.Phone AS AttorneyPhone
	   , att.Fax AS AttorneyFax
       --, apt.Resource1
       --, apt.OperatorCode
       --, apt.Operator
       , apt.CancelDate
       --, apt.CancelUser
       , apt.CancelReasonCode
       , apt.CancelReasonDescription
       , apt.IsDeletedAppt
  FROM dbo.Appointments apt
  LEFT JOIN Integration.Stg.Insurances ins ON apt.InsCode = ins.Code
  LEFT JOIN Integration.Stg.Attorneys att ON apt.RefAttyCode = att.Code
  LEFT JOIN Integration.Stg.ReferringPhysicians dr ON apt.ReferralCode = dr.Code
  LEFT JOIN Integration.Stg.AppointmentsList al ON al.SequenceNo = apt.ApptId
GO

GRANT EXECUTE ON Rpt.SSRSAppointmentRawData TO [public] 

GO


USE EMRIWarehouse
GO
/****** Object:  StoredProcedure Rpt.SSRSProcessingStatsSalesForce    Script Date: 5/29/2018 4:09:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE Rpt.SSRSProcessingStatsSalesForce
	@IsAccount bit = 0
	, @StartDate date = null
	, @EndDate date = null
AS

IF @StartDate IS NULL
SET @StartDate = (DATEADD(DAY, -30, GETDATE()))

IF @EndDate IS NULL
SET @EndDate = (SELECT GETDATE())

IF @IsAccount = 1
BEGIN
	SELECT DATEADD(DAY, -1, ProcessingDate) AS ProcessingDate
		   , Created
		   , Modified
	FROM	(
				SELECT	COALESCE(CAST(a.createDate AS date), tt.ModifiedDate) AS ProcessingDate 
						, COUNT(a.accountId) AS Created
						, COALESCE(tt.Modified, 0) AS Modified
		
				FROM Integration.dbo.Accounts a
				FULL JOIN (	
							SELECT COUNT(accountId) AS Modified
								   , CAST(ModifiedDate AS date) AS ModifiedDate 
							FROM Integration.dbo.Accounts 
							GROUP BY CAST(ModifiedDate AS date)
						) tt ON CAST(a.CreateDate AS date) = tt.ModifiedDate
				GROUP BY CAST(a.CreateDate AS date)
						 , tt.Modified
						 , tt.ModifiedDate
			) t1
	WHERE t1.ProcessingDate BETWEEN @StartDate AND @EndDate
END

IF @IsAccount = 0
BEGIN
	SELECT DATEADD(DAY, -1, ProcessingDate) AS ProcessingDate
		   , Created
		   , Modified
	FROM	(
				SELECT	COALESCE(CAST(a.createDate AS date), tt.ModifiedDate) AS ProcessingDate 
						, COUNT(a.ContactId) AS Created
						, COALESCE(tt.Modified, 0) AS Modified
		
				FROM Integration.dbo.Contacts a
				FULL JOIN (	
							SELECT COUNT(ContactId) AS Modified
								   , CAST(ModifiedDate AS date) AS ModifiedDate 
							FROM Integration.dbo.Contacts 
							GROUP BY CAST(ModifiedDate AS date)
						) tt ON CAST(a.CreateDate AS date) = tt.ModifiedDate
				GROUP BY CAST(a.CreateDate AS date)
						 , tt.Modified
						 , tt.ModifiedDate
			) t1
	WHERE t1.ProcessingDate BETWEEN @StartDate AND @EndDate
END

GO
GRANT EXECUTE ON Rpt.SSRSProcessingStatsSalesForce TO public AS dbo
GO

USE EMRIWarehouse
GO

CREATE PROCEDURE dbo.uspGetModality
AS

SELECT DISTINCT Modality
FROM dbo.Appointments

GO

GRANT EXECUTE ON dbo.uspGetModality TO PUBLIC;
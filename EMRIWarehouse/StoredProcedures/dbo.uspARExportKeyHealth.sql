USE [EMRIWarehouse]
GO
/****** Object:  StoredProcedure [dbo].[uspARExportKeyHealth]    Script Date: 10/24/2017 8:53:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[uspARExportKeyHealth]
AS

DECLARE @nullJoin varchar(10) 
SET @nullJoin = 'zzz|}*$'

SELECT	bt.ApptId AS ApptNo
		, '' AS Status --Missing Att in the sample, need to find out what this is
		, bt.PostingDate AS PostDate
		, bt.Plc AS Facility
		, kh.VendorName AS VendorShortName
		, bt.Account AS PatientId
		, dbo.udfParseLastName(bt.PatientName) AS PatientLastName
		, dbo.udfParseFirstName(bt.PatientName) AS PatientFirstName
		, '' AS PatientMName
		, bt.BirthDate
		, bt.PatientAddress AS PatientAddress1
		, '' AS PatientAddress2
		, bt.PatientCity 
		, bt.PatientState
		, bt.PatientZip
		, bt.PatientPhone 
		, bt.DOI AS PatientDateOfInjury
		, '' AS PatientGender
		, '' AS PatientEmail
		, bt.AttorneyName AS LawFirmName
		, bt.AttorneyPhone AS LawFirmPhone
		, bt.AttorneyFax  AS LawFirmFax 
		, bt.AttorneyAddress1 AS LawFirmAddress1 
		, bt.AttorneyAddress2 AS LawFirmAddress2 
		, bt.AttorneyCity  AS LawFirmCity 
		, bt.AttorneyState AS LawFirmState
		, bt.AttorneyZip  AS LawFirmZip 
		, '' AttorneyFirstName
		, '' AttorneyMiddleName
		, '' AttorneyLastName
		, bt.ApptId AS ExamNo
		, bt.Modality AS 'Procedure'
		, bt.ApptCPTCode AS CPTCode
		, bt.ApptCPTDescription AS CPTDescription
		, bt.DOS as DateOfService
		, '' AS ReferralDate
		, bt.RefName
		, CASE WHEN bt.RefName LIKE '%,%' THEN dbo.udfParseFirstName(bt.RefName) ELSE bt.RefName END AS ReferringDrFirstName
		, CASE WHEN bt.RefName LIKE '%,%' THEN dbo.udfParseLastName(bt.RefName) ELSE '' END AS ReferringDrLastName
		, '' AS 'RefDrAddress 1'
		, '' AS 'RefDrAddress 2'
		, '' AS 'RefDrCity'
		, '' AS 'RefDrState'	
		, '' AS 'RefDrZip'
		, '' AS 'RefDrPhone'
		, '' AS 'RefDrFax'
		, '' AS 'RefDrEmail'
		, '' AS 'Classification'
		, COALESCE(fs.Rate, (Balance * .355)) AS GrossCharge
		, bt.Diag AS ICD10
FROM dbo.BilledTransactions bt
LEFT JOIN XRef.FeeSchedule fs ON bt.ARClass = fs.ARClass 
							  AND bt.ApptCPTCode = fs.code
							  AND COALESCE(bt.InsCode1, @nullJoin) = COALESCE(fs.Payor, @nullJoin)
							  AND bt.DOS BETWEEN fs.EffectiveStartDate AND fs.EffectiveEndDate
LEFT JOIN XRef.KeyHealthVendorNames kh ON bt.Plc = kh.Facility
WHERE bt.ARClass = ('PILIEN')
	  AND COALESCE(bt.InsCode1, '') NOT IN ('195', '241', '367', 'Key001', 'key002', 'key003')

GO

USE EMRIWarehouse
GO

ALTER PROCEDURE Rpt.SSRSScheduledVsCompleted 
@StartDate date = null
, @EndDate date = null
, @Modality varchar(20) = null

AS

IF @StartDate IS NULL
BEGIN

	SET @StartDate = (SELECT dbo.udfFirstDayOfMonth(GETDATE()))
	SET @EndDate = GETDATE()

END

;WITH Appt_CTE AS (
					SELECT DISTINCT	apt.ApptId
							, act.DOS
							, apt.Facility
							, apt.OperatorCode
							, apt.Operator
							, act.AppointmentStatus
							, stat.IsScheduled
							, CASE WHEN act.AppointmentStatusId = 4 THEN 1 ELSE 0 END AS ExamComplete
							, ROW_NUMBER() OVER (PARTITION BY act.DOS, act.ApptId
												 ORDER BY CASE act.AppointmentStatusId WHEN 4 THEN 1
																					   WHEN stat.IsScheduled THEN 2
																					   ELSE 3 END) AS RN
					FROM dbo.Appointments apt
					JOIN XRef.AppointmentStatus stat ON apt.AppointmentStatusId = stat.AppointmentStatusId
					JOIN dbo.AppointmentChangeTracking act ON apt.ApptId = Act.ApptId
					WHERE act.DOS >= @StartDate 
							AND (@Modality IS NULL OR apt.Modality = @Modality)
							AND act.DOS < @EndDate
							AND COALESCE(apt.Resource1,'') NOT IN ('INTAKE', 'RFA', 'Underwriting', 'PPW')
							AND apt.Facility <> 'INTAKE'

					)

SELECT	ApptId
		, DOS
		, Facility
		, OperatorCode
		, Operator
		, AppointmentStatus
		, IsScheduled
		, ExamComplete
FROM Appt_CTE 
WHERE RN = 1


GO

GRANT EXECUTE ON Rpt.SSRSScheduledVsCompleted TO PUBLIC;
GO

--select * from dbo.AppointmentChangeTracking where Account =  17048 order by DOS
--select * from dbo.Appointments where Account =  17048 order by DOS
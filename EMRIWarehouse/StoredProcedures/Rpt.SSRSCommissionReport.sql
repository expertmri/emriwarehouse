USE [EMRIWarehouse]
GO
/****** Object:  StoredProcedure [Rpt].[SSRSCommissionReport]    Script Date: 10/11/2019 10:34:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [Rpt].[SSRSCommissionReport]
@UserName varchar(255)
, @StartDate date


AS
 
--DECLARE @StartDate date = null
DECLARE @EndDate date = null

SET @EndDate = DATEADD(DAY, -1, DATEADD(MONTH, 3, @StartDate))

DECLARE @AccountsReq int = 5
DECLARE @ScansReqQtr int = 4
DECLARE @ScansReqMonth int = 3
DECLARE @OfficePresentations int = 6

;WITH accounts AS (
		SELECT	m.SF_AccountId
				, a.AccountName
				, a.SalesRepresentative
				, m.AccountStatusDetailed
				, wf.ScanCountMonthly
				, wf.MonthYear AS MonthDate
				, 1 AS isStatusChanged
				, m.MonthDate AS StatusChangedDate
		FROM ScansByAccountWorkflow wf 
		JOIN SF.Accounts a ON wf.SF_AccountId = a.SF_AccountId
		JOIN SF.AccountsByMonth m ON wf.SF_AccountId = m.SF_AccountId
		WHERE m.AccountStatusDetailed IN ('Restart', 'New' )
			  AND wf.MonthYear BETWEEN @StartDate AND @EndDate
			  AND wf.MonthYear = m.MonthDate
			  --AND wf.SF_AccountId = '001f4000019Mu8ZAAS'
	)
, lastScan AS (
		SELECT MIN(DOS) AS lastScanDate
			   , s.SF_AccountId
		FROM dbo.Scans s 
		JOIN accounts a ON a.SF_AccountId = s.SF_AccountId
		WHERE DOS > a.MonthDate
		GROUP BY s.SF_AccountId
	)
, completedTask AS ( 
	SELECT	u.SF_AccountId
			, u.CompletedBy
			, MAX(completedDate) AS LastCompletedDate 
	FROM SF.UserTaskProductivity u
	JOIN lastScan l ON l.SF_AccountId = u.SF_AccountId
	WHERE TaskStatus = 'Completed'
		  AND u.CompletedDate < l.lastScanDate
	GROUP BY u.SF_AccountId
			 , u.CompletedBy
	)

SELECT	a.SF_AccountId
		, a.AccountName
		, a.SalesRepresentative
		, a.AccountStatusDetailed
		, a.ScanCountMonthly
		, a.MonthDate
		, c.LastCompletedDate
		, lastScanDate AS StatusChangedDate
		, 1 as isStatusChanged
INTO #accounts
FROM accounts a
LEFT JOIN completedTask c ON a.SF_AccountId = c.SF_AccountId AND a.SalesRepresentative = c.CompletedBy
JOIN lastScan l ON a.SF_AccountId = l.SF_AccountId

INSERT INTO #accounts
SELECT	wf.SF_AccountId
		, a.AccountName
		, a.SalesRepresentative
		, a.AccountStatusDetailed
		, wf.ScanCountMonthly
		, wf.MonthYear AS MonthDate
		, a.LastCompletedDate
		, a.StatusChangedDate
		, 0 AS isStatusChanged
FROM ScansByAccountWorkflow wf 
LEFT JOIN #accounts a ON wf.SF_AccountId = a.SF_AccountId 
WHERE wf.MonthYear > a.MonthDate 
	  AND wf.MonthYear <= @EndDate


;WITH presentations AS (
		SELECT	a.SalesRepresentative
				--, AccountStatusDetailed
				, COUNT(SF_AccountId) AS TotalOfficePresent
		FROM Integration.SF.Events e 
		JOIN SF.Accounts a ON a.SF_AccountId = e.AccountId
		WHERE e.ActivityDate BETWEEN @StartDate AND @EndDate
				AND Activity_Type__c = 'Office Presentation'
		GROUP BY SalesRepresentative
)

, finalData AS (
		SELECT	a.SF_AccountId
				, a.AccountName
				, a.SalesRepresentative
				, a.AccountStatusDetailed
				, a.ScanCountMonthly
				, a.MonthDate
				, a.LastCompletedDate
				, a.isStatusChanged
				, a.StatusChangedDate
				--, ts.TotalScans
				, COALESCE(p.TotalOfficePresent, 0) AS TotalOfficePresent
				, CASE WHEN ts.TotalScans >= @ScansReqQtr OR ms.MaxMonthlyScans >= @ScansReqMonth  THEN 'Yes'
						--WHEN a.ScanCountMonthly >= @ScansReqMonth THEN 'Yes' --9/4/2019 duplications
						ELSE 'No' END AS ScanCommission
				, COALESCE(tsk.TaskCommission, 'No') AS TaskCommission
				, CASE WHEN ta.TotalAccounts >= @AccountsReq THEN 'Yes' ELSE 'No' END AS AccountsCommission
				, CASE WHEN p.TotalOfficePresent >= @OfficePresentations THEN 'Yes' ELSE 'No' END AS PresentationCommission
				, CASE WHEN (ts.TotalScans >= @ScansReqQtr OR ms.MaxMonthlyScans >= @ScansReqMonth) 
								AND COALESCE(tsk.TaskCommission, 'No') = 'Yes' 
								AND ta.TotalAccounts >= @AccountsReq
								AND p.TotalOfficePresent >= @OfficePresentations
							THEN 'Yes' 
							ELSE 'No' END AS PayCommission
		FROM #accounts a
		JOIN (
				SELECT	SF_AccountId
						, MAX(ScanCountMonthly) AS MaxMonthlyScans
				FROM #accounts
				GROUP BY SF_AccountId
			) ms ON a.SF_AccountId = ms.SF_AccountId
		JOIN (
				SELECT	SF_AccountId
						, SUM(ScanCountMonthly) AS TotalScans
				FROM #accounts
				GROUP BY SF_AccountId
			) ts ON a.SF_AccountId = ts.SF_AccountId
		LEFT JOIN	(
						SELECT	SF_AccountId
								, 'Yes' AS TaskCommission
						FROM #accounts a
						WHERE isStatusChanged = 1 
								AND a.StatusChangedDate <= DATEADD(DAY, 120, a.LastCompletedDate)
					) tsk ON a.SF_AccountId = tsk.SF_AccountId
		JOIN (
				SELECT	SalesRepresentative
						, AccountStatusDetailed
						, COUNT(SF_AccountId) AS TotalAccounts
				FROM #accounts
				GROUP BY SalesRepresentative
							, AccountStatusDetailed
			) ta ON a.SalesRepresentative = ta.SalesRepresentative AND ta.AccountStatusDetailed = a.AccountStatusDetailed
		LEFT JOIN presentations p ON a.SalesRepresentative = p.SalesRepresentative
			
)

--using Min date as a join condition otherwise the data returns dollars for every month which sums up in the report and we just want one commission for an account, not monthly
SELECT	fd.*
		, com.TotalCommission
FROM finalData fd
JOIN dbo.SSRSUsers s ON (fd.SalesRepresentative = s.UserName  OR s.isAdmin = 1)
LEFT JOIN (
		SELECT SF_AccountId
				, MAX(CASE WHEN AccountStatusDetailed = 'Restart' AND PayCommission = 'Yes' THEN 50
					   WHEN AccountStatusDetailed = 'New' AND PayCommission = 'Yes' THEN 100
					   ELSE 0 END) AS TotalCommission
				, MIN(MonthDate) AS MonthDate
		FROM finalData
		GROUP BY SF_AccountId
	) com ON fd.SF_AccountId = com.SF_AccountId AND fd.MonthDate = com.MonthDate
WHERE s.domainUser = @UserName 

GO

GRANT EXECUTE ON Rpt.SSRSCommissionReport  TO PUBLIC;


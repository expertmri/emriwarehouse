USE EMRIWarehouse
GO

ALTER PROCEDURE Rpt.SSRSRetentionByDoctor 
	@StartDate date
	, @EndDate date
	, @Modality varchar(20)
AS

--DECLARE @StartDate date
--		, @EndDate date

--SET @StartDate = (SELECT CAST('1/1/' + CAST(DATEPART(YEAR, GETDATE()) AS varchar(10)) AS date))
--SET @EndDate = (SELECT CAST(GETDATE() AS date))

SELECT	appt.ReferringDr
		, COUNT(COALESCE (appt.ApptId, appt.Account)) as Referral
		, COALESCE(comp.Completed, 0) AS Completed
	    , appt.Modality
		, COALESCE(CAST(comp.Completed AS decimal) / COUNT(COALESCE (appt.ApptId, appt.Account)), 0) AS RetentionRatio
FROM dbo.Appointments appt
LEFT JOIN XRef.ARClassGroup arg ON appt.ARClass = arg.ARClass
LEFT JOIN	(
				SELECT	COUNT(appt.ApptId) AS Completed
						, appt.ReferringDr
						, appt.Modality
				FROM dbo.Appointments appt
				LEFT JOIN XRef.ARClassGroup arg ON appt.ARClass = arg.ARClass
				WHERE AppointmentStatus = 'Exam Complete'
					  AND COALESCE(arg.IsNonRevenue, 0) = 0 
					  AND appt.ARClass IS NOT NULL
					  AND (appt.Modality = @Modality OR @Modality IS NULL)
					  AND appt.BookedDate BETWEEN @StartDate AND @EndDate
				GROUP BY appt.ReferringDr 
						 , appt.Modality
			) comp ON appt.ReferringDr = comp.ReferringDr AND comp.Modality = appt.Modality
WHERE appt.BookedDate BETWEEN @StartDate AND @EndDate
		AND COALESCE(arg.IsNonRevenue, 0) = 0 AND appt.ARClass IS NOT NULL --slundy we don't want NULL ARClass, however we do not want to filter out records that have not yet been added to the XRef table yet
		AND (appt.Modality = @Modality OR @Modality IS NULL)
GROUP BY appt.ReferringDr
		 , appt.Modality
		 , comp.Completed

GO

GRANT EXECUTE ON Rpt.SSRSRetentionByDoctor TO PUBLIC;
GO
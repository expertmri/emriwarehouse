USE EMRIWarehouse
GO

ALTER PROCEDURE Rpt.SSRSSFTaskDetails
AS

SELECT	t.TaskId
		, t.CreatedDate
		, t.AssignedTo
		, a.SF_AccountId
		, a.AccountName
		, t.Priority
		, t.Task
		, t.Activity
		, t.Reason
		, t.TaskStatus
		, CASE WHEN t.TaskStatus = 'Completed' THEN t.CompletedBy END AS CompletedBy
		, CASE WHEN t.TaskStatus = 'Completed' THEN CompletedDate END AS CompletedDate
		, t.DueDate
--FROM Integration.SF.Tasks t
FROM SF.UserTaskProductivity t
JOIN SF.Accounts a ON t.SF_AccountId = a.SF_AccountId
--LEFT JOIN Integration.SF.Users u ON t.CreatedById = u.Id
--LEFT JOIN Integration.SF.Users u2 ON t.LastModifiedById = u2.Id
--LEFT JOIN Integration.SF.Users u3 ON t.OwnerId = u3.Id
WHERE t.Task IS NOT NULL

GO

GRANT EXECUTE ON Rpt.SSRSSFTaskDetails TO PUBLIC;


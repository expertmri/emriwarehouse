USE [EMRIWarehouse]
GO

/****** Object:  StoredProcedure [Rpt].[SSRSCurrentOnBooks]    Script Date: 3/19/2018 11:14:00 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE Rpt.SSRSCurrentOnBooksByResource
@Modality varchar(20)

AS

SELECT	Resource1 AS 'Site'
		, apt.Modality
		, apt.DOS
		, COUNT(apt.ApptId) AS RcdCnt
FROM dbo.Appointments apt
JOIN XRef.AppointmentStatus stat ON apt.AppointmentStatusId = stat.AppointmentStatusId
WHERE stat.IsOnBooks = 1
		AND apt.Modality = @Modality
		AND COALESCE(apt.Resource1,'') NOT IN ('INTAKE', 'RFA', 'Underwriting', 'PPW')

GROUP BY apt.Resource1 
		, apt.Modality
		, apt.DOS
		
GO

GRANT EXECUTE ON [Rpt].[SSRSCurrentOnBooksByResource] TO [public] AS [dbo]
GO
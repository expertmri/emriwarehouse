USE [EMRIWarehouse]
GO
/****** Object:  StoredProcedure [Rpt].[SSRSPendingToBeScheduled_V2]    Script Date: 10/24/2017 8:53:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [Rpt].[SSRSPendingStatus]
AS


SELECT * 
FROM dbo.appointments appt
--JOIN XRef.AppointmentStatus stat ON appt.AppointmentStatusId = stat.AppointmentStatusId
WHERE appt.AppointmentStatusId = 6 --'Pending Status' 
	  AND DATEDIFF(DAY, DOS, CAST(GETDATE() as date)) > 1
	  AND appt.Facility <> 'INTAKE' --added per saba 12/19/2017


GO
GRANT EXECUTE ON [Rpt].[SSRSPendingStatus] TO [public] AS [dbo]
GO

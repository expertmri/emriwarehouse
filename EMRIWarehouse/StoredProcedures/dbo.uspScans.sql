USE EMRIWarehouse
GO

ALTER PROCEDURE dbo.uspScans
AS

/*
This is the base scan data for all reporting

*/

BEGIN TRAN

	TRUNCATE TABLE dbo.Scans

	INSERT INTO dbo.Scans (
		SF_AccountId
		, ApptId
		, Account
		, BookedDate
		, DOS
		, Modality
		, Facility
		, AccountType
		, IsWorkComp
		, IsNonRevenue
		, IsPersonalInjury
		, ARClass
		, ARGroup
		, DirectorGroup
	)

	SELECT  c.SF_AccountId
			, apt.ApptId
			, apt.Account
			, apt.BookedDate
			, apt.DOS
			, apt.Modality
			, apt.Facility
			, 'Doctor' AS AccountType
			, ar.IsWorkComp
			, ar.IsNonRevenue
			, ar.IsPersonalInjury
			, ar.ARClass
			, ar.ARGroup
			, ar.DirectorGroup
	FROM dbo.Appointments apt
	JOIN Integration.dbo.Accounts c ON apt.ReferralCode = c.SourceSystemPk AND c.SourceTable = 'Doctor'
	LEFT JOIN XRef.ARClassGroup ar ON apt.ARClass = ar.ARClass
	WHERE apt.AppointmentStatusId = 4 --Exam Complete

	UNION ALL

	SELECT  c.SF_AccountId
			, apt.ApptId
			, apt.Account
			, apt.BookedDate
			, apt.DOS
			, apt.Modality
			, apt.Facility
			, 'Attorney' AS AccountType
			, ar.IsWorkComp
			, ar.IsNonRevenue
			, ar.IsPersonalInjury
			, ar.ARClass
			, ar.ARGroup
			, ar.DirectorGroup
	FROM dbo.Appointments apt
	JOIN Integration.dbo.Accounts c ON apt.RefAttyCode = c.SourceSystemPk AND c.SourceTable = 'Attorney'
	LEFT JOIN XRef.ARClassGroup ar ON apt.ARClass = ar.ARClass
	WHERE apt.AppointmentStatusId = 4 --Exam Complete

	EXEC dbo.uspScansByAccountWorkflow
	
		
COMMIT
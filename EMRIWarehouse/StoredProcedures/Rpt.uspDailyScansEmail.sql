USE [EMRIWarehouse]
GO
/****** Object:  StoredProcedure [Rpt].[uspDailyScansEmail]    Script Date: 10/24/2017 8:53:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [Rpt].[uspDailyScansEmail] --1
	@Debug INT = 0
AS

SET NOCOUNT ON

/*Email and CSS variables*/
DECLARE @subject1 nvarchar(255)
	   , @EmailBody nvarchar(max)
	   , @EmailBody1 nvarchar(max)
	   , @EmailBody2 nvarchar(max)
	   , @EmailBody3 nvarchar(max)
	   , @EmailBody4 nvarchar(max)
	   , @CSSRowHeader nvarchar(max)
	   , @CSSRowHeader1 nvarchar(max)
	   , @CSSRowHeader2 nvarchar(max)
	   , @CSSRowHeader3 nvarchar(max)
	   , @CSSRowHeader4 nvarchar(max)
	   , @CSSRowHeaderCenter nvarchar(max)
	   , @CSSRowHeaderCenter1 nvarchar(max)
	   , @CSSRowHeaderCenter2 nvarchar(max)
	   , @CSSRowHeaderCenter3 nvarchar(max)
	   , @CSSRowHeaderCenter4 nvarchar(max)
	   , @emailTo nvarchar(MAX)
	   --, @ReportDates nvarchar(50)
	   --, @StartWeekDate date
	   --, @EndWeekDate date
	   --, @StartMonthDate date
	   --, @EndMonthDate datetime

SET @subject1 = 'Daily Scans Report ' + CONVERT(varchar(10), GETDATE(), 101)
IF @Debug = 1
	BEGIN
		SET @emailTo = 'scott.lundy@expertMri.net'
	END

IF @Debug = 0
	BEGIN
		SET @emailTo = 'scott.lundy@expertMri.net; sattar.mir@expertmri.net; saba.ejaz@expertmri.net; Iris.arzate@expertmri.com'
	END

/*-------------------------------------------------------------------------------------------------------------
											SCANS RECEIVED
-------------------------------------------------------------------------------------------------------------*/

DECLARE @Calendar TABLE (
		CDate date
		, CMonth int
		, Daily int
		, MTD int
		, YTD int
	)
	
DECLARE @CurrentDate date = '1/1/2017'
WHILE @CurrentDate <= GETDATE()

	BEGIN
		INSERT INTO @Calendar (CDate, CMonth)
		SELECT @CurrentDate, DATEPART(Month, @CurrentDate)

		SET @CurrentDate = DATEADD(day, 1, @CurrentDate)
	END

SELECT COUNT(Account) as DailyCount
	   , r.BookedDate
INTO #Daily
FROM dbo.Appointments r
WHERE ARClass not in ('CPR', 'RE', 'SOH', 'CCH')
GROUP BY r.BookedDate

UPDATE c
SET Daily = COALESCE(d.DailyCount, 0)
FROM @Calendar c
LEFT JOIN #Daily d ON d.BookedDate = c.CDate

SET @CurrentDate = (SELECT MIN(CDate) FROM @Calendar)
DECLARE @MaxDate date = (SELECT MAX(CDate) FROM @Calendar)

WHILE @CurrentDate <= @MaxDate

BEGIN
	UPDATE @Calendar
	SET MTD = (	SELECT SUM(Daily) 
				FROM @Calendar 
				WHERE CDate <= @CurrentDate 
					  AND DATEPART(month, @CurrentDate) = CMonth
			   )
		, YTD = (	SELECT SUM(Daily)  
					FROM @Calendar 
					WHERE CDate <= @CurrentDate
				)
	WHERE CDate = @CurrentDate

	SET @CurrentDate = DATEADD(day, 1, @CurrentDate)
END


SELECT	r.BookedDate as DOS
		, COUNT(COALESCE (r.ApptId, r.Account)) as DailyCount
	    , c.MTD
		, c.YTD
	    , r.Modality
INTO #Piv
FROM @Calendar c
LEFT JOIN dbo.Appointments r ON c.CDate = r.BookedDate 
AND ARClass not in ('CPR', 'RE', 'SOH', 'CCH')
AND c.Daily <> 0
GROUP BY r.BookedDate
		 , c.MTD
		 , c.YTD
	     , r.Modality


SELECT	CONVERT(varchar(10), DOS, 101) AS Received
		, COALESCE([ARTH], 0) AS ARTH
		, COALESCE([CR], 0) AS CR
		, COALESCE([CT], 0) AS CT
		, COALESCE([MRI], 0) AS MRI
		, MTD AS MTD
		, YTD AS YTD
INTO #Scans
FROM	(
			SELECT	DOS
					, DailyCount
					, MTD
					, YTD
					, Modality
			FROM #Piv
		) tt
PIVOT (SUM(DailyCount) FOR Modality IN ([CR], [ARTH], [MRI], [CT])) as t
WHERE DATEPART(MONTH, DOS) = CASE DATEPART(DAY, GETDATE()) WHEN 1 THEN (DATEPART(MONTH, GETDATE()) - 1)
			ELSE DATEPART(MONTH, GETDATE()) END
--WHERE DATEPART(MONTH, DOS) = DATEPART(MONTH, GETDATE())
UNION 
SELECT	'Total' AS Received
		, SUM(COALESCE([ARTH], 0)) AS ARTH
		, SUM(COALESCE([CR], 0)) AS CR
		, SUM(COALESCE([CT], 0)) AS CT
		, SUM(COALESCE([MRI], 0)) AS MRI
		, MAX(MTD) AS MTD
		, MAX(YTD) AS YTD
FROM	(
			SELECT	DOS
					, DailyCount
					, MTD
					, YTD
					, Modality
			FROM #Piv
		) tt
PIVOT (SUM(DailyCount) FOR Modality IN ([CR], [ARTH], [MRI], [CT])) as t
WHERE DATEPART(MONTH, DOS) = CASE DATEPART(DAY, GETDATE()) WHEN 1 THEN (DATEPART(MONTH, GETDATE()) - 1)
			ELSE DATEPART(MONTH, GETDATE()) END
ORDER BY Received

SELECT @EmailBody = CAST((

							SELECT	Received AS 'td', ''
									, ARTH AS 'tdcenter', ''
									, CR AS 'tdcenter', ''
									, CT AS 'tdcenter', ''
									, MRI AS 'tdcenter', ''
									, MTD AS 'tdcenter', ''
									, YTD AS 'tdcenter', ''
							FROM #Scans
						 FOR XML PATH('tr'), ELEMENTS) AS NVARCHAR(MAX))


-- ============== ADD Row Styling ==================================
	SET @EmailBody = REPLACE(@EmailBody, '<td>', '<td style="padding 0.25rem;font: 12px arial, sans-serif;;text-align: left;border: 1px solid #ccc;background: #d8ecf3;">') --#D6EBFF
	SET @EmailBody = REPLACE(@EmailBody, '<tdcenter>', '<td style="padding: 0.25rem;font: 12px arial, sans-serif;;text-align: center;border: 1px solid #ccc;background: #d8ecf3;">')
	SET @CSSRowHeader = '<th style="padding: 0.25rem;font: 15px arial, sans-serif;text-align: left;border: 1px solid #ccc;background: #ADD8E6;">'
	SET @CSSRowHeaderCenter = '<th style="padding: 0.25rem;font: 15px arial, sans-serif;text-align: center;border: 1px solid #ccc;background: #ADD8E6;">'
-- ============== Add rest of the HTML Body ========================

	SET @EmailBody = 
	'<html>
	<head>
	  <title></title>
	</head>
	<body onload="_l=''t'';" style="font: normal small/1.1 sans-serif;">
	<b><font color="black">Scans Received per Modality</font></b><br><br>


	  <table class="orange" style="border-collapse: collapse;width: 40%;"> 
	<thead>
	<tr> 
		' + @CSSRowHeader + 'Received</th> 
		' + @CSSRowHeaderCenter + 'ARTH</th>
		' + @CSSRowHeaderCenter + 'CR</th>
		' + @CSSRowHeaderCenter + 'CT</th>
		' + @CSSRowHeaderCenter + 'MRI</th>
		' + @CSSRowHeaderCenter + 'MTD</th>
		' + @CSSRowHeaderCenter + 'YTD</th>
	</tr> 
	</thead> 
	<tbody>
	' 
	+ ISNULL(@EmailBody,'')
	+ 
	'</tbody> 
	</table>
	</body>
	</html>';

/*-------------------------------------------------------------------------------------------------------------
								EXAM COMPLETE AND NO SHOW SECTIONS
-------------------------------------------------------------------------------------------------------------*/
SELECT	r.DOS
		--, r.Facility
		, r.ReferringDr
		, r.AppointmentStatus
		, r.AppointmentListType
		, COUNT(COALESCE (r.ApptId, r.Account)) as DailyCount
	    , r.Modality
INTO #Piv2
FROM dbo.Appointments r 
WHERE ARClass not in ('CPR', 'RE', 'SOH', 'CCH')
GROUP BY r.DOS
		--, r.Facility
		, r.ReferringDr
		, r.AppointmentStatus
		, r.AppointmentListType
	    , r.Modality

SELECT	CONVERT(varchar(10), DOS, 101) AS Received
		--, Facility AS 'td', ''
		, ReferringDr 
		, COALESCE([ARTH], 0) AS ARTH
		, COALESCE([CR], 0) AS CR
		, COALESCE([CT], 0) AS CT
		, COALESCE([MRI], 0) AS MRI
		, COALESCE([ARTH], 0) + COALESCE([CR], 0) + COALESCE([CT], 0) + COALESCE([MRI], 0) AS GrandTotal
INTO #Complete
FROM	(
			SELECT	DOS
					--, Facility
					, ReferringDr
					, DailyCount
					, Modality
			FROM #Piv2
			WHERE AppointmentStatus = 'Exam Complete'
		) tt
PIVOT (SUM(DailyCount) FOR Modality IN ([CR], [ARTH], [MRI], [CT])) as t
WHERE DATEPART(MONTH, DOS) = CASE DATEPART(DAY, GETDATE()) WHEN 1 THEN (DATEPART(MONTH, GETDATE()) - 1)
			ELSE DATEPART(MONTH, GETDATE()) END
UNION
SELECT	'Total' AS Received
		, '' AS ReferringDr
		, SUM(COALESCE([ARTH], 0)) AS ARTH
		, SUM(COALESCE([CR], 0)) AS CR
		, SUM(COALESCE([CT], 0)) AS CT
		, SUM(COALESCE([MRI], 0)) AS MRI
		, SUM(COALESCE([ARTH], 0) + COALESCE([CR], 0) + COALESCE([CT], 0) + COALESCE([MRI], 0)) AS GrandTotal
FROM	(
			SELECT	DOS
					, DailyCount
					, Modality
			FROM #Piv2
			WHERE AppointmentStatus = 'Exam Complete'
		) tt
PIVOT (SUM(DailyCount) FOR Modality IN ([CR], [ARTH], [MRI], [CT])) as t
WHERE DATEPART(MONTH, DOS) = CASE DATEPART(DAY, GETDATE()) WHEN 1 THEN (DATEPART(MONTH, GETDATE()) - 1)
			ELSE DATEPART(MONTH, GETDATE()) END


----------------------------------------------EXAM COMPLETE REPORT-----------------------------------------------

SELECT @EmailBody1 = CAST((
							SELECT	Received AS 'td', ''
									--, Facility AS 'td', ''
									, ReferringDr AS 'td', ''
									, ARTH AS 'tdcenter', ''
									, CR AS 'tdcenter', ''
									, CT AS 'tdcenter', ''
									, MRI AS 'tdcenter', ''
									, GrandTotal AS 'tdcenter', ''
							FROM #Complete
							ORDER BY Received, ReferringDr
					 FOR XML PATH('tr'), ELEMENTS) AS NVARCHAR(MAX))

-- ============== ADD Row Styling ==================================
	SET @EmailBody1 = REPLACE(@EmailBody1, '<td>', '<td style="padding 0.25rem;font: 12px arial, sans-serif;;text-align: left;border: 1px solid #ccc;background: #d8ecf3;">') --#D6EBFF
	SET @EmailBody1 = REPLACE(@EmailBody1, '<tdcenter>', '<td style="padding: 0.25rem;font: 12px arial, sans-serif;;text-align: center;border: 1px solid #ccc;background: #d8ecf3;">')
	SET @CSSRowHeader1 = '<th style="padding: 0.25rem;font: 15px arial, sans-serif;text-align: left;border: 1px solid #ccc;background: #ADD8E6;">'
	SET @CSSRowHeaderCenter1 = '<th style="padding: 0.25rem;font: 15px arial, sans-serif;text-align: center;border: 1px solid #ccc;background: #ADD8E6;">'
-- ============== Add rest of the HTML Body ========================

	SET @EmailBody = @EmailBody +
	'<html>
	<head>
	  <title></title>
	</head>
	<body onload="_l=''t'';" style="font: normal small/1.1 sans-serif;">
	<br><br><b><font color="black">Exam Completed</font></b><br><br>


	  <table class="orange" style="border-collapse: collapse;width: 50%;"> 
	<thead>
	<tr> 
		' + @CSSRowHeader1 + 'DOS</th> 
		' + @CSSRowHeader1 + 'Referring Doctor</th> 
		' + @CSSRowHeaderCenter1 + 'ARTH</th>
		' + @CSSRowHeaderCenter1 + 'CR</th>
		' + @CSSRowHeaderCenter1 + 'CT</th>
		' + @CSSRowHeaderCenter1 + 'MRI</th>
		' + @CSSRowHeaderCenter1 + 'Grand Total</th>
	</tr> 
	</thead> 
	<tbody>
	' 
	+ ISNULL(@EmailBody1,'')
	+ 
	'</tbody> 
	</table>
	</body>
	</html>';


-------------------------------------------NO SHOW QUERY----------------------------------------------------------

	SELECT	CONVERT(varchar(10), DOS, 101) AS Received
			--, Facility AS 'td', ''
			, ReferringDr 
			, COALESCE([ARTH], 0) AS ARTH
			, COALESCE([CR], 0) AS CR
			, COALESCE([CT], 0) AS CT
			, COALESCE([MRI], 0) AS MRI
			, COALESCE([ARTH], 0) + COALESCE([CR], 0) + COALESCE([CT], 0) + COALESCE([MRI], 0) AS GrandTotal
	INTO #NoShow
	FROM	(
				SELECT	DOS
						--, Facility
						, ReferringDr 
						, DailyCount
						, Modality
				FROM #Piv2
				WHERE AppointmentStatus = 'No Show'
			) tt
	PIVOT (SUM(DailyCount) FOR Modality IN ([CR], [ARTH], [MRI], [CT])) as t
	WHERE DATEPART(MONTH, DOS) = CASE DATEPART(DAY, GETDATE()) WHEN 1 THEN (DATEPART(MONTH, GETDATE()) - 1)
				ELSE DATEPART(MONTH, GETDATE()) END
	UNION
	SELECT	'Total' AS Received
			, '' AS ReferringDr
			, SUM(COALESCE([ARTH], 0)) AS ARTH
			, SUM(COALESCE([CR], 0)) AS CR
			, SUM(COALESCE([CT], 0)) AS CT
			, SUM(COALESCE([MRI], 0)) AS MRI
			, SUM(COALESCE([ARTH], 0) + COALESCE([CR], 0) + COALESCE([CT], 0) + COALESCE([MRI], 0)) AS GrandTotal
	FROM	(
				SELECT	DOS
						, DailyCount
						, Modality
				FROM #Piv2
				WHERE AppointmentStatus = 'No Show'
			) tt
	PIVOT (SUM(DailyCount) FOR Modality IN ([CR], [ARTH], [MRI], [CT])) as t
	WHERE DATEPART(MONTH, DOS) = CASE DATEPART(DAY, GETDATE()) WHEN 1 THEN (DATEPART(MONTH, GETDATE()) - 1)
				ELSE DATEPART(MONTH, GETDATE()) END



----------------------------------------------NO SHOW REPORT-----------------------------------------------
SELECT @EmailBody2 = CAST((
							SELECT	Received AS 'td', ''
									--, Facility AS 'td', ''
									, ReferringDr AS 'td', ''
									, ARTH AS 'tdcenter', ''
									, CR AS 'tdcenter', ''
									, CT AS 'tdcenter', ''
									, MRI AS 'tdcenter', ''
									, GrandTotal AS 'tdcenter', ''
							FROM #NoShow
							ORDER BY Received, ReferringDr
					FOR XML PATH('tr'), ELEMENTS) AS NVARCHAR(MAX))

-- ============== ADD Row Styling ==================================
	SET @EmailBody2 = REPLACE(@EmailBody2, '<td>', '<td style="padding 0.25rem;font: 12px arial, sans-serif;;text-align: left;border: 1px solid #ccc;background: #d8ecf3;">') --#D6EBFF
	SET @EmailBody2 = REPLACE(@EmailBody2, '<tdcenter>', '<td style="padding: 0.25rem;font: 12px arial, sans-serif;;text-align: center;border: 1px solid #ccc;background: #d8ecf3;">')
	SET @CSSRowHeader2 = '<th style="padding: 0.25rem;font: 15px arial, sans-serif;text-align: left;border: 1px solid #ccc;background: #ADD8E6;">'
	SET @CSSRowHeaderCenter2 = '<th style="padding: 0.25rem;font: 15px arial, sans-serif;text-align: center;border: 1px solid #ccc;background: #ADD8E6;">'
-- ============== Add rest of the HTML Body ========================

	SET @EmailBody = @EmailBody +
	'<html>
	<head>
	  <title></title>
	</head>
	<body onload="_l=''t'';" style="font: normal small/1.1 sans-serif;">
	<br><br><b><font color="black">No Show</font></b><br><br>


	  <table class="orange" style="border-collapse: collapse;width: 50%;"> 
	<thead>
	<tr> 
		' + @CSSRowHeader2 + 'DOS</th> 
		' + @CSSRowHeader2 + 'Referring Doctor</th> 
		' + @CSSRowHeaderCenter2 + 'ARTH</th>
		' + @CSSRowHeaderCenter2 + 'CR</th>
		' + @CSSRowHeaderCenter2 + 'CT</th>
		' + @CSSRowHeaderCenter2 + 'MRI</th>
		' + @CSSRowHeaderCenter1 + 'Grand Total</th>
	</tr> 
	</thead> 
	<tbody>
	' 
	+ ISNULL(@EmailBody2,'')
	+ 
	'</tbody> 
	</table>
	</body>
	</html>';

/*-------------------------------------------------------------------------------------------------------------
								PENDING SCHEDULING SECTION
-------------------------------------------------------------------------------------------------------------*/


SELECT	r.BookedDate as DOS
		--, r.Facility
		, r.ReferringDr
		, r.AppointmentStatus
		, r.AppointmentListType
		, COUNT(r.Account) as DailyCount
	    , r.Modality
INTO #Piv3
FROM dbo.Appointments r 
WHERE ARClass not in ('CPR', 'RE', 'SOH', 'CCH')
GROUP BY r.BookedDate
		--, r.Facility
		, r.ReferringDr
		, r.AppointmentStatus
		, r.AppointmentListType
	    , r.Modality

SELECT	CONVERT(varchar(10), DOS, 101) AS Received
		, COALESCE([ARTH], 0) AS ARTH
		, COALESCE([CR], 0) AS CR
		, COALESCE([CT], 0) AS CT
		, COALESCE([MRI], 0) AS MRI
		, COALESCE([ARTH], 0) + COALESCE([CR], 0) + COALESCE([CT], 0) + COALESCE([MRI], 0) AS GrandTotal
INTO #Pending
FROM	(
			SELECT	DOS
					, DailyCount
					, Modality
			FROM #Piv3
			WHERE AppointmentStatus IN ('Pending', 'Pending Scheduling')
					AND AppointmentListType <> 'Cancelled'
		) tt
PIVOT (SUM(DailyCount) FOR Modality IN ([CR], [ARTH], [MRI], [CT])) as t
UNION 
SELECT	'Total' AS Received
		, SUM(COALESCE([ARTH], 0)) AS ARTH
		, SUM(COALESCE([CR], 0)) AS CR
		, SUM(COALESCE([CT], 0)) AS CT
		, SUM(COALESCE([MRI], 0)) AS MRI
		, SUM(COALESCE([ARTH], 0) + COALESCE([CR], 0) + COALESCE([CT], 0) + COALESCE([MRI], 0)) AS GrandTotal
FROM	(
			SELECT	DOS
					, DailyCount
					, Modality
			FROM #Piv3
			WHERE AppointmentStatus IN ('Pending', 'Pending Scheduling')
					AND AppointmentListType <> 'Cancelled'
		) tt
PIVOT (SUM(DailyCount) FOR Modality IN ([CR], [ARTH], [MRI], [CT])) as t

SELECT @EmailBody3 = CAST((
							SELECT	Received AS 'td', ''
									, COALESCE([ARTH], 0) AS 'tdcenter', ''
									, COALESCE([CR], 0) AS 'tdcenter', ''
									, COALESCE([CT], 0) AS 'tdcenter', ''
									, COALESCE([MRI], 0) AS 'tdcenter', ''
									, COALESCE([ARTH], 0) + COALESCE([CR], 0) + COALESCE([CT], 0) + COALESCE([MRI], 0) AS 'tdcenter', ''
							FROM #Pending
							ORDER BY Received
					FOR XML PATH('tr'), ELEMENTS) AS NVARCHAR(MAX))

-- ============== ADD Row Styling ==================================
	SET @EmailBody3 = REPLACE(@EmailBody3, '<td>', '<td style="padding 0.25rem;font: 12px arial, sans-serif;;text-align: left;border: 1px solid #ccc;background: #d8ecf3;">') --#D6EBFF
	SET @EmailBody3 = REPLACE(@EmailBody3, '<tdcenter>', '<td style="padding: 0.25rem;font: 12px arial, sans-serif;;text-align: center;border: 1px solid #ccc;background: #d8ecf3;">')
	SET @CSSRowHeader3 = '<th style="padding: 0.25rem;font: 15px arial, sans-serif;text-align: left;border: 1px solid #ccc;background: #ADD8E6;">'
	SET @CSSRowHeaderCenter3 = '<th style="padding: 0.25rem;font: 15px arial, sans-serif;text-align: center;border: 1px solid #ccc;background: #ADD8E6;">'
-- ============== Add rest of the HTML Body ========================

	SET @EmailBody = @EmailBody +
	'<html>
	<head>
	  <title></title>
	</head>
	<body onload="_l=''t'';" style="font: normal small/1.1 sans-serif;">
	<br><br><b><font color="black">Pending to be Scheduled</font></b><br><br>


	  <table class="orange" style="border-collapse: collapse;width: 40%;"> 
	<thead>
	<tr> 
		' + @CSSRowHeader3 + 'Received</th> 
		' + @CSSRowHeaderCenter3 + 'ARTH</th>
		' + @CSSRowHeaderCenter3 + 'CR</th>
		' + @CSSRowHeaderCenter3 + 'CT</th>
		' + @CSSRowHeaderCenter3 + 'MRI</th>
		' + @CSSRowHeaderCenter3 + 'Grand Total</th>
	</tr> 
	</thead> 
	<tbody>
	' 
	+ ISNULL(@EmailBody3,'')
	+ 
	'</tbody> 
	</table>
	</body>
	</html>';


/*-------------------------------------------------------------------------------------------------------------
								3RD PARTY MARKETING
-------------------------------------------------------------------------------------------------------------*/

SELECT	MarketingReferral 
		, [1] 
		, [2] 
		, [3] 
		, [4] 
		, [5] 
		, [6] 
		, [7] 
		, [8] 
		, [9] 
		, [10] 
		, [11] 
		, [12] 
		, [1]+[2]+[3]+[4]+[5]+[6]+[7]+[8]+[9]+[10]+[11]+[12] AS GrandTotal
INTO #Marketing		
FROM
		(
			SELECT	DATEPART(MONTH, DOS) AS DateMonth 
					, r.MarketingReferral
					, ApptId
			FROM dbo.Appointments r
			WHERE MarketingReferral IS NOT NULL
					AND Modality = 'MRI'
					AND AppointmentStatus = 'Exam Complete'
	  
		) tt
PIVOT (COUNT(ApptId) FOR DateMonth IN ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12])) as t
UNION 
SELECT	'Total' AS MarketingReferral
		, SUM([1]) AS [1] 
		, SUM([2]) AS [2] 
		, SUM([3]) AS [3] 
		, SUM([4]) AS [4] 
		, SUM([5]) AS [5] 
		, SUM([6]) AS [6] 
		, SUM([7]) AS [7] 
		, SUM([8]) AS [8] 
		, SUM([9]) AS [9] 
		, SUM([10]) AS [10]
		, SUM([11]) AS [11]
		, SUM([12]) AS [12]
		, SUM([1]+[2]+[3]+[4]+[5]+[6]+[7]+[8]+[9]+[10]+[11]+[12]) AS GrandTotal
FROM	(
			SELECT	DATEPART(MONTH, DOS) AS DateMonth 
					, r.MarketingReferral
					, ApptId
			FROM dbo.Appointments r
			WHERE MarketingReferral IS NOT NULL
					AND Modality = 'MRI'
					AND AppointmentStatus = 'Exam Complete'
		) tt
PIVOT (COUNT(ApptId) FOR DateMonth IN ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12])) as t

SELECT @EmailBody4 = CAST((
							SELECT	MarketingReferral AS 'td', ''
									, [1] AS 'tdcenter', ''
									, [2] AS 'tdcenter', ''
									, [3] AS 'tdcenter', ''
									, [4] AS 'tdcenter', ''
									, [5] AS 'tdcenter', ''
									, [6] AS 'tdcenter', ''
									, [7] AS 'tdcenter', ''
									, [8] AS 'tdcenter', ''
									, [9] AS 'tdcenter', ''
									, [10] AS 'tdcenter', ''
									, [11] AS 'tdcenter', ''
									, [12] AS 'tdcenter', ''
									, GrandTotal AS 'tdcenter', ''
		
							FROM #Marketing
					FOR XML PATH('tr'), ELEMENTS) AS NVARCHAR(MAX))

-- ============== ADD Row Styling ==================================
	SET @EmailBody4 = REPLACE(@EmailBody4, '<td>', '<td style="padding 0.25rem;font: 12px arial, sans-serif;;text-align: left;border: 1px solid #ccc;background: #d8ecf3;">') --#D6EBFF
	SET @EmailBody4 = REPLACE(@EmailBody4, '<tdcenter>', '<td style="padding: 0.25rem;font: 12px arial, sans-serif;;text-align: center;border: 1px solid #ccc;background: #d8ecf3;">')
	SET @CSSRowHeader4 = '<th style="padding: 0.25rem;font: 15px arial, sans-serif;text-align: left;border: 1px solid #ccc;background: #ADD8E6;">'
	SET @CSSRowHeaderCenter4 = '<th style="padding: 0.25rem;font: 15px arial, sans-serif;text-align: center;border: 1px solid #ccc;background: #ADD8E6;">'
-- ============== Add rest of the HTML Body ========================


SET @EmailBody = @EmailBody +
	'<html>
	<head>
	  <title></title>
	</head>
	<body onload="_l=''t'';" style="font: normal small/1.1 sans-serif;">
	<br><br><b><font color="black">3rd Party Marketing Stats</font></b><br><br>


	  <table class="orange" style="border-collapse: collapse;width: 65%;"> 
	<thead>
	<tr> 
		' + @CSSRowHeader4 + '3rd Party</th> 
		' + @CSSRowHeaderCenter4 + 'Jan</th>
		' + @CSSRowHeaderCenter4 + 'Feb</th>
		' + @CSSRowHeaderCenter4 + 'Mar</th>
		' + @CSSRowHeaderCenter4 + 'Apr</th>
		' + @CSSRowHeaderCenter4 + 'May</th>
		' + @CSSRowHeaderCenter4 + 'Jun</th>
		' + @CSSRowHeaderCenter4 + 'Jul</th>
		' + @CSSRowHeaderCenter4 + 'Aug</th>
		' + @CSSRowHeaderCenter4 + 'Sep</th>
		' + @CSSRowHeaderCenter4 + 'Oct</th>
		' + @CSSRowHeaderCenter4 + 'Nov</th>
		' + @CSSRowHeaderCenter4 + 'Dec</th>
		' + @CSSRowHeaderCenter4 + 'Grand Total</th>
	</tr> 
	</thead> 
	<tbody>
	' 
	+ ISNULL(@EmailBody4,'')
	+ 
	'</tbody> 
	</table>
	</body>
	</html>';
	
EXEC msdb.dbo.sp_send_dbmail  
  @recipients = @emailTo
, @subject = @subject1
, @body_format = 'HTML'
, @body = @EmailBody



GO

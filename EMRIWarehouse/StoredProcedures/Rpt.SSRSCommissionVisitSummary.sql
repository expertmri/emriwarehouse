USE EMRIWarehouse;
GO

ALTER PROCEDURE Rpt.SSRSCommissionVisitSummary
--'SATURN\scott.lundy', '8/1/2019', 0
--'OSMD\jon.elliot', '11/1/2019', 0
@UserName varchar(255)
, @VarStartDate date
, @reportOutput int = 0

AS

-- just for testing
DECLARE @StartDate date = @VarStartDate

DECLARE @EndDate date
		, @LastQtrStartDate date
		, @LastQtrEndDate date

SET @EndDate = DATEADD(DAY, -1, DATEADD(MONTH, 3, @StartDate))
SET @LastQtrStartDate = DATEADD(MONTH, -3, @StartDate)
SET @LastQtrEndDate = DATEADD(DAY, -1, @StartDate)

--SELECT @StartDate, @EndDate, @LastQtrStartDate, @LastQtrEndDate

DECLARE @AccountsReq int = 15
		, @ScansReqQtr int = 3
		, @ScansReqMonth int = 3
		, @ActiveVisits float = .80
		, @WeeklyVisits int = 40
		, @LeadPayout int = 20
		, @RestartAccount int = 50
		, @NewAccount int = 100
		, @LastCompletedTaskDays int = 180

/********************** NEW AND RESTART ACCOUNTS *************************/
;WITH accounts AS (
		SELECT	DISTINCT m.SF_AccountId
				, a.AccountName
				, a.SalesRepresentative
				, m.AccountStatusDetailed
				, wf.ScanCountMonthly
				, wf.MonthYear AS MonthDate
				, 1 AS isStatusChanged
				, m.MonthDate AS StatusChangedDate
		FROM ScansByAccountWorkflow wf 
		JOIN SF.Accounts a ON wf.SF_AccountId = a.SF_AccountId
		JOIN SF.AccountsByMonth m ON wf.SF_AccountId = m.SF_AccountId
		WHERE m.AccountStatusDetailed IN ('Restart', 'New' )
			  AND wf.MonthYear BETWEEN @StartDate AND @EndDate
			  AND wf.MonthYear = m.MonthDate
			  --AND AccountType = 'Doctor'
	)
, lastScan AS (
		SELECT MIN(DOS) AS lastScanDate
			   , s.SF_AccountId
		FROM dbo.Scans s 
		JOIN accounts a ON a.SF_AccountId = s.SF_AccountId
		WHERE DOS > a.MonthDate
		GROUP BY s.SF_AccountId
	)
, completedTask AS ( 
	SELECT	u.SF_AccountId
			, u.CompletedBy
			, MAX(completedDate) AS LastCompletedDate 
	FROM SF.UserTaskProductivity u
	JOIN lastScan l ON l.SF_AccountId = u.SF_AccountId
	WHERE TaskStatus = 'Completed'
		  AND u.CompletedDate < l.lastScanDate
		  AND u.Activity in ('In Person Meeting','Office Meeting','Office Presentation')
	GROUP BY u.SF_AccountId
			 , u.CompletedBy
	)

SELECT DISTINCT	a.SF_AccountId
		, a.AccountName
		, a.SalesRepresentative
		, a.AccountStatusDetailed
		, a.ScanCountMonthly
		, a.MonthDate
		, c.LastCompletedDate
		, lastScanDate AS StatusChangedDate
		, 1 as isStatusChanged
INTO #accounts
FROM accounts a
LEFT JOIN completedTask c ON a.SF_AccountId = c.SF_AccountId AND a.SalesRepresentative = c.CompletedBy
JOIN lastScan l ON a.SF_AccountId = l.SF_AccountId

INSERT INTO #accounts
SELECT DISTINCT	wf.SF_AccountId
		, a.AccountName
		, a.SalesRepresentative
		, a.AccountStatusDetailed
		, wf.ScanCountMonthly
		, wf.MonthYear AS MonthDate
		, a.LastCompletedDate
		, a.StatusChangedDate
		, 0 AS isStatusChanged
FROM ScansByAccountWorkflow wf 
LEFT JOIN #accounts a ON wf.SF_AccountId = a.SF_AccountId 
WHERE wf.MonthYear > a.MonthDate 
	  AND wf.MonthYear <= @EndDate
	  --AND AccountType = 'Doctor'

--;WITH accountData AS (
		SELECT DISTINCT	a.SF_AccountId
				, a.SalesRepresentative
				, a.AccountStatusDetailed
				, CASE WHEN ts.TotalScans >= @ScansReqQtr OR ms.MaxMonthlyScans >= @ScansReqMonth  THEN 1
						ELSE 0 END AS IsScanCommission
				, COALESCE(tsk.TaskCommission, 0) AS IsTaskCommission
				, CASE WHEN (ts.TotalScans >= @ScansReqQtr OR ms.MaxMonthlyScans >= @ScansReqMonth) 
								AND COALESCE(tsk.TaskCommission, 0) = 1
							THEN 1
							ELSE 0 END AS IsPayAccountsCommission
		INTO #accountData 
		FROM #accounts a
		JOIN (
				SELECT	SF_AccountId
						, MAX(ScanCountMonthly) AS MaxMonthlyScans
				FROM #accounts
				GROUP BY SF_AccountId
			) ms ON a.SF_AccountId = ms.SF_AccountId
		JOIN (
				SELECT	a.SF_AccountId
						, SUM(a.ScanCountMonthly) AS TotalScans
				FROM #accounts a
				GROUP BY a.SF_AccountId
			) ts ON a.SF_AccountId = ts.SF_AccountId
		LEFT JOIN	(
						SELECT	SF_AccountId
								, 1 AS TaskCommission
						FROM #accounts a
						WHERE isStatusChanged = 1 
								AND a.StatusChangedDate <= DATEADD(DAY, @LastCompletedTaskDays, a.LastCompletedDate)
					) tsk ON a.SF_AccountId = tsk.SF_AccountId
--)
;WITH finalData AS (
		SELECT	COUNT(ad.SF_AccountId) AS TotalAccounts
				, CASE WHEN COUNT(ad.SF_AccountId) > @AccountsReq THEN 1 ELSE 0 END AS IsPayAccountsCommission
				, ad.SalesRepresentative
				, ad.AccountStatusDetailed
		FROM #accountData ad 
		WHERE ad.IsPayAccountsCommission = 1
		GROUP BY ad.SalesRepresentative
				, ad.AccountStatusDetailed
)

SELECT SalesRepresentative	
		, COALESCE([New], 0) AS NewAccounts
		, COALESCE([Restart], 0) AS RestartAccounts
		, CASE WHEN [New] > @AccountsReq THEN [New]* @NewAccount ELSE 0 END AS NewAccountCommission
		, CASE WHEN [Restart] > @AccountsReq THEN [Restart] * @RestartAccount ELSE 0 END AS RestartAccountCommission
		, IsPayAccountsCommission
INTO #newRestartAccounts
FROM (
		SELECT	fd.SalesRepresentative
				, fd.AccountStatusDetailed
				, fd.TotalAccounts
				, fd.IsPayAccountsCommission
		FROM finalData fd
	) tt
PIVOT (MAX(tt.TotalAccounts) FOR tt.AccountStatusDetailed IN ([New],[Restart])) as t

/********************** - END - NEW AND RESTART ACCOUNTS *************************/

/*********************** 80% ACTIVE ACCOUNTS VISITED*****************/
--DECLARE @StartDate date = '11/1/2019'
--DECLARE @EndDate date
--drop table #activeAcct
--drop table #outputActiveVisits
--SET @EndDate = DATEADD(DAY, -1, DATEADD(MONTH, 3, @StartDate))
--DECLARE @ActiveVisits float = .80

;WITH activeAcct AS (
	SELECT m.SF_AccountId
		   , a.SalesRepresentative
	--INTO #activeAcct
	FROM SF.AccountsByMonth m
	JOIN SF.Accounts a ON m.SF_AccountId = a.SF_AccountId
	WHERE m.MonthDate = @StartDate
		  AND m.IsActive = 1
)
, visitedAcct AS (
	SELECT	u.SF_AccountId
			, u.CompletedBy as SalesRepresentative
			, MIN(completedDate) AS visitDate
	FROM SF.UserTaskProductivity u
	WHERE u.TaskStatus = 'Completed'
		  AND u.CompletedDate BETWEEN @StartDate AND @EndDate
		  AND u.Activity in ('In Person Meeting','Office Meeting','Office Presentation') -- Added 3/10/20 per saba email
	GROUP BY u.SF_AccountId
			, u.CompletedBy
)
--, activeVists AS (
	SELECT a.SF_AccountId
		   , a.SalesRepresentative
		   , v.visitDate
		   , CASE WHEN v.visitDate IS NOT NULL THEN 1 ELSE 0 END AS isVisited
	INTO #outputActiveVisits
	FROM activeAcct a
	LEFT JOIN visitedAcct v ON a.SalesRepresentative = v.SalesRepresentative AND a.SF_AccountId = v.SF_AccountId
--)
;WITH totalActive AS (
	SELECT	COUNT(a.SF_AccountId) AS totalAccounts
			, a.SalesRepresentative
	FROM #outputActiveVisits a
	GROUP BY a.SalesRepresentative
)

SELECT	v.SalesRepresentative
		, t.totalAccounts AS Goal
		, SUM(v.isVisited) AS CurrentStatus
		, @ActiveVisits AS PercentGoalActiveVisits
		, CAST(SUM(v.isVisited) AS float) / CAST(t.totalAccounts AS float) AS PercentActiveVisited
		, CASE WHEN CAST(SUM(v.isVisited) AS float) / CAST(t.totalAccounts AS float) >= @ActiveVisits THEN 1 ELSE 0 END AS IsCommissionActiveVisits
INTO #activeVisits
FROM #outputActiveVisits v
JOIN totalActive t ON t.SalesRepresentative = v.SalesRepresentative
GROUP BY v.SalesRepresentative
		 , t.totalAccounts


/*********************** END 80% ACTIVE ACCOUNTS VISITED*************/

/*********************** 40 UNIQUE ACCOUNTS VISITED WEEKLY *************/
--DECLARE @StartDate date = '8/1/2019'
--DECLARE @EndDate date
--DECLARE @WeeklyVisits int = 40
--SET @EndDate = DATEADD(DAY, -1, DATEADD(MONTH, 3, @StartDate))

DECLARE @StartDOW int
		, @EndDOW int
		, @StartProrated int
		, @EndProrated int
		, @TotalVisits int
		, @NoWeeks int
		--, @StartWeekNo int
		--, @EndWeekNo int

SET @StartDOW = DATEPART(dw, @StartDate)
SET @EndDOW = DATEPART(dw, @EndDate)
SET @NoWeeks = DATEDIFF(wk, @StartDate, @EndDate)

-- get how many days left in the week for partial weeks and prorate the @weekly visits for those weeks
SET @StartProrated = (SELECT CASE WHEN @StartDOW = 1 THEN @WeeklyVisits ELSE ((8 - CAST(DATEPART(dw, @StartDate) AS float)) / 7) * @WeeklyVisits END)
SET @EndProrated = (SELECT CASE WHEN @EndDOW = 7 THEN @WeeklyVisits ELSE (CAST(DATEPART(dw, @StartDate) AS float) / 7) * @WeeklyVisits END)
SET @TotalVisits = (SELECT ((DATEDIFF(wk, @StartDate, @EndDate) - 1) * @WeeklyVisits) + @StartProrated + @EndProrated)

;WITH visitedAcct AS (
	SELECT DISTINCT u.SF_AccountId
					, u.CompletedBy AS SalesRepresentative
					, MIN(completedDate) AS visitDate
					--, completedDate AS visitDate
					, DATEPART(wk, u.CompletedDate) AS WeekCompleted
	FROM SF.UserTaskProductivity u
	WHERE u.TaskStatus = 'Completed'
		  AND u.CompletedDate BETWEEN @StartDate AND @EndDate
		  AND u.Activity in ('In Person Meeting','Office Meeting','Office Presentation') -- Added 3/10/20 per saba email
	GROUP BY u.SF_AccountId
			, u.CompletedBy
			, DATEPART(wk, u.CompletedDate) 
)
SELECT	v.SalesRepresentative
		, @TotalVisits AS QtrGoal
		, COUNT(v.SF_AccountId) AS QtrActual
		, @TotalVisits/@NoWeeks AS WeeklyGoal
		, COUNT(v.SF_AccountId)/@NoWeeks AS WeeklyActual
		, CASE WHEN @TotalVisits <= COUNT(v.SF_AccountId) THEN 1 ELSE 0 END AS isCommissionUniqueWeekly
INTO #fortyWeeks
FROM visitedAcct v
GROUP BY v.SalesRepresentative

/*********************** END 40 UNIQUE ACCOUNTS VISITED WEEKLY *************/

/********************** LEAD CONVERSION *************************/
--DECLARE @StartDate date = '11/1/2019'
--DECLARE @EndDate date
--DECLARE @LeadPayout int = 20
--SET @EndDate = DATEADD(DAY, -1, DATEADD(MONTH, 3, @StartDate))
--drop table #leadConv
--drop table #leadCommissionDetail

-- MODIFIED 3/5/2020 to show leads from all time and those converted in period
CREATE TABLE #leadConv (SF_AccountId varchar(255), AccountName varchar(255), AccountAddress varchar(255), ConvDate date, FirstReferral date, ConvTime int, CreatedBy varchar(255))

INSERT INTO #leadConv
EXEC Rpt.SSRSLeadConversion @StartDate

-- we only want leads converted during the Qtr and the First Exam must be no more than one day before the lead is entered
DELETE FROM #leadConv
WHERE ConvDate NOT BETWEEN @StartDate AND @EndDate
	  OR ConvTime < -1

SELECT l.CreatedBy AS SalesRepresentative
	   , l.SF_AccountId
	   , l.AccountName
	   , l.AccountAddress
	   , l.ConvDate
	   , l.FirstReferral
INTO #leadCommissionDetail
FROM #leadConv l
UNION ALL
SELECT	u.FirstName + ' ' + u.LastName AS SalesRepresentative
		, a.Id AS SF_AccountId
		, a.Name AS AccountName
		, a.BillingStreet + ' ' + a.BillingCity + ' ' + a.BillingStateCode + ' ' + a.BillingPostalCode AS AccountAddress
		, null AS ConvDate
		, null AS FirstReferral
FROM Integration.SF.Account a
JOIN Integration.SF.Users u ON u.Id = a.CreatedById
WHERE source__C = 'Lead'

SELECT	COUNT(l.SF_AccountId) AS TotalLeads
		, COUNT(l.ConvDate) AS NoLeadsConv
		, COUNT(l.ConvDate) * @LeadPayout AS LeadCommission
		, l.SalesRepresentative
INTO #leadCommission
FROM #leadCommissionDetail l
GROUP BY l.SalesRepresentative

/********************** END LEAD CONVERSION *************************/

/********************** TERRITORY GROWTH *************************/
--DECLARE @StartDate date = '11/1/2019'
--DECLARE @EndDate date
--		, @LastQtrStartDate date
--		, @LastQtrEndDate date
--SET @EndDate = DATEADD(DAY, -1, DATEADD(MONTH, 3, @StartDate))
--SET @LastQtrStartDate = DATEADD(MONTH, -3, @StartDate)
--SET @LastQtrEndDate = DATEADD(DAY, -1, @StartDate)

;WITH currentQtr AS (
	SELECT	SUM(ScanCountMonthly) AS ScanTotal
			, a.SalesRepresentative
			, a.ZoneRegion
	FROM ScansByAccountWorkflow s
	JOIN SF.Accounts a ON s.SF_AccountId = a.SF_AccountId
	WHERE s.MonthYear BETWEEN @StartDate AND @EndDate
		  AND AccountType = 'Doctor'
	GROUP BY a.SalesRepresentative
			 , a.ZoneRegion
)
, lastQtr AS (
	SELECT	SUM(ScanCountMonthly) AS ScanTotal
			, a.SalesRepresentative
			, a.ZoneRegion
	FROM ScansByAccountWorkflow s
	JOIN SF.Accounts a ON s.SF_AccountId = a.SF_AccountId
	WHERE s.MonthYear BETWEEN @LastQtrStartDate AND @LastQtrEndDate
		  AND AccountType = 'Doctor'
	GROUP BY a.SalesRepresentative
			 , a.ZoneRegion
)
, baseData AS (
	SELECT SUM(c.ScanTotal) AS currentScans
			, SUM(l.ScanTotal) AS lastScans
			, SUM(t.BaseScans) AS totalBase
			, c.SalesRepresentative
	FROM currentQtr c
	JOIN lastQtr l ON c.SalesRepresentative = l.SalesRepresentative AND l.ZoneRegion = c.ZoneRegion
	JOIN XRef.TerritoryScans t ON t.ZoneRegion = c.ZoneRegion
	GROUP BY c.SalesRepresentative
)
, finalTerritory AS (
	SELECT	b.SalesRepresentative
			, b.lastScans
			, b.currentScans
			, CASE WHEN b.currentScans >= b.totalBase AND b.currentScans > b.lastScans THEN CAST(((CAST(b.currentScans AS float) - CAST(b.lastScans AS float))/CAST(b.lastScans AS float)) AS float)
				ELSE '0'
			END AS percentGrowth
	FROM baseData b
)
SELECT f.SalesRepresentative
	   , f.percentGrowth
	   , f.lastScans AS ScansLastQtr
	   , f.currentScans AS ScansCurrentQtr
	   , CASE WHEN f.percentGrowth >= .05 AND f.percentGrowth < .10 THEN CEILING(f.percentGrowth* 100) * 100
			  WHEN f.percentGrowth >= .10 AND f.percentGrowth < .15 THEN CEILING(f.percentGrowth* 100) * 105
			  WHEN f.percentGrowth >= .15 AND f.percentGrowth < .20 THEN CEILING(f.percentGrowth* 100) * 110
			  WHEN f.percentGrowth >= .20 AND f.percentGrowth < .25 THEN CEILING(f.percentGrowth* 100) * 115
			  WHEN f.percentGrowth >= .25 AND f.percentGrowth < .30 THEN CEILING(f.percentGrowth* 100) * 120
			  WHEN f.percentGrowth >= .30 THEN 5000 -- CEILING(f.percentGrowth * 100) * 162
			  ELSE 0 END AS territoryCommission
INTO #territoryGrowth
FROM finalTerritory f

/********************** END TERRITORY GROWTH *************************/

-- Drill down for 80% active visits
IF @reportOutput = 1 
	SELECT o.SF_AccountId
			, o.SalesRepresentative
			, o.visitDate
			, a.AccountName
	FROM #outputActiveVisits o
	JOIN SF.Accounts a ON o.SF_AccountId = a.SF_AccountId
	JOIN dbo.SSRSUsers s ON (o.SalesRepresentative = s.UserName  OR s.isAdmin = 1)
WHERE s.domainUser = @UserName 

-- Drill Down for New Accounts
ELSE IF @reportOutput = 2
	SELECT	a.SalesRepresentative
			, a.SF_AccountId
			, a.AccountName
			, a.AccountStatusDetailed
			, a.LastCompletedDate
			, a.MonthDate
			, a.StatusChangedDate
			, a.ScanCountMonthly
			, CASE WHEN ad.IsScanCommission = 1 THEN 'Yes' ELSE 'No' END AS IsScanCommission
			, CASE WHEN ad.IsTaskCommission = 1 THEN 'Yes' ELSE 'No' END AS IsTaskCommission
			, CASE WHEN ad.IsPayAccountsCommission = 1 THEN 'Yes' ELSE 'No' END AS IsPayAccountsCommission
	FROM #accounts a
	JOIN #accountData ad ON a.SF_AccountId = ad.SF_AccountId
	--JOIN SF.Accounts a ON o.SF_AccountId = a.SF_AccountId
	JOIN dbo.SSRSUsers s ON (a.SalesRepresentative = s.UserName  OR s.isAdmin = 1)
	WHERE s.domainUser = @UserName
			AND a.AccountStatusDetailed = 'New' 
			--AND ad.IsPayAccountsCommission = 1

-- Drill Down for Restart Accounts
ELSE IF @reportOutput = 3
	SELECT	a.SalesRepresentative
			, a.SF_AccountId
			, a.AccountName
			, a.AccountStatusDetailed
			, a.LastCompletedDate
			, a.MonthDate
			, a.StatusChangedDate
			, a.ScanCountMonthly
			, CASE WHEN ad.IsScanCommission = 1 THEN 'Yes' ELSE 'No' END AS IsScanCommission
			, CASE WHEN ad.IsTaskCommission = 1 THEN 'Yes' ELSE 'No' END AS IsTaskCommission
			, CASE WHEN ad.IsPayAccountsCommission = 1 THEN 'Yes' ELSE 'No' END AS IsPayAccountsCommission
	FROM #accounts a
	JOIN #accountData ad ON a.SF_AccountId = ad.SF_AccountId
	--JOIN SF.Accounts a ON o.SF_AccountId = a.SF_AccountId
	JOIN dbo.SSRSUsers s ON (a.SalesRepresentative = s.UserName  OR s.isAdmin = 1)
	WHERE s.domainUser = @UserName
			AND a.AccountStatusDetailed = 'Restart' 

-- Drill Down for leads converted
ELSE IF @reportOutput = 4
	SELECT l.*
	FROM #leadCommissionDetail l
	JOIN SF.Accounts a ON l.SalesRepresentative = a.SalesRepresentative
	JOIN dbo.SSRSUsers s ON (l.SalesRepresentative = s.UserName  OR s.isAdmin = 1)
	WHERE s.domainUser = @UserName 

-- Full Summary Report
ELSE
BEGIN 
	;WITH Regions AS (
		SELECT a.SalesRepresentative,
			   Zones = STUFF( (SELECT DISTINCT ', '+ ZoneRegion 
							  FROM SF.Accounts 
							  WHERE SalesRepresentative = a.SalesRepresentative
							  FOR XML PATH('')
							 ), 1, 1, ''
						   ) 
		FROM SF.Accounts a
		GROUP BY a.SalesRepresentative
	)

	SELECT	a.SalesRepresentative
			, r.Zones
			, a.Goal
			, a.CurrentStatus
			, a.PercentGoalActiveVisits
			, a.PercentActiveVisited
			, a.IsCommissionActiveVisits
			, f.QtrGoal
			, f.QtrActual
			, f.WeeklyGoal
			, f.WeeklyActual
			, f.isCommissionUniqueWeekly
			, @AccountsReq AS AccountsReq
			, COALESCE(nr.NewAccounts, 0) AS NewAccounts
			, COALESCE(nr.NewAccountCommission, 0) AS NewAccountCommission
			, COALESCE(nr.RestartAccounts, 0) AS RestartAccounts
			, COALESCE(nr.RestartAccountCommission, 0) AS RestartAccountCommission
			, l.TotalLeads
			, l.NoLeadsConv
			, l.LeadCommission
			, t.ScansLastQtr
			, t.ScansCurrentQtr
			, t.percentGrowth
			, t.territoryCommission
			, CASE WHEN a.IsCommissionActiveVisits = 1 AND f.isCommissionUniqueWeekly = 1 AND nr.IsPayAccountsCommission = 1 THEN
				COALESCE(nr.NewAccountCommission, 0) + COALESCE(nr.RestartAccountCommission, 0) + l.LeadCommission + t.territoryCommission
				ELSE 0 END AS totalCommission
	FROM #activeVisits a
	LEFT JOIN Regions r ON a.SalesRepresentative = r.SalesRepresentative
	LEFT JOIN #fortyWeeks f ON a.SalesRepresentative = f.SalesRepresentative
	LEFT JOIN #territoryGrowth t ON a.SalesRepresentative = t.SalesRepresentative
	LEFT JOIN #leadCommission l ON a.SalesRepresentative = l.SalesRepresentative
	LEFT JOIN #newRestartAccounts nr ON a.SalesRepresentative = nr.SalesRepresentative
	JOIN dbo.SSRSUsers s ON (f.SalesRepresentative = s.UserName  OR s.isAdmin = 1)
	WHERE s.domainUser = @UserName 
END

GO

GRANT EXECUTE ON Rpt.SSRSCommissionVisitSummary TO PUBLIC;
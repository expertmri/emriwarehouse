USE EMRIWarehouse
GO

ALTER PROCEDURE Rpt.SSRSCollectionOpenARPI
	@Modality varchar(100) = 'ALL'
	, @Payor varchar(255) = 'ALL'
	, @ARClass varchar(255) = 'ALL'

AS

SELECT	bt.ApptId
		, LTRIM(RTRIM(bt.Insurance1)) AS Insurance1
		, bt.Modality
		, bt.ARClass
		, SUM(bt.Balance) AS TotalBilled
FROM dbo.BilledTransactions bt
LEFT JOIN dbo.ChargesCredits cc ON bt.TransactionNumber = cc.ChargeTransactionNo
JOIN XRef.ARClassGroup ar ON ar.ARClass = bt.ARClass
WHERE bt.ApptId IS NOT NULL
	  AND IsCharge = 1
	  AND (@Payor = bt.Insurance1 OR @Payor = 'ALL')
	  AND (@Modality = bt.Modality OR @Modality = 'ALL')
	  AND ar.ARClass IN ('PIINT', 'PIINCP')
	  AND (@ARClass = ar.ARClass OR @ARClass = 'ALL')
	  AND bt.InsCode1 <> '1795'
	  AND cc.ChargeTransactionNo IS NULL
GROUP BY bt.ApptId
		 , bt.Insurance1
		 , bt.Modality
		 , bt.ARClass


GO

GRANT EXECUTE ON Rpt.SSRSCollectionOpenARPI TO PUBLIC;
GO

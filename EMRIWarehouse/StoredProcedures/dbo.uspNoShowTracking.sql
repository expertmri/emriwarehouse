USE EMRIWarehouse
GO

ALTER PROCEDURE dbo.uspNoShowTracking
AS

INSERT INTO dbo.NoShowTracking
	(
		ApptId
      , CaseId
      , Account
      , AppointmentListType
      , BookedDate
      , DOS
      , DOI
      , Modality
      , Facility
      , ARClass
      , AppointmentStatusId
      , AppointmentStatus
      , MarketingReferral
      , ReferringDrId
      , ReferralCode
      , ReferringDr
      , ReferringGroup
      , InsCode
	  , InsName
      , RefAttyCode
	  , ReferringAttorney
	  , OperatorCode
	  , Operator
	  , CancelDate
	  , CancelUser
	  , CancelReasonCode
	  , CancelReasonDescription
	)

SELECT appt.ApptId
      , appt.CaseId
      , appt.Account
      , appt.AppointmentListType
      , appt.BookedDate
      , appt.DOS
      , appt.DOI
      , appt.Modality
      , appt.Facility
      , appt.ARClass
      , appt.AppointmentStatusId
      , appt.AppointmentStatus
      , appt.MarketingReferral
      , appt.ReferringDrId
      , appt.ReferralCode
      , appt.ReferringDr
      , appt.ReferringGroup
      , appt.InsCode
	  , appt.InsName
	  , appt.RefAttyCode
      , appt.ReferringAttorney
	  , appt.OperatorCode
	  , appt.Operator
	  , appt.CancelDate
	  , appt.CancelUser
	  , appt.CancelReasonCode
	  , appt.CancelReasonDescription
FROM dbo.Appointments appt
LEFT JOIN dbo.NoShowTracking ns ON appt.ApptId = ns.ApptId
WHERE appt.AppointmentStatusId = 5
	  AND ns.NoShowTrackingID IS NULL

GO


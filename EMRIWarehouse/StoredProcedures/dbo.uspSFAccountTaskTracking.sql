USE EMRIWarehouse
GO

ALTER PROCEDURE dbo.uspSFAccountTaskTracking
AS

SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


SELECT	Account_Id__C AS AccountId
		, acct.AccountName
		, acct.Address1
		, acct.City
		, acct.Zip
		, acct.Phone
		, 'ADS Push' AS Task
		, null AS Outcome
		, null AS "User"
		, null AS CreatedDate
		, MIN(Date_Processed__c) AS CompleteDate
		, Account_Type__c AS AccountType
FROM Integration.SF.ADSQueue ads
LEFT JOIN EMRIWarehouse.SF.Accounts acct ON ads.Account_Id__c = acct.SF_AccountId
WHERE Is_Processed__c = 1
	  AND Account_Id__c IS NOT NULL
GROUP BY Account_Id__c
		 , acct.AccountName
		 , acct.Address1
		 , acct.City
		 , acct.Zip
		 , acct.Phone
		 , Account_Type__c
UNION ALL
SELECT	t.AccountId
		, acct.AccountName
		, acct.Address1
		, acct.City
		, acct.Zip
		, acct.Phone
		, t.Task_Type__c AS Task
		, t.Outcome__c AS Outcome
		, u. FirstName + ' ' + u.LastName AS "User"
		, t.CreatedDate
		, COALESCE(t.Date_Completed__c , t.LastModifiedDate) AS CompleteDate
		, null as AccountType
FROM Integration.SF.Tasks t
LEFT JOIN Integration.SF.Users u ON t.LastModifiedById = u.Id
LEFT JOIN EMRIWarehouse.SF.Accounts acct ON t.AccountId = acct.SF_AccountId
WHERE t.Task_Type__c IN	(	
							'OB Initial Call'
							, 'OB Second Call'
							, 'OB Package'
							, 'OB Initial Visit'
							, 'OB Second Visit'
						)









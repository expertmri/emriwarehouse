USE [EMRIWarehouse]
GO
/****** Object:  StoredProcedure [dbo].[uspARExportMLWC]    Script Date: 10/24/2017 8:53:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[uspARExportMLWC]
AS

DECLARE @nullJoin varchar(10) 
SET @nullJoin = 'zzz|}*$'

SELECT	bt.PracticeName AS plus_id
		, bt.Account AS MRN
		, bt.SSN
		, dbo.udfParseLastName(bt.PatientName) AS LastName
		, dbo.udfParseFirstName(bt.PatientName) AS FirstName
		, bt.BirthDate
		, bt.DOS
		, bt.DOI
		, bt.PatientAddress AS 'Address'
		, bt.PatientCity AS City
		, bt.PatientState AS 'State'
		, bt.PatientZip AS 'Zip Code'
		, bt.PatientPhone AS Phone
		, bt.InsCode1
		, Insurance1
		, bt.InsuranceAddress
		, bt.InsuranceCity
		, bt.InsuranceState
		, bt.InsuranceZip
		, bt.InsurancePolicy AS ClaimNo
		, bt.ADJNumber
		, '' AS ADJ2
		, '' AS ADJ3
		, bt.Employer
		, bt.AttorneyCode AS AttorneyCode
		, bt.AttorneyName AS AttorneyName
		, bt.AttorneyAddress1 AS AttorneyAddress1 
		, bt.AttorneyAddress2 AS AttorneyAddress2 
		, bt.AttorneyCity  AS AttorneyCity 
		, bt.AttorneyState AS AttorneyState
		, bt.AttorneyZip  AS AttorneyZip 
		, bt.AttorneyPhone AS AttorneyPhone
		, bt.AttorneyFax  AS AttorneyFax 
		, bt.TransactionNumber
		, bt.PostingDate
		, bt.DOS as ServiceDate
		, bt.ChargesCPTCode AS CPTCode
		, bt.ChargesCPTDescription AS CPTDescription
		, bt.Diag
		, bt.RefCode
		, bt.RefName
		, bt.Plc
		, bt.Balance
		, COALESCE(fs.Rate, (Balance * .355)) AS FeeSchedule
		, bt.ARClass
		, bt.ApptId
FROM dbo.BilledTransactions bt
LEFT JOIN XRef.FeeSchedule fs ON bt.ARClass = fs.ARClass 
							  AND bt.ChargesCPTCode = fs.code
							  AND COALESCE(bt.InsCode1, @nullJoin) = COALESCE(fs.Payor, @nullJoin)
							  AND bt.DOS BETWEEN fs.EffectiveStartDate AND fs.EffectiveEndDate
WHERE bt.ARClass = ('MLWC')




GO

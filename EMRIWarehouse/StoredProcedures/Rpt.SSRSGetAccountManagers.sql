USE EMRIWarehouse
GO

ALTER PROCEDURE Rpt.SSRSGetAccountManagers
AS

SELECT AccountManager
FROM (
		SELECT 'ALL' AS AccountManager
		UNION ALL
		SELECT DISTINCT AccountManager AS AccountManager
		FROM SF.Accounts
	  ) tt
ORDER BY CASE WHEN AccountManager = 'ALL' THEN 0 ELSE 1 END,  AccountManager 

GO

GRANT EXECUTE ON Rpt.SSRSGetAccountManagers TO PUBLIC;
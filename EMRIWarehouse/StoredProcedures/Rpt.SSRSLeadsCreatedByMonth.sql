USE EMRIWarehouse
GO

ALTER PROCEDURE Rpt.SSRSLeadsCreatedByMonth
AS

DECLARE @FirstOfYear date = (SELECT dbo.udfFirstDayOfYear(GETDATE()))

SELECT	a.Originator__c
--u.FirstName + ' ' + u.LastName AS UserName
		, dbo.udfFirstDayOfMonth(a.CreatedDate) AS CreateDate
		, COUNT(a.CreatedDate) AS RcdCount
FROM Integration.SF.Account a 
JOIN Integration.SF.Users u ON a.CreatedById = u.Id
WHERE a.CreatedDate >= @FirstOfYear
GROUP BY	a.Originator__c
--u.FirstName + ' ' + u.LastName
		 , dbo.udfFirstDayOfMonth(a.CreatedDate)

GO

GRANT EXECUTE ON Rpt.SSRSLeadsCreatedByMonth TO PUBLIC;
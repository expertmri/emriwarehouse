USE EMRIWarehouse
GO

ALTER PROCEDURE Rpt.SSRSCompletedByAttorneyPI
@Range varchar(20) = null

AS

DECLARE @StartDate date
		, @FirstOfMonth date
		, @EndDate date
		, @YearStartDate date

SET @YearStartDate = (SELECT CAST('1/1/' + CAST(DATEPART(YEAR, GETDATE()) AS varchar(10)) AS date)) 

SELECT @FirstOfMonth = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)
	   , @EndDate = DATEADD(DAY, -1, @FirstOfMonth)
	   , @StartDate = DATEADD(MONTH, -3, @FirstOfMonth)

;WITH AvgCTE AS
	(
		SELECT  code AS InsCode
				, CAST(COUNT (CASE WHEN DOS BETWEEN @StartDate AND @EndDate THEN apt.ApptId ELSE NULL END) as decimal)/DATEDIFF(DAY, @StartDate, @EndDate) AS QtrAvg
				, CAST(COUNT(CASE WHEN DOS >= @FirstOfMonth THEN apt.ApptId ELSE NULL END) AS decimal)/ DATEDIFF(DAY, @FirstOfMonth , CAST(GETDATE() AS date)) AS MthAvg
		FROM dbo.Appointments apt
		JOIN Integration.Stg.Insurances ins ON apt.InsCode = ins.Code
		JOIN XRef.AppointmentStatus stat ON apt.AppointmentStatusId = stat.AppointmentStatusId
		LEFT JOIN XRef.ARClassGroup ar ON apt.ARClass = ar.ARClass
		WHERE apt.Modality = 'MRI'
			  AND apt.AppointmentStatusId = 4 --Exam Complete
			  AND ar.IsPersonalInjury = 1
			  --AND code = '649'
		GROUP BY code  		
	)

, AllData_CTE AS
	(	
		SELECT	ins.Name AS Insurance
				, ins.Address1 
					+ RTRIM(LTRIM(+ ' ' + COALESCE(ins.Address2,''))) 
					+ ' ' + REPLACE( ins.City, ' ', '') 
					+ ' ' + ins.State 
					+ ' ' + ins.ZipCode  AS InsAddress
				, DATEADD(MONTH, DATEDIFF(MONTH, 0, apt.DOS), 0) AS MnthNmbr
				, dbo.udfGetMonthName(apt.DOS) AS DOS
				, COUNT(apt.ApptId) AS Completed
				, CASE WHEN cte.QtrAvg = 0 THEN 'NEW'
					   WHEN cte.MthAvg >= cte.QtrAvg THEN 'GOOD'
					   WHEN cte.MthAvg < cte.QtrAvg THEN 'DROPPED'
				  END AS AccountStatus
				, CASE WHEN cte.MthAvg < cte.QtrAvg THEN CEILING(((cte.QtrAvg - cte.MthAvg)/cte.QtrAvg) * 100)
				  END AS PercentDrop
		FROM dbo.Appointments apt
		JOIN Integration.Stg.Insurances ins ON apt.InsCode = ins.Code
		JOIN XRef.AppointmentStatus stat ON apt.AppointmentStatusId = stat.AppointmentStatusId
		LEFT JOIN XRef.ARClassGroup ar ON apt.ARClass = ar.ARClass
		LEFT JOIN AvgCTE cte ON apt.InsCode = cte.InsCode
		WHERE apt.Modality = 'MRI'
			  AND apt.AppointmentStatusId = 4 --Exam Complete
			  AND ar.IsPersonalInjury = 1
			  AND apt.DOS >= @YearStartDate
			  --and ins.code in ('649')
		GROUP BY ins.Name
				, ins.Address1 
					+ RTRIM(LTRIM(+ ' ' + COALESCE(ins.Address2,''))) 
					+ ' ' + REPLACE( ins.City, ' ', '') 
					+ ' ' + ins.State 
					+ ' ' + ins.ZipCode
				, DATEADD(MONTH, DATEDIFF(MONTH, 0, apt.DOS), 0)
				, dbo.udfGetMonthName(apt.DOS)
				, cte.QtrAvg
				, cte.MthAvg
		)

, FinalData AS 
	(
		SELECT	tt.Insurance
				, tt.InsAddress
				, tt.MnthNmbr
				, tt.DOS
				, tt.Completed
				, tt.AccountStatus
				, CASE WHEN tt.PercentDrop > 100 THEN 100 ELSE tt.PercentDrop END AS PercentDrop
				, CASE WHEN tt.AccountStatus = 'DROPPED' THEN
						CASE WHEN	tt.PercentDrop <= 10 THEN '0-10'
							 WHEN tt.PercentDrop BETWEEN 11 AND 20 THEN '11-20'
							 WHEN tt.PercentDrop BETWEEN 21 AND 40 THEN '21-40'
							 WHEN tt.PercentDrop BETWEEN 41 AND 60 THEN '41-60'
							 WHEN tt.PercentDrop BETWEEN 61 AND 80 THEN '61-80'
							 WHEN tt.PercentDrop BETWEEN 81 AND 99 THEN '81-99'
							 ELSE '100' END
				END AS DropRange
		FROM AllData_CTE tt
	)

SELECT	Insurance
		, InsAddress
		, MnthNmbr
		, DOS
		, Completed
		, AccountStatus
		, PercentDrop
		, DropRange
FROM FinalData
WHERE @Range IS NULL OR DropRange = @Range

GO

GRANT EXECUTE ON Rpt.SSRSCompletedByAttorneyPI TO PUBLIC;


USE EMRIWarehouse
GO

ALTER PROCEDURE Rpt.SSRSSFUserProductivity
@UserName varchar(255)

AS

/* IF YOU MODIFY THE OLDEST START DATE THAT IS USED FOR FILTERING, ALSO CHANGE Rpt.uspGetProductivityUserNames*/

SET DATEFIRST 1 --used to change the start of the week to Monday instead of system default of Sunday for just this session

DECLARE @StartLastWeek date = (SELECT DATEADD(wk, -1, DATEADD(DAY, 1-DATEPART(WEEKDAY, GETDATE()), DATEDIFF(dd, 0, GETDATE())))) --Start week on sunday
		, @EndLastWeek date = (SELECT DATEADD(wk, 0, DATEADD(DAY, 0-DATEPART(WEEKDAY, GETDATE()), DATEDIFF(dd, 0, GETDATE()))))
		, @StartLastMonth date = (SELECT dbo.udfFirstDayLastMonth(GETDATE()))
		, @EndLastMonth date = (SELECT dbo.udfEndDayLastMonth(GETDATE()))
		
--separate decalre since I didn't use SET and reference variables that are just declared		
DECLARE @StartLastQtr date = (SELECT DATEADD(MONTH, -2, @StartLastMonth))
		, @EndLastQtr date = @EndLastMonth

-- SELECT @StartLastWeek as StartLastWeek, @EndLastWeek as EndLastWeek, @StartLastMonth as StartLastMonth, @EndLastMonth as EndLastMonth, @StartLastQtr as StartLastQtr, @EndLastQtr as EndLastQtr

SELECT COALESCE(u.CompletedBy, u.AssignedTo) AS UserName
	   , u.Task
	   , u.Reason
	   , u.Activity
	   , SUM(CASE WHEN u.CompletedDate > @EndLastWeek THEN 1 ELSE 0 END) AS CompletedThisWeek
	   , SUM(CASE WHEN u.CompletedDate BETWEEN @StartLastWeek AND @EndLastWeek THEN 1 ELSE 0 END) AS CompletedLastWeek
	   , SUM(CASE WHEN u.CompletedDate BETWEEN @StartLastMonth AND @EndLastMonth THEN 1 ELSE 0 END) AS CompletedLastMonth
	   , SUM(CASE WHEN u.CompletedDate BETWEEN @StartLastQtr AND @EndLastQtr THEN 1 ELSE 0 END) AS CompletedLastQtr
FROM SF.UserTaskProductivity u
JOIN dbo.SSRSUsers s ON (COALESCE(u.CompletedBy, u.AssignedTo) = s.UserName  OR s.isAdmin = 1)
WHERE CompletedDate >= @StartLastQtr
--	  AND (COALESCE(u.CompletedBy, u.AssignedTo) = @UserName OR @UserName = 'ALL')
	  AND u.TaskStatus = 'Completed'
	  AND s.domainUser = @UserName 
GROUP BY COALESCE(u.CompletedBy, u.AssignedTo)
	     , u.Task
	     , u.Reason
	     , u.Activity



GO

GRANT EXECUTE ON Rpt.SSRSSFUserProductivity  TO PUBLIC

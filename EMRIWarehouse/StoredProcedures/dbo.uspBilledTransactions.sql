USE [EMRIWarehouse]
GO
/****** Object:  StoredProcedure [dbo].[uspBilledTransactions]    Script Date: 10/24/2017 8:53:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[uspBilledTransactions]
AS

TRUNCATE TABLE dbo.BilledTransactions

EXEC dbo.uspChargesCredits

/****************************************************************************************************/
/*										Credits AKA payments										*/
/****************************************************************************************************/

INSERT INTO dbo.BilledTransactions (
		PracticeName
		, Entity
		, ApptId
		, TransactionNumber
		, Account
		, SSN
		, PatientName
		, BirthDate
		, Sex
		, PatientAddress
		, PatientCity
		, PatientState
		, PatientZip
		, PatientPhone
		, InsCode1
		, Insurance1
		, InsurancePolicy
		, ADJNumber
		, InsCode2
		, Insurance2
		, ARClass
		, InsuranceAddress
		, InsuranceCity
		, InsuranceState
		, InsuranceZip
		, InsurancePhone
		, InsuranceFax
		, PostingDate
		, DOS
		, DOI
		, ApptBaseCPTCode
		, ApptCPTCode
		, ApptCPTDescription
		, ChargesBaseCPT
		, ChargesCPTCode
		, ChargesCPTDescription
		, Modality
		, Diag
		, OP
		, Provider
		, ProvFirstName
		, ProvLastName
		, ProviderName
		, NPI
		, RefCode
		, RefName
		, Employer
		, AttorneyCode
		, AttorneyName 
		, AttorneyAddress1 
		, AttorneyAddress2 
		, AttorneyCity 
		, AttorneyState
		, AttorneyZip 
		, AttorneyPhone
		, AttorneyFax 
		, Plc
		, Dpt
		, Qty
		, Balance
		, IsCharge
		, FinancialCode
		, Operator
	)

SELECT	e.CompanyName as PracticeName
		, e.Id AS Entity
		, apt2.apptId
		, cc.CreditTransactionNo
		, apt2.Account
		, p.SSN
		, p.Name AS PatientName
		, p.Dob AS Birthdate
		, p.Sex
		, RTRIM(p.Address1  + ' ' + COALESCE(p.Address2, ''))
		, apt.City
		, apt.State
		, p.ZipCode
		, p.HomePhone
		, COALESCE(cg.InsCode1, apt2.InsCode)
		, COALESCE(cg.InsName1, apt2.InsName)
		--, apt2.InsCode
		--, ins.Name AS Insurance1
		, dbo.udfParseInsPolicy(apt.InsPolicy) AS InsurancePolicy
		, null--dbo.udfParseAdj(apt2.ApptId) AS ADJ
		, cg.InsCode2
		, cg.InsName2 AS Insurance2
		--, apt2.ARClass
		, cg.ARClass
		, RTRIM(ins.Address1 + ' ' + COALESCE(ins.Address2, ''))
		, ins.City 
		, ins.State
		, ins.ZipCode
		, ins.Phone
		, ins.Fax
		, cr.PostingDate
		, apt2.DOS
		, apt2.DOI
		, LEFT(apt.ProcCode, 5) AS ApptBaseCPTCode
		, apt.ProcCode AS ApptCPTCode
		, apt.ProcDesc AS ApptCPTDescription
		, LEFT(cg.Cpt, 5) AS ChargesBaseCPT 
		, cg.Cpt AS ChargesCPTCode
		, cg.CptDescr AS ChargesCPTDescription
		, apt2.Modality
		, Dx1 AS Diag
		, null AS OP
		, cg.Provider
		, cg.ProvFirstName
		, cg.ProvLastName
		, cg.ProvLastName + ', '+ cg.ProvFirstName AS ProviderName
		, cg.ProvNPI AS NPI
		, apt.RefCode
		, apt.RefName
		, g.Employer
		, att.Code
		, att.PracticeName
		, att.Address1 
		, att.Address2 
		, att.City 
		, att.State
		, att.ZipCode
		, att.Phone
		, att.Fax 
		, CASE WHEN CHARINDEX(' ', apt.PlaceCode) = 0 THEN apt.PlaceCode
			   ELSE LEFT(apt.PlaceCode, CHARINDEX(' ', apt.PlaceCode) - 1)
		  END AS Place
		, cg.Department
		, cg.Qty
		, - SUM(CASE WHEN LEN(Amount) <= 2 THEN Amount ELSE LEFT(Amount, LEN(Amount) - 2) + '.' + RIGHT(Amount,2) END) AS Balance
		, 0 AS IsCharge
		, cr.FinancialCode
		, cr.Operator
FROM Integration.Stg.Charges cg
JOIN dbo.ChargesCredits cc ON cc.ChargeTransactionNo = cg.TransactionNo
JOIN Integration.Stg.Credits cr ON cr.TransactionNo = cc.CreditTransactionNo
LEFT JOIN Integration.Stg.Appointments apt ON cg.AptId = apt.ApptId AND apt.PatName NOT LIKE '+%'
LEFT JOIN dbo.Appointments apt2 ON cg.AptId = apt2.ApptId
LEFT JOIN Integration.Stg.Entities e ON e.Id = cg.Entity
--LEFT JOIN Integration.Stg.Insurances ins ON apt.InsCode = ins.Id
LEFT JOIN Integration.Stg.Insurances ins ON COALESCE(cg.InsCode1, apt2.InsCode)  = ins.Id
LEFT JOIN Integration.Stg.Patients p ON p.Account = apt.Account
LEFT JOIN Integration.Stg.Guarantors g ON g.Account = apt.Account
LEFT JOIN Integration.Stg.Attorneys att ON p.Reserved = att.Code
GROUP BY e.CompanyName
		, e.Id
		, apt2.apptId
		, cc.CreditTransactionNo
		, apt2.Account
		, p.SSN
		, p.[Name] 
		, p.Dob 
		, p.Sex
		, RTRIM(p.Address1  + ' ' + COALESCE(p.Address2, ''))
		, apt.City
		, apt.State
		, p.ZipCode
		, p.HomePhone
		--, apt2.InsCode
		--, ins.Name
		, COALESCE(cg.InsCode1, apt2.InsCode)
		, COALESCE(cg.InsName1, apt2.InsName)
		, dbo.udfParseInsPolicy(apt.InsPolicy)
		--dbo.udfParseAdj(apt2.ApptId) AS ADJ
		, cg.InsCode2
		, cg.InsName2
		--, apt2.ARClass
		, cg.ARClass
		, RTRIM(ins.Address1 + ' ' + COALESCE(ins.Address2, ''))
		, ins.City 
		, ins.State
		, ins.ZipCode
		, ins.Phone
		, ins.Fax
		, cr.PostingDate
		, apt2.DOS
		, apt2.DOI
		, LEFT(apt.ProcCode, 5)
		, apt.ProcCode
		, apt.ProcDesc
		, LEFT(cg.Cpt, 5)
		, cg.Cpt
		, cg.CptDescr
		, apt2.Modality
		, Dx1 
		, cg.Provider
		, cg.ProvFirstName
		, cg.ProvLastName
		, cg.ProvLastName + ', '+ cg.ProvFirstName
		, cg.ProvNPI 
		, apt.RefCode
		, apt.RefName
		, g.Employer
		, att.Code
		, att.PracticeName
		, att.Address1 
		, att.Address2 
		, att.City 
		, att.State
		, att.ZipCode
		, att.Phone
		, att.Fax 
		, CASE WHEN CHARINDEX(' ', apt.PlaceCode) = 0 THEN apt.PlaceCode
			   ELSE LEFT(apt.PlaceCode, CHARINDEX(' ', apt.PlaceCode) - 1)
		  END
		, cg.Department
		, cg.Qty
		, cr.FinancialCode
		, cr.Operator

/****************************************************************************************************/
/*										Charges														*/
/****************************************************************************************************/

INSERT INTO dbo.BilledTransactions (
		PracticeName
		, Entity
		, ApptId
		, TransactionNumber
		, Account
		, SSN
		, PatientName
		, BirthDate
		, Sex
		, PatientAddress
		, PatientCity
		, PatientState
		, PatientZip
		, PatientPhone
		, InsCode1
		, Insurance1
		, InsurancePolicy
		, ADJNumber
		, InsCode2
		, Insurance2
		, ARClass
		, InsuranceAddress
		, InsuranceCity
		, InsuranceState
		, InsuranceZip
		, InsurancePhone
		, InsuranceFax
		, PostingDate
		, DOS
		, DOI
		, ApptBaseCPTCode
		, ApptCPTCode
		, ApptCPTDescription
		, ChargesBaseCPT
		, ChargesCPTCode
		, ChargesCPTDescription
		, Modality
		, Diag
		, OP
		, Provider
		, ProvFirstName
		, ProvLastName
		, ProviderName
		, NPI
		, RefCode
		, RefName
		, Employer
		, AttorneyCode
		, AttorneyName 
		, AttorneyAddress1 
		, AttorneyAddress2 
		, AttorneyCity 
		, AttorneyState
		, AttorneyZip 
		, AttorneyPhone
		, AttorneyFax 
		, Plc
		, Dpt
		, Qty
		, Balance
		, IsCharge
		, Operator
	)

SELECT DISTINCT	e.CompanyName as PracticeName
		, e.Id AS Entity
		, apt.apptId
		, cg.TransactionNo
		, apt2.Account
		, p.SSN
		, p.Name AS PatientName
		, p.Dob AS Birthdate
		, p.Sex
		, RTRIM(p.Address1  + ' ' + COALESCE(p.Address2, ''))
		, apt.City
		, apt.State
		, p.ZipCode
		, p.HomePhone
		, COALESCE(cg.InsCode1, apt2.InsCode)
		, COALESCE(cg.InsName1, apt2.InsName)
		--, apt2.InsCode
		--, ins.Name AS Insurance1
		, dbo.udfParseInsPolicy(apt.InsPolicy) AS InsurancePolicy
		, null--dbo.udfParseAdj(apt2.ApptId) AS ADJ
		, cg.InsCode2
		, cg.InsName2 AS Insurance2
		--, apt2.ARClass
		, cg.ARClass
		, RTRIM(ins.Address1 + ' ' + COALESCE(ins.Address2, ''))
		, ins.City 
		, ins.State
		, ins.ZipCode
		, ins.Phone
		, ins.Fax
		, cg.PostingDate
		, apt2.DOS
		, apt2.DOI
		, LEFT(apt.ProcCode, 5) AS ApptBaseCPTCode
		, apt.ProcCode AS ApptCPTCode
		, apt.ProcDesc AS ApptCPTDescription
		, LEFT(cg.Cpt, 5) AS ChargesBaseCPT 
		, cg.Cpt AS ChargesCPTCode
		, cg.CptDescr AS ChargesCPTDescription
		, apt2.Modality
		, Dx1 AS Diag
		, null AS OP
		, cg.Provider
		, cg.ProvFirstName
		, cg.ProvLastName
		, cg.ProvLastName + ', '+ cg.ProvFirstName AS ProviderName
		, cg.ProvNPI AS NPI
		, apt.RefCode
		, apt.RefName
		, g.Employer
		, att.Code
		, att.PracticeName
		, att.Address1 
		, att.Address2 
		, att.City 
		, att.State
		, att.ZipCode
		, att.Phone
		, att.Fax 
		, CASE WHEN CHARINDEX(' ', apt.PlaceCode) = 0 THEN apt.PlaceCode
			   ELSE LEFT(apt.PlaceCode, CHARINDEX(' ', apt.PlaceCode) - 1)
		  END AS Place
		, Department
		, Qty
		, SqlAmount AS Balance
		, 1 AS IsCharge
		, cg.Operator
FROM Integration.Stg.Charges cg
LEFT JOIN Integration.Stg.Appointments apt ON cg.AptId = apt.ApptId AND apt.PatName NOT LIKE '+%'
LEFT JOIN dbo.Appointments apt2 ON cg.AptId = apt2.ApptId
LEFT JOIN Integration.Stg.Entities e ON e.Id = cg.Entity
--LEFT JOIN Integration.Stg.Insurances ins ON apt.InsCode = ins.Id
LEFT JOIN Integration.Stg.Insurances ins ON COALESCE(cg.InsCode1, apt.InsCode) = ins.Id
LEFT JOIN Integration.Stg.Patients p ON p.Account = apt.Account
LEFT JOIN Integration.Stg.Guarantors g ON g.Account = apt.Account
LEFT JOIN Integration.Stg.Attorneys att ON p.Reserved = att.Code



GO

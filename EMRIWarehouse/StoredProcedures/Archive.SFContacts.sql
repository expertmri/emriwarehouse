USE EMRIWarehouse
GO

CREATE PROCEDURE Archive.SFContacts
   @SF_ContactId varchar(255)
   , @SFUser varchar(255)
   , @Activity varchar(50)
AS 

SET NOCOUNT ON;
	
BEGIN
	INSERT INTO Archive.Contacts (
			ContactId
			, SF_ContactId
			, FirstName
			, LastName
			, MiddleName
			, Salutation
			, Suffix
			, JobTitle
			, Phone
			, Fax
			, Email
			, CreateDate
			, ModifiedDate
			, ADSKey
			, ContactType
			, Activity
			, ModifiedUser
			, InsertDate
		)
	SELECT	ContactId
			, SF_ContactId
			, FirstName
			, LastName
			, MiddleName
			, Salutation
			, Suffix
			, JobTitle
			, Phone
			, Fax
			, Email
			, CreateDate
			, ModifiedDate
			, ADSKey
			, ContactType
			, @Activity
			, @SFUser
			, GETDATE()
	FROM SF.Contacts
	WHERE SF_ContactId = @SF_ContactId
END
	
GO




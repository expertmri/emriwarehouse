USE EMRIWarehouse
GO

ALTER PROCEDURE Rpt.uspGetOpenTaskUserNames
AS

SELECT DISTINCT AssignedTo AS UserName
FROM SF.UserTaskProductivity
WHERE TaskStatus = 'Open'
UNION ALL
SELECT 'ALL' AS UserName
ORDER BY UserName 

GO

GRANT EXECUTE ON Rpt.uspGetOpenTaskUserNames TO PUBLIC;
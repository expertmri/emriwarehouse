USE [EMRIWarehouse]
GO
/****** Object:  StoredProcedure [Rpt].[SSRSScansReceivedPerModality]    Script Date: 10/24/2017 8:53:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [Rpt].[SSRSScansReceivedPerModality]
AS

DECLARE @Calendar TABLE (
		CDate date
		, CMonth int
		, Daily int
		, MTD int
		, YTD int
	)
	
DECLARE @CurrentDate date = '1/1/2017'
WHILE @CurrentDate <= GETDATE()

	BEGIN
		INSERT INTO @Calendar (CDate, CMonth)
		SELECT @CurrentDate, DATEPART(Month, @CurrentDate)

		SET @CurrentDate = DATEADD(day, 1, @CurrentDate)
	END

SELECT COUNT(Account) as DailyCount
	   , r.BookedDate
INTO #Daily
FROM dbo.Appointments r
WHERE ARClass not in ('CPR', 'RE', 'SOH', 'CCH')
GROUP BY r.BookedDate

UPDATE c
SET Daily = COALESCE(d.DailyCount, 0)
FROM @Calendar c
LEFT JOIN #Daily d ON d.BookedDate = c.CDate

SET @CurrentDate = (SELECT MIN(CDate) FROM @Calendar)
DECLARE @MaxDate date = (SELECT MAX(CDate) FROM @Calendar)

WHILE @CurrentDate <= @MaxDate

BEGIN
	UPDATE @Calendar
	SET MTD = (	SELECT SUM(Daily) 
				FROM @Calendar 
				WHERE CDate <= @CurrentDate 
					  AND DATEPART(month, @CurrentDate) = CMonth
			   )
		, YTD = (	SELECT SUM(Daily)  
					FROM @Calendar 
					WHERE CDate <= @CurrentDate
				)
	WHERE CDate = @CurrentDate

	SET @CurrentDate = DATEADD(day, 1, @CurrentDate)
END


SELECT	r.BookedDate as DOS
		, COUNT(COALESCE (r.ApptId, r.Account)) as DailyCount
	    , c.MTD
		, c.YTD
	    , r.Modality
INTO #Piv
FROM @Calendar c
LEFT JOIN dbo.Appointments r ON c.CDate = r.BookedDate 
AND ARClass not in ('CPR', 'RE', 'SOH', 'CCH')
--WHERE DATEPART(MONTH, DOS) = CASE DATEPART(DAY, GETDATE()) WHEN 1 THEN (DATEPART(MONTH, GETDATE()) - 1)
--			ELSE DATEPART(MONTH, GETDATE()) END
GROUP BY r.BookedDate
		 , c.MTD
		 , c.YTD
	     , r.Modality


SELECT CONVERT(varchar(10), DOS, 101) AS Received
		, COALESCE([ARTH], 0) AS ARTH
		, COALESCE([CR], 0) AS CR
		, COALESCE([CT], 0) AS CT
		, COALESCE([MRI], 0) AS MRI
		, MTD AS MTD
		, YTD AS YTD
FROM	(
			SELECT	DOS
					, DailyCount
					, MTD
					, YTD
					, Modality
			FROM #Piv
		) tt
PIVOT (SUM(DailyCount) FOR Modality IN ([CR], [ARTH], [MRI], [CT])) as t
WHERE DATEPART(MONTH, DOS) = CASE DATEPART(DAY, GETDATE()) WHEN 1 THEN (DATEPART(MONTH, GETDATE()) - 1)
			ELSE DATEPART(MONTH, GETDATE()) END

GO
GRANT EXECUTE ON [Rpt].[SSRSScansReceivedPerModality] TO [public] AS [dbo]
GO

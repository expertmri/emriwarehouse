USE [EMRIWarehouse]
GO
/****** Object:  StoredProcedure [Rpt].[SSRSCashPriceStats]    Script Date: 11/14/2019 10:27:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [Rpt].[SSRSCashPriceStats]
AS

SELECT	apt.BookedDate
		, apt.BookedTime
		, apt.ApptId
		, apt.Account
		, apt.Facility
		, apt.DOS
		, apt.ApptTime
		, apt.Modality
		, apt.ARClass
		, apt.InsName
		, COALESCE(CASE WHEN apt.AppointmentStatusId = 4 THEN--Exam Completed
				CASE WHEN apt.ARClass = 'PIINT' THEN 380 
					   WHEN apt.ARClass = 'PILIEN' THEN 400  
					   ELSE ABS(SUM(bt.Balance)) END
			   ELSE ABS(SUM(bt.Balance)) END, 0) AS TotalPayments
		, apt.CancelReasonCode
		, apt.CancelReasonDescription
		, p.PatientEmail
		, '(' + REPLACE(p.PatHomePhone, '/', ') ') AS PatHomePhone
		, '(' + REPLACE(p.PatientMobilePhone, '/', ') ') AS PatientMobilePhone
		, apt.AppointmentStatus
		, apt.MarketingReferral
		, apt.Operator
FROM EMRIWarehouse.dbo.Appointments apt
LEFT JOIN Integration.stg.Appointments p ON apt.ApptId = p.ApptId
LEFT JOIN dbo.BilledTransactions bt ON COALESCE(apt.ApptId, apt.Account) = COALESCE(bt.ApptId, bt.Account) AND ischarge = 0 AND Financialcode IN ('CASH','CC','CCOP','GRP','HFSP','INCK','KHP','MISP','MLP','PRP','PTCK')
WHERE MarketingReferral IN ('TV TV AD', 'SM SOCIAL MEDIA', 'S2 SOCIAL MEDIA GETEXPERT')
GROUP BY apt.BookedDate
		, apt.BookedTime
		, apt.ApptId
		, apt.Account
		, apt.Facility
		, apt.DOS
		, apt.ApptTime
		, apt.Modality
		, apt.ARClass
		, apt.InsName
		, apt.CancelReasonCode
		, apt.CancelReasonDescription
		, p.PatientEmail
		, '(' + REPLACE(p.PatHomePhone, '/', ') ') 
		, '(' + REPLACE(p.PatientMobilePhone, '/', ') ') 
		, apt.AppointmentStatus
		, apt.MarketingReferral
		, apt.Operator
		, apt.AppointmentStatusId

		GO

GRANT EXECUTE ON Rpt.SSRSCashPriceStats TO PUBLIC;
GO





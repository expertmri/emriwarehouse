USE [EMRIWarehouse]
GO
/****** Object:  StoredProcedure [Rpt].[SSRSCompletedScansMarketingReport]    Script Date: 4/11/2018 12:07:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE Rpt.SSRSCompletedScansMarketingReport
@RawOutput bit = 0
, @TopNDroppedOutput bit = 0
, @TopNRange int = 50
, @DetailedOutput bit = 0
, @NewReferrer bit = 0

AS

DECLARE @StartDate date
		, @FirstOfMonth date
		, @FirstDayLastMonth date
		, @EndDayLastMonth date

SELECT @FirstOfMonth = dbo.udfFirstDayOfMonth(GETDATE())
	   , @FirstDayLastMonth = dbo.udfFirstDayLastMonth(GETDATE())
	   , @EndDayLastMonth = dbo.udfEndDayLastMonth(GETDATE())

CREATE TABLE #Monthly (
	SFPK varchar(50)
	, ReferralName varchar(255)
	, ScanCountMonthly int
	, SixMonthAvg int
	, SixMonthTotal int
	, MonthlyForecast int
	, PercentDrop int
	, AccountStatus varchar(40)
	, MonthDate date
	, MonthName varchar(20)
	, ContactType varchar(30)
	, RN int
	)


INSERT INTO #Monthly (SFPK, ReferralName, ScanCountMonthly, MonthDate, MonthName, ContactType)
SELECT  sfc.SF_ContactId AS SFPK
		, dbo.udfConcatFullName(sfc.FirstName, sfc.MiddleName, sfc.LastName, sfc.Suffix) AS ReferralName
		, COUNT(apt.ApptId) AS ScanCountMonthly
		, dbo.udfFirstDayOfMonth(apt.DOS) AS MonthDate
		, dbo.udfGetMonthName(apt.DOS) AS MonthName
		, 'Doctor' AS ContactType
FROM dbo.Appointments apt
LEFT JOIN Integration.dbo.Contacts c ON apt.ReferralCode = c.SourceSystemPk AND c.ContactType = 'Doctor'
JOIN SF.Contacts sfc ON sfc.SF_ContactId = c.SF_ContactId
JOIN XRef.AppointmentStatus stat ON apt.AppointmentStatusId = stat.AppointmentStatusId
LEFT JOIN XRef.ARClassGroup ar ON apt.ARClass = ar.ARClass
WHERE apt.Modality = 'MRI'
		AND apt.AppointmentStatusId = 4 --Exam Complete
		AND ar.IsNonRevenue = 0
		AND apt.DOS >= DATEADD(MONTH, -6, @FirstOfMonth)
GROUP BY sfc.SF_ContactId
		 , dbo.udfConcatFullName(sfc.FirstName, sfc.MiddleName, sfc.LastName, sfc.Suffix)
		 , dbo.udfFirstDayOfMonth(apt.DOS) 
		 , dbo.udfGetMonthName(apt.DOS)
UNION ALL
SELECT  sfc.SF_AccountId AS SFPK
		, sfc.AccountName AS ReferralName
		, COUNT(apt.ApptId) AS ScanCountMonthly
		, dbo.udfFirstDayOfMonth(apt.DOS) AS MonthDate
		, dbo.udfGetMonthName(apt.DOS) AS MonthName
		, 'Attorney' AS ContactType
FROM dbo.Appointments apt
LEFT JOIN Integration.dbo.Accounts c ON apt.RefAttyCode = c.SourceSystemPk AND c.SourceTable = 'Attorney'
JOIN SF.Accounts sfc ON sfc.SF_AccountId = c.SF_AccountId
JOIN XRef.AppointmentStatus stat ON apt.AppointmentStatusId = stat.AppointmentStatusId
LEFT JOIN XRef.ARClassGroup ar ON apt.ARClass = ar.ARClass
WHERE apt.Modality = 'MRI'
		AND apt.AppointmentStatusId = 4 --Exam Complete
		AND ar.IsNonRevenue = 0
		AND ar.IsPersonalInjury = 1
		AND apt.DOS >= DATEADD(MONTH, -6, @FirstOfMonth)
GROUP BY sfc.SF_AccountId
		 , sfc.AccountName
		 , dbo.udfFirstDayOfMonth(apt.DOS) 
		 , dbo.udfGetMonthName(apt.DOS)

INSERT INTO #Monthly (SFPK, ReferralName, ScanCountMonthly, MonthDate, MonthName, ContactType)
SELECT DISTINCT dm.SFPK
		, dm.ReferralName
		, 0 AS ScanCountMonthly
		, dbo.udfFirstDayOfMonth(GETDATE()) AS MonthDate
		, dbo.udfGetMonthName(GETDATE())
		, 'Doctor' AS ContactType
FROM #Monthly dm
LEFT JOIN	( SELECT SFPK
					 , MAX(MonthDate) AS LatestScanDate
			  FROM #Monthly
			  GROUP BY SFPK
			) dm2 ON dm.SFPK = dm2.SFPK AND dm2.LatestScanDate = @FirstOfMonth
WHERE dm2.SFPK IS NULL

INSERT INTO #Monthly (SFPK, ReferralName, ScanCountMonthly, MonthDate, MonthName, ContactType)
SELECT DISTINCT dm.SFPK
		, dm.ReferralName
		, 0 AS ScanCountMonthly
		, dbo.udfFirstDayOfMonth(GETDATE()) AS MonthDate
		, dbo.udfGetMonthName(GETDATE())
		, 'Attorney' AS ContactType
FROM #Monthly dm
LEFT JOIN	( SELECT SFPK
					 , MAX(MonthDate) AS LatestScanDate
			  FROM #Monthly
			  GROUP BY SFPK
			) dm2 ON dm.SFPK = dm2.SFPK AND dm2.LatestScanDate = @FirstOfMonth
WHERE dm2.SFPK IS NULL

;WITH avgMonthly AS
(
	SELECT SFPK
			, SUM(ScanCountMonthly) AS SixMonthTotal
	FROM #Monthly dm
	WHERE dm.[MonthName] <> dbo.udfGetMonthName(GETDATE())
	GROUP BY SFPK
)

UPDATE m
SET SixMonthAvg = COALESCE(ct.SixMonthTotal, 0)/6
	, SixMonthTotal = COALESCE(ct.SixMonthTotal, 0)
	, MonthlyForecast = tt.MonthlyForecast
	, AccountStatus = CASE WHEN tt.MonthlyForecast < CAST(COALESCE(ct.SixMonthTotal, 0)/6 AS decimal) THEN 'DROPPED' ELSE 'GOOD' END
	, PercentDrop = CASE WHEN tt.MonthlyForecast < CAST(COALESCE(ct.SixMonthTotal, 0)/6 AS decimal)  THEN CEILING(((CAST(COALESCE(ct.SixMonthTotal, 0)/6 AS decimal) - tt.MonthlyForecast)/CAST(COALESCE(ct.SixMonthTotal, 0)/6 AS decimal)) * 100) END
FROM #Monthly m
LEFT JOIN avgMonthly ct ON m.SFPK = ct.SFPK
LEFT JOIN	(
				SELECT SFPK
					   , CAST((CAST(ScanCountMonthly AS decimal)/CAST(DATEPART(DAY, GETDATE()) - 1 AS decimal))  * CAST(DAY(EOMONTH(GETDATE())) AS decimal) AS decimal) AS MonthlyForecast
				FROM #Monthly
				WHERE @FirstOfMonth = dbo.udfFirstDayOfMonth(MonthDate)
			) tt ON m.SFPK = tt.SFPK

;WITH contactList AS
	(
		SELECT DISTINCT SFPK
			   , ContactType
			   , SixMonthTotal 
		FROM #Monthly
		WHERE AccountStatus = 'DROPPED'
	)

, Ranking AS
	(
		SELECT DISTINCT SFPK
			   , ROW_NUMBER() OVER (PARTITION BY ContactType ORDER BY SixMonthTotal DESC) AS RN 
		FROM contactList
	)

UPDATE m
SET RN = r.RN
FROM #Monthly m
JOIN Ranking r ON m.SFPK = r.SFPK

--Never referred
SELECT  c.SF_ContactId AS SFPK
INTO #never
FROM dbo.Appointments apt
LEFT JOIN Integration.dbo.Contacts c ON apt.ReferralCode = c.SourceSystemPk AND c.ContactType = 'Doctor'
JOIN XRef.AppointmentStatus stat ON apt.AppointmentStatusId = stat.AppointmentStatusId
LEFT JOIN XRef.ARClassGroup ar ON apt.ARClass = ar.ARClass
WHERE apt.Modality = 'MRI'
		AND apt.AppointmentStatusId = 4 --Exam Complete
		AND ar.IsNonRevenue = 0
GROUP BY c.SF_ContactId 
HAVING MIN(apt.DOS) >= @FirstOfMonth
UNION ALL
SELECT  c.SF_AccountId AS SFPK
FROM dbo.Appointments apt
LEFT JOIN Integration.dbo.Accounts c ON apt.RefAttyCode = c.SourceSystemPk AND c.SourceTable = 'Attorney'
JOIN XRef.AppointmentStatus stat ON apt.AppointmentStatusId = stat.AppointmentStatusId
LEFT JOIN XRef.ARClassGroup ar ON apt.ARClass = ar.ARClass
WHERE apt.Modality = 'MRI'
		AND apt.AppointmentStatusId = 4 --Exam Complete
		AND ar.IsNonRevenue = 0
		AND ar.IsPersonalInjury = 1
GROUP BY c.SF_AccountId
HAVING MIN(apt.DOS) >= @FirstOfMonth		 

IF @RawOutput = 1
SELECT	SFPK
		, ReferralName
		, ScanCountMonthly
		, SixMonthAvg
		, SixMonthTotal
		, MonthlyForecast
		, PercentDrop
		, AccountStatus
		, MonthDate
		, MonthName
		, ContactType
		, RN
FROM #Monthly

IF @TopNDroppedOutput = 1
SELECT	SFPK
		, ReferralName
		, ScanCountMonthly
		, SixMonthAvg
		, SixMonthTotal
		, MonthlyForecast
		, PercentDrop
		, AccountStatus
		, MonthDate
		, MonthName
		, ContactType
		, RN
		, CASE WHEN AccountStatus = 'DROPPED' THEN
						CASE WHEN PercentDrop <= 10 THEN '0-10'
							 WHEN PercentDrop BETWEEN 11 AND 20 THEN '11-20'
							 WHEN PercentDrop BETWEEN 21 AND 40 THEN '21-40'
							 WHEN PercentDrop BETWEEN 41 AND 60 THEN '41-60'
							 WHEN PercentDrop BETWEEN 61 AND 80 THEN '61-80'
							 WHEN PercentDrop BETWEEN 81 AND 99 THEN '81-99'
							 ELSE '100' END
				END AS DropRange
FROM #Monthly
WHERE AccountStatus = 'DROPPED' 
	  AND RN <= @TopNRange

IF @DetailedOutput = 1
SELECT	m.SFPK
		, m.ReferralName
		, a.AccountName
		, LTRIM(RTRIM(a.Address1 + ' ' + COALESCE(a.Address2, ''))) AS Address
		, a.City
		, a.StateName
		, a.Zip
		, a.Phone
		, a.Fax
		, m.ScanCountMonthly
		, m.SixMonthAvg
		, m.SixMonthTotal
		, m.MonthlyForecast
		, m.PercentDrop
		, m.AccountStatus
		, m.MonthDate
		, m.MonthName
		, m.ContactType
		, m.RN
FROM #Monthly m
JOIN SF.Accounts a ON m.SFPK = a.SF_AccountId
WHERE AccountStatus = 'DROPPED' 
	  AND RN <= @TopNRange
	  AND m.ContactType = 'Attorney'
UNION ALL
SELECT	m.SFPK
		, m.ReferralName
		, a.AccountName
		, LTRIM(RTRIM(a.Address1 + ' ' + COALESCE(a.Address2, ''))) AS Address
		, a.City
		, a.StateName
		, a.Zip
		, a.Phone
		, a.Fax
		, m.ScanCountMonthly
		, m.SixMonthAvg
		, m.SixMonthTotal
		, m.MonthlyForecast
		, m.PercentDrop
		, m.AccountStatus
		, m.MonthDate
		, m.MonthName
		, m.ContactType
		, m.RN
FROM #Monthly m
JOIN Integration.dbo.Contacts c ON m.SFPK = c.SF_ContactId AND c.ContactType = 'Doctor'
JOIN Integration.dbo.Accounts a2 ON c.AccountId = a2.AccountId
JOIN SF.Accounts a ON a.SF_AccountId = a2.SF_AccountId
WHERE AccountStatus = 'DROPPED' 
	  AND RN <= @TopNRange
	  AND m.ContactType = 'Doctor'

IF @NewReferrer = 1
SELECT	m.SFPK
		, m.ReferralName
		, a.AccountName
		, LTRIM(RTRIM(a.Address1 + ' ' + COALESCE(a.Address2, ''))) AS Address
		, a.City
		, a.StateName
		, a.Zip
		, a.Phone
		, a.Fax
		, m.ScanCountMonthly
		, m.MonthlyForecast
		, m.MonthDate
		, m.MonthName
		, m.ContactType
FROM #Monthly m
JOIN #never n ON m.SFPK = n.SFPK
JOIN SF.Accounts a ON m.SFPK = a.SF_AccountId
WHERE m.ContactType = 'Attorney'
UNION ALL
SELECT	m.SFPK
		, m.ReferralName
		, a.AccountName
		, LTRIM(RTRIM(a.Address1 + ' ' + COALESCE(a.Address2, ''))) AS Address
		, a.City
		, a.StateName
		, a.Zip
		, a.Phone
		, a.Fax
		, m.ScanCountMonthly
		, m.MonthlyForecast
		, m.MonthDate
		, m.MonthName
		, m.ContactType
FROM #Monthly m
JOIN #never n ON m.SFPK = n.SFPK
JOIN Integration.dbo.Contacts c ON m.SFPK = c.SF_ContactId AND c.ContactType = 'Doctor'
JOIN Integration.dbo.Accounts a2 ON c.AccountId = a2.AccountId
JOIN SF.Accounts a ON a.SF_AccountId = a2.SF_AccountId
WHERE m.ContactType = 'Doctor'

GO	

GRANT EXECUTE ON Rpt.SSRSCompletedScansMarketingReport TO [public] 

GO

USE EMRIWarehouse
GO

ALTER PROCEDURE Rpt.SSRSEventDetails 
@EventName varchar(150) = 'ALL'

AS

SELECT e.AccountId AS SF_AccountId
		, a.Name AS AccountName
		, a.BillingStreet + ' ' + a.BillingStateCode + ', '+ a.BillingPostalCode AS Address
		--, a.Address1 + ' ' + a.City + ', ' + a.StateName + ' ' + a.Zip AS Address
		, a.Phone
		, r.Start_Date__c AS EventDate
		, e.Subject AS EventName
INTO #events
FROM Integration.SF.Events e
--JOIN SF.Accounts a ON e.AccountId = a.SF_AccountId /*shouldn't use because it only shows actual accounts opposed to leads */
JOIN Integration.SF.Account a ON e.AccountId = a.id
JOIN Integration.SF.Reference r ON e.Subject = r.Name
WHERE r.RecordTypeId = '012f4000001Du3pAAC' --exclude user created events
	  AND (r.Name = @EventName OR @EventName = 'ALL')

SELECT s.SF_AccountId
	   , s.DOS
	   , s.ApptId
INTO #scans	  
FROM dbo.Scans s
WHERE s.IsNonRevenue = 0
	  AND s.Modality = 'MRI'

SELECT	s.SF_AccountId
		, MIN(s.DOS) AS FirstScan
INTO #scansFirst
FROM #scans s
GROUP BY s.SF_AccountId

SELECT	s.SF_AccountId
		, COUNT(s.ApptId) AS ScansPrior
		, e.EventDate
INTO #scansPrior
FROM #scans s
JOIN #events e ON s.SF_AccountId = e.SF_AccountId
WHERE DOS BETWEEN DATEADD(MONTH, -3, e.EventDate) AND  e.EventDate 
GROUP BY s.SF_AccountId
		 , e.EventDate

SELECT	s.SF_AccountId
		, COUNT(s.ApptId) AS ScansAfter
		, e.EventDate
INTO #scansAfter
FROM #scans s
JOIN #events e ON s.SF_AccountId = e.SF_AccountId
WHERE DOS > e.EventDate AND DOS < DATEADD(MONTH, 3, e.EventDate)
GROUP BY s.SF_AccountId
		 , e.EventDate


SELECT	s.SF_AccountId
		, COUNT(s.ApptId) AS ScansAfter
		, e.EventDate
INTO #scansCurrent
FROM #scans s
JOIN #events e ON s.SF_AccountId = e.SF_AccountId
WHERE DOS > CAST(DATEADD(MONTH, -3, GETDATE()) AS DATE)
GROUP BY s.SF_AccountId
		 , e.EventDate

SELECT DISTINCT	e.SF_AccountId
		, e.AccountName
		, e.Address
		, e.Phone
		, e.EventDate
		, e.EventName
		, COALESCE(p.ScansPrior, 0)/3 AS ScansPrior
		, COALESCE(a.ScansAfter, 0)/3 AS ScansAfter
		, COALESCE(c.ScansAfter, 0)/3 AS ScansCurrent
		, f.FirstScan
FROM #events e
LEFT JOIN #scansPrior p ON e.SF_AccountId = p.SF_AccountId AND e.EventDate = p.EventDate
LEFT JOIN #scansAfter a ON e.SF_AccountId = a.SF_AccountId AND e.EventDate = a.EventDate
LEFT JOIN #scansCurrent c ON e.SF_AccountId = c.SF_AccountId AND e.EventDate = c.EventDate
LEFT JOIN #scansFirst f ON e.SF_AccountId = f.SF_AccountId

GO

GRANT EXECUTE ON Rpt.SSRSEventDetails TO PUBLIC;


--select * from Integration.sf.Reference where RecordTypeId = '012f4000001Du3pAAC'
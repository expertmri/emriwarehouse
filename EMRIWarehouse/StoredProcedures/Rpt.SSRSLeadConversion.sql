USE EMRIWarehouse
GO

ALTER PROCEDURE Rpt.SSRSLeadConversion 
@RunYear date = null
AS

--DECLARE @RunYear date = (SELECT dbo.udfFirstDayOfYear(GETDATE()))
--DECLARE @RunYear date = (SELECT dbo.udfFirstDayOfYear('12/31/2019'))
IF @RunYear IS NULL
BEGIN
	SET @RunYear = (SELECT dbo.udfFirstDayOfYear(GETDATE()))
END

SELECT  a.Id AS SF_AccountId
		, a.[Name]
		, a.BillingStreet + ' ' + a.BillingCity + ' ' + a.BillingStateCode + ' ' + a.BillingPostalCode AS AccountAddress
		, CAST(a.CreatedDate AS date) AS CreatedDate
		, tt.FirstExam
		, DATEDIFF(DAY, a.CreatedDate, tt.FirstExam) AS ConversionTime
		, u.FirstName + ' ' + u.LastName AS CreatedBy 
FROM Integration.SF.Account a
--LEFT JOIN Integration.SF.Users u ON a.OwnerId = u.Id
LEFT JOIN Integration.SF.Users u ON a.CreatedById = u.Id
JOIN (
		SELECT	a.SF_AccountId AS SF_AccountId
				, MIN(DOS) AS FirstExam
		FROM EMRIWarehouse.dbo.Appointments appt
		LEFT JOIN Integration.dbo.Accounts c ON c.SourceSystemPk = appt.RefAttyCode and c.SourceTable = 'Attorney'
		LEFT JOIN Integration.dbo.Accounts a ON a.SourceSystemPk = appt.ReferringDrId and a.SourceTable = 'Doctor'
		GROUP BY a.SF_AccountId
	) tt ON a.Id = tt.SF_AccountId
WHERE a.CreatedDate >= @RunYear

GO

GRANT EXECUTE ON Rpt.SSRSLeadConversion TO PUBLIC;


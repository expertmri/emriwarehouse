USE EMRIWarehouse
GO

ALTER PROCEDURE dbo.uspGetModality
AS

SELECT DISTINCT Modality
FROM dbo.Appointments
UNION
SELECT 'ALL'

GO

GRANT EXECUTE ON dbo.uspGetModality TO PUBLIC;
USE [EMRIWarehouse]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE Rpt.SSRSPendingReferrals
AS

SELECT appt.*
		, att.Phone as AttyPhone
		, att.Fax as AttyFax
		, dr.Phone as DrPhone
		, dr.FaxNumber as DrFax
FROM dbo.appointments appt
LEFT JOIN Integration.Stg.Attorneys att ON appt.RefAttyCode = att.Code
LEFT JOIN Integration.Stg.ReferringPhysicians dr ON appt.ReferralCode = dr.Code
WHERE	(
			appt.ReferringDr IN ('TBD To Be Determind', 'Pending')
			OR appt.InsName IN ('TBD To Be Determind', 'Pending')
			OR appt.ReferringAttorney IN ('TBD To Be Determind', 'Pending')
			OR LEN(att.Phone) < 10
		    OR LEN(att.Fax) < 10
		    OR att.Phone like '000%'
		    OR att.Fax like '000%'
		    OR LTRIM(RTRIM(att.Phone)) = ''
		    OR LTRIM(RTRIM(att.Fax)) = ''
			OR LEN(dr.Phone) < 10
		    OR LEN(dr.FaxNumber) < 10
		    OR dr.Phone like '000%'
		    OR dr.FaxNumber like '000%'
		    OR LTRIM(RTRIM(dr.Phone)) = ''
		    OR LTRIM(RTRIM(dr.FaxNumber)) = ''
		)
		AND appt.Facility <> 'INTAKE' 


GO
GRANT EXECUTE ON Rpt.SSRSPendingReferrals TO [public] AS [dbo]
GO

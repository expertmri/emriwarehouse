USE EMRIWarehouse
GO

ALTER PROCEDURE Rpt.uspGetProductivityUserNames
AS

SELECT DISTINCT CompletedBy AS UserName
FROM SF.UserTaskProductivity
WHERE CompletedDate > DATEADD(MONTH, -2, dbo.udfFirstDayLastMonth(GETDATE()))
UNION ALL
SELECT 'ALL' AS UserName
ORDER BY UserName 

GO

GRANT EXECUTE ON Rpt.uspGetProductivityUserNames TO PUBLIC;
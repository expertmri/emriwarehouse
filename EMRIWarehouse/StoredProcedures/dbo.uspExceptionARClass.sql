USE EMRIWarehouse
GO

ALTER PROCEDURE dbo.uspExceptionARClass
@Debug int = 0

AS

DECLARE @subject1 nvarchar(255)
	   , @EmailBody nvarchar(max)
	   , @EmailBody2 nvarchar(max)
	   , @CSSRowHeader nvarchar(max)
	   , @CSSRowHeaderCenter nvarchar(max)
	   , @emailTo nvarchar(MAX)
	   , @RcdCount int

SET @subject1 = 'New ARClass added to ADS'
IF @Debug = 1
	BEGIN
		SET @emailTo = 'scott.lundy@expertMri.com'
	END

IF @Debug = 0
	BEGIN
		SET @emailTo = 'scott.lundy@expertMri.com; saba.ejaz@expertmri.com;'
	END

SET @RcdCount = (
					SELECT COUNT(DISTINCT appt.ARClass)
					FROM dbo.Appointments appt
					LEFT JOIN [XRef].[ARClassGroup] acg ON appt.ARClass = acg.ARClass
					WHERE acg.ARClassGroupId IS NULL AND appt.ARClass IS NOT NULL
				)
	
SELECT @EmailBody = CAST((
							SELECT DISTINCT appt.ARClass AS 'tdcenter', ''
											, COALESCE(acg.ARGroup, '') AS 'td', ''
											, COALESCE(acg.DirectorGroup, '') AS 'td', ''
											, COALESCE(acg.IsNonRevenue, '')  AS 'tdcenter', ''
											, COALESCE(acg.IsPersonalInjury, '')  AS 'tdcenter', ''
											, COALESCE(acg.IsWorkComp, '')  AS 'tdcenter', ''
							FROM dbo.Appointments appt
							LEFT JOIN [XRef].[ARClassGroup] acg ON appt.ARClass = acg.ARClass
							WHERE acg.ARClassGroupId IS NULL AND appt.ARClass IS NOT NULL
							FOR XML PATH('tr'), ELEMENTS) AS NVARCHAR(MAX))

-- ============== ADD Row Styling ==================================
	SET @EmailBody = REPLACE(@EmailBody, '<td>', '<td style="padding-left 5%;font: 12px arial, sans-serif;;text-align: left;border: 1px solid #ccc;background: #d8ecf3;">') --#D6EBFF
	SET @EmailBody = REPLACE(@EmailBody, '<tdcenter>', '<td style="padding: 0.25rem;font: 12px arial, sans-serif;;text-align: center;border: 1px solid #ccc;background: #d8ecf3;">')
	SET @CSSRowHeader = '<th style="padding: 0.25rem;font: 15px arial, sans-serif;text-align: left;border: 1px solid #ccc;background: #ADD8E6;">'
	SET @CSSRowHeaderCenter = '<th style="padding: 0.25rem;font: 15px arial, sans-serif;text-align: center;border: 1px solid #ccc;background: #ADD8E6;">'
-- ============== Add rest of the HTML Body ========================

	SET @EmailBody = 
	'<html>
	<head>
	  <title></title>
	</head>
	<body onload="_l=''t'';" style="font: normal small/1.1 sans-serif;">
	<font color="black">The below ARClasses were added to ADS but do not exist in XRef.ARClassGroup and will be excluded from all reports until they are added to the table. Please provide the below fields 
			and the appropriate values.</font><br><br>


	  <table class="orange" style="border-collapse: collapse;width: 60%;"> 
	<thead>
	<tr> 
		' + @CSSRowHeaderCenter + 'ARClass</th> 
		' + @CSSRowHeaderCenter + 'ARGroup</th>
		' + @CSSRowHeaderCenter + 'DirectorGroup</th>
		' + @CSSRowHeaderCenter + 'IsNonRevenue</th>
		' + @CSSRowHeaderCenter + 'IsPersonalInjury</th>
		' + @CSSRowHeaderCenter + 'IsWorkComp</th>
	</tr> 
	</thead> 
	<tbody>
	' 
	+ ISNULL(@EmailBody,'')
	+ 
	'</tbody> 
	</table>
	</body>
	</html>';

SELECT @EmailBody2 = CAST((
							SELECT DISTINCT acg.ARClass AS 'td', ''
											, COALESCE(acg.ARGroup, '') AS 'td', ''
											, COALESCE(acg.DirectorGroup, '') AS 'td', ''
											, COALESCE(acg.IsNonRevenue, '0')  AS 'tdcenter', ''
											, COALESCE(acg.IsPersonalInjury, '0')  AS 'tdcenter', ''
											, COALESCE(acg.IsWorkComp, '0')  AS 'tdcenter', ''
							FROM XRef.ARClassGroup acg 
							FOR XML PATH('tr'), ELEMENTS) AS NVARCHAR(MAX))

-- ============== ADD Row Styling ==================================
	SET @EmailBody2 = REPLACE(@EmailBody2, '<td>', '<td style="padding-left 5%;font: 12px arial, sans-serif;;text-align: left;border: 1px solid #ccc;background: #d8ecf3;">') --#D6EBFF
	SET @EmailBody2 = REPLACE(@EmailBody2, '<tdcenter>', '<td style="padding: 0.25rem;font: 12px arial, sans-serif;;text-align: center;border: 1px solid #ccc;background: #d8ecf3;">')
	--SET @CSSRowHeader2 = '<th style="padding: 0.25rem;font: 15px arial, sans-serif;text-align: left;border: 1px solid #ccc;background: #ADD8E6;">'
	--SET @CSSRowHeaderCenter2 = '<th style="padding: 0.25rem;font: 15px arial, sans-serif;text-align: center;border: 1px solid #ccc;background: #ADD8E6;">'
-- ============== Add rest of the HTML Body ========================

	SET @EmailBody = @EmailBody +
	'<html>
	<head>
	  <title></title>
	</head>
	<body onload="_l=''t'';" style="font: normal small/1.1 sans-serif;">
	<br><br><font color="black">Below is the existing data in XRef.ARClassGroup. It is best for report groupings to reuse existing ARGroups and DirectorGroups where appropriate.</font><br><br>


	  <table class="orange" style="border-collapse: collapse;width: 60%;"> 
	<thead>
	<tr> 
		' + @CSSRowHeaderCenter + 'ARClass</th> 
		' + @CSSRowHeaderCenter + 'ARGroup</th>
		' + @CSSRowHeaderCenter + 'DirectorGroup</th>
		' + @CSSRowHeaderCenter + 'IsNonRevenue</th>
		' + @CSSRowHeaderCenter + 'IsPersonalInjury</th>
		' + @CSSRowHeaderCenter + 'IsWorkComp</th>
	</tr> 
	</thead> 
	<tbody>
	' 
	+ ISNULL(@EmailBody2,'')
	+ 
	'</tbody> 
	</table>
	</body>
	</html>';


IF @RcdCount > 0
BEGIN
	EXEC msdb.dbo.sp_send_dbmail  
	  @recipients = @emailTo
	, @subject = @subject1
	, @body_format = 'HTML'
	, @body = @EmailBody
END


GO
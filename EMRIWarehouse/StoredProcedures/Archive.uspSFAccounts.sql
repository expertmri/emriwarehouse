USE EMRIWarehouse
GO

ALTER PROCEDURE Archive.uspSFAccounts
   @SF_AccountId varchar(255)
   , @SFUser varchar(255)
   , @Activity varchar(50)
AS 

SET NOCOUNT ON;
	
BEGIN
	INSERT INTO Archive.Accounts (
			AccountId
			, SF_AccountId
			, AccountName
			, Phone
			, Fax
			, Website
			, Facility
			, Address1
			--, Address2
			, City
			, StateName
			, Zip
			, ADSAccountId
			, ParentId
			, AccountManager
			, Activity
			, ModifiedUser
			, InsertDate
		)
	SELECT	AccountId
			, SF_AccountId
			, AccountName
			, Phone
			, Fax
			, Website
			, Facility
			, Address1
			--, Address2
			, City
			, StateName
			, Zip
			, ADSAccountId
			, ParentId
			, AccountManager
			, @SFUser
			, @Activity
			, GETDATE()
	FROM SF.Accounts
	WHERE SF_AccountId = @SF_AccountId
END

GO




USE EMRIWarehouse
GO

CREATE PROCEDURE Rpt.SSRSCurrentBacklog
AS

SELECT COALESCE(ar.DirectorGroup, 'Unknown')  AS ARClass
		, apt.Modality
		, COUNT(apt.ApptId) AS RcdCnt
FROM dbo.Appointments apt
JOIN XRef.AppointmentStatus stat ON apt.AppointmentStatusId = stat.AppointmentStatusId
LEFT JOIN XRef.ARClassGroup ar ON apt.ARClass = ar.ARClass
WHERE stat.IsBacklog = 1
GROUP BY ar.DirectorGroup
		 , apt.Modality


GO

GRANT EXECUTE ON Rpt.SSRSCurrentBacklog TO PUBLIC;

USE [EMRIWarehouse]
GO
/****** Object:  StoredProcedure [Rpt].[SSRSScansReceivedPerModality]    Script Date: 11/1/2017 12:02:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [Rpt].[SSRSScansReceivedPerModality]
AS

DECLARE @StartDate date
		, @EndDate date
		, @YTD int

SET @StartDate = (SELECT CAST('1/1/' + CAST(DATEPART(YEAR, GETDATE()) AS varchar(10)) AS date))
SET @EndDate = (SELECT CAST(GETDATE() AS date))
SET @YTD = (
				SELECT COUNT(COALESCE (appt.ApptId, appt.Account))
				FROM dbo.Appointments appt
				JOIN XRef.ARClassGroup arg ON appt.ARClass = arg.ARClass 
				WHERE appt.BookedDate < DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)
					  AND appt.BookedDate >= @StartDate
					  AND arg.IsNonRevenue = 0 
					  AND Modality IS NOT NULL 
			)

SELECT	appt.BookedDate as DOS
		, COALESCE (appt.ApptId, appt.Account) as Referral
	    , appt.Modality
		, appt.ReferringDr
		, appt.ReferringAttorney
		, CASE WHEN Ranking > 20 THEN 21 ELSE Ranking END AS Ranking
		, CASE WHEN AttyRanking > 20 THEN 21 ELSE AttyRanking END AS AttyRanking
		, appt.ARClass
		, CEILING(DATEPART(DAY, appt.BookedDate)/7.0) AS WeekGroup
		, @YTD as YTD
FROM dbo.Appointments appt
JOIN XRef.ARClassGroup arg ON appt.ARClass = arg.ARClass
LEFT JOIN (
			SELECT	RANK() OVER (ORDER BY COUNT(COALESCE (appt.ApptId, appt.Account)) desc) as Ranking
					, appt.ReferringDr
			FROM dbo.Appointments appt
			JOIN XRef.ARClassGroup arg ON appt.ARClass = arg.ARClass
			WHERE --appt.BookedDate BETWEEN DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) AND @EndDate
					appt.BookedDate BETWEEN CASE DATEPART(DAY, GETDATE()) WHEN 1 THEN CAST(DATEADD(MONTH, -1, GETDATE()) AS date)
										ELSE DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) END 
							AND @EndDate
					AND arg.IsNonRevenue = 0 
					AND Modality = 'MRI'
			GROUP BY appt.ReferringDr
		  ) rn ON rn.ReferringDr = appt.ReferringDr
LEFT JOIN (
			SELECT	RANK() OVER (ORDER BY COUNT(COALESCE (appt.ApptId, appt.Account)) desc) as AttyRanking
					, appt.ReferringAttorney
			FROM dbo.Appointments appt
			WHERE --appt.BookedDate BETWEEN DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) AND @EndDate
					appt.BookedDate BETWEEN CASE DATEPART(DAY, GETDATE()) WHEN 1 THEN CAST(DATEADD(MONTH, -1, GETDATE()) AS date)
										ELSE DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) END 
							AND @EndDate
			--AND appt.ARClass IN ('PILIEN', 'PIFAC', 'PIINT') --REMOVED 8/13/2020 SLUNDY PER SABA EMAIL
			AND appt.ARClass LIKE ('PI%')
			AND Modality = 'MRI'
			GROUP BY appt.ReferringAttorney
		  ) arn ON arn.ReferringAttorney = appt.ReferringAttorney
WHERE appt.BookedDate BETWEEN @StartDate AND @EndDate
		AND arg.IsNonRevenue = 0 
		AND Modality IS NOT NULL



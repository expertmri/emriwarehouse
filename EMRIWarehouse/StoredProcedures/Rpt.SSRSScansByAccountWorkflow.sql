USE [EMRIWarehouse]
GO
/****** Object:  StoredProcedure [Rpt].[SSRSScansByAccountWorkflow]    Script Date: 7/25/2018 12:07:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE Rpt.SSRSScansByAccountWorkflow
@ZoneRegion varchar(4000) 

AS

SET NOCOUNT ON

DECLARE @FirstOfMonth date
		, @StartLastWeek date = (SELECT DATEADD(wk, -1, DATEADD(DAY, 1-DATEPART(WEEKDAY, GETDATE()), DATEDIFF(dd, 0, GETDATE())))) --Start week on sunday
		, @EndLastWeek date = (SELECT DATEADD(wk, 0, DATEADD(DAY, 0-DATEPART(WEEKDAY, GETDATE()), DATEDIFF(dd, 0, GETDATE()))))
		, @DEBUG bit = 0

SELECT @FirstOfMonth = dbo.udfFirstDayOfMonth(GETDATE())

--Parse the multiple select dropdown from SSRS into separate rows and then join the results on it
SELECT value AS ZoneName
INTO #zones
FROM STRING_SPLIT(@ZoneRegion, ',')



;WITH lastWeek AS (
		SELECT COUNT(ss.ApptId) AS LastWeekScans
				, ss.SF_AccountId
		FROM EMRIWarehouse.dbo.Scans ss
		WHERE ss.Modality = 'MRI'
				AND ss.IsNonRevenue = 0
				AND ss.DOS BETWEEN @StartLastWeek AND @EndLastWeek
		GROUP BY SF_AccountId
	)


SELECT	s.SF_AccountId
		, a.SalesRepresentative
		, a.AccountHandler
		, a.ZoneRegion AS Zone
		, a.AccountName
		, a.Address1 AS [Address]
		, a.City
		, a.StateName
		, a.Zip
		, a.Phone
		, a.Fax
		, s.ScanCountMonthly
		, s.ThreeMonthAvg
		, lw.LastWeekScans
		, s.FirstReferralDate
		, s.LastReferralDate
		, CAST((s.PercentAvg * 100) AS varchar) + ' %'  AS PercentAvg
		, s.AccountCategory
		, s.AccountStatus
		, CASE WHEN ThreeMonthAvg = 0 THEN 'Level 0'
				WHEN CEILING(ThreeMonthAvg) < 10 THEN 'Level 1' --There is no rounding done so 9.9 is Level 1
				WHEN CEILING(ThreeMonthAvg) >= 10 AND CEILING(ThreeMonthAvg) < 20 THEN 'Level 2'
				WHEN CEILING(ThreeMonthAvg) >= 20 AND CEILING(ThreeMonthAvg) < 50 THEN 'Level 3'
				WHEN CEILING(ThreeMonthAvg) >= 50 THEN 'Level 4'
			END AS ScanCategory
		, s.BusinessType
		, s.MonthYear AS MonthDate
		, s.MonthName
		, s.AccountType
FROM dbo.ScansByAccountWorkflow s
LEFT JOIN SF.Accounts a ON a.SF_AccountId = s.SF_AccountId
LEFT JOIN lastWeek lw ON s.SF_AccountId = lw.SF_AccountId
JOIN #zones z ON a.ZoneRegion = z.ZoneName
WHERE MonthYear >= DATEADD(MONTH, -6, @FirstOfMonth)

GO	

GRANT EXECUTE ON Rpt.SSRSScansByAccountWorkflow TO [public] 

GO



USE EMRIWarehouse
GO

ALTER PROCEDURE dbo.uspGetAttorneyPIAll
AS

SELECT Insurance1
FROM (
		SELECT DISTINCT bt.Insurance1
		FROM dbo.BilledTransactions bt
		JOIN XRef.ARClassGroup arc ON bt.ARClass = arc.ARClass
		WHERE arc.IsPersonalInjury = 1
			  AND bt.InsCode1 <> '1795'
		UNION 
		SELECT 'ALL'
	) tt
ORDER BY CASE WHEN Insurance1 = 'ALL' THEN 0 ELSE 1 END
		 , Insurance1

GO

GRANT EXECUTE ON dbo.uspGetAttorneyPIAll TO PUBLIC;
GO	  
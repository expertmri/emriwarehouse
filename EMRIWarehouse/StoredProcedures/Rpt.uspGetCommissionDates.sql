USE [EMRIWarehouse]
GO
/****** Object:  StoredProcedure [Rpt].[uspGetCommissionDates]    Script Date: 10/11/2019 10:34:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE Rpt.uspGetCommissionDates

AS

CREATE TABLE #dates (StartDate date, EndDate date)

DECLARE @StartDate date = '8/1/2019'
DECLARE @EndDate date

WHILE @StartDate <  GETDATE()

BEGIN
	
	SET @EndDate = DATEADD(DAY, -1, DATEADD(MONTH, 3, @StartDate))

	INSERT INTO #dates (StartDate, EndDate)
	SELECT  @StartDate
			, @EndDate

	SET @StartDate = DATEADD(DAY, 1, @EndDate)

END

SELECT	StartDate
		, EndDate
		, CONVERT(varchar(10), StartDate, 101) + ' - ' + CONVERT(varchar(10), EndDate, 101)  AS DateRange 
FROM #dates



 

GO

GRANT EXECUTE ON Rpt.uspGetCommissionDates  TO PUBLIC;


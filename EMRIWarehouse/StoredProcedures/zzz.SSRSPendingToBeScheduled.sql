USE [EMRIWarehouse]
GO
/****** Object:  StoredProcedure [Rpt].[SSRSPendingToBeScheduled]    Script Date: 3/14/2018 12:36:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [Rpt].[SSRSPendingToBeScheduled]
AS


SELECT	appt.BookedDate as DOS
		, appt.ReferringDr
		, appt.ARClass
		, appt.Account
	    , appt.Modality
		--, CASE WHEN CEILING(DATEPART(DAY, appt.BookedDate)/15.0) > 4 THEN 4 
		--	   ELSE CEILING(DATEPART(DAY, appt.BookedDate)/15.0)
		--  END AS Aging
		, CASE WHEN CEILING(DATEDIFF(DAY, appt.BookedDate, GETDATE())/15.0) > 4 THEN 4 
			   ELSE CEILING(DATEDIFF(DAY, appt.BookedDate, GETDATE())/15.0)
		  END AS Aging
FROM dbo.Appointments appt
JOIN XRef.AppointmentStatus stat ON appt.AppointmentStatusId = stat.AppointmentStatusId
JOIN XRef.ARClassGroup arg ON appt.ARClass = arg.ARClass
WHERE appt.AppointmentStatusId IN (1,2,5) --'Pending', 'Pending Scheduling','No Show'
 	  AND AppointmentListType <> 'Cancelled'
	  AND appt.Modality = 'MRI'
	  AND arg.IsNonRevenue = 0 




USE [EMRIWarehouse]
GO
/****** Object:  StoredProcedure [dbo].[uspAppointments]    Script Date: 12/7/2017 9:29:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE dbo.uspAppointments
AS

SELECT DISTINCT aptlist.SequenceNo AS ApptId --need to use this field because Cancelled appts do not make it to the Appt table in ADS
		, c.CaseNumber
		, aptlist.Account
		, CASE WHEN aptlist.ListType = '1' THEN 'Appointment never scheduled'
			  WHEN aptlist.ListType = '2' THEN 'Appointment needs to be rescheduled'
			  WHEN aptlist.ListType = '3' THEN 'Cancelled'
			  ELSE 'Scheduled'
		  END AS AppointmentListType
		, CASE WHEN aptlist.ListType NOT IN ('1','2','3') THEN
					CASE WHEN DATEDIFF(dd, CAST(ListType AS date), BookDate) > 2 THEN CAST(ListType as date) 
					ELSE COALESCE(bd.BookDate, aptlist.DateCreated) END
			   ELSE COALESCE(bd.BookDate, aptlist.DateCreated) 
		  END AS BookedDate
		, CONVERT(varchar(15),CAST(REPLACE(aptlist.TimeCreated, '-',':') AS TIME),100) AS BookedTime
		, CASE WHEN aptlist.ListType IN ('1','2','3') THEN aptlist.DateCreated
			   ELSE CONVERT(varchar(10),CAST(apt.ApptDate as date), 101) 
		  END AS DOS
		, REPLACE(REPLACE(apt.ApptTime, 'a', 'AM'), 'p', 'PM') AS ApptTime
		, CONVERT(varchar(10),CAST(c.DateFrom as date), 101) AS DOI
		--, atype.Modality
		, cd.Modality
		, aptlist.ProcCode
		, l.Name AS Facility
		, dr.Id AS ReferringDrId
		, COALESCE(apt.ApptCode, aptlist.ApptCode) AS ApptCode
		, atype.Description AS AppointmentDescription
		, COALESCE(apt.RefCode, aptlist.RefCode) AS RefCode
		, dr.Name AS ReferringDr
		, dr.GroupName AS ReferringGroup
		, ins.Id AS InsCode
		, ins.Name AS InsName
		, p.Reserved AS RefAttyCode
		, COALESCE(att.PracticeName, ins.Name) AS ReferringAttorney
		, apt.Resource1
		, apt.CheckIn
		, apt.NoShow
		, c.ARClass
		, rr.Code + ' ' + rr.Description AS MarketingReferral
		, COALESCE(apt.[Location], aptlist.[Location]) AS OperatorCode
		, op.[Name] AS Operator
		, aptlist.SqlDateModified AS CancelDate
		, aptlist.CancelUser
		, aptlist.CancelCode AS CancelReasonCode
		, cxl.Description AS CancelReasonDescription
		, CASE WHEN aptlist.CancelCode = 'DUPAPT' THEN 1 ELSE 0 END AS IsDeletedAppt
		, ent.CompanyName AS BillingEntity
		, ent.IrsNumber AS TaxId
INTO #Appts
FROM Integration.Stg.AppointmentsList aptlist
LEFT JOIN Integration.Stg.Appointments apt on aptlist.SequenceNo = apt.ApptId AND apt.PatName  NOT LIKE '+%'
LEFT JOIN Integration.Stg.AptBookDates bd ON apt.ApptId = bd.aptId
LEFT JOIN Integration.Stg.Locations l ON l.Id = CASE WHEN CHARINDEX(' ', aptlist.PlaceCode) = 0 THEN aptlist.PlaceCode
													 ELSE LEFT(aptlist.PlaceCode, CHARINDEX(' ', aptlist.PlaceCode) - 1)
												END
LEFT JOIN EMRIWarehouse.Xref.ApptCodes cd ON LEFT(aptlist.ApptCode, 2) = cd.ADSApptCode
--LEFT JOIN Integration.Stg.AppointmentType atype ON aptlist.ApptCode = atype.AppTypeCode
LEFT JOIN Integration.Stg.Cases c ON c.Account = aptlist.Account
						 AND c.CaseNumber = CASE WHEN RTRIM(LEFT(aptlist.CaseId, 2)) IN ('1','2','3') THEN RTRIM(LEFT(aptlist.CaseId, 2)) ELSE null END
LEFT JOIN Integration.Stg.Patients p on aptlist.Account = p.Account
LEFT JOIN Integration.Stg.RecallReasons rr ON LEFT(p.RecallInfo, CHARINDEX('^', p.RecallInfo) - 1) = rr.Code
LEFT JOIN Integration.Stg.Referrals dr ON COALESCE(apt.RefCode, aptlist.RefCode) = dr.Code
LEFT JOIN Integration.Stg.Insurances ins ON COALESCE(apt.InsCode, aptlist.InsCode) = ins.Id
LEFT JOIN Integration.Stg.Operators op ON COALESCE(apt.Location, aptlist.Location) = op.Code
LEFT JOIN Integration.Stg.AppointmentType atype ON apt.ApptCode = atype.AppTypeCode
LEFT JOIN Integration.Stg.Attorneys att ON p.Reserved = att.Code
LEFT JOIN Integration.Stg.AppointmentCancelCodes cxl ON aptlist.CancelCode = cxl.Code
LEFT JOIN Integration.Stg.Entities ent ON aptlist.Entity = ent.Code
WHERE aptlist.ListType <> 'TEDELE'
	  AND aptlist.PatName NOT LIKE 'Test,%'
	  AND aptlist.PatName NOT LIKE '%reserved%'
	  AND aptlist.PatName  NOT LIKE '+%'
	  

TRUNCATE TABLE dbo.Appointments 
INSERT INTO dbo.Appointments (
		ApptId
		, CaseId
		, Account
		, AppointmentListType
		, BookedDate
		, BookedTime
		, DOS
		, ApptTime
		, DOI
		, Modality
		, CPTCode
		, Facility
		, ReferringDrId
		, ReferralCode
		, ReferringDr
		, ReferringGroup
		, InsCode
		, InsName
		, RefAttyCode
		, ReferringAttorney
		, ARClass
		, AppointmentStatusId
		, AppointmentStatus
		, AppointmentCode
		, AppointmentDescription
		, MarketingReferral
		, Resource1
		, OperatorCode
		, Operator
		, CancelDate
		, CancelUser
		, CancelReasonCode
		, CancelReasonDescription
		, IsDeletedAppt 
		, BillingEntity
		, TaxId
	)
SELECT  apt.ApptId
		, apt.CaseNumber
		, apt.Account
		, apt.AppointmentListType
		, apt.BookedDate
		, apt.BookedTime
		, apt.DOS
		, apt.ApptTime
		, apt.DOI
		, apt.Modality
		, apt.ProcCode
		, apt.Facility
		, apt.ReferringDrId
		, apt.RefCode
		, apt.ReferringDr
		, apt.ReferringGroup
		, apt.InsCode
		, apt.InsName
		, apt.RefAttyCode
		, apt.ReferringAttorney
		, apt.ARClass
		, CASE WHEN apt.AppointmentListType = 'Cancelled' THEN 7
			   WHEN apt.Resource1 IN ('Intake', 'RFA', 'PPPW', 'Underwriting') THEN 1
			   WHEN apt.DOS >= CAST(GETDATE() as date) THEN 3
			   WHEN apt.DOS < CAST(GETDATE() as date) AND apt.CheckIn = 1 THEN 4
			   WHEN apt.DOS < CAST(GETDATE() as date) AND apt.NoShow = 1 THEN 5
			   WHEN apt.DOS < CAST(GETDATE() as date) AND apt.NoShow IS NULL AND apt.CheckIn IS NULL THEN 2
			   ELSE 6
		  END as AppointmentStatusId
		, CASE WHEN apt.AppointmentListType = 'Cancelled' THEN 'Cancelled'
			   WHEN apt.Resource1 IN ('Intake', 'RFA', 'PPPW', 'Underwriting') THEN 'Pending Scheduling'
			   WHEN apt.DOS >= CAST(GETDATE() as date) THEN 'On Schedule'
			   WHEN apt.DOS < CAST(GETDATE() as date) AND apt.CheckIn = 1 THEN 'Exam Complete'
			   WHEN apt.DOS < CAST(GETDATE() as date) AND apt.NoShow = 1 THEN 'No Show'
			   WHEN apt.DOS < CAST(GETDATE() as date) AND apt.NoShow IS NULL AND apt.CheckIn IS NULL THEN 'Pending'
			   ELSE 'Pending Status'
		  END as AppointmentStatus
		, apt.ApptCode
		, apt.AppointmentDescription
		, MarketingReferral
		, Resource1
		, OperatorCode
		, Operator
		, apt.CancelDate
		, apt.CancelUser
		, apt.CancelReasonCode
		, apt.CancelReasonDescription
		, IsDeletedAppt 
		, COALESCE(BillingEntity, 'EXPERT MRI, PC')
		, COALESCE(TaxId, '82-0713584')
		
FROM #Appts apt

UPDATE dbo.Appointments
SET RowCheckSum = CHECKSUM(OperatorCode, DOS, Resource1, AppointmentStatusId, AppointmentCode, ARClass) 

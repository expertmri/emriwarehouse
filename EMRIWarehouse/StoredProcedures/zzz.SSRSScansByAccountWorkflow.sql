USE [EMRIWarehouse]
GO
/****** Object:  StoredProcedure [Rpt].[SSRSScansByAccountWorkflow]    Script Date: 7/25/2018 12:07:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE Rpt.SSRSScansByAccountWorkflow 
@ExportToSF bit = 0
, @SFAccountId varchar(255) = null
, @PersistData bit = 0

AS

SET NOCOUNT ON

DECLARE @StartDate date
		, @FirstOfMonth date
		, @FirstDayLastMonth date
		, @EndDayLastMonth date
		, @DEBUG bit = 0

SELECT @FirstOfMonth = dbo.udfFirstDayOfMonth(GETDATE())
	   , @FirstDayLastMonth = dbo.udfFirstDayLastMonth(GETDATE())
	   , @EndDayLastMonth = dbo.udfEndDayLastMonth(GETDATE())

CREATE TABLE #Scans (
	SFPK varchar(50)
	, ApptId int
	, DOS date
	, AccountType varchar(30)
	, IsWorkComp bit
	)

CREATE TABLE #Monthly (
	SFPK varchar(50)
	, ReferralName varchar(255)
	, ScanCountMonthly int
	, ThreeMonthAvg float
	, FirstReferralDate date
	, LastReferralDate date
	, PercentAvg float
	, AccountCategory varchar(40)
	, AccountStatus varchar(40)
	, MonthDate date
	, MonthName varchar(20)
	, AccountType varchar(30)
	, BusinessType varchar(30)
	, RN int
	)

CREATE TABLE #Calendar (
	MonthDate date
	)

DECLARE @BeginDate date = '1/1/2017'

WHILE @BeginDate <= @FirstOfMonth
BEGIN
	INSERT INTO #Calendar
	SELECT @BeginDate

	SET @BeginDate = DATEADD(MONTH, 1, @BeginDate)
END

CREATE TABLE #ScansCalendar (
	SFPK varchar(50)
	, MonthDate date
	, AccountType varchar(30)
	)

CREATE TABLE #Status (
	LowerPercent float
	, UpperPercent float
	, StatusName varchar(20)
)

INSERT INTO #Status (LowerPercent, UpperPercent, StatusName)
VALUES (0, .2, '1% to 19% Drop')
, (.2, .4, '20% to 39% Drop')
, (.4, .6, '40% to 59% Drop')
, (.6, .8, '60% to 79% Drop')
, (.8, 1, '80% to 100% Drop')
, (1, 9999999, 'Good')

INSERT INTO #Scans (SFPK, ApptId, DOS, AccountType, IsWorkComp)
SELECT  c.SF_AccountId AS SFPK
		, apt.ApptId
		, apt.DOS
		, 'Doctor' AS AccountType
		, ar.IsWorkComp
FROM dbo.Appointments apt
JOIN Integration.dbo.Accounts c ON apt.ReferralCode = c.SourceSystemPk AND c.SourceTable = 'Doctor'
LEFT JOIN XRef.ARClassGroup ar ON apt.ARClass = ar.ARClass
WHERE apt.Modality = 'MRI'
		AND apt.AppointmentStatusId = 4 --Exam Complete
		AND ar.IsNonRevenue = 0
UNION ALL
SELECT  c.SF_AccountId AS SFPK
		, apt.ApptId
		, apt.DOS
		, 'Attorney' AS AccountType
		, ar.IsWorkComp
FROM dbo.Appointments apt
JOIN Integration.dbo.Accounts c ON apt.RefAttyCode = c.SourceSystemPk AND c.SourceTable = 'Attorney'
LEFT JOIN XRef.ARClassGroup ar ON apt.ARClass = ar.ARClass
WHERE apt.Modality = 'MRI'
		AND apt.AppointmentStatusId = 4 --Exam Complete
		AND ar.IsNonRevenue = 0

INSERT INTO #ScansCalendar (SFPK, MonthDate, AccountType)
SELECT	DISTINCT SFPK
		, c.MonthDate
		, AccountType
FROM #Scans
JOIN #Calendar c ON 1=1
WHERE SFPK IS NOT NULL
	  
	  
INSERT INTO #Monthly (SFPK, ScanCountMonthly, MonthDate, MonthName, AccountType, RN)
SELECT  sc.SFPK
		, COUNT(ApptId) AS ScanCountMonthly
		, sc.MonthDate
		, dbo.udfGetMonthName(sc.MonthDate) AS MonthName
		, sc.AccountType
		, ROW_NUMBER() OVER(PARTITION BY sc.SFPK ORDER BY sc.MonthDate desc) AS RN
FROM #ScansCalendar sc
LEFT JOIN #Scans s ON s.SFPK = sc.SFPK AND dbo.udfFirstDayOfMonth(DOS) = sc.MonthDate
GROUP BY sc.SFPK
		 , sc.MonthDate
		 , dbo.udfGetMonthName(DOS)
		 , sc.AccountType

/*Get First and Last Referral Dates*/
;WITH ReferralDates AS
(
	SELECT SFPK
			, MAX(DOS) AS LastReferralDate
			, MIN(DOS) AS FirstReferralDate
	FROM #Scans s
	GROUP BY SFPK
)

UPDATE m
SET LastReferralDate = ct.LastReferralDate
	, FirstReferralDate = ct.FirstReferralDate
FROM #Monthly m
LEFT JOIN ReferralDates ct ON m.SFPK = ct.SFPK

/*Calculate 3 Month Average*/
;WITH Average AS
(
	SELECT SFPK
		   , SUM(ScanCountMonthly) AS TotalScans
	FROM #Monthly
	WHERE MonthDate >= DATEADD(MONTH, -3, @FirstDayLastMonth) AND MonthDate < @FirstDayLastMonth 
	GROUP BY SFPK
)

UPDATE m
SET ThreeMonthAvg = COALESCE(CAST(TotalScans AS float),0)/3.00
FROM #Monthly m
LEFT JOIN Average a ON m.SFPK = a.SFPK

/*Calculate Current Month Drop*/
UPDATE #Monthly
SET PercentAvg = CASE WHEN ThreeMonthAvg = 0 OR ScanCountMonthly = 0 THEN 0 
					  WHEN ScanCountMonthly >= ThreeMonthAvg THEN CAST(ScanCountMonthly AS float)/ThreeMonthAvg
					  ELSE 1- (COALESCE(CAST(ScanCountMonthly AS float)/ThreeMonthAvg, 0)) END --Get % drop so need to subtract from 1
WHERE MonthDate = @FirstDayLastMonth

UPDATE #Monthly
SET AccountCategory = 'Inactive'
	, AccountStatus = CASE WHEN LastReferralDate >  DATEADD(MONTH, -3,@FirstOfMonth) THEN 'Less than 3 months' ELSE 'More than 3 months' END
WHERE AccountStatus IS NULL
	  AND LastReferralDate < @FirstDayLastMonth

UPDATE #Monthly
SET AccountCategory = 'Active'
	, AccountStatus = 'New'
WHERE AccountStatus IS NULL
	  AND FirstReferralDate >= @FirstDayLastMonth

UPDATE m
SET AccountCategory = 'Active'
	, AccountStatus = StatusName 
FROM #Monthly m
JOIN #Status s ON PercentAvg >= LowerPercent AND PercentAvg < UpperPercent
WHERE AccountStatus IS NULL
	  AND PercentAvg <> 0

--July has Scans but June has none
UPDATE #Monthly
SET AccountCategory = 'Active'
	, AccountStatus = 'Restart' 
WHERE RN = 2
	  AND ScanCountMonthly = 0
	  AND LastReferralDate >= @FirstOfMonth
	  AND AccountStatus IS NULL

----June or July has Scans but May does not
--UPDATE #Monthly
--SET AccountCategory = 'Active'
--	, AccountStatus = 'Restart' 
--WHERE RN = 2
--	  AND ScanCountMonthly > 0
--	  AND LastReferralDate < @FirstOfMonth
--	  AND ThreeMonthAvg = 0
--	  AND AccountStatus IS NULL

--June or July has Scans but May does not
UPDATE #Monthly
SET AccountCategory = 'Active'
	, AccountStatus = 'Restart' 
WHERE LastReferralDate >= @FirstDayLastMonth
	  AND ScanCountMonthly = 0
	  AND RN = 3
	  AND AccountStatus IS NULL

-----Do this after all status logic is done
;WITH MissingStatus AS
(
	SELECT	SFPK
			, AccountStatus
			, AccountCategory
			, PercentAvg
	FROM #Monthly
	WHERE AccountStatus IS NULL OR PercentAvg IS NULL
)

UPDATE m
SET AccountCategory = m2.AccountCategory
	, AccountStatus = m2.AccountStatus
	, PercentAvg = m2.PercentAvg
FROM MissingStatus m
JOIN #Monthly m2 ON m.SFPK = m2.SFPK AND m2.SFPK IS NOT NULL
--WHERE m.AccountStatus IS NOT NULL


;WITH WorkComp AS
(
	SELECT	SFPK
			, COUNT(apptId) AS WCScans
	FROM #Scans
	WHERE DOS >= DATEADD(MONTH, -4, @FirstOfMonth) AND DOS < @FirstOfMonth
		  AND IsWorkComp = 1
	GROUP BY SFPK
)
, TotalScans AS
(
	SELECT	SFPK
			, COUNT(apptId) AS TotalScans
	FROM #Scans
	WHERE DOS >= DATEADD(MONTH, -4, @FirstOfMonth) AND DOS < @FirstOfMonth
	GROUP BY SFPK
)

UPDATE	m
SET BusinessType = CASE WHEN CAST(COALESCE(WCScans, 0) AS float) / TotalScans >= .3 THEN 'WC' ELSE 'Hybrid' END
FROM #Monthly m
LEFT JOIN TotalScans ts ON ts.SFPK = m.SFPK
LEFT JOIN WorkComp wc ON ts.SFPK = wc.SFPK

IF @DEBUG = 1
SELECT * FROM #Monthly

ELSE

/* Write the output to a table to be used by the export process*/
IF @ExportToSF = 1
BEGIN	  
	INSERT INTO Integration.OUTBOUND.AccountCategorizationWorkflow (
		SF_AccountId 
		, BusinessType 
		, AccountStatus
		, AccountCategory
		, PercentChange
		, MarketingType
		, ScansLastMonth
		, ScansThreeMonthAverage
		)
	SELECT	SFPK
			, BusinessType
			, CASE WHEN AccountCategory = 'Inactive' THEN AccountCategory + ' ' + AccountStatus ELSE AccountCategory END
			, CASE WHEN ThreeMonthAvg = 0 THEN 'Level 0'
				   WHEN CEILING(ThreeMonthAvg) < 10 THEN 'Level 1' --There is no rounding done so 9.9 is Level 1
				   WHEN CEILING(ThreeMonthAvg) >= 10 AND CEILING(ThreeMonthAvg) < 20 THEN 'Level 2'
				   WHEN CEILING(ThreeMonthAvg) >= 20 AND CEILING(ThreeMonthAvg) < 50 THEN 'Level 3'
				   WHEN CEILING(ThreeMonthAvg) >= 50 THEN 'Level 4'
			  END AS ScanCategory
			, CASE WHEN AccountCategory = 'Active' THEN AccountStatus ELSE 'N/A' END
			, null AS MarketingType
			, ScanCountMonthly
			, CAST(ThreeMonthAvg as decimal(5,2))
	FROM #Monthly m
	WHERE MonthDate = @FirstDayLastMonth
		  AND (m.SFPK = @SFAccountId OR @SFAccountId IS NULL)
END

IF @ExportToSF = 0 AND @PersistData = 0
BEGIN	  
	SELECT	SFPK AS SF_AccountId
			--, a.AccountManager
			, a2.Account_Handler__c AS AccountManager
			, a2.Zone__c AS Zone
			, a.AccountName
			, a.Address1 AS [Address]
			, a.Phone
			, a.Fax
			, ScanCountMonthly
			, ThreeMonthAvg
			, FirstReferralDate
			, LastReferralDate
			, CAST((PercentAvg * 100) AS varchar) + ' %'  AS PercentAvg
			, AccountCategory
			, AccountStatus
			, CASE WHEN ThreeMonthAvg = 0 THEN 'Level 0'
				   WHEN CEILING(ThreeMonthAvg) < 10 THEN 'Level 1' --There is no rounding done so 9.9 is Level 1
				   WHEN CEILING(ThreeMonthAvg) >= 10 AND CEILING(ThreeMonthAvg) < 20 THEN 'Level 2'
				   WHEN CEILING(ThreeMonthAvg) >= 20 AND CEILING(ThreeMonthAvg) < 50 THEN 'Level 3'
				   WHEN CEILING(ThreeMonthAvg) >= 50 THEN 'Level 4'
			  END AS ScanCategory
			, BusinessType
			, MonthDate
			, MonthName
			, AccountType
	FROM #Monthly m
	LEFT JOIN SF.Accounts a ON a.SF_AccountId = m.SFPK
	LEFT JOIN Integration.SF.Account a2 ON a.SF_AccountId = a2.Id
	WHERE MonthDate >= DATEADD(MONTH, -6, @FirstOfMonth)
END

IF @PersistData = 1
BEGIN	  
	SELECT	SFPK AS SF_AccountId
			--, a.AccountManager
			, a2.Account_Handler__c AS AccountManager
			--, a2.Zone__c AS Zone
			--, a.AccountName
			--, a.Address1 AS [Address]
			--, a.Phone
			--, a.Fax
			, ScanCountMonthly
			--, ThreeMonthAvg
			--, FirstReferralDate
			--, LastReferralDate
			--, CAST((PercentAvg * 100) AS varchar) + ' %'  AS PercentAvg
			--, AccountCategory
			--, AccountStatus
			--, CASE WHEN ThreeMonthAvg = 0 THEN 'Level 0'
			--	   WHEN CEILING(ThreeMonthAvg) < 10 THEN 'Level 1' --There is no rounding done so 9.9 is Level 1
			--	   WHEN CEILING(ThreeMonthAvg) >= 10 AND CEILING(ThreeMonthAvg) < 20 THEN 'Level 2'
			--	   WHEN CEILING(ThreeMonthAvg) >= 20 AND CEILING(ThreeMonthAvg) < 50 THEN 'Level 3'
			--	   WHEN CEILING(ThreeMonthAvg) >= 50 THEN 'Level 4'
			--  END AS ScanCategory
			--, BusinessType
			, MonthDate
			, MonthName
			, GETDATE() AS InsertDate
			--, AccountType
	INTO dbo.ScansMRIComplete
	FROM #Monthly m
	LEFT JOIN SF.Accounts a ON a.SF_AccountId = m.SFPK
	LEFT JOIN Integration.SF.Account a2 ON a.SF_AccountId = a2.Id
	WHERE MonthDate >= DATEADD(MONTH, -6, @FirstOfMonth)
END

GO	

GRANT EXECUTE ON Rpt.SSRSScansByAccountWorkflow TO [public] 

GO



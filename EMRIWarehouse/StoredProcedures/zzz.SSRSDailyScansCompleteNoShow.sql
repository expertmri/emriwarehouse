USE [EMRIWarehouse]
GO
/****** Object:  StoredProcedure [Rpt].[SSRSDailyScansCompleteNoShow]    Script Date: 10/24/2017 8:53:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [Rpt].[SSRSDailyScansCompleteNoShow]
	@AppointmentStatus varchar(100)
AS

SELECT ApptId
		, ReferringDr
		, DOS
FROM dbo.Appointments 
WHERE AppointmentStatus = @AppointmentStatus
	  AND Modality = 'MRI'
	  AND DATEPART(MONTH, DOS) = CASE DATEPART(DAY, GETDATE()) WHEN 1 THEN (DATEPART(MONTH, GETDATE()) - 1)
			ELSE DATEPART(MONTH, GETDATE()) END


GO
GRANT EXECUTE ON [Rpt].[SSRSDailyScansCompleteNoShow] TO [public] AS [dbo]
GO

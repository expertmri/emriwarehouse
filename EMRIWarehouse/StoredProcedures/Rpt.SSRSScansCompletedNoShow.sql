USE [EMRIWarehouse]
GO
/****** Object:  StoredProcedure [Rpt].[SSRSScansCompletedNoShow]    Script Date: 8/13/2020 1:15:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [Rpt].[SSRSScansCompletedNoShow]-- 'Exam Complete'
	@AppointmentStatus varchar(100)
AS


DECLARE @StartDate date
		, @EndDate date
		, @YTD int
		, @FirstOfMonth date
		, @FirstOfLastMonth date

SET @StartDate = (SELECT CAST('1/1/' + CAST(DATEPART(YEAR, GETDATE()) AS varchar(10)) AS date))
SET @EndDate = (SELECT CAST(GETDATE() AS date))
SET @FirstOfMonth = (SELECT CAST(DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) AS date))
SET @FirstOfLastMonth = (SELECT CAST(DATEADD(MONTH, DATEDIFF(MONTH, 0, DATEADD(MONTH, -1, GETDATE())), 0)  AS date))

SET @YTD = (
				SELECT COUNT(appt.ApptId)
				FROM dbo.Appointments appt
				JOIN XRef.ARClassGroup arg ON appt.ARClass = arg.ARClass
				WHERE --appt.DOS < DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)
						appt.DOS < CASE DATEPART(DAY, GETDATE()) WHEN 1 THEN CAST(DATEADD(MONTH, -1, GETDATE()) AS date)
										ELSE DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) END
						AND appt.DOS >= @StartDate				 
						AND appt.ARClass IS NOT NULL 
						AND COALESCE(arg.IsNonRevenue, 0) = 0 
						AND Modality IS NOT NULL 
						AND AppointmentStatus = @AppointmentStatus
			)

IF @AppointmentStatus = 'Exam Complete'
BEGIN

		SELECT	DOS
				, appt.ApptId
				, appt.Modality
				, appt.ReferringDr
				, appt.ReferringAttorney
				, appt.Facility
				, CASE WHEN Ranking > 20 THEN 21 ELSE Ranking END AS Ranking
				, CASE WHEN AttyRanking > 20 THEN 21 ELSE AttyRanking END AS AttyRanking
				, appt.ARClass
				, CEILING(DATEPART(DAY, appt.DOS)/7.0) AS WeekGroup
				, @YTD as YTD
				, drAvg.DrRollingAvg
				, AttyAvg.AttyRollingAvg
				, FacAvg.FacilityRollingAvg
				, COALESCE(gl.MonthlyGoal, 0) AS MonthlyGoal
		FROM dbo.Appointments appt
		JOIN XRef.ARClassGroup arg ON appt.ARClass = arg.ARClass 
		LEFT JOIN (
					SELECT	RANK() OVER (ORDER BY COUNT(appt.ApptId) desc) as Ranking
							, appt.ReferringDr
					FROM dbo.Appointments appt
					JOIN XRef.ARClassGroup arg ON appt.ARClass = arg.ARClass
					WHERE appt.DOS BETWEEN CASE DATEPART(DAY, GETDATE()) WHEN 1 THEN CAST(DATEADD(MONTH, -1, GETDATE()) AS date)
												ELSE @FirstOfMonth END 
									AND @EndDate
					AND arg.IsNonRevenue = 0 
					AND Modality = 'MRI'
					AND AppointmentStatus = @AppointmentStatus
					GROUP BY appt.ReferringDr
				  ) rn ON rn.ReferringDr = appt.ReferringDr
		LEFT JOIN (
					SELECT	RANK() OVER (ORDER BY COUNT(appt.ApptId) desc) as AttyRanking
							, appt.ReferringAttorney
					FROM dbo.Appointments appt
					WHERE --appt.DOS BETWEEN DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) AND @EndDate
							appt.DOS BETWEEN CASE DATEPART(DAY, GETDATE()) WHEN 1 THEN CAST(DATEADD(MONTH, -1, GETDATE()) AS date)
												ELSE DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) END 
									AND @EndDate
					--AND appt.ARClass IN ('PILIEN', 'PIFAC', 'PIINT') --REMOVED 8/13/2020 SLUNDY PER SABA EMAIL
					AND appt.ARClass LIKE ('PI%')
					AND Modality = 'MRI'
					GROUP BY appt.ReferringAttorney
				  ) arn ON arn.ReferringAttorney = appt.ReferringAttorney
		LEFT JOIN ( SELECT COUNT(appt.ApptId)/3 AS DrRollingAvg
							, appt.ReferringDr
					FROM dbo.Appointments appt
					JOIN XRef.ARClassGroup arg ON appt.ARClass = arg.ARClass AND COALESCE(arg.IsNonRevenue, 0) = 0 
					WHERE appt.DOS >= DATEADD(MONTH, -3 , GETDATE())
						  AND AppointmentStatus = @AppointmentStatus
						  AND arg.IsNonRevenue = 0 
						  AND Modality = 'MRI'
					GROUP BY appt.ReferringDr
				   ) drAvg ON drAvg.ReferringDr = appt.ReferringDr
		LEFT JOIN ( SELECT COUNT(appt.ApptId)/3 AS AttyRollingAvg
							, appt.ReferringAttorney
					FROM dbo.Appointments appt
					WHERE appt.DOS >= DATEADD(MONTH, -3 , GETDATE())
						  AND AppointmentStatus = @AppointmentStatus
						  --AND appt.ARClass IN ('PILIEN', 'PIFAC', 'PIINT') --REMOVED 8/13/2020 SLUNDY PER SABA EMAIL
						  AND appt.ARClass LIKE ('PI%')
						  AND Modality = 'MRI'
					GROUP BY appt.ReferringAttorney 
				   ) AttyAvg ON AttyAvg.ReferringAttorney = appt.ReferringAttorney
		LEFT JOIN ( SELECT COUNT(appt.ApptId)/3 AS FacilityRollingAvg
							, appt.Facility
					FROM dbo.Appointments appt
					JOIN XRef.ARClassGroup arg ON appt.ARClass = arg.ARClass AND COALESCE(arg.IsNonRevenue, 0) = 0 
					WHERE appt.DOS >= DATEADD(MONTH, -3 , GETDATE())
						  AND AppointmentStatus = @AppointmentStatus
						  AND arg.IsNonRevenue = 0 
						  AND Modality = 'MRI'
					GROUP BY appt.Facility
				   ) FacAvg ON FacAvg.Facility = appt.Facility
		LEFT JOIN XRef.GoalsByFacility gl ON appt.Facility = gl.FacilityName 
												AND gl.Modality = 'MRI'
												AND gl.MonthDate = CASE DATEPART(DAY, GETDATE()) WHEN 1 THEN @FirstOfLastMonth
												ELSE @FirstOfMonth END 
		WHERE -- appt.DOS BETWEEN @StartDate AND @EndDate --Modified 11/13/2020 to reduce how much data was being brought back
				appt.DOS >= @FirstOfLastMonth 
				AND arg.IsNonRevenue = 0 
				AND appt.Modality IS NOT NULL
				AND AppointmentStatus = @AppointmentStatus

END

IF @AppointmentStatus = 'No Show'
BEGIN

	SELECT	appt.DOS
			, appt.ApptId
			, appt.Modality
			, appt.ReferringDr
			, appt.ReferringAttorney
			, appt.Facility
			, 0 AS Ranking
			, 0 AS AttyRanking
			, appt.ARClass
			, CEILING(DATEPART(DAY, appt.DOS)/7.0) AS WeekGroup
			, @YTD as YTD
			, 0 AS DrRollingAvg
			, 0 AS AttyRollingAvg
			, COALESCE(FacAvg.FacilityRollingAvg, 0) AS FacilityRollingAvg
			, 0 AS MonthlyGoal
		FROM dbo.NoShowTracking appt
		JOIN XRef.ARClassGroup arg ON appt.ARClass = arg.ARClass 
		LEFT JOIN ( SELECT COUNT(appt.ApptId)/3 AS FacilityRollingAvg
							, appt.Facility
					FROM dbo.NoShowTracking appt
					JOIN XRef.ARClassGroup arg ON appt.ARClass = arg.ARClass AND COALESCE(arg.IsNonRevenue, 0) = 0 
					WHERE appt.DOS >= DATEADD(MONTH, -3 , GETDATE())
						  AND AppointmentStatus = @AppointmentStatus
						  AND arg.IsNonRevenue = 0 
						  AND Modality = 'MRI'
					GROUP BY appt.Facility
				   ) FacAvg ON FacAvg.Facility = appt.Facility
		WHERE -- appt.DOS BETWEEN @StartDate AND @EndDate --Modified 11/13/2020 to reduce how much data was being brought back
				appt.DOS >= @FirstOfLastMonth 
				AND arg.IsNonRevenue = 0 
				AND appt.Modality IS NOT NULL
				AND AppointmentStatus = @AppointmentStatus

END



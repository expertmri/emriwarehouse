USE [EMRIWarehouse]
GO
/****** Object:  StoredProcedure [Rpt].[SSRSCollectionAnalysisPI]    Script Date: 3/1/2019 11:25:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [Rpt].[SSRSCollectionAnalysisPI]
	@Modality varchar(100) = 'ALL'
	, @Payor varchar(255) = 'ALL'
	, @ARClass varchar(255) = 'ALL'

AS

SELECT	bt.ApptId
		, bt.Insurance1
		, bt.Modality
		, bt.ARClass
		, - SUM(bt.Balance) AS TotalSettlementDollars
FROM dbo.BilledTransactions bt
JOIN Integration.Stg.FinancialCodes fc ON bt.FinancialCode = fc.Code
JOIN XRef.ARClassGroup arc ON bt.ARClass = arc.ARClass
WHERE bt.ApptId IS NOT NULL
	  AND IsCharge = 0
	  AND (@Payor = bt.Insurance1 OR @Payor = 'ALL')
	  AND (@Modality = bt.Modality OR @Modality = 'ALL')
	  --AND arc.IsPersonalInjury = 1
	  AND arc.ARClass IN ('PIINT', 'PIINCP')
	  AND (@ARClass = arc.ARClass OR @ARClass = 'ALL')
	  AND bt.InsCode1 NOT IN ('1795', '952') --EM260
	  AND (bt.FinancialCode not like '%WO' OR fc.SpecialFieldA = 1)
GROUP BY bt.ApptId
		 , bt.Insurance1
		 , bt.Modality
		 , bt.ARClass



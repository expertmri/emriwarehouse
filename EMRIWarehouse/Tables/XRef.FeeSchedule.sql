USE [EMRIWarehouse]
GO
/****** Object:  Table [XRef].[FeeSchedule]    Script Date: 10/24/2017 8:53:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [XRef].[FeeSchedule](
	[FeeScheduleId] [int] IDENTITY(1,1) NOT NULL,
	[ID] [varchar](255) NULL,
	[Code] [varchar](255) NULL,
	[Key] [varchar](255) NULL,
	[Description] [varchar](255) NULL,
	[ShortDescription] [varchar](255) NULL,
	[Modifiers] [varchar](255) NULL,
	[Cpt4CodeSet1] [varchar](255) NULL,
	[InverseCode] [float] NULL,
	[SpecialFldA] [varchar](255) NULL,
	[SpecialFldB] [varchar](255) NULL,
	[SpecialFldC] [varchar](255) NULL,
	[ARClass] [varchar](255) NULL,
	[Payor] [varchar](20) NULL,
	[Rate] [float] NULL,
	[EffectiveStartDate] [date] NULL,
	[InsertDate] [datetime] NULL CONSTRAINT [DF_FeeSchedule_InsertDate]  DEFAULT (getdate()),
	[EffectiveEndDate] [date] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [idx_FeeSched_ARClass]    Script Date: 10/24/2017 8:53:59 AM ******/
CREATE NONCLUSTERED INDEX [idx_FeeSched_ARClass] ON [XRef].[FeeSchedule]
(
	[ARClass] ASC
)
INCLUDE ( 	[Code],
	[Rate]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Trigger [XRef].[tr_FeeScheduleEndDate]    Script Date: 10/24/2017 8:53:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [XRef].[tr_FeeScheduleEndDate]
ON [XRef].[FeeSchedule]

AFTER INSERT

AS 

DECLARE @nullJoin varchar(10) 
SET @nullJoin = 'zzz|}*$'

UPDATE Xref.FeeSchedule
SET Payor = null
WHERE Payor = 'NULL'

;WITH dates_cte AS 
	(
			SELECT	EffectiveStartDate
					, ROW_NUMBER() OVER (PARTITION BY Code, ARClass, Payor ORDER BY EffectiveStartDate) AS RN
					, Code
					, ARClass
					, Payor
					, EffectiveEndDate
			FROM XRef.FeeSchedule
	)

/* 
--Use the SELECT statement for testing and debugging
SELECT	fs.Code
		, fs.ARClass
		, fs.Payor
		, fs.EffectiveStartDate
		, COALESCE(DATEADD(d, -1, d.EffectiveStartDate), '12/31/2199') AS EffectiveEndDate
*/

UPDATE fs
SET EffectiveEndDate = COALESCE(DATEADD(d, -1, d.EffectiveStartDate), '12/31/2199')
FROM dates_cte fs
LEFT JOIN dates_cte d ON fs.Code = d.Code
						AND fs.ARClass = d.ARClass
						AND COALESCE(fs.Payor, @nullJoin) = COALESCE(d.Payor, @nullJoin)
						AND d.RN = fs.RN + 1


GO

USE [EMRIWarehouse]
GO
/****** Object:  Table [XRef].[ARClassGroup]    Script Date: 10/24/2017 8:53:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [XRef].[ARClassGroup](
	[ARClassGroupId] [int] IDENTITY(1,1) NOT NULL,
	[ARClass] [varchar](50) NULL,
	[ARGroup] [varchar](20) NULL,
	[CreateDate] [datetime] NULL CONSTRAINT [DF_ARClassGroup_CreateDate]  DEFAULT (getdate()),
	[IsNonRevenue] [bit] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

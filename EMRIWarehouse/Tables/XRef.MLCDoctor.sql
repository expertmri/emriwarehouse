USE EMRIWarehouse
GO

CREATE TABLE XRef.MLCDoctor
	(
		MLCDoctorId int IDENTITY(1,1)
		, ADSReferringPhysicianId varchar(100)
		, ADSReferringPhysicianCode varchar(100)
		, ReferringDoctor varchar(200)
		, CreateDate datetime CONSTRAINT DF_MLCDoctor_CreateDate DEFAULT GETDATE()
	)
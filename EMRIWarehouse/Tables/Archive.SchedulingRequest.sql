USE EMRIWarehouse
GO

/****** Object:  Table dbo.SchedulingRequest    Script Date: 12/21/2018 8:57:13 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE Archive.SchedulingRequest(
	SchedulingRequestId bigint NOT NULL,
	WebFormRequestId bigint NULL,
	FirstName varchar(255) NULL,
	LastName varchar(255) NULL,
	DOB date NULL,
	Email varchar(255) NULL,
	Phone varchar(20) NULL,
	Modality varchar(25) NULL,
	ReferralSource varchar(50) NULL,
	CallSummary varchar(4000) NULL,
	Account varchar(20) NULL,
	SchedulingStatus varchar(255) NULL,
	IsProcessed bit NULL,
	CreateDate datetime NULL,
	ModifiedDate datetime NULL,
	InsertDate datetime 
) ON [PRIMARY]
GO





USE EMRIWarehouse
GO

CREATE TABLE XRef.FactoringCompanies (
	FactoringCompaniesId int IDENTITY(1,1)
	, InsCode varchar(100)
	, CreateDate datetime CONSTRAINT DF_FactoringCompanies_CreateDate DEFAULT GETDATE()
	)

	INSERT INTO XRef.FactoringCompanies (InsCode)
	SELECT Code
	FROM Integration.Stg.Insurances
	WHERE [name] in (
						'HEALTHCARE FINANCIAL SOL'
					   ,'KEY HEALTH MEDICAL SOL'
					   ,'PROCARE FUNDING LLC'
					   ,'MEDLEGAL SOLUTIONS'
					   ,'NATIONWIDE MEDICAL PRO I'
					)

USE EMRIWarehouse
GO

/****** Object:  Table SF.Accounts    Script Date: 10/2/2018 9:11:21 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE Archive.Accounts(
	ID bigint IDENTITY(1,1)
	, AccountId int NOT NULL
	, SF_AccountId varchar(100) NULL
	, AccountName varchar(200) NULL
	, Phone varchar(20) NULL
	, Fax varchar(20) NULL
	, Website varchar(255) NULL
	, Facility varchar(250) NULL
	, Address1 varchar(100) NULL
	, Address2 varchar(100) NULL
	, City varchar(100) NULL
	, StateName varchar(30) NULL
	, Zip varchar(10) NULL
	, ADSAccountId int NULL
	, ParentId varchar(24) NULL
	, AccountManager varchar(150) NULL
	, Activity varchar(40)
	, ModifiedUser varchar(255)
	, InsertDate datetime NULL
) ON [PRIMARY]
GO

SET ANSI_PADDING ON
GO

ALTER TABLE Archive.Accounts ADD  CONSTRAINT DF_Accounts_InsertDate  DEFAULT (GETDATE()) FOR InsertDate
GO



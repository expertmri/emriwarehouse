USE [EMRIWarehouse]
GO

/****** Object:  Table [dbo].[SSRSUsers]    Script Date: 10/11/2019 10:17:37 AM ******/
DROP TABLE [dbo].[SSRSUsers]
GO

/****** Object:  Table [dbo].[SSRSUsers]    Script Date: 10/11/2019 10:17:37 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[SSRSUsers](
	SSRSUserId int IDENTITY (1,1),
	[SF_UserName] [nvarchar](80) NULL,
	[domainUser] [varchar](262) NULL,
	[SF_UserId] [nvarchar](18) NULL,
	[UserName] [nvarchar](121) NULL,
	[isAdmin] [int] NOT NULL,
	InsertDate datetime CONSTRAINT DF_SSRSUsers_InsertDate DEFAULT GETDATE()
) ON [PRIMARY]
GO



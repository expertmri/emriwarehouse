USE [EMRIWarehouse]
GO
/****** Object:  Table [XRef].[BilledTransPaymentStatus]    Script Date: 10/24/2017 8:53:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [XRef].[BilledTransPaymentStatus](
	[BilledTransPaymentStatusId] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NULL,
	[TransactionDescription] [varchar](100) NULL,
	[IsPayment] [bit] NULL CONSTRAINT [DF_BilledTransPaymentStatus_IsPayment]  DEFAULT ((0)),
	[IsAdjustment] [bit] NULL CONSTRAINT [DF_BilledTransPaymentStatus_IsAdjustment]  DEFAULT ((0)),
	[CreateDate] [datetime] NULL CONSTRAINT [DF_BilledTransPaymentStatus_CreateDate]  DEFAULT (getdate())
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [idx_BilledTransPaymentStatus_code]    Script Date: 10/24/2017 8:53:59 AM ******/
CREATE UNIQUE CLUSTERED INDEX [idx_BilledTransPaymentStatus_code] ON [XRef].[BilledTransPaymentStatus]
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

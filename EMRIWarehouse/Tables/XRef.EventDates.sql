CREATE TABLE XRef.EventDates (
	EventDateId int IDENTITY(1,1)
	, EventName varchar(255)
	, EventDate date
	, InsertDate datetime CONSTRAINT DF_EventDates_InsertDate DEFAULT GETDATE()
	)
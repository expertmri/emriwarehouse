USE [EMRIWarehouse]
GO

/****** Object:  Table [dbo].[Scans]    Script Date: 3/21/2019 2:33:40 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE dbo.Scans (
	ScanId bigint IDENTITY (1,1)
	, SF_AccountId varchar(50)
	, ApptId varchar(10)
	, Account int
	, BookedDate date
	, DOS date
	, Modality varchar(40)
	, Facility varchar(50)
	, AccountType varchar(20)
	, IsWorkComp bit
	, IsNonRevenue bit
	, IsPersonalInjury bit
	, ARClass varchar(20)
	, ARGroup varchar(20)
	, DirectorGroup varchar(20)
	, InsertDate datetime CONSTRAINT DF_Scans_InsertDate DEFAULT GETDATE()
) ON [PRIMARY]
GO

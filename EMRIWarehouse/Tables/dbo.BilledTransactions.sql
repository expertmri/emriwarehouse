USE [EMRIWarehouse]
GO

ALTER TABLE [dbo].[BilledTransactions] DROP CONSTRAINT [DF_BilledTransactions_InsertDate]
GO

/****** Object:  Index [idx_BilledTransactions_ARClassInsCode]    Script Date: 5/22/2018 2:30:55 PM ******/
DROP INDEX [idx_BilledTransactions_ARClassInsCode] ON [dbo].[BilledTransactions]
GO

/****** Object:  Index [idx_BilledTransactions_ARClass]    Script Date: 5/22/2018 2:30:55 PM ******/
DROP INDEX [idx_BilledTransactions_ARClass] ON [dbo].[BilledTransactions]
GO

/****** Object:  Table [dbo].[BilledTransactions]    Script Date: 5/22/2018 2:30:55 PM ******/
DROP TABLE [dbo].[BilledTransactions]
GO

/****** Object:  Table [dbo].[BilledTransactions]    Script Date: 5/22/2018 2:30:55 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BilledTransactions](
	[BilledTransactionsId] [bigint] IDENTITY(1,1) NOT NULL,
	[PracticeName] [varchar](150) NULL,
	[Entity] [int] NULL,
	[ApptId] [int] NULL,
	[TransactionNumber] [int] NULL,
	[Account] [varchar](50) NULL,
	[SSN] [varchar](10) NULL,
	[PatientName] [varchar](255) NULL,
	[BirthDate] [date] NULL,
	[Sex] [char](1) NULL,
	[PatientAddress] [varchar](255) NULL,
	[PatientCity] [varchar](255) NULL,
	[PatientState] [varchar](100) NULL,
	[PatientZip] [varchar](15) NULL,
	[PatientPhone] [varchar](15) NULL,
	[InsCode1] [varchar](40) NULL,
	[Insurance1] [varchar](255) NULL,
	[ADJNumber] [varchar](40) NULL,
	[InsurancePolicy] [varchar](100) NULL,
	[InsCode2] [varchar](40) NULL,
	[Insurance2] [varchar](255) NULL,
	[ARClass] [varchar](40) NULL,
	[InsuranceAddress] [varchar](255) NULL,
	[InsuranceCity] [varchar](255) NULL,
	[InsuranceState] [varchar](10) NULL,
	[InsuranceZip] [varchar](10) NULL,
	[InsurancePhone] [varchar](20) NULL,
	[InsuranceFax] [varchar](20) NULL,
	[PostingDate] [date] NULL,
	[DOS] [date] NULL,
	[DOI] [date] NULL,
	[ApptBaseCPTCode] [varchar](5) NULL,
	[ApptCPTCode] [varchar](10) NULL,
	[ApptCPTDescription] [varchar](255) NULL,
	[ChargesBaseCPT] [varchar](5) NULL,
	[ChargesCPTCode] [varchar](10) NULL,
	[ChargesCPTDescription] [varchar](255) NULL,
	[Modality] [varchar](30) NULL,
	[Diag] [varchar](10) NULL,
	[OP] [varchar](10) NULL,
	[Provider] [varchar](255) NULL,
	[ProvFirstName] [varchar](255) NULL,
	[ProvLastName] [varchar](255) NULL,
	[ProviderName] [varchar](255) NULL,
	[NPI] [varchar](20) NULL,
	[RefCode] [varchar](30) NULL,
	[RefName] [varchar](255) NULL,
	[Employer] [varchar](255) NULL,
	[AttorneyCode] [varchar](50) NULL,
	[AttorneyName] [varchar](255) NULL,
	[AttorneyAddress1] [varchar](255) NULL,
	[AttorneyAddress2] [varchar](255) NULL,
	[AttorneyCity] [varchar](255) NULL,
	[AttorneyState] [varchar](2) NULL,
	[AttorneyZip] [varchar](10) NULL,
	[AttorneyPhone] [varchar](15) NULL,
	[AttorneyFax] [varchar](15) NULL,
	[Plc] [varchar](10) NULL,
	[Dpt] [varchar](10) NULL,
	[Qty] [int] NULL,
	[Balance] [float] NULL,
	IsCharge bit,
	[FinancialCode] varchar(100),
	[InsertDate] [datetime] NULL
) ON [PRIMARY]
GO

SET ANSI_PADDING ON
GO

/****** Object:  Index [idx_BilledTransactions_ARClass]    Script Date: 5/22/2018 2:30:56 PM ******/
CREATE NONCLUSTERED INDEX [idx_BilledTransactions_ARClass] ON [dbo].[BilledTransactions]
(
	[ARClass] ASC
)
INCLUDE ( 	[PracticeName],
	[ApptId],
	[TransactionNumber],
	[Account],
	[SSN],
	[PatientName],
	[BirthDate],
	[PatientAddress],
	[PatientCity],
	[PatientState],
	[PatientZip],
	[PatientPhone],
	[InsCode1],
	[Insurance1],
	[ADJNumber],
	[InsurancePolicy],
	[InsuranceAddress],
	[InsuranceCity],
	[InsuranceState],
	[InsuranceZip],
	[InsurancePhone],
	[InsuranceFax],
	[PostingDate],
	[DOS],
	[DOI],
	[ChargesCPTCode],
	[ChargesCPTDescription],
	[Diag],
	[RefCode],
	[RefName],
	[Employer],
	[AttorneyCode],
	[AttorneyName],
	[AttorneyAddress1],
	[AttorneyAddress2],
	[AttorneyCity],
	[AttorneyState],
	[AttorneyZip],
	[AttorneyPhone],
	[AttorneyFax],
	[Plc],
	[Balance]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

SET ANSI_PADDING ON
GO

/****** Object:  Index [idx_BilledTransactions_ARClassInsCode]    Script Date: 5/22/2018 2:30:56 PM ******/
CREATE NONCLUSTERED INDEX [idx_BilledTransactions_ARClassInsCode] ON [dbo].[BilledTransactions]
(
	[ARClass] ASC,
	[InsCode1] ASC
)
INCLUDE ( 	[ApptId],
	[Account],
	[PatientName],
	[BirthDate],
	[PatientAddress],
	[PatientCity],
	[PatientState],
	[PatientZip],
	[PatientPhone],
	[PostingDate],
	[DOS],
	[DOI],
	[ApptBaseCPTCode],
	[ApptCPTCode],
	[ApptCPTDescription],
	[ChargesBaseCPT],
	[ChargesCPTCode],
	[ChargesCPTDescription],
	[Modality],
	[Diag],
	[RefName],
	[AttorneyName],
	[AttorneyAddress1],
	[AttorneyAddress2],
	[AttorneyCity],
	[AttorneyState],
	[AttorneyZip],
	[AttorneyPhone],
	[AttorneyFax],
	[Plc],
	[Balance]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

ALTER TABLE [dbo].[BilledTransactions] ADD  CONSTRAINT [DF_BilledTransactions_InsertDate]  DEFAULT (getdate()) FOR [InsertDate]
GO



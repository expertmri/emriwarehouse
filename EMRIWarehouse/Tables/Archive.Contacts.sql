USE [EMRIWarehouse]
GO

/****** Object:  Table [SF].[Contacts]    Script Date: 10/2/2018 9:28:28 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE Archive.Contacts(
	ID bigint IDENTITY(1,1),
	ContactId int NOT NULL,
	SF_ContactId varchar(100) NULL,
	FirstName varchar(255) NULL,
	LastName varchar(255) NULL,
	MiddleName varchar(255) NULL,
	Salutation varchar(20) NULL,
	Suffix varchar(20) NULL,
	JobTitle varchar(75) NULL,
	Phone varchar(20) NULL,
	Fax varchar(20) NULL,
	Email varchar(255) NULL,
	CreateDate datetime NULL,
	ModifiedDate datetime NULL,
	ADSKey varchar(50) NULL,
	ContactType varchar(50) NULL,
	Activity varchar(40),
	InsertDate datetime
) ON [PRIMARY]
GO

ALTER TABLE Archive.Contacts ADD  CONSTRAINT DF_Contacts_CreateDate  DEFAULT (GETDATE()) FOR InsertDate
GO



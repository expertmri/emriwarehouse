USE [EMRIWarehouse]
GO

ALTER TABLE [SF].[Accounts] DROP CONSTRAINT [DF_Accounts_CreateDate]
GO

/****** Object:  Index [idx_Accounts_AccountId]    Script Date: 10/10/2019 2:42:49 PM ******/
DROP INDEX [idx_Accounts_AccountId] ON [SF].[Accounts]
GO

/****** Object:  Table [SF].[Accounts]    Script Date: 10/10/2019 2:42:49 PM ******/
DROP TABLE [SF].[Accounts]
GO

/****** Object:  Table [SF].[Accounts]    Script Date: 10/10/2019 2:42:49 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [SF].[Accounts](
	[AccountId] [int] IDENTITY(1,1) NOT NULL,
	[SF_AccountId] [varchar](100) NULL,
	[AccountName] [varchar](200) NULL,
	[Phone] [varchar](50) NULL,
	[Fax] [varchar](50) NULL,
	[Website] [varchar](255) NULL,
	[Facility] [varchar](250) NULL,
	[Address1] [varchar](255) NULL,
	[City] [varchar](255) NULL,
	[StateName] [varchar](50) NULL,
	[Zip] [varchar](50) NULL,
	[ADSAccountId] [varchar](100) NULL,
	[ParentId] [varchar](24) NULL,
	[AccountManager] [varchar](255) NULL,
	[KeyChecksum]  AS (checksum([AccountName],[Phone],[Fax],[Facility],[Address1],[City],[Zip],[ParentId],[AccountManager])),
	[CreateDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedUser] [varchar](255) NULL,
	[AccountHandler] [varchar](150) NULL,
	[SalesRepresentative] [varchar](150) NULL,
	ZoneRegion varchar(255),
	[LastActivityDate] [date] NULL,
	[CurrentHandler] [varchar](150) NULL
) ON [PRIMARY]
GO

SET ANSI_PADDING ON
GO

/****** Object:  Index [idx_Accounts_AccountId]    Script Date: 10/10/2019 2:42:49 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [idx_Accounts_AccountId] ON [SF].[Accounts]
(
	[SF_AccountId] ASC
)
INCLUDE ( 	[AccountName],
	[Phone],
	[Fax],
	[Address1]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

ALTER TABLE [SF].[Accounts] ADD  CONSTRAINT [DF_Accounts_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO



USE EMRIWarehouse
GO

/****** Object:  Table dbo.Appointments    Script Date: 3/12/2018 1:02:04 PM ******/
DROP TABLE dbo.Appointments
GO

/****** Object:  Table dbo.Appointments    Script Date: 3/12/2018 1:02:04 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE dbo.Appointments(
	ApptId varchar(10) NULL
	, CaseId int NULL
	, Account int NOT NULL
	, AppointmentListType varchar(35) NOT NULL
	, BookedDate date NULL
	, BookedTime varchar(20)
	, DOS date NULL
	, ApptTime varchar(20)
	, DOI date NULL
	, Modality varchar(10) NULL
	, Facility varchar(50) NULL
	, ARClass varchar(50) NULL
	, AppointmentStatusId int NULL
	, AppointmentStatus varchar(18) NULL
	, AppointmentCode varchar(20)
	, AppointmentDescription varchar(255)
	, MarketingReferral varchar(75) NULL
	, ReferringDrId varchar(50) NULL
	, ReferralCode varchar(50) NULL
	, ReferringDr varchar(150) NULL
	, ReferringGroup varchar(150) NULL
	, InsCode varchar(100) NULL
	, InsName varchar(255)
	, RefAttyCode varchar(100)
	, ReferringAttorney varchar(200) NULL
	, Resource1 varchar(50) NULL
	, OperatorCode varchar(10) NULL
	, Operator varchar(200) NULL
	, CancelDate datetime
	, CancelUser varchar(150)
	, CancelReasonCode varchar(20)
	, CancelReasonDescription varchar(255)
	, IsDeletedAppt bit CONSTRAINT DF_Appointments_IsDeletedAppt DEFAULT (0)
	, RowCheckSum int
) ON [PRIMARY]
GO



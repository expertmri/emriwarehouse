USE [EMRIWarehouse]
GO
/****** Object:  Table [XRef].[ApptCodes]    Script Date: 10/24/2017 8:53:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [XRef].[ApptCodes](
	[ApptCodesId] [int] IDENTITY(1,1) NOT NULL,
	[ADSApptCode] [varchar](10) NULL,
	[Modality] [varchar](10) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

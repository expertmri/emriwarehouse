USE [EMRIWarehouse]
GO

/****** Object:  Table [dbo].[NoShowTracking]    Script Date: 2/21/2018 8:45:35 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[NoShowTracking](
	[NoShowTrackingID] [int] IDENTITY(1,1) NOT NULL,
	[ApptId] [int] NULL,
	[CaseId] [int] NULL,
	[Account] [int] NULL,
	[AppointmentListType] [varchar](100) NULL,
	[BookedDate] [date] NULL,
	[DOS] [date] NULL,
	[DOI] [date] NULL,
	[Modality] [varchar](10) NULL,
	[Facility] [varchar](50) NULL,
	[ARClass] [varchar](50) NULL,
	[AppointmentStatusId] [int] NULL,
	[AppointmentStatus] [varchar](100) NULL,
	[MarketingReferral] [varchar](75) NULL,
	[ReferringDrId] [varchar](50) NULL,
	[ReferralCode] [varchar](50) NULL,
	[ReferringDr] [varchar](255) NULL,
	[ReferringGroup] [varchar](255) NULL,
	[InsCode] [varchar](100) NULL,
	[ReferringAttorney] [varchar](255) NULL,
	[CreateDate] [datetime] NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[NoShowTracking] ADD  CONSTRAINT [DF_NoShowTracking_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO


ALTER TABLE [dbo].[NoShowTracking] ADD OperatorCode varchar(10)
GO
ALTER TABLE [dbo].[NoShowTracking] ADD Operator varchar(200)
GO

ALTER TABLE [NoShowTracking]
	ADD CancelDate datetime
		, CancelUser varchar(150)
		, CancelReasonCode varchar(20)
		, CancelReasonDescription varchar(255)
		, InsName varchar(255)
		, RefAttyCode varchar(100)
USE EMRIWarehouse
GO

DROP TABLE  dbo.SchedulingRequest 
GO

CREATE TABLE dbo.SchedulingRequest (
	ShedulingRequestId bigint IDENTITY(1,1)
	, WebFormRequestId bigint
	, FirstName varchar(255)
	, LastName varchar(255)
	, DOB date
	, Email varchar(255)
	, Phone varchar(20)
	, Modality varchar(25)
	, ReferralSource varchar(50)
	, CallSummary varchar(4000)
	, Account varchar(20)
	, SchedulingStatus varchar(255)
	, IsProcessed bit CONSTRAINT SchedulingRequestIsProcessed DEFAULT (0)
	, CreateDate datetime
	, ModifiedDate datetime
	)
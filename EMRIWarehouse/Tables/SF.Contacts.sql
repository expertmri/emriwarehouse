USE EMRIWarehouse
GO

CREATE TABLE SF.Contacts (
	ContactId int IDENTITY(1,1) NOT NULL
	, SF_ContactId varchar(100) NULL
	, FirstName varchar(255) NULL
	, LastName varchar(255) NULL
	, MiddleName varchar(255) NULL
	, Salutation varchar(20)
	, Suffix varchar(20)
	, JobTitle varchar(75) NULL
	, Phone varchar(20) NULL
	, Fax varchar(20) NULL
	, Email varchar(255) NULL
	, CreateDate datetime NULL
	, ModifiedDate datetime NULL
) ON [PRIMARY]
GO

ALTER TABLE SF.Contacts ADD  CONSTRAINT DF_Contacts_CreateDate  DEFAULT (getdate()) FOR CreateDate
GO



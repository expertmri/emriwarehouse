USE [EMRIWarehouse]
GO

ALTER TABLE [SF].[UserTaskProductivity] DROP CONSTRAINT [DF_UserTaskProductivity_InsertDate]
GO

/****** Object:  Index [idx_UserTaskProductivity_CompletedDate]    Script Date: 9/18/2019 10:38:53 AM ******/
DROP INDEX [idx_UserTaskProductivity_CompletedDate] ON [SF].[UserTaskProductivity]
GO

/****** Object:  Table [SF].[UserTaskProductivity]    Script Date: 9/18/2019 10:38:53 AM ******/
DROP TABLE [SF].[UserTaskProductivity]
GO

/****** Object:  Table [SF].[UserTaskProductivity]    Script Date: 9/18/2019 10:38:53 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [SF].[UserTaskProductivity](
	[UserTaskProductivityId] [bigint] IDENTITY(1,1) NOT NULL,
	[SF_AccountId] [varchar](30) NULL,
	[TaskId] [varchar](30) NULL,
	[Task] [varchar](150) NULL,
	[TaskStatus] [varchar](30) NULL,
	[Activity] [varchar](255) NULL,
	[Reason] [varchar](255) NULL,
	CompletedBy [varchar](255) NULL,
	AssignedTo [varchar](255) NULL,
	Priority varchar(100),
	[CreatedDate] [datetime] NULL,
	[CompletedDate] [datetime] NULL,
	DueDate datetime,
	[InsertDate] [datetime] NULL
) ON [PRIMARY]
GO

SET ANSI_PADDING ON
GO

/****** Object:  Index [idx_UserTaskProductivity_CompletedDate]    Script Date: 9/18/2019 10:38:53 AM ******/
CREATE NONCLUSTERED INDEX [idx_UserTaskProductivity_CompletedDate] ON [SF].[UserTaskProductivity]
(
	[CompletedDate] ASC
)
INCLUDE ( 	CompletedBy,
	[Task],
	[TaskId],
	[SF_AccountId],
	[Reason]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

ALTER TABLE [SF].[UserTaskProductivity] ADD  CONSTRAINT [DF_UserTaskProductivity_InsertDate]  DEFAULT (getdate()) FOR [InsertDate]
GO



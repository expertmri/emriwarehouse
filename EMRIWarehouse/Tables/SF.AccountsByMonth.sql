USE EMRIWarehouse
GO

CREATE TABLE SF.AccountsByMonth (
	AccountsByMonthId bigint IDENTITY (1,1)
	, SF_AccountId varchar(30)
	, IsActive bit
	, AccountStatus varchar(50)
	, MonthDate date
	, InsertDate datetime CONSTRAINT DF_AccountsByMonth_InsertDate DEFAULT GETDATE()
)
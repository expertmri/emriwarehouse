USE EMRIWarehouse
GO

CREATE TABLE XRef.AppointmentStatus (
	AppointmentStatusId int IDENTITY(1,1)
	, AppointmentStatus varchar(30)
	, IsBacklog bit CONSTRAINT DF_AppointmentStatus_IsBacklog DEFAULT (0)
	, IsOnBooks bit CONSTRAINT DF_AppointmentStatus_IsOnBooks DEFAULT (0)
	, IsScheduled bit CONSTRAINT DF_AppointmentStatus_IsScheduled DEFAULT (0)
	, CreateDate datetime CONSTRAINT DF_AppointmentStatus_CreateDate DEFAULT GETDATE()
	)

SET IDENTITY_INSERT XRef.AppointmentStatus ON	

INSERT INTO XRef.AppointmentStatus (
		AppointmentStatusId 
		, AppointmentStatus 
		, IsBacklog 
		, IsOnBooks
		, IsScheduled
		, CreateDate 
	)

VALUES	('1', 'Pending Scheduling', 1, 0)
		, ('2', 'Pending', 1, 0)
		, ('3', 'On Schedule', 0, 1)
		, ('4', 'Exam Complete', 0, 0)
		, ('5', 'No Show', 0, 0)
		, ('6', 'Pending Status', 1, 0)
		, ('7', 'Cancelled', 0, 0)

SET IDENTITY_INSERT XRef.AppointmentStatus OFF


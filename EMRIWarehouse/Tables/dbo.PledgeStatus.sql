USE [EMRIWarehouse]
GO
/****** Object:  Table [dbo].[PledgeStatus]    Script Date: 10/24/2017 8:53:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PledgeStatus](
	[PledgeStatusId] [int] IDENTITY(1,1) NOT NULL,
	[PledgeStatus] [varchar](40) NULL,
	[CreateDate] [datetime] NULL CONSTRAINT [DF_PledgeStatus_CreateDate]  DEFAULT (getdate())
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

USE EMRIWarehouse
GO

CREATE TABLE dbo.ChargesCredits (
	ChargesCreditsId int IDENTITY(1,1)
	, ChargeTransactionNo int
	, CreditTransactionNo int
	, Amount varchar(100)
)
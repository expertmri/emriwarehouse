USE [EMRIWarehouse]
GO

/****** Object:  Table [dbo].[RTAReportMetrics]    Script Date: 7/9/2018 4:03:23 PM ******/
DROP TABLE [dbo].[RTAReportMetrics]
GO

/****** Object:  Table [dbo].[RTAReportMetrics]    Script Date: 7/9/2018 4:03:23 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE dbo.RTAReportMetrics(
	StudyDate datetime
	, ReceivedDate datetime
	, TimeInSubmitted datetime
	, TimeInVerified datetime
	, TimeInDictated datetime
	, TimeInPreliminary datetime
	, TimeInFinal datetime
	, RA varchar(100)
	, PatientId varchar(100)
	, Modality varchar(20)
	, ReportStatus varchar(50)
	, AccessionNumber varchar(25)
	, SiteName varchar(255)
	, RefPhysician varchar(255)
	, InsertDate datetime CONSTRAINT DF_RTAReportMEtrics_InsertDate DEFAULT GETDATE()
) ON [PRIMARY]
GO



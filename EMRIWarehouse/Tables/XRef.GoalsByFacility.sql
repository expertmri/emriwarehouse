USE EMRIWarehouse
GO

CREATE TABLE XRef.GoalsByFacility (
GoalsByFacilityId int IDENTITY (1,1)
, FacilityName varchar(150)
, Modality varchar(20)
, BaseGoal int
, MonthlyGoal int
, MonthDate date
, CreateDate datetime CONSTRAINT DF_GoalsByFacility_CreateDate DEFAULT GETDATE()
, ModifiedDate datetime
)

--INSERT INTO XRef.GoalsByFacility (FacilityName, Modality, MonthlyGoal, CalendarYear)
--VALUES	('BELLFLOWER', 'MRI', 991, 2018)
--		, ('RIVERSIDE', 'MRI', 375, 2018)
--		, ('WINNETKA', 'MRI', 233, 2018)
--		, ('SAN DIEGO', 'MRI', 190, 2018)
--		, ('BEVERLY HILLS', 'MRI', 111 , 2018)
--		, ('BAKERSFIELD-STAND-UP', 'MRI', 223, 2018)
--		, ('LOS ANGELES', 'MRI', 63, 2018)
--		, ('LANCASTER', 'MRI', 62, 2018)
--		, ('CULVER CITY', 'MRI', 33, 2018)
--		, ('MISSION VEIJO', 'MRI', 18, 2018)


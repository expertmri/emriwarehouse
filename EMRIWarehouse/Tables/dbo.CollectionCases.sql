USE EMRIWarehouse
GO

CREATE TABLE dbo.CollectionCases (
	CollectionCasesId int IDENTITY(1,1)
	, Account int
	, Payor varchar(20)
	, PISettlementAmnt float
	, CaseSettlementDate date
	, CaseReason varchar(255)
	, TotalMedicals float
	, ExpertLienAmount float
	, LastUpdated date
	, DateOfLoss date
	, LienReductionOffer float
	, Prorata float
	, CaseStatus varchar(100)
	, ARClass varchar(20)
	, SignedLienOnFile varchar(20)
	, CaseManager varchar(100)
	, CaseMangerEmail varchar(255)
	, CreateDate datetime CONSTRAINT DF_CollectionCases_CreateDate DEFAULT GETDATE()
)




USE [EMRIWarehouse]
GO
/****** Object:  Table [XRef].[KeyHealthVendorNames]    Script Date: 10/24/2017 8:53:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [XRef].[KeyHealthVendorNames](
	[KeyHealthVendorNamesId] [int] IDENTITY(1,1) NOT NULL,
	[Facility] [varchar](20) NULL,
	[VendorName] [varchar](255) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

USE EMRIWarehouse
GO

/****** Object:  Table dbo.NoShowTracking    Script Date: 2/21/2018 8:45:35 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE dbo.AppointmentChangeTracking (
	AppointmentChangeTrackingId int IDENTITY(1,1) NOT NULL,
	ApptId int NULL,
	CaseId int NULL,
	Account int NULL,
	AppointmentListType varchar(100) NULL,
	BookedDate date NULL,
	DOS date NULL,
	DOI date NULL,
	Modality varchar(10) NULL,
	Facility varchar(50) NULL,
	ARClass varchar(50) NULL,
	AppointmentStatusId int NULL,
	AppointmentStatus varchar(100) NULL,
	MarketingReferral varchar(75) NULL,
	ReferringDrId varchar(50) NULL,
	ReferralCode varchar(50) NULL,
	ReferringDr varchar(255) NULL,
	ReferringGroup varchar(255) NULL,
	InsCode varchar(100) NULL,
	ReferringAttorney varchar(255) NULL,
	OperatorCode varchar(10),
	Operator varchar(200),
	CreateDate datetime NULL CONSTRAINT [DF_AppointmentChangeTracking_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
) 
GO

ALTER TABLE dbo.AppointmentChangeTracking ADD  CONSTRAINT DF_AppointmentChangeTracking_CreateDate  DEFAULT (GETDATE()) FOR CreateDate
GO


ALTER TABLE AppointmentChangeTracking
	ADD CancelDate datetime
		, CancelUser varchar(150)
		, CancelReasonCode varchar(20)
		, CancelReasonDescription varchar(255)
		, InsName varchar(255)
		, RefAttyCode varchar(100)
		, Resource1 varchar(50)
		, RowCheckSum int
		, AppointmentCode varchar(100)



/****** Object:  Index [idx_AppointmentChangeTracking_ApptIdCheckSum]    Script Date: 3/28/2018 2:33:44 PM ******/
CREATE CLUSTERED INDEX [idx_AppointmentChangeTracking_ApptIdCheckSum] ON [dbo].[AppointmentChangeTracking]
(
	[ApptId] ASC,
	[RowCheckSum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



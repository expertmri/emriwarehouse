USE EMRIWarehouse
GO

CREATE TABLE dbo.ScansByAccountWorkflow (
	SF_AccountId varchar(50)
	, ReferralName varchar(255)
	, ScanCountMonthly int
	, ThreeMonthAvg float
	, FirstReferralDate date
	, LastReferralDate date
	, PercentAvg float
	, AccountCategory varchar(40)
	, AccountStatus varchar(40)
	, MonthYear date
	, MonthName varchar(20)
	, AccountType varchar(30)
	, BusinessType varchar(30)
	, RN int
	, CreateDate datetime CONSTRAINT DF_ScansByAccountWorkflow_CreateDate DEFAULT GETDATE()
	)
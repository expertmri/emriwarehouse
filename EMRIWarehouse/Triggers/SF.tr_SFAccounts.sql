USE EMRIWarehouse
GO

ALTER TRIGGER SF.tr_SFAccounts
   ON  SF.Accounts
   AFTER DELETE,UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	IF EXISTS (SELECT * FROM inserted) AND EXISTS (SELECT * FROM deleted)
	
	BEGIN
		INSERT INTO Archive.Accounts (
				AccountId
				, SF_AccountId
				, AccountName
				, Phone
				, Fax
				, Website
				, Facility
				, Address1
				, Address2
				, City
				, StateName
				, Zip
				, ADSAccountId
				, ParentId
				, AccountManager
				, Activity
				, ModifiedUser
				, InsertDate
			)
		SELECT	deleted.AccountId
				, deleted.SF_AccountId
				, deleted.AccountName
				, deleted.Phone
				, deleted.Fax
				, deleted.Website
				, deleted.Facility
				, deleted.Address1
				, deleted.Address2
				, deleted.City
				, deleted.StateName
				, deleted.Zip
				, deleted.ADSAccountId
				, deleted.ParentId
				, deleted.AccountManager
				, COALESCE(deleted.ModifiedUser, 'Original Record')
				, 'UPDATE'
				, GETDATE()
		FROM deleted
	END

	IF EXISTS(SELECT * FROM deleted) AND NOT EXISTS(SELECT * FROM inserted)

	BEGIN
		INSERT INTO Archive.Accounts (
				AccountId
				, SF_AccountId
				, AccountName
				, Phone
				, Fax
				, Website
				, Facility
				, Address1
				, Address2
				, City
				, StateName
				, Zip
				, ADSAccountId
				, ParentId
				, AccountManager
				, Activity
				, ModifiedUser
				, InsertDate
			)
		SELECT	deleted.AccountId
				, deleted.SF_AccountId
				, deleted.AccountName
				, deleted.Phone
				, deleted.Fax
				, deleted.Website
				, deleted.Facility
				, deleted.Address1
				, deleted.Address2
				, deleted.City
				, deleted.StateName
				, deleted.Zip
				, deleted.ADSAccountId
				, deleted.ParentId
				, deleted.AccountManager
				, 'DELETE'
				, COALESCE(deleted.ModifiedUser, 'Original Record')
				, GETDATE()
		FROM deleted
	END

END
	
GO




USE EMRIWarehouse
GO

CREATE TRIGGER SF.tr_SFContacts
   ON  SF.Contacts
   AFTER DELETE,UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	IF EXISTS (SELECT * FROM inserted) AND EXISTS (SELECT * FROM deleted)
	
	BEGIN
		INSERT INTO Archive.Contacts (
				ContactId
				, SF_ContactId
				, FirstName
				, LastName
				, MiddleName
				, Salutation
				, Suffix
				, JobTitle
				, Phone
				, Fax
				, Email
				, CreateDate
				, ModifiedDate
				, ADSKey
				, ContactType
				, Activity
				, ModifiedUser
				, InsertDate
			)
		SELECT	deleted.ContactId
				, deleted.SF_ContactId
				, deleted.FirstName
				, deleted.LastName
				, deleted.MiddleName
				, deleted.Salutation
				, deleted.Suffix
				, deleted.JobTitle
				, deleted.Phone
				, deleted.Fax
				, deleted.Email
				, deleted.CreateDate
				, deleted.ModifiedDate
				, deleted.ADSKey
				, deleted.ContactType
				, 'UPDATE'
				, COALESCE(deleted.ModifiedUser, 'Original Record')
				, GETDATE()
		FROM deleted
	END

	IF EXISTS(SELECT * FROM deleted) AND NOT EXISTS(SELECT * FROM inserted)

	BEGIN
		INSERT INTO Archive.Contacts (
				ContactId
				, SF_ContactId
				, FirstName
				, LastName
				, MiddleName
				, Salutation
				, Suffix
				, JobTitle
				, Phone
				, Fax
				, Email
				, CreateDate
				, ModifiedDate
				, ADSKey
				, ContactType
				, Activity
				, ModifiedUser
				, InsertDate
			)
		SELECT	deleted.ContactId
				, deleted.SF_ContactId
				, deleted.FirstName
				, deleted.LastName
				, deleted.MiddleName
				, deleted.Salutation
				, deleted.Suffix
				, deleted.JobTitle
				, deleted.Phone
				, deleted.Fax
				, deleted.Email
				, deleted.CreateDate
				, deleted.ModifiedDate
				, deleted.ADSKey
				, deleted.ContactType
				, 'DELETE'
				, COALESCE(deleted.ModifiedUser, 'Original Record')
				, GETDATE()
		FROM deleted
	END

END
	
GO




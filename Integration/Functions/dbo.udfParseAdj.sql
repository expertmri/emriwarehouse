USE [Integration]
GO
/****** Object:  UserDefinedFunction [dbo].[udfParseAdj]    Script Date: 10/24/2017 9:07:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[udfParseAdj] (@InsPolicy varchar(100))
	RETURNS varchar(100)
AS

BEGIN
DECLARE @Adj varchar(100)

SET @Adj = (
				SELECT SUBSTRING(
									@InsPolicy 
									, PATINDEX('%ADJ%', @InsPolicy) 
									, (LEN(@InsPolicy) - (PATINDEX('%ADJ%', @InsPolicy) - 1))
								)
				WHERE @InsPolicy LIKE '%ADJ'
			)

RETURN @Adj;
END;
GO

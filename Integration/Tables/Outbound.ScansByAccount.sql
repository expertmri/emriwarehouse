USE [Integration]
GO

DROP TABLE [OUTBOUND].[ScansByAccount];

/****** Object:  Table [OUTBOUND].[ScansByContact]    Script Date: 3/12/2018 1:46:47 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [OUTBOUND].[ScansByAccount](
	[DOSMonthYear] [varchar](9) NULL,
	[DOSMonth] [int] NULL,
	[DOSYear] [int] NULL,
	[ARGroup] [varchar](20) NULL,
	[ReferringDr] [varchar](150) NULL,
	[Modality] [varchar](10) NULL,
	[ExamCount] [int] NULL,
	[ThreeMonthAvg] [varchar](10) NULL,
	[LastMonth] [int] NULL,
	[OwnerId] [varchar](25) NULL,
	[SF_AccountId] [varchar](255) NULL
) ON [PRIMARY]
GO



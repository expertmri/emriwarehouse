USE [Integration]
GO
/****** Object:  Table [Inbound].[BilledTransactions]    Script Date: 10/24/2017 9:07:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Inbound].[BilledTransactions](
	[BilledTransactionsId] [bigint] IDENTITY(1,1) NOT NULL,
	[PracticeName] [varchar](150) NULL,
	[AccountNumber] [varchar](20) NULL,
	[PatientName] [varchar](150) NULL,
	[ARType] [varchar](20) NULL,
	[InsCode1] [varchar](20) NULL,
	[Insurance1] [varchar](100) NULL,
	[TransactionNumber] [int] NULL,
	[PostingDate] [date] NULL,
	[ServiceDate] [date] NULL,
	[CPTCode] [varchar](20) NULL,
	[CPTDescription] [varchar](100) NULL,
	[Diag] [varchar](10) NULL,
	[OP] [varchar](5) NULL,
	[RefCode] [varchar](20) NULL,
	[RefName] [varchar](150) NULL,
	[Plc] [varchar](10) NULL,
	[Dpt] [varchar](5) NULL,
	[Qty] [varchar](20) NULL,
	[Balance] [float] NULL,
	[Provider] [varchar](100) NULL,
	[ProviderName] [varchar](150) NULL,
	[NPI] [int] NULL,
	[CaseNumber] [int] NULL,
	[CaseType] [varchar](50) NULL,
	[CaseEffectiveDate] [date] NULL,
	[SubmissionDates] [varchar](255) NULL,
	[Flags] [varchar](20) NULL,
	[Encounter] [varchar](25) NULL,
	[InsCd1] [varchar](20) NULL,
	[Ins1] [varchar](100) NULL,
	[SourceFileName] [varchar](255) NULL,
	[IsImported] [bit] NULL,
	[InsertDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [Inbound].[BilledTransactions] ADD  CONSTRAINT [DF_BilledTransactions_Imported]  DEFAULT ((0)) FOR [IsImported]
GO
ALTER TABLE [Inbound].[BilledTransactions] ADD  CONSTRAINT [DF_BilledTransactions_InsertDate]  DEFAULT (getdate()) FOR [InsertDate]
GO

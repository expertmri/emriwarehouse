USE Integration
GO
--DROP TABLE  dbo.GlobalVoipFtpFiles
CREATE TABLE dbo.GlobalVoipFtpFiles (
FtpFileName varchar(20)
, LastModifiedDate datetime
, InsertDate datetime CONSTRAINT DF_GlobalVoipFtpFiles_InsertDate DEFAULT GETDATE()
)

INSERT INTO dbo.GlobalVoipFtpFiles (FtpFileName, LastModifiedDate)
VALUES ('20181031', '10/31/2018')


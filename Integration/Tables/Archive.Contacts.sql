USE [Integration]
GO

/****** Object:  Table [dbo].[Contacts]    Script Date: 10/4/2018 9:26:00 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE Archive.Contacts(
	ID bigint IDENTITY(1,1)
	, ContactId int 
	, AccountId int NULL
	, OriginalSFContactId varchar(100)
	, MergedSFContactId varchar(100)
	, ModifiedUser varchar(255)
	, InsertDate datetime CONSTRAINT DF_ArchiveContacts_InsertDate DEFAULT GETDATE()
	
) 
GO



USE Integration
GO

CREATE TABLE Archive.AccountContact (
	ID bigint IDENTITY(1,1)
	, AccountContactId int
	, SF_AccountId varchar(255)
	, SF_ContactId varchar(255)
	, Activity varchar(40)
	, ModifiedUser varchar(255)
	, InsertDate datetime 
)



USE [Integration]
GO

/****** Object:  Table [dbo].[AccountContact]    Script Date: 12/18/2017 1:27:49 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccountContact](
	[AccountContactId] [int] IDENTITY(1,1) NOT NULL,
	[SF_AccountId] [varchar](100) NULL,
	[SF_ContactId] [varchar](100) NULL,
	[CreateDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY]
GO

SET ANSI_PADDING ON
GO

/****** Object:  Index [PK_AccountContact]    Script Date: 12/18/2017 1:27:50 PM ******/
CREATE UNIQUE CLUSTERED INDEX [PK_AccountContact] ON [dbo].[AccountContact]
(
	[SF_AccountId] ASC,
	[SF_ContactId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

ALTER TABLE [dbo].[AccountContact] ADD  CONSTRAINT [DF_AccountContact_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO



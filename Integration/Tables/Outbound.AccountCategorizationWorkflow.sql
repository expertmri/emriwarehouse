USE Integration
GO

DROP TABLE OUTBOUND.AccountCategorizationWorkflow 
GO

CREATE TABLE OUTBOUND.AccountCategorizationWorkflow (
	SF_AccountId varchar(50)
	, BusinessType varchar(10)
	, AccountStatus varchar(30)
	, AccountCategory varchar(20)
	, PercentChange varchar(30)
	, MarketingType varchar(30)
	, ScansLastMonth int
	, ScansThreeMonthAverage varchar(20)
	, ScansThisMonth int
	, ScansLastWeek int
	, ScansYTD int
	, LastReferralDate date
)


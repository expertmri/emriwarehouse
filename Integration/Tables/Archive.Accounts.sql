USE Integration
GO

/****** Object:  Table dbo.Accounts    Script Date: 10/4/2018 9:14:00 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE Archive.Accounts(
	ID bigint IDENTITY(1,1)
	, AccountId int 
	, OriginalSFAccountId varchar(100) NULL
	, MergedSFAccountId varchar(100) NULL
	, ModifiedUser varchar(255)
	, InsertDate datetime NULL CONSTRAINT DF_ArchiveAccounts_InsertDate DEFAULT GETDATE()
) 
GO


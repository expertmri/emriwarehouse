USE [Integration]
GO

ALTER TABLE [dbo].[Accounts] DROP CONSTRAINT [DF_Accounts_CreateDate]
GO

ALTER TABLE [dbo].[Accounts] DROP CONSTRAINT [DF_Accounts_IsExported]
GO

/****** Object:  Table [dbo].[Accounts]    Script Date: 12/18/2017 1:19:09 PM ******/
DROP TABLE [dbo].[Accounts]
GO

/****** Object:  Table [dbo].[Accounts]    Script Date: 12/18/2017 1:19:09 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Accounts](
	[AccountId] [int] IDENTITY(2068,1) NOT NULL,
	[SourceSystemPK] [varchar](100) NULL,
	[SourceTable] [varchar](200) NULL,
	[AccountName] [varchar](200) NULL,
	[Phone] [varchar](20) NULL,
	[Fax] [varchar](20) NULL,
	[Website] [varchar](150) NULL,
	[Facility] [varchar](250) NULL,
	[Address1] [varchar](100) NULL,
	[Address2] [varchar](100) NULL,
	[City] [varchar](100) NULL,
	[StateName] [varchar](30) NULL,
	[Zip] [varchar](10) NULL,
	SF_AccountId varchar(100),
	[IsExported] [int] NULL,
	[CreateDate] [datetime] NULL,
	[AccountNumber]  AS ('A-'+CONVERT([varchar],[AccountId]))
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Accounts] ADD  CONSTRAINT [DF_Accounts_IsExported]  DEFAULT ((0)) FOR [IsExported]
GO

ALTER TABLE [dbo].[Accounts] ADD  CONSTRAINT [DF_Accounts_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO



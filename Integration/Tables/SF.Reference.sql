USE Integration
GO

CREATE TABLE SF.Reference (
    Account_Coordinator__c nvarchar(18)
    , Account_Manager__c nvarchar(18)
    , Active__c bit
    , Changed_By__c nvarchar(18)
    , CreatedById nvarchar(18)
    , CreatedDate datetime
    , End_Date__c date
    , Id nvarchar(18)
    , Inactive_Date__c datetime
    , IsDeleted bit
    , LastModifiedById nvarchar(18)
    , LastModifiedDate datetime
    , LastReferencedDate datetime
    , LastViewedDate datetime
    , Lat_LL__c float
    , Lat_LR__c float
    , Lat_UL__c float
    , Lat_UR__c float
    , Lon_LL__c float
    , Lon_LR__c float
    , Lon_UL__c float
    , Lon_UR__c float
    , Marketing_Coordinator__c nvarchar(18)
    , Name nvarchar(80)
    , OwnerId nvarchar(18)
    , RecordTypeId nvarchar(18)
    , Sr_Account_Manager__c nvarchar(18)
    , Start_Date__c date
    , SystemModstamp datetime
    , User__c nvarchar(18)
    , WC_Account_Coordinator__c nvarchar(18)
    , WC_Account_Manager__c nvarchar(18)
	, InsertDate datetime CONSTRAINT DF_Reference_InsertDate DEFAULT GETDATE()
)
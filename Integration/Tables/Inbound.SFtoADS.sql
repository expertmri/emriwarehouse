USE [Integration]
GO

/****** Object:  Table [INBOUND].[SFtoADS]    Script Date: 9/4/2018 1:38:55 PM ******/
DROP TABLE [INBOUND].[SFtoADS]
GO

/****** Object:  Table [INBOUND].[SFtoADS]    Script Date: 9/4/2018 1:38:55 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [INBOUND].[SFtoADS](
	[ID] [varchar](50) NULL,
	[OWNERID] [varchar](50) NULL,
	[ISDELETED] [varchar](50) NULL,
	[NAME] [varchar](50) NULL,
	[CREATEDDATE] [varchar](50) NULL,
	[CREATEDBYID] [varchar](50) NULL,
	[LASTMODIFIEDDATE] [varchar](50) NULL,
	[LASTMODIFIEDBYID] [varchar](50) NULL,
	[SYSTEMMODSTAMP] [varchar](50) NULL,
	[LASTACTIVITYDATE] [varchar](50) NULL,
	[LASTVIEWEDDATE] [varchar](50) NULL,
	[LASTREFERENCEDDATE] [varchar](50) NULL,
	[ADS_ACCOUNT_KEY__C] [varchar](50) NULL,
	[ADS_CONTACT_KEY__C] [varchar](50) NULL,
	[ACCOUNT_ID__C] [varchar](50) NULL,
	[ADDRESS1__C] [varchar](255) NULL,
	[ADDRESS2__C] [varchar](255) NULL,
	[CITY__C] [varchar](50) NULL,
	[CONTACT_ID__C] [varchar](50) NULL,
	[CONTACT_TYPE__C] [varchar](50) NULL,
	[EMAIL__C] [varchar](50) NULL,
	[ERROR_MESSAGE__C] [varchar](50) NULL,
	[FAX__C] [varchar](50) NULL,
	[FIRST_NAME__C] [varchar](50) NULL,
	[FULL_NAME__C] [varchar](50) NULL,
	[GENERATION__C] [varchar](50) NULL,
	[IS_PROCESSED__C] [varchar](50) NULL,
	[LAST_NAME__C] [varchar](50) NULL,
	[MIDDLE_NAME__C] [varchar](50) NULL,
	[PHONE__C] [varchar](50) NULL,
	[PRACTICE__C] [varchar](50) NULL,
	[SALUTATION__C] [varchar](50) NULL,
	[STATE__C] [varchar](50) NULL,
	[SUFFIX__C] [varchar](50) NULL,
	[WEBSITE__C] [varchar](50) NULL,
	[ZIP__C] [varchar](50) NULL,
	[DATE_PROCESSED__C] [varchar](50) NULL,
	[AREA_OF_SPECIALTY__C] [varchar](50) NULL,
	[SOURCE__C] [varchar](50) NULL,
	[PrimaryKey] [varchar](100) NULL,
	[AccountFK] [varchar](32) NULL,
	[AccountName] [varchar](200) NULL,
	[AccountOriginator] [int] NULL,
	[Title] [varchar](75) NULL,
	[ContactName] [varchar](255) NULL,
	[JobTitle] [varchar](50) NULL,
	[Email] [varchar](255) NULL,
	[Phone] [varchar](20) NULL,
	[Fax] [varchar](20) NULL,
	[Website] [varchar](150) NULL,
	[Facility] [varchar](250) NULL,
	[Address1] [varchar](100) NULL,
	[Address2] [varchar](100) NULL,
	[City] [varchar](100) NULL,
	[State] [varchar](30) NULL,
	[Zip] [varchar](10) NULL,
	[ContactType] [varchar](40) NULL
) ON [PRIMARY]
GO



USE Integration
GO

DROP TABLE  SF.Events;

CREATE TABLE SF.Events(
    AccountId NVARCHAR(18)
	, Account_City__C  NVARCHAR(255)
	, Account_Street_Address__C  NVARCHAR(255)
	, Account_Zip_Code__C  NVARCHAR(100)
    , Activity_Status__c NVARCHAR(255)
    , Activity_Type__c NVARCHAR(255)
    , ActivityDate DATE
    , ActivityDateTime DATETIME
    , Completed_By__c NVARCHAR(18)
    , Contact__c NVARCHAR(18)
    , CreatedById NVARCHAR(18)
    , CreatedDate DATETIME
    , Date_Completed__c DATE
    , Description NTEXT
	, Due_Date__C DATETIME
    , DurationInMinutes INT
    , EndDateTime DATETIME
	, Event_Reference_Id__c NVARCHAR(100)
    , EventSubtype NVARCHAR(100)
    , GroupEventType NVARCHAR(100)
    , Id NVARCHAR(18)
    , IsAllDayEvent BIT
    , IsArchived BIT
    , IsChild BIT
    , IsDeleted BIT
    , IsGroupEvent BIT
    , IsPrivate BIT
    , IsRecurrence BIT
    , IsReminderSet BIT
    , LastModifiedById NVARCHAR(18)
    , LastModifiedDate DATETIME
    , Location NVARCHAR(255)
    , Missing_Information__c NVARCHAR(1300)
    , Outcome__c NVARCHAR(255)
    , OwnerId NVARCHAR(18)
    , Reason__c NVARCHAR(255)
    , RecurrenceActivityId NVARCHAR(18)
    , RecurrenceDayOfMonth INT
    , RecurrenceDayOfWeekMask INT
    , RecurrenceEndDateOnly DATE
    , RecurrenceInstance NVARCHAR(100)
    , RecurrenceInterval INT
    , RecurrenceMonthOfYear NVARCHAR(100)
    , RecurrenceStartDateTime DATETIME
    , RecurrenceTimeZoneSidKey NVARCHAR(100)
    , RecurrenceType NVARCHAR(100)
    , ReminderDateTime DATETIME
    , ShowAs NVARCHAR(100)
    , StartDateTime DATETIME
    , Subject NVARCHAR(255)
    , SystemModstamp DATETIME
    , Task_Type__c NVARCHAR(255)
    , Type NVARCHAR(100)
    , WhatCount INT
    , WhatId NVARCHAR(18)
    , WhoCount INT
    , WhoId NVARCHAR(18)
	, InsertDate datetime CONSTRAINT DF_Events_InsertDate DEFAULT GETDATE()

)
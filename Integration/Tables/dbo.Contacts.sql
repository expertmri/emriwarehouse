USE [Integration]
GO

ALTER TABLE [dbo].[Contacts] DROP CONSTRAINT [DF_Contacts_IsExported]
GO

ALTER TABLE [dbo].[Contacts] DROP CONSTRAINT [DF_Contacts_CreateDate]
GO

/****** Object:  Table [dbo].[Contacts]    Script Date: 12/18/2017 1:20:34 PM ******/
DROP TABLE [dbo].[Contacts]
GO

/****** Object:  Table [dbo].[Contacts]    Script Date: 12/18/2017 1:20:34 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Contacts](
	[ContactId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](75) NULL,
	[ContactName] [varchar](255) NULL,
	[JobTitle] [varchar](50) NULL,
	[Phone] [varchar](20) NULL,
	[Fax] [varchar](20) NULL,
	[Email] [varchar](255) NULL,
	[SourceSystemPk] [varchar](100) NULL,
	[SourceSystem] [varchar](20) NULL,
	[AccountId] [int] NULL,
	[AccountNumber] [varchar](100) NULL,
	[ContactType] [varchar](40) NULL,
	[ContactGroupCode] [varchar](100) NULL,
	SF_ContactId varchar(100),
	[CreateDate] [datetime] NULL,
	[IsExported] [bit] NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Contacts] ADD  CONSTRAINT [DF_Contacts_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO

ALTER TABLE [dbo].[Contacts] ADD  CONSTRAINT [DF_Contacts_IsExported]  DEFAULT ((0)) FOR [IsExported]
GO



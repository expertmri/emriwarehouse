USE [Integration]
GO
/****** Object:  Table [Outbound].[ScansByContact]    Script Date: 10/24/2017 9:07:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Outbound].[ScansByContact](
	[DOSMonthYear] [varchar](9) NULL,
	[DOSMonth] [int] NULL,
	[DOSYear] [int] NULL,
	[ARClass] [varchar](50) NULL,
	[ARGroup] [varchar](20) NULL,
	[ReferringDr] [varchar](150) NULL,
	[Modality] [varchar](10) NULL,
	[ExamCount] [int] NULL,
	[LatestExam] [varchar](10) NULL,
	[TrailingSixMos] [int] NULL,
	[TrailingTwelveMos] [int] NULL,
	[SF_ContactId] [varchar](255) NULL,
	[OwnerId] [varchar](25) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

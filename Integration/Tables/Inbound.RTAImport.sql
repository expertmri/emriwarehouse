USE Integration
GO

CREATE TABLE INBOUND.RTAImport (
	RTAImportId int IDENTITY(1,1)
	, StudyDate varchar(100)
	, ReceivedDate varchar(100)
	, TimeInSubmitted varchar(100)
	, TimeInVerified varchar(100)
	, TimeInDictated varchar(100)
	, TimeInPreliminary varchar(100)
	, TimeInFinal datetime
	, RA varchar(100)
	, PatientId varchar(100)
	, PatientName varchar(255)
	, Birthdate varchar(20)
	, Modality varchar(20)
	, StudyDescription varchar(255)
	, INumber int
	, ReportStatus varchar(50)
	, AccessionNumber varchar(25)
	, SiteName varchar(255)
	, RefPhysician varchar(255)
	, InsertDate datetime CONSTRAINT DF_RTAImport_InsertDate DEFAULT GETDATE()

)
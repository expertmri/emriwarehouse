USE [Integration]
GO

/****** Object:  Index [idx_AAppointmentsList_SequenceNo]    Script Date: 6/4/2019 9:22:44 PM ******/
DROP INDEX [idx_AAppointmentsList_SequenceNo] ON [Stg].[AppointmentsList]
GO

/****** Object:  Table [Stg].[AppointmentsList]    Script Date: 6/4/2019 9:22:44 PM ******/
DROP TABLE [Stg].[AppointmentsList]
GO

/****** Object:  Table [Stg].[AppointmentsList]    Script Date: 6/4/2019 9:22:44 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Stg].[AppointmentsList](
	[ID] [varchar](254) NOT NULL,
	[Entity] [int] NOT NULL,
	[Account] [varchar](255) NOT NULL,
	[PatName] [varchar](255) NOT NULL,
	[ListType] [varchar](100) NULL,
	[Resource] [varchar](255) NOT NULL,
	[Slot] [varchar](255) NOT NULL,
	[SubSlot] [varchar](255) NOT NULL,
	[Address1] [varchar](255) NULL,
	[Address2] [varchar](255) NULL,
	[City] [varchar](255) NULL,
	[State] [varchar](255) NULL,
	[Zip] [varchar](10) NULL,
	[ApptTime] [varchar](255) NULL,
	[ApptDuration] [varchar](255) NULL,
	[Resource1] [varchar](255) NULL,
	[Resource2] [varchar](255) NULL,
	[Resource3] [varchar](255) NULL,
	[Resource4] [varchar](255) NULL,
	[Resource5] [varchar](255) NULL,
	[ApptCode] [varchar](255) NULL,
	[ApptNote] [varchar](1500) NULL,
	[BCodeInfo] [varchar](255) NULL,
	[CancelCode] [varchar](255) NULL,
	[CancelUser] [varchar](255) NULL,
	[CaseId] [varchar](255) NULL,
	[CheckIn] [varchar](255) NULL,
	[DateCreated] [varchar](255) NULL,
	[DateModified] [varchar](255) NULL,
	[InsCode] [varchar](255) NULL,
	[InsPolicy] [varchar](255) NULL,
	[Location] [varchar](255) NULL,
	[NoShow] [varchar](255) NULL,
	[Override] [varchar](255) NULL,
	[PatDob] [varchar](255) NULL,
	[PatHomePhone] [varchar](255) NULL,
	[PatMedicalRecord] [varchar](255) NULL,
	[PatSSN] [varchar](255) NULL,
	[PatSex] [varchar](255) NULL,
	[PatWorkPhone] [varchar](255) NULL,
	[PatientEmail] [varchar](255) NULL,
	[PatientFax] [varchar](255) NULL,
	[PatientMobilePhone] [varchar](255) NULL,
	[PatientNote] [varchar](255) NULL,
	[PlaceCode] [varchar](255) NULL,
	[ProcCode] [varchar](255) NULL,
	[ProcDesc] [varchar](255) NULL,
	[RefCode] [varchar](255) NULL,
	[RefName] [varchar](255) NULL,
	[RefPhone] [varchar](255) NULL,
	[ReschCancelDate] [varchar](255) NULL,
	[SequenceNo] [varchar](255) NULL,
	[SqlDateCreated] [date] NULL,
	[SqlDateModified] [date] NULL,
	[SqlReschCancelDate] [date] NULL,
	[Status] [varchar](255) NULL,
	[TimeCreated] [varchar](255) NULL,
	[TimeModified] [varchar](255) NULL,
	[UserCreated] [varchar](255) NULL,
	[UserFlags] [varchar](255) NULL,
	[UserModified] [varchar](255) NULL
) ON [PRIMARY]
GO

SET ANSI_PADDING ON
GO

/****** Object:  Index [idx_AAppointmentsList_SequenceNo]    Script Date: 6/4/2019 9:22:44 PM ******/
CREATE NONCLUSTERED INDEX [idx_AAppointmentsList_SequenceNo] ON [Stg].[AppointmentsList]
(
	[SequenceNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



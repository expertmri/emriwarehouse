USE [Integration]
GO

ALTER TABLE [SF].[Tasks] DROP CONSTRAINT [DF_SFTasks_InsertDate]
GO

/****** Object:  Table [SF].[Tasks]    Script Date: 6/6/2019 2:39:21 PM ******/
DROP TABLE [SF].[Tasks]
GO

/****** Object:  Table [SF].[Tasks]    Script Date: 6/6/2019 2:39:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [SF].[Tasks](
	[AccountId] [nvarchar](18) NULL,
	[Activity_Status__c] [nvarchar](255) NULL,
	[Activity_Type__c] [nvarchar](255) NULL,
	[ActivityDate] [date] NULL,
	[CallDisposition] [nvarchar](255) NULL,
	[CallDurationInSeconds] [int] NULL,
	[CallObject] [nvarchar](255) NULL,
	[CallType] [nvarchar](40) NULL,
	Completed_by__c nvarchar(255),
	[CreatedById] [nvarchar](18) NULL,
	[CreatedDate] [datetime] NULL,
	[Date_Completed__c] [date] NULL,
	[Description] [nvarchar](max) NULL,
	[Id] [nvarchar](18) NULL,
	[IsArchived] [bit] NULL,
	[IsClosed] [bit] NULL,
	[IsDeleted] [bit] NULL,
	[IsHighPriority] [bit] NULL,
	[IsRecurrence] [bit] NULL,
	[IsReminderSet] [bit] NULL,
	[LastModifiedById] [nvarchar](18) NULL,
	[LastModifiedDate] [datetime] NULL,
	[Outcome__c] [nvarchar](255) NULL,
	[OwnerId] [nvarchar](18) NULL,
	[Priority] [nvarchar](40) NULL,
	[Reason__c] [nvarchar](255) NULL,
	[RecordTypeId] [nvarchar](18) NULL,
	[RecurrenceActivityId] [nvarchar](18) NULL,
	[RecurrenceDayOfMonth] [int] NULL,
	[RecurrenceDayOfWeekMask] [int] NULL,
	[RecurrenceEndDateOnly] [date] NULL,
	[RecurrenceInstance] [nvarchar](40) NULL,
	[RecurrenceInterval] [int] NULL,
	[RecurrenceMonthOfYear] [nvarchar](40) NULL,
	[RecurrenceRegeneratedType] [nvarchar](40) NULL,
	[RecurrenceStartDateOnly] [date] NULL,
	[RecurrenceTimeZoneSidKey] [nvarchar](40) NULL,
	[RecurrenceType] [nvarchar](40) NULL,
	[ReminderDateTime] [datetime] NULL,
	[Status] [nvarchar](40) NULL,
	[Subject] [nvarchar](255) NULL,
	[SystemModstamp] [datetime] NULL,
	[Task_Type__c] [nvarchar](255) NULL,
	[TaskSubtype] [nvarchar](40) NULL,
	[WhatCount] [int] NULL,
	[WhatId] [nvarchar](18) NULL,
	[WhoCount] [int] NULL,
	[WhoId] [nvarchar](18) NULL,
	[InsertDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [SF].[Tasks] ADD  CONSTRAINT [DF_SFTasks_InsertDate]  DEFAULT (getdate()) FOR [InsertDate]
GO



USE [Integration]
GO
/****** Object:  Table [SF].[Contacts]    Script Date: 10/24/2017 9:07:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [SF].[Contacts](
	[ID1] [int] NOT NULL,
	[ContactGroupCode] [nvarchar](255) NULL,
	[ContactId] [int] NULL,
	[AccountId] [int] NULL,
	[AccountNumber] [nvarchar](255) NULL,
	[Title] [nvarchar](255) NULL,
	[CLEAN_CONTACT_NAME] [nvarchar](255) NULL,
	[AccountName] [nvarchar](255) NULL,
	[Phone] [nvarchar](255) NULL,
	[Fax] [nvarchar](255) NULL,
	[Email] [nvarchar](255) NULL,
	[ContactType] [nvarchar](255) NULL,
	[Website] [nvarchar](255) NULL,
	[SourcePK] [nvarchar](255) NULL,
	[SourceSystem] [nvarchar](255) NULL,
	[ContactType2] [nvarchar](255) NULL,
	[RecordTypeId] [nvarchar](255) NULL,
	[SFAccountId] [nvarchar](255) NULL,
	[OwnerId] [nvarchar](255) NULL,
	[Surname] [nvarchar](255) NULL,
	[Firstname] [nvarchar](255) NULL,
	[MiddleName] [nvarchar](255) NULL,
	[Suffix] [nvarchar](255) NULL,
	[SFContactId] [nvarchar](255) NULL,
	[Active] [nvarchar](255) NULL
) ON [PRIMARY]

GO

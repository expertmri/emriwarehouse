USE [Integration]
GO
/****** Object:  Table [Outbound].[ScansByContactLastReferral]    Script Date: 10/24/2017 9:07:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Outbound].[ScansByContactLastReferral](
	
	[LatestReferral] [date] NULL,
	[SF_ContactId] [varchar](255) NULL,
	[OwnerId] [varchar](25) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

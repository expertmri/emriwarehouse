USE [Integration]
GO

/****** Object:  Table [OUTBOUND].[ScansByContactLastReferral]    Script Date: 3/12/2018 1:47:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [OUTBOUND].[ScansByAccountLastReferral](
	[LatestReferral] [date] NULL,
	[SF_AccountId] [varchar](255) NULL,
	[OwnerId] [varchar](25) NULL
) ON [PRIMARY]
GO



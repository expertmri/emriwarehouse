USE [Integration]
GO

ALTER TABLE [dbo].[Inbound.PhoneRecords] DROP CONSTRAINT [DF_PhoneRecordsInsertDate]
GO

/****** Object:  Table [dbo].[Inbound.PhoneRecords]    Script Date: 10/29/2018 3:47:04 PM ******/
DROP TABLE Inbound.PhoneRecords
GO

/****** Object:  Table [dbo].[Inbound.PhoneRecords]    Script Date: 10/29/2018 3:47:04 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE Inbound.PhoneRecords(
	[AcctNum] [varchar](255) NULL,
	[SvcType] [varchar](255) NULL,
	[SvcNum] [varchar](255) NULL,
	[CallDate] [varchar](255) NULL,
	[UsageTypeID] [varchar](255) NULL,
	[OrigCC] [varchar](255) NULL,
	[OrigNumber] [varchar](255) NULL,
	[OrigDesc] [varchar](255) NULL,
	[TermCC] [varchar](255) NULL,
	[TermNumber] [varchar](255) NULL,
	[TermDesc] [varchar](255) NULL,
	[DialedNoLRN] [varchar](255) NULL,
	[CallDur] [varchar](255) NULL,
	[RS_RatePlan] [varchar](255) NULL,
	[RS_BillDur] [varchar](255) NULL,
	[RS_Rate] [varchar](255) NULL,
	[RS_Surcharge] [varchar](255) NULL,
	[RS_Total] [varchar](255) NULL,
	[EU_RatePlan] [varchar](255) NULL,
	[EU_BillDur] [varchar](255) NULL,
	[EU_Rate] [varchar](255) NULL,
	[EU_Surcharge] [varchar](255) NULL,
	[EU_Total] [varchar](255) NULL,
	[InsertDate] [datetime] NULL
) ON [PRIMARY]
GO

ALTER TABLE Inbound.PhoneRecords ADD  CONSTRAINT [DF_PhoneRecordsInsertDate]  DEFAULT (getdate()) FOR [InsertDate]
GO



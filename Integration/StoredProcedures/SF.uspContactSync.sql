USE Integration
GO

ALTER PROCEDURE SF.uspContactSync
AS

BEGIN TRAN

UPDATE dest
SET SF_ContactId = src.Id
    , FirstName = src.FirstName
    , LastName = src.LastName
    , MiddleName = src.MiddleName
    , Salutation = src.Salutation
    , Suffix = src.Suffix
    , JobTitle = src.Title
    , Phone = src.Phone
    , Fax = src.Fax
    , Email = src.Email
    --, ADSKey = src.
    , ContactType = src.Contact_Type__c
	, ModifiedDate = GETDATE()
	, ModifiedUser = 'NightlySync'
FROM EMRIWarehouse.SF.Contacts dest
JOIN Integration.SF.Contact src ON src.Id = dest.SF_ContactId
WHERE src.Mark_for_Deletion__c = 0
	  --AND dest.KeyChecksum <> CHECKSUM(src.Name, src.Phone, src.Fax, src.Facility_Location__c, src.BillingStreet, src.BillingCity, src.BillingPostalCode, src.ParentId, src.Acct_Mgr_Active__c)
	  --Need to cast from nvarchar to varchar if we will use a checksum to compare rows...for now just update all to match since it is a DW and not OLTP and worried about change tracking


INSERT INTO EMRIWarehouse.SF.Contacts(
		SF_ContactId
      , FirstName
      , LastName
      , MiddleName
      , Salutation
      , Suffix
      , JobTitle
      , Phone
      , Fax
      , Email
      , ADSKey
      , ContactType
      , CreateDate
	)
SELECT 
		src.Id
		, src.FirstName
		, src.LastName
		, src.MiddleName
		, src.Salutation
		, src.Suffix
		, src.Title
		, src.Phone
		, src.Fax
		, src.Email
		, null
		, src.Contact_Type__c
		, GETDATE()
FROM Integration.SF.Contact src
LEFT JOIN EMRIWarehouse.SF.Contacts dest ON src.Id = dest.SF_ContactId
WHERE dest.ContactId is null
	  AND src.Mark_for_Deletion__c = 0
	  AND src.Source__c = 'RIS'

COMMIT


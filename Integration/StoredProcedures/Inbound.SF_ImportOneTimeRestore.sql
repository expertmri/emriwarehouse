USE [Integration]
GO

ALTER PROCEDURE Inbound.SF_ImportOneTimeRestore
AS
truncate table dbo.Contacts
truncate table dbo.Accounts

INSERT INTO dbo.Contacts (
		--ContactId
		Title
		, ContactName
		, JobTitle
		, Phone
		, Fax
		, Email
		, SourceSystemPk
		, SourceSystem
		, AccountId
		, AccountNumber
		, ContactType
		, ContactGroupCode
		, SF_ContactId
		, CreateDate
		, IsExported
  )
  
  SELECT c.Salutation
		 , LTRIM(RTRIM(c.LastName + ', ' + c.FirstName + ' ' + c.MiddleName))
		 , null
		 , c.Phone
		 , c.Fax
		 , c.Email
		 , c.PrimaryKey__C
		 , 'ADS'
		 , CAST(REPLACE(c.AccountNumber__C, 'A-', '') as int) AS AccountId
		 , c.AccountNumber__C
		 , c.Contact_Type__C
		 , c.ContactGroupCode__C
		 , Id
		 , c.CreatedDate
		 , 1 as IsExported
  FROM dbo.Contact_Inbound c

  SET IDENTITY_INSERT  dbo.Accounts ON

  INSERT INTO dbo.Accounts (
		AccountId
		, SourceSystemPK
		, SourceTable
		, AccountName
		, Phone
		, Fax
		, Website
		, Facility
		, Address1
		, Address2
		, City
		, StateName
		, Zip
		, SF_AccountId
		, IsExported
		, CreateDate
		--, AccountNumber  --computed
	)

SELECT	a.ADSAccountID__C
		, null --c.SourceSystemPk
		, null --c.ContactType
		, a.[Name] AS AccountName
		, a.Phone
		, a.Fax
		, null AS website
		, null AS facility
		, a.BillingStreet
		, null as Address2
		, a.BillingCity
		, a.BillingState
		, a.BillingPostalCode
		, Id
		, 1 as IsExported
		, a.CreatedDate
FROM dbo.account_INBOUND a
--JOIN dbo.Contacts c ON a.ADSAccountID__C = c.AccountId

SET IDENTITY_INSERT  dbo.Accounts OFF

update a
set SourceSystemPK = c.SourceSystemPK
	, SourceTable = c.ContactType
from dbo.Accounts a
join dbo.Contacts c ON a.AccountId = c.AccountId

update a
set SourceSystemPk = att.Id
	, SourceTable = 'Attorney'
from dbo.Accounts a
join Integration.stg.Attorneys att on a.AccountName = att.PracticeName and att.phone = a.phone
where SourceSystemPK is null

update a
set SourceSystemPk = att.Id
	, SourceTable = 'Attorney'
from dbo.Accounts a
join Integration.stg.Attorneys att on a.AccountName = att.PracticeName and left(att.address1,5) = left(a.address1,5)
where SourceSystemPK is null

update a
set SourceSystemPk = att.Id
	, SourceTable = 'Attorney'
from dbo.Accounts a
join Integration.stg.Attorneys att on  att.phone = a.phone and left(att.address1,5) = left(a.address1,5)
where SourceSystemPK is null

update a
set SourceSystemPk = r.Id
	, SourceTable = 'Physician'
from dbo.Accounts a
join Integration.stg.ReferringPhysicians r on a.phone = r.phone and left(r.address1,5) = left(a.address1,5) and a.fax = r.FaxNumber
where SourceSystemPK is null

--select * from dbo.Accounts a where SourceSystemPK is null

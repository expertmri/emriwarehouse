USE Integration
GO

ALTER PROCEDURE SF.uspAccountSync
AS

--For adding additional columns as needed
--ALTER TABLE EMRIWarehouse.SF.Accounts ADD LastActivityDate date

BEGIN TRAN

DELETE dest
FROM EMRIWarehouse.SF.Accounts dest
JOIN Integration.SF.Account src ON src.Id = dest.SF_AccountId
WHERE src.Mark_for_Deletion__c = 1

UPDATE dest
SET	dest.AccountName = src.Name
	, dest.Phone = src.Phone
	, dest.Fax = src.Fax
	--, dest.Website = src.Website
	, dest.Facility = src.Facility_Location__c
	, dest.Address1 = src.BillingStreet
	, dest.City = src.BillingCity
	, dest.StateName = src.BillingStateCode
	, dest.Zip = src.BillingPostalCode
	, dest.ParentId = src.ParentId
	, dest.AccountManager = src.Acct_Mgr_Active__c
	, dest.AccountHandler = src.Account_Handler__c
	, dest.CurrentHandler = src.Current_Handler__c
	, dest.SalesRepresentative = u.FirstName + ' ' + u.LastName
	, dest.ADSAccountId = src.ADSAccountId__c
	, dest.LastActivityDate = src.LastActivityDate
	, dest.ZoneRegion = src.Zone__c
	, ModifiedDate = GETDATE()
	, ModifiedUser = 'NightlySync'
FROM EMRIWarehouse.SF.Accounts dest
JOIN Integration.SF.Account src ON src.Id = dest.SF_AccountId
LEFT JOIN Integration.SF.Users u ON u.Id = src.SalesRepresentative
WHERE src.Mark_for_Deletion__c = 0
	  --AND dest.KeyChecksum <> CHECKSUM(src.Name, src.Phone, src.Fax, src.Facility_Location__c, src.BillingStreet, src.BillingCity, src.BillingPostalCode, src.ParentId, src.Acct_Mgr_Active__c)
	  --Need to cast from nvarchar to varchar if we will use a checksum to compare rows...for now just update all to match since it is a DW and not OLTP and worried about change tracking
	  
	  
INSERT INTO EMRIWarehouse.SF.Accounts (
		SF_AccountId
		, AccountName
		, Phone
		, Fax
		--, Website
		, Facility
		, Address1
		, City
		, StateName
		, Zip
		, ParentId
		, AccountManager
		, AccountHandler
		, CurrentHandler
		, SalesRepresentative
		, ZoneRegion
		, LastActivityDate
		, ADSAccountId
		, CreateDate
	)
SELECT 
			src.Id
			, src.Name
			, src.Phone
			, src.Fax
			--, src.Website
			, src.Facility_Location__c
			, src.BillingStreet
			, src.BillingCity
			, src.BillingStateCode
			, src.BillingPostalCode
			, src.ParentId
			, src.Acct_Mgr_Active__c
			, src.Account_Handler__c
			, src.Current_Handler__c
			, u.FirstName + ' ' + u.LastName
			, src.Zone__c
			, src.LastActivityDate
			, src.ADSAccountId__c
			, GETDATE()
FROM Integration.SF.Account src
LEFT JOIN Integration.SF.Users u ON u.Id = src.SalesRepresentative
LEFT JOIN EMRIWarehouse.SF.Accounts dest ON src.Id = dest.SF_AccountId
WHERE dest.AccountId is null
	  AND src.Mark_for_Deletion__c = 0
	  AND src.ADSAccountId__c is not null

COMMIT


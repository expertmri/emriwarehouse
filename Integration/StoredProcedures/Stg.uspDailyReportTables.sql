USE [Integration]
GO
/****** Object:  StoredProcedure [Stg].[uspDailyReportTables]    Script Date: 2/20/2018 1:52:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
*********************************************************************************************************
 Name:			Stg.uspDailyReportTables
 Date:			7/11/2017           
 Author:		Scott Lundy
 Description:	Temporary loading of data from ADS until a permanent solution is built
 Called by:
 Modified:
*********************************************************************************************************
*/

ALTER PROCEDURE [Stg].[uspDailyReportTables]
AS

BEGIN TRAN
	IF OBJECT_ID('Stg.Appointments', 'U') IS NOT NULL 
	  DROP TABLE Stg.Appointments; 

	SELECT	*
			, Checksum(*) AS RowCheckSum 
	INTO Stg.Appointments 
	FROM ADS..Adsc_MedicsPremier.Appointments
COMMIT

BEGIN TRAN
	IF OBJECT_ID('Stg.AptBookDates', 'U') IS NOT NULL 
	  DROP TABLE Stg.AptBookDates; 

	SELECT	*
			, Checksum(*) AS RowCheckSum 
	INTO Stg.AptBookDates 
	FROM ADS..Adsc_MedicsPremier.AptBookDates
COMMIT

BEGIN TRAN
	IF OBJECT_ID('Stg.Locations', 'U') IS NOT NULL 
	  DROP TABLE Stg.Locations; 

	SELECT	*
			, Checksum(*) AS RowCheckSum 
	INTO Stg.Locations 
	FROM ADS..Adsc_MedicsPremier.Locations
COMMIT
--IF OBJECT_ID('Stg.AppointmentsList', 'U') IS NOT NULL 
--  DROP TABLE Stg.AppointmentsList;

--SELECT *
--INTO Stg.AppointmentsList
--FROM ADS..Adsc_MedicsPremier.AppointmentsList
--WHERE Account <> '*'

BEGIN TRAN
	TRUNCATE TABLE Stg.AppointmentsList

	INSERT INTO Stg.AppointmentsList
		(
			ID
		   , Entity
		   , Account
		   , PatName
		   , ListType
		   , Resource
		   , Slot
		   , SubSlot
		   , Address1
		   , Address2
		   , City
		   , State
		   , Zip
		   , ApptTime
		   , ApptDuration
		   , Resource1
		   , Resource2
		   , Resource3
		   , Resource4
		   , Resource5
		   , ApptCode
		   , ApptNote
		   , BCodeInfo
		   , CancelCode
		   , CancelUser
		   , CaseId
		   , CheckIn
		   --,Comment
		   , DateCreated
		   , DateModified
		   , InsCode
		   , InsPolicy
		   , Location
		   , NoShow
		   , Override
		   , PatDob
		   , PatHomePhone
		   , PatMedicalRecord
		   , PatSSN
		   , PatSex
		   , PatWorkPhone
		   , PatientEmail
		   , PatientFax
		   , PatientMobilePhone
		   , PatientNote
		   , PlaceCode
		   , ProcCode
		   , ProcDesc
		   --,Procedures
		   , RefCode
		   , RefName
		   , RefPhone
		   , ReschCancelDate
		   , SequenceNo
		   , SqlDateCreated
		   , SqlDateModified
		   , SqlReschCancelDate
		   , Status
		   , TimeCreated
		   , TimeModified
		   , UserCreated
		   , UserFlags
		   , UserModified
		)

	SELECT ID
		   , Entity
		   , CAST(Account as int) as Account
		   , PatName
		   , ListType
		   , Resource
		   , Slot
		   , SubSlot
		   , Address1
		   , Address2
		   , City
		   , State
		   , Zip
		   , ApptTime
		   , ApptDuration
		   , Resource1
		   , Resource2
		   , Resource3
		   , Resource4
		   , Resource5
		   , ApptCode
		   , ApptNote
		   , BCodeInfo
		   , CancelCode
		   , CancelUser
		   , CaseId
		   , CheckIn
		   --,Comment
		   , DateCreated
		   , DateModified
		   , InsCode
		   , InsPolicy
		   , Location
		   , NoShow
		   , Override
		   , PatDob
		   , PatHomePhone
		   , PatMedicalRecord
		   , PatSSN
		   , PatSex
		   , PatWorkPhone
		   , PatientEmail
		   , PatientFax
		   , PatientMobilePhone
		   , PatientNote
		   , PlaceCode
		   , ProcCode
		   , ProcDesc
		   --,Procedures
		   , RefCode
		   , RefName
		   , RefPhone
		   , ReschCancelDate
		   , SequenceNo
		   , SqlDateCreated
		   , SqlDateModified
		   , SqlReschCancelDate
		   , Status
		   , TimeCreated
		   , TimeModified
		   , UserCreated
		   , UserFlags
		   , UserModified
	FROM ADS..Adsc_MedicsPremier.AppointmentsList
	WHERE Account <> '*'
COMMIT

BEGIN TRAN
	IF OBJECT_ID('Stg.Cases', 'U') IS NOT NULL 
	  DROP TABLE Stg.Cases;

	SELECT * 
	INTO Stg.Cases
	FROM ADS..Adsc_MedicsPremier.Cases
COMMIT
--TRUNCATE TABLE Stg.Cases

--INSERT INTO Stg.Cases
--	(	
--		ARClass
--		, Account
--		, AcctPatCase
--		, CaseAuthorization
--		, CaseNumber
--		, DateFrom
--		, DateThru
--		, Department
--		, Description
--		, Dx1
--		, Dx2
--		, Dx3
--		, Dx4
--		, Entity
--		, InsuranceOrder
--		, Modifier1
--		, Modifier2
--		, Operator
--		, Patient
--		, PlaceOfService
--		, PostFrom
--		, PostThru
--		, ProcedureCode
--		, Provider
--		, Quantity
--		, ReferringPhysician
--		, SqlDateFrom
--		, SqlDateThru
--		, Status
--	)

--SELECT	ARClass
--		, Account
--		, AcctPatCase
--		, CaseAuthorization
--		, CaseNumber
--		, DateFrom
--		, DateThru
--		, Department
--		, Description
--		, Dx1
--		, Dx2
--		, Dx3
--		, Dx4
--		, Entity
--		, InsuranceOrder
--		, Modifier1
--		, Modifier2
--		, Operator
--		, Patient
--		, PlaceOfService
--		, PostFrom
--		, PostThru
--		, ProcedureCode
--		, Provider
--		, Quantity
--		, ReferringPhysician
--		, SqlDateFrom
--		, SqlDateThru
--		, Status
--FROM ADS..Adsc_MedicsPremier.Cases

BEGIN TRAN
	IF OBJECT_ID('Stg.Patients', 'U') IS NOT NULL 
	  DROP TABLE Stg.Patients;

	--SELECT *
	--INTO Stg.Patients
	--FROM ADS..Adsc_MedicsPremier.Patients

SELECT  ID
      , CAST(Account as varchar(50)) AS Account
      , Patient
      , Name
      , Address1
      , Address2
      , ZipCode
      , HomePhone
      , Ssn
      , Sex
      , MedicalRecord
      , Dob
      , ProviderCode
      , AdmissionDate
      , DischargeDate
      , ReferringDocCode
      , Diag1Code
      , Note
      , ReminderCode
      , Reserved
      , Relationship
      , LastCharge
      , PatInsOrd
      , Notes1
      , Notes2
      , RecallInfo
      , PatPolicy
      , Location
      , DateCreated
      , EntityAllowed
      , Ethnicity
      , PrefLanguage
      , Race
      , TimeCreated
INTO Stg.Patients
FROM ADS..Adsc_MedicsPremier.Patients

COMMIT

BEGIN TRAN
	IF OBJECT_ID('Stg.Attorneys', 'U') IS NOT NULL 
	  DROP TABLE Stg.Attorneys;

	SELECT *
	INTO Stg.Attorneys
	FROM ADS..adsc_medicsPremier.Attorneys
COMMIT

BEGIN TRAN
	IF OBJECT_ID('Stg.ReferringPhysicians', 'U') IS NOT NULL 
	  DROP TABLE Stg.ReferringPhysicians;

	SELECT	ID
			, Code
			, Name
			, Entity
			, Address1
			, Address2 
			, City
			, State	
			, ZipCode	
			, Phone
			, ReferralID
			, SpecialFldA
			, SpecialFldB
			, SpecialFldC
			, LastReferralDate
			, OtherInfo
			, MedicareNumber
			, MedicaidNumber
			, BlueShieldNumber
			, SpecialtyCode
			, LabelName
			, GroupName
			, FaxNumber
			--, AutoFax
			, Email
			--, AutoEmail
			, MarkedAsDeleted
			, SalesmanCode
			, NPI
			, Password
			, Taxonomy
			, TaxId
	--Notes 
	INTO Stg.ReferringPhysicians
	FROM ADS..[Adsc_MedicsPremier].ReferringPhysicians
COMMIT

BEGIN TRAN
	IF OBJECT_ID('Stg.Referrals', 'U') IS NOT NULL 
	  DROP TABLE Stg.Referrals;

	SELECT  ID
			, Code	
			, Name	
			, Entity	
			, Address1	
			, Address2	
			, City	
			, State	
			, ZipCode	
			, Phone
			, LastReferralDate
			, GroupName	
			, FaxNumber
			, NPI
	INTO Stg.Referrals
	FROM ADS..Adsc_MedicsPremier.Referrals
COMMIT

BEGIN TRAN
	IF OBJECT_ID('Stg.Insurances', 'U') IS NOT NULL 
	  DROP TABLE Stg.Insurances ;

	SELECT * 
	INTO Stg.Insurances 
	FROM ADS..Adsc_MedicsPremier.Insurances
COMMIT

BEGIN TRAN
	IF OBJECT_ID('Stg.Charges', 'U') IS NOT NULL 
	  DROP TABLE Stg.Charges ;

	SELECT * 
	INTO Stg.Charges 
	FROM ADS..Adsc_MedicsPremier.Charges
	WHERE AutoPost IS NOT NULL -----MUST GET THIS FIELD ADDED BACK
COMMIT

BEGIN TRAN
	IF OBJECT_ID('Stg.Credits', 'U') IS NOT NULL 
	  DROP TABLE Stg.Credits ;

	SELECT * 
	INTO Stg.Credits
	FROM ADS..Adsc_MedicsPremier.Credits
COMMIT

BEGIN TRAN
	IF OBJECT_ID('Stg.Guarantors', 'U') IS NOT NULL 
	  DROP TABLE Stg.Guarantors ;

	SELECT * 
	INTO Stg.Guarantors
	FROM ADS..Adsc_MedicsPremier.Guarantors
COMMIT

BEGIN TRAN
	IF OBJECT_ID('Stg.RecallReasons', 'U') IS NOT NULL 
	  DROP TABLE Stg.RecallReasons ;

	SELECT * 
	INTO Stg.RecallReasons
	FROM ADS..Adsc_MedicsPremier.RecallReasons
COMMIT

BEGIN TRAN
	IF OBJECT_ID('Stg.Operators', 'U') IS NOT NULL 
	  DROP TABLE Stg.Operators ;

	SELECT Address
			, City
			, Code
			, Entities
			, LastLogon
			, CASE WHEN Code = 'SRG' THEN 'SANDRA GARCIA' ELSE Name  END AS Name
			--, Password
			, Phone
			, SecurityGroup
			, SpA
			, SpB
			, SpC
			, State
			, Zip
	INTO Stg.Operators
	FROM ADS..Adsc_MedicsPremier.Operators
COMMIT

BEGIN TRAN
	IF OBJECT_ID('Stg.AppointmentType', 'U') IS NOT NULL 
	  DROP TABLE Stg.AppointmentType ;

	SELECT * 
	INTO Stg.AppointmentType
	FROM ads..[Adsc_MedicsPremier].[AppointmentType]
	WHERE AppTypeCode <> 'NEW'
COMMIT

BEGIN TRAN
	IF OBJECT_ID('Stg.AppointmentCancelCodes', 'U') IS NOT NULL 
	  DROP TABLE Stg.AppointmentCancelCodes ;

	SELECT *
	INTO Stg.AppointmentCancelCodes
	FROM ADS..Adsc_MedicsPremier.AppointmentCancelCodes
COMMIT

BEGIN TRAN
	IF OBJECT_ID('Stg.GuarantorInsurances', 'U') IS NOT NULL 
	  DROP TABLE Stg.GuarantorInsurances ;

	SELECT * 
	INTO Stg.GuarantorInsurances
	FROM ads..[Adsc_MedicsPremier].[GuarantorInsurances]
COMMIT

BEGIN TRAN
	IF OBJECT_ID('Stg.GuarantorInsuranceOrder', 'U') IS NOT NULL 
	  DROP TABLE Stg.GuarantorInsuranceOrder ;

	SELECT * 
	INTO Stg.GuarantorInsuranceOrder
	FROM ads..[Adsc_MedicsPremier].GuarantorInsuranceOrder
COMMIT


BEGIN TRAN
	IF OBJECT_ID('Stg.FinancialCodes', 'U') IS NOT NULL 
	  DROP TABLE Stg.FinancialCodes ;

	SELECT * 
	INTO Stg.FinancialCodes
	FROM ads..[Adsc_MedicsPremier].[FinancialCodes]
COMMIT

BEGIN TRAN
	IF OBJECT_ID('Stg.CollectionCase', 'U') IS NOT NULL 
	  DROP TABLE Stg.CollectionCase ;
	
	SELECT *
	INTO Stg.CollectionCase
	FROM [ADS]..[Adsc_MedicsPremier].[userform] 
	WHERE userformname = 'picst'
COMMIT

BEGIN TRAN
	IF OBJECT_ID('Stg.UserFormDefinition', 'U') IS NOT NULL 
	  DROP TABLE Stg.UserFormDefinition ;

	SELECT *
	INTO Stg.UserFormDefinition
	FROM [ADS]..[Adsc_MedicsPremier].[UserFormDefinition]

COMMIT

BEGIN TRAN
	IF OBJECT_ID('Stg.Study', 'U') IS NOT NULL 
	  DROP TABLE Stg.Study; 

	SELECT	*
	INTO Stg.Study
	FROM ADS..Adsc_MedicsPremier.Study
COMMIT

BEGIN TRAN
	IF OBJECT_ID('Stg.TransactionMessages', 'U') IS NOT NULL 
	  DROP TABLE Stg.TransactionMessages; 

	SELECT	*
	INTO Stg.TransactionMessages 
	FROM ADS..Adsc_MedicsPremier.TransactionMessages
COMMIT

BEGIN TRAN
	IF OBJECT_ID('Stg.Payments', 'U') IS NOT NULL 
	  DROP TABLE Stg.Payments; 

	SELECT	p.TransactionNo
			, p.TransNo1
			, p.PostingDate
			, p.Operator
			, p.ClassOfCode
			, p.CheckNo
	INTO Stg.Payments 
	FROM ADS..Adsc_MedicsPremier.Payments p
COMMIT

BEGIN TRAN
	TRUNCATE TABLE Stg.DebitAdj;
	INSERT INTO Stg.DebitAdj
	SELECT	ID
			, TransactionNo
			, PostingDate
			, Entity
			, AccountNo
			, PatientNo
			, Type
			, Department
			, InverseFinanceKey
			, Reference
			, TransactionAmount
			, UndistributedAmount
			, NoTimesLeftToPrint
			, NoTimesPrinted
			, Provider
			, Referral
			, POS
			, ClassOfCode
			, Operator
			, Message
			, TransactionDate
			, MarkedAsDeleted
			, CheckNo
			, Amount1
			, Amount2
			, Amount3
			, Amount4
			, Amount5
			, Amount6
			, TransNo1
			, TransNo2
			, TransNo3
			, TransNo4
			, TransNo5
			, TransNo6
			, TransactionSqlDate
	FROM ADS..Adsc_MedicsPremier.debitadj
COMMIT

BEGIN TRAN
	IF OBJECT_ID('Stg.CreditAdj', 'U') IS NOT NULL 
	  DROP TABLE Stg.CreditAdj; 

	SELECT TransactionNo
			, ClassOfCode
			, TransactionAmount
	INTO Stg.CreditAdj
	FROM ADS..Adsc_MedicsPremier.creditadj
COMMIT

BEGIN TRAN
	IF OBJECT_ID('Stg.Providers', 'U') IS NOT NULL 
	  DROP TABLE Stg.Providers; 

	SELECT *
	INTO Stg.Providers
	FROM ADS..Adsc_MedicsPremier.Providers
COMMIT

BEGIN TRAN
	IF OBJECT_ID('Stg.UserForm', 'U') IS NOT NULL 
	  DROP TABLE Stg.UserForm; 

	SELECT 	Account
			, Patient
			, Field1 
			, Field2 
			, Field3 
			, Field4 
			, Field5 
			, Field6 
			, Field7 
			, Field8 
			, Field9 
			, Field10
			, Field11
			, Field12
			, Field13
			, Field14
			, Field15
			, Field16
			, Field17
			, Field18
			, Field19
			, Field20
			, Field21
			, Field22
			, Field23
			, Field24
			, Field25
			, Field26
			, Field27
			, Field28
			, Field29
			, Field30
			, Field84
			, Field90
			, Field99
			, UserFormName
	INTO Stg.UserForm
	FROM [ADS]..[Adsc_MedicsPremier].[userform]
	WHERE [UserFormName] in ('NOTES', 'COLLNOT', 'PICST', 'STLMT')

COMMIT


UPDATE EMRIWarehouse.dbo.LastRefresh
SET LastRefresh = GETDATE()


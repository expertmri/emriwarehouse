USE Integration
GO

ALTER PROCEDURE OUTBOUND.uspScansByAccount

AS

SELECT DOSMonthYear
        , DOSMonth
        , DOSYear
        , ARGroup
        , ReferringDr
        , Modality
        , ExamCount
        , ThreeMonthAvg
		, LastMonth
		, OwnerId
		, SF_AccountId
		, SF_AccountId + '_' + CAST(DOSYear AS varchar) + '_' + CAST(DOSMonth AS varchar) + '_' + ARGroup + '_' +  Modality AS ExtKey
FROM OUTBOUND.ScansByAccount
USE [Integration]
GO
/****** Object:  StoredProcedure [Stg].[uspKillLongRunningRefresh]    Script Date: 3/24/2018 7:19:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE Stg.uspKillLongRunningRefresh
@jobId uniqueidentifier
AS

DECLARE @jobStartTime datetime
		, @jobRunTime int

--SET @jobId = 'DF94437E-D1D5-495B-AC35-64F12FFDB138' --Integration Nightly Refresh
--SET @jobId = 'DA1880EF-CD1A-4507-9D7D-6CD66D26C302' --Integration Afternoon Refresh

SET @jobStartTime =	(
						SELECT MAX(sja.run_requested_date)
						FROM msdb.dbo.sysjobs_view sjv
						JOIN msdb.dbo.sysjobactivity sja ON sjv.job_id = sja.job_id
						WHERE sja.run_requested_date is not null
								AND sja.stop_execution_date is null
								AND @jobId = sjv.job_id
					)


SET @jobRunTime = (SELECT DATEDIFF(MINUTE, @jobStartTime, GETDATE()))

IF @jobRunTime > 6 --If nightly refresh runs more than 4 minutes kill it

BEGIN

	DECLARE @body NVARCHAR(MAX)
			, @subject1 NVARCHAR(255)
			, @emailTo NVARCHAR(255)

	SET @emailTo = 'saba.ejaz@expertmri.com; scott.lundy@expertmri.com'
	SET @subject1 = 'ADS Refresh Failed'
	SET @body = 'The refresh from ADS failed to complete and was killed by a separate process. No further action is needed and you can manually rerun the job from SQL Server Agent.'

	--Kill the job
	EXEC msdb.dbo.sp_stop_job @job_id = @jobId

	--Update the job tracking to show it completed so it doesn't get picked up by the process again
	UPDATE sja
	SET stop_execution_date = GETDATE()
	FROM msdb.dbo.sysjobs_view sjv
	JOIN msdb.dbo.sysjobactivity sja ON sjv.job_id = sja.job_id
	WHERE sja.run_requested_date is not null
			AND sja.stop_execution_date is null
			AND @jobId = sjv.job_id

	--Send notification email
	EXEC msdb.dbo.sp_send_dbmail  
	 @recipients = @emailTo
	, @subject = @subject1
	, @body = @body

END

USE [Integration]
GO
/****** Object:  StoredProcedure [OUTBOUND].[uspAccountCategorizationWorkflow]    Script Date: 8/20/2019 1:16:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [OUTBOUND].[uspAccountCategorizationWorkflow] 
@SFAccountId varchar(255) = NULL

AS

TRUNCATE TABLE OUTBOUND.AccountCategorizationWorkflow

EXEC EMRIWarehouse.Rpt.SSRSScansByAccountWorkflow @ExportToSF = 1, @SFAccountId = @SFAccountId


ALTER schema zzz transfer [OUTBOUND].[uspAccountCategorizationWorkflow] 
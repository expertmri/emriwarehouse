USE [Integration]
GO
/****** Object:  StoredProcedure [SF].[ScansBycontactExport]    Script Date: 1/22/2018 12:41:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE SF.uspScansByAccountExport 
	@SFAccountId varchar(255) = null
AS

TRUNCATE TABLE OUTBOUND.ScansByAccount

DECLARE @OwnerId varchar(25) = '005f4000000e552'
		--, @SFAccountId varchar(255) = null

INSERT INTO OUTBOUND.ScansByAccount (
		DOSMonthYear
        , DOSMonth
        , DOSYear
        , ARGroup
        , ReferringDr
        , Modality
        , ExamCount
        , ThreeMonthAvg
		, LastMonth
		, OwnerId
		, SF_AccountId
	)

SELECT	CAST(DATEPART(MONTH, r.DOS) AS varchar(2)) + '/1/'+ CAST(DATEPART(YEAR, r.DOS) AS varchar(4)) AS DOSMonthYear
		, DATEPART(MONTH, r.DOS) AS DOSMonth
		, DATEPART(YEAR, r.DOS) AS DOSYear
		, ar.ARGroup
		, '' AS ReferringDr --Legacy layout still being used, do not send this data since we don't have clean names, let SF handle the names
		, r.Modality
		, COUNT(COALESCE (r.ApptId, r.Account)) as ExamCount
		, 0 AS ThreeMonthAvg --future need possibly
		, 0 AS LastMonth --future need possibly
		, @OwnerId
		, c.SF_AccountId AS SF_AccountId
FROM EMRIWarehouse.dbo.Appointments r
LEFT JOIN Integration.dbo.Accounts c ON c.SourceSystemPk = r.RefAttyCode and c.SourceTable = 'Attorney'
JOIN (
		SELECT	c.SF_AccountId AS SF_AccountId
				, MAX(DOS) AS LatestExam
		FROM EMRIWarehouse.dbo.Appointments appt
		LEFT JOIN Integration.dbo.Accounts c ON c.SourceSystemPk = appt.RefAttyCode and c.SourceTable = 'Attorney'
		GROUP BY c.SF_AccountId
	  ) lastexam ON c.SF_AccountId = lastexam.SF_AccountId
JOIN EMRIWarehouse.XRef.ARClassGroup ar ON r.ARClass = ar.ARClass
WHERE AppointmentStatus = 'Exam Complete'
	  AND COALESCE(c.SF_AccountId, '') <> ''
	  AND (@SFAccountId = c.SF_AccountId OR @SFAccountId IS NULL)
	  AND r.Account <> '10021'
	  AND ar.IsNonRevenue = 0 --Added 11/11/2019
GROUP BY CAST(DATEPART(MONTH, r.DOS) AS varchar(2)) + '/1/'+ CAST(DATEPART(YEAR, r.DOS) AS varchar(4))
		 , DATEPART(MONTH, r.DOS)  
		 , DATEPART(YEAR, r.DOS) 
		 , ar.ARGroup
		 , r.Modality
		 , c.SF_AccountId
UNION ALL
SELECT	CAST(DATEPART(MONTH, r.DOS) AS varchar(2)) + '/1/'+ CAST(DATEPART(YEAR, r.DOS) AS varchar(4)) AS DOSMonthYear
		, DATEPART(MONTH, r.DOS) AS DOSMonth
		, DATEPART(YEAR, r.DOS) AS DOSYear
		, ar.ARGroup
		, '' AS ReferringDr --Legacy layout still being used, do not send this data since we don't have clean names, let SF handle the names
		, r.Modality
		, COUNT(COALESCE (r.ApptId, r.Account)) as ExamCount
		, 0 AS ThreeMonthAvg --future need possibly
		, 0 AS LastMonth --future need possibly
		, @OwnerId
		, a.SF_AccountId AS SF_AccountId
FROM EMRIWarehouse.dbo.Appointments r
LEFT JOIN Integration.dbo.Accounts a ON a.SourceSystemPk = r.ReferringDrId and a.SourceTable = 'Doctor'
JOIN (
		SELECT	a.SF_AccountId AS SF_AccountId
				, MAX(DOS) AS LatestExam
		FROM EMRIWarehouse.dbo.Appointments appt
		LEFT JOIN Integration.dbo.Accounts c ON c.SourceSystemPk = appt.RefAttyCode and c.SourceTable = 'Attorney'
		LEFT JOIN Integration.dbo.Accounts a ON a.SourceSystemPk = appt.ReferringDrId and a.SourceTable = 'Doctor'
		GROUP BY a.SF_AccountId
	  ) lastexam ON a.SF_AccountId = lastexam.SF_AccountId
JOIN EMRIWarehouse.XRef.ARClassGroup ar ON r.ARClass = ar.ARClass
WHERE AppointmentStatus = 'Exam Complete'
	  AND COALESCE(a.SF_AccountId, '') <> ''
	  AND (@SFAccountId = a.SF_AccountId OR @SFAccountId IS NULL)
	  AND r.Account <> '10021'
	  AND ar.IsNonRevenue = 0 --Added 11/11/2019
GROUP BY CAST(DATEPART(MONTH, r.DOS) AS varchar(2)) + '/1/'+ CAST(DATEPART(YEAR, r.DOS) AS varchar(4))
		 , DATEPART(MONTH, r.DOS)  
		 , DATEPART(YEAR, r.DOS) 
		 , ar.ARGroup
		 , r.Modality
		 , a.SF_AccountId

/**** Not sending this information like we do for the Contacts SLUNDY 8/13/2018

INSERT INTO OUTBOUND.ScansByAccountLastReferral (
		LatestReferral
		, SF_AccountId
		, OwnerId

	)

SELECT DISTINCT LatestExam
				, SF_AccountId
				, @OwnerId
FROM OUTBOUND.ScansByAccount


UPDATE sc
SET TrailingSixMos = COALESCE(tt.TrailingSixMos, 0)/6
FROM OUTBOUND.ScansByAccount sc
OUTER APPLY (
				SELECT SUM(ExamCount) AS TrailingSixMos
						, SF_AccountId
				FROM OUTBOUND.ScansByAccount sc2
				WHERE sc2.DOSMonthYear >= CAST(DATEADD(MONTH, -7, sc.DOSMonthYear) as date) 
					AND sc2.DOSMonthYear < sc.DOSMonthYear
					AND sc.SF_AccountId = sc2.SF_AccountId
					AND sc.ARGroup = sc2.ARGroup
					AND sc.Modality = sc2.Modality
				GROUP BY SF_AccountId
			)tt

UPDATE sc
SET TrailingTwelveMos = COALESCE(tt.TrailingTwelveMos, 0)
FROM OUTBOUND.ScansByAccount sc
OUTER APPLY (
				SELECT SUM(ExamCount) AS TrailingTwelveMos
						, SF_AccountId
				FROM OUTBOUND.ScansByAccount sc2
				WHERE sc2.DOSMonthYear >= CAST(DATEADD(MONTH, -13, sc.DOSMonthYear) as date) 
					AND sc2.DOSMonthYear < sc.DOSMonthYear
					AND sc.SF_AccountId = sc2.SF_AccountId
					AND sc.ARGroup = sc2.ARGroup
					AND sc.Modality = sc2.Modality
				GROUP BY SF_AccountId
			)tt

****/
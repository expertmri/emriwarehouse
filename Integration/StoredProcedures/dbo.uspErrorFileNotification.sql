USE INTEGRATION
GO

CREATE PROCEDURE dbo.uspErrorFileNotification
	@ErrorFiles varchar(max)
	, @JobName varchar(max)
AS

DECLARE @body NVARCHAR(MAX)
, @subject1 NVARCHAR(255)
, @emailTo NVARCHAR(255)

SET @emailTo = 'scott.lundy@expertmri.net'
SET @subject1 = @JobName + ' File Errors ' + CONVERT(varchar,GETDATE(), 101)
SET @body = @ErrorFiles



EXEC msdb.dbo.sp_send_dbmail  
 @recipients = @emailTo
, @subject = @subject1
, @body = @body

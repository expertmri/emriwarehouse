USE Integration
GO

ALTER PROCEDURE dbo.uspRTAReportMetrics
AS

BEGIN TRAN

	DECLARE @nullData varchar(100) = '0000-00-00 00:00:00';

	TRUNCATE TABLE EMRIWarehouse.dbo.RTAReportMetrics 

	--UPDATE Integration.INBOUND.RTAImport 
	--SET StudyDate = CASE WHEN StudyDate = @nullData THEN null ELSE StudyDate END
	--	, ReceivedDate = CASE WHEN ReceivedDate = @nullData THEN null ELSE ReceivedDate END
	--	, TimeInSubmitted = CASE WHEN TimeInSubmitted = @nullData THEN null ELSE TimeInSubmitted END
	--	, TimeInVerified = CASE WHEN TimeInVerified = @nullData THEN null ELSE TimeInVerified END
	--	, TimeInDicated = CASE WHEN TimeInDicated = @nullData THEN null ELSE TimeInDicated END
	--	, TimeInPreliminary	= CASE WHEN TimeInPreliminary = @nullData THEN null ELSE TimeInPreliminary END
	--	, TimeInFinal = CASE WHEN TimeInFinal = @nullData THEN null ELSE TimeInFinal END
	
	INSERT INTO EMRIWarehouse.dbo.RTAReportMetrics (
		StudyDate
		, ReceivedDate
		, TimeInSubmitted
		, TimeInVerified
		, TimeInDictated
		, TimeInPreliminary
		, TimeInFinal
		, RA
		, PatientId
		, Modality
		, ReportStatus
		, AccessionNumber
		, SiteName
		, RefPhysician
		)

	SELECT
		CASE WHEN StudyDate = @nullData THEN null ELSE StudyDate END AS StudyDate
		, CASE WHEN ReceivedDate = @nullData THEN null ELSE ReceivedDate END AS ReceivedDate
		, CASE WHEN TimeInSubmitted = @nullData THEN null ELSE TimeInSubmitted END AS TimeInSubmitted
		, CASE WHEN TimeInVerified = @nullData THEN null ELSE TimeInVerified END AS TimeInVerified
		, CASE WHEN TimeInDictated = @nullData THEN null ELSE TimeInDictated END AS TimeInDictated
		, CASE WHEN TimeInPreliminary = @nullData THEN null ELSE TimeInPreliminary END AS TimeInPreliminary
		, CASE WHEN TimeInFinal = @nullData THEN null ELSE TimeInFinal END AS TimeInFinal
		, RA
		, PatientId
		, Modality
		, ReportStatus
		, AccessionNumber
		, SiteName
		, RefPhysician
	FROM Integration.INBOUND.RTAImport 

	TRUNCATE TABLE Integration.INBOUND.RTAImport 

COMMIT;


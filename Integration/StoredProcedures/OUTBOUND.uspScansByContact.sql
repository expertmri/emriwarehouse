USE Integration
GO

ALTER PROCEDURE OUTBOUND.uspScansByContact

AS

SELECT DOSMonthYear
        , DOSMonth
        , DOSYear
        , ARGroup
        , ReferringDr
        , Modality
        , ExamCount
        , TrailingSixMos
        , TrailingTwelveMos
        , SF_ContactId
        , OwnerId
		, SF_ContactId + '_' + CAST(DOSYear AS varchar) + '_' + CAST(DOSMonth AS varchar) + '_' + ARGroup + '_' +  Modality AS ExtKey
FROM OUTBOUND.ScansByContact
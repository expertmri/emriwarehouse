 USE Integration
GO

ALTER PROCEDURE OUTBOUND.uspReferralTextMessages
AS

DECLARE @currHour int = (SELECT DATEPART(hh, GETDATE()) )

IF @currHour BETWEEN 11 AND 17

BEGIN
	
	DECLARE @textGroup varchar(255) =	'8186440046@vtext.com;' --Sattar Mir        
										+ '7147263878@tmomail.net;' --Iris Arzate         
										+ '6617553135@mms.att.net;' --Leesa Olsson     
										+ '5626886432@messaging.sprintpcs.com;' --Mary Nava
										+ '3235481480@sms.myboostmobile.com;' --Sandra Garcia
										+ '7148182229@tmomail.net;' --Saba Ejaz
										+ '5622666146@vtext.com;' --James Stannard
										--+ '9497358216@mms.att.net;' --Scott Lundy
										+ 'saba.ejaz@expertmri.com;' --saba email
									


	DECLARE @totalRefs varchar(10) = (
										SELECT CAST(COUNT(a.ApptId) as varchar(10)) 
										FROM EMRIWarehouse.dbo.Appointments a 
										WHERE a.BookedDate = CAST(GETDATE() AS date) 
												AND a.Modality = 'MRI'
												AND a.AppointmentStatusId <> 7 --cancelled
									)
	DECLARE @PIRefs varchar(10) = (		
									SELECT CAST(COUNT(a.ApptId) AS varchar(10)) AS PICount
									FROM EMRIWarehouse.dbo.Appointments a 
									JOIN EMRIWarehouse.XRef.ARClassGroup ar ON a.ARClass = ar.ARClass
									WHERE a.BookedDate = CAST(GETDATE() AS date) 
										  AND ar.IsPersonalInjury = 1
										  AND a.Modality = 'MRI'
										  AND a.AppointmentStatusId <> 7 --cancelled
								)


	DECLARE @textBody varchar(255) = (SELECT 'Created Today: ' + @totalRefs + CHAR(13)+CHAR(10) + 'PI Referrals: ' + @PIRefs)

	EXEC msdb.dbo.sp_send_dbmail 
		@recipients = @textGroup,
		@subject = "Total Referrals",
		@body = @textBody
END
GO
USE [Integration]
GO
/****** Object:  StoredProcedure [SF].[uspAccountContactExport]    Script Date: 10/24/2017 9:07:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [SF].[uspAccountContactExport]
AS

BEGIN TRAN

	SELECT  ID AS ADSId
			, PracticeName AS AccountName
			, PracticeName AS ContactName
			, Phone
			, Fax
			, Email
			--, Website
			, Address1
			, Address2
			, City
			, State
			, ZipCode
			, 'Attorney' AS SourceTable
	INTO #Accounts
	FROM Stg.Attorneys
	UNION ALL  
	SELECT  ID AS ADSId
			, GroupName AS AccountName
			, LabelName AS ContactName
			, Phone
			, FaxNumber as Fax
			, null as Email
			--, Website
			, Address1
			, Address2
			, City
			, State
			, ZipCode
			, 'Doctor' AS SourceTable
	FROM Stg.ReferringPhysicians

	INSERT INTO dbo.Accounts (
			SourceSystemPk
			, SourceTable
			, AccountName
			, Phone
			, Fax
			, Website
			, Facility
			, Address1
			, Address2
			, City
			, StateName
			, Zip
		)

	SELECT  a.ADSId
			, a.SourceTable
			, a.AccountName
			, a.Phone
			, a.Fax
			, NULL AS Website
			, NULL AS Facility
			, a.Address1
			, a.Address2
			, a.City
			, COALESCE(sc.StateName, a.State)
			, a.ZipCode
	FROM #Accounts a
	LEFT JOIN EMRIWarehouse.XRef.StateCodes sc on a.State = sc.StateCode
	LEFT JOIN dbo.Accounts acct ON acct.SourceSystemPk = a.ADSId AND acct.SourceTable = a.SourceTable
	WHERE acct.AccountId IS NULL

	INSERT INTO dbo.Contacts (
			Title
			, ContactName
			, JobTitle
			, Phone
			, Fax
			, Email
			, SourceSystemPK
			, AccountId
			, AccountNumber
			, ContactType
		)

	SELECT DISTINCT CASE WHEN a.SourceTable = 'Doctor' THEN 'MD'
						WHEN a.SourceTable = 'Attorney' THEN 'Mr/Mrs'
					END AS Title
		   , a.ContactName
		   , NULL AS JobTitle
		   , a.Phone
		   , a.Fax
		   , a.Email
		   , a.ADSId
		   , acct.AccountId
		   , acct.AccountNumber
		   , a.SourceTable AS ContactType
	FROM dbo.Accounts acct
	JOIN #Accounts a ON acct.SourceSystemPK = a.ADSId AND acct.SourceTable = a.SourceTable
	LEFT JOIN dbo.Contacts c ON c.SourceSystemPk = a.ADSId AND c.ContactType = a.SourceTable
	WHERE c.ContactId IS NULL

COMMIT
GO

USE Integration
GO
/****** Object:  StoredProcedure [Rpt].[SSRSScansByAccountWorkflow]    Script Date: 7/25/2018 12:07:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE OUTBOUND.uspScansByAccountWorkflow 
@SFAccountId varchar(255) = null

AS

SET NOCOUNT ON

/*********************************NEED TO MOVE THIS LOGIC TO EMRIWarehouse.dbo.uspScansByAccountWorkflow*************************/

DECLARE @FirstDayLastMonth date = (SELECT EMRIWarehouse.dbo.udfFirstDayLastMonth(GETDATE()))
		, @FirstOfMonth date = (SELECT EMRIWarehouse.dbo.udfFirstDayOfMonth(GETDATE()))
		, @StartLastWeek date = (SELECT DATEADD(wk, -1, DATEADD(DAY, 1-DATEPART(WEEKDAY, GETDATE()), DATEDIFF(dd, 0, GETDATE())))) --Start week on sunday
		, @EndLastWeek date = (SELECT DATEADD(wk, 0, DATEADD(DAY, 0-DATEPART(WEEKDAY, GETDATE()), DATEDIFF(dd, 0, GETDATE()))))
		, @FirstOfYear date = (SELECT EMRIWarehouse.dbo.udfFirstDayOfYear(GETDATE()))


;WITH lastWeek AS (
		SELECT COUNT(ss.ApptId) AS LastWeekScans
				, ss.SF_AccountId
		FROM EMRIWarehouse.dbo.Scans ss
		WHERE ss.Modality = 'MRI'
				AND ss.IsNonRevenue = 0
				AND ss.DOS BETWEEN @StartLastWeek AND @EndLastWeek
		GROUP BY SF_AccountId
	)
, YTD as (
		SELECT COUNT(ss.ApptId) AS YTDScans
				, ss.SF_AccountId
		FROM EMRIWarehouse.dbo.Scans ss
		WHERE ss.Modality = 'MRI'
				AND ss.IsNonRevenue = 0
				AND ss.DOS > @FirstOfYear
		GROUP BY SF_AccountId
	) 
, Scans AS (
		SELECT LastWeekScans
				, YTDScans
				, ly.SF_AccountId
		FROM ytd AS ly
		LEFT JOIN lastWeek lw ON ly.SF_AccountId = lw.SF_AccountId
	)

SELECT  s2.ScanCountMonthly
		, s2.LastReferralDate
		, s.SF_AccountId
		, s.LastWeekScans
		, s.YTDScans
INTO #scans
FROM EMRIWarehouse.dbo.ScansByAccountWorkflow s2 
LEFT JOIN Scans s ON s.SF_AccountId = s2.SF_AccountId AND MonthYear = @FirstOfMonth
--FROM Scans s
--LEFT JOIN EMRIWarehouse.dbo.ScansByAccountWorkflow s2 ON s.SF_AccountId = s2.SF_AccountId AND MonthYear = @FirstOfMonth

/*********************************NEED TO MOVE THIS LOGIC TO EMRIWarehouse.dbo.uspScansByAccountWorkflow*************************/
   

BEGIN TRAN 
	TRUNCATE TABLE OUTBOUND.AccountCategorizationWorkflow 

	INSERT INTO OUTBOUND.AccountCategorizationWorkflow (
		SF_AccountId 
		, BusinessType 
		, AccountStatus
		, AccountCategory
		, PercentChange
		, MarketingType
		, ScansLastMonth
		, ScansThreeMonthAverage
		, ScansThisMonth
		, ScansLastWeek
		, ScansYTD
		, LastReferralDate
		)
	SELECT	s.SF_AccountId
			, s.BusinessType
			, CASE WHEN AccountCategory = 'Inactive' THEN AccountCategory + ' ' + AccountStatus ELSE AccountCategory END
			, CASE WHEN ThreeMonthAvg = 0 THEN 'Level 0'
				   WHEN CEILING(ThreeMonthAvg) < 10 THEN 'Level 1' --There is no rounding done so 9.9 is Level 1
				   WHEN CEILING(ThreeMonthAvg) >= 10 AND CEILING(ThreeMonthAvg) < 20 THEN 'Level 2'
				   WHEN CEILING(ThreeMonthAvg) >= 20 AND CEILING(ThreeMonthAvg) < 50 THEN 'Level 3'
				   WHEN CEILING(ThreeMonthAvg) >= 50 THEN 'Level 4'
			  END AS ScanCategory
			, CASE WHEN AccountCategory = 'Active' THEN AccountStatus ELSE 'N/A' END
			, null AS MarketingType
			, s.ScanCountMonthly
			, CAST(ThreeMonthAvg as decimal(5,2))
			, COALESCE(s2.ScanCountMonthly, 0)
			, COALESCE(s2.LastWeekScans, 0)
			, COALESCE(s2.YTDScans, 0)
			, COALESCE(s2.LastReferralDate, s.LastReferralDate)
	FROM EMRIWarehouse.dbo.ScansByAccountWorkflow s
	LEFT JOIN #scans s2 ON s.SF_AccountId = s2.SF_AccountId
	WHERE MonthYear = @FirstDayLastMonth
		  AND (s.SF_AccountId = @SFAccountId OR @SFAccountId IS NULL)

COMMIT

USE [Integration]
GO
/****** Object:  StoredProcedure [SF].[ScansBycontactExport]    Script Date: 1/22/2018 12:41:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [SF].[uspScansByContactExport]
	@SFContactId varchar(255) = null
AS

TRUNCATE TABLE OUTBOUND.ScansByContact
TRUNCATE TABLE OUTBOUND.ScansByContactLastReferral

DECLARE @OwnerId varchar(25) = '005f4000000e552'

INSERT INTO OUTBOUND.ScansByContact (
		DOSMonthYear
        , DOSMonth
        , DOSYear
        --, ARClass
        , ARGroup
        , ReferringDr
        , Modality
        , ExamCount
        , LatestExam
		, SF_ContactId
		, OwnerId
	)

SELECT	CAST(DATEPART(MONTH, r.DOS) AS varchar(2)) + '/1/'+ CAST(DATEPART(YEAR, r.DOS) AS varchar(4)) AS DOSMonthYear
		, DATEPART(MONTH, r.DOS) AS DOSMonth
		, DATEPART(YEAR, r.DOS) AS DOSYear
		--, r.ARClass
		, ar.ARGroup
		, '' AS ReferringDr --c.ContactName
		, r.Modality
		, COUNT(COALESCE (r.ApptId, r.Account)) as ExamCount
		, CONVERT(varchar(10), MAX(lastexam.LatestExam), 101) AS LatestExam
		, c.SF_ContactId
		, @OwnerId
FROM EMRIWarehouse.dbo.Appointments r
JOIN Integration.dbo.Contacts c ON c.SourceSystemPk = r.ReferringDrId and c.ContactType = 'Doctor'
JOIN (
		SELECT	SF_ContactId
				--, ContactName
				, MAX(DOS) AS LatestExam
		FROM EMRIWarehouse.dbo.Appointments appt
		JOIN Integration.dbo.Contacts c ON c.SourceSystemPk = appt.ReferringDrId AND c.ContactType = 'Doctor'
		GROUP BY SF_ContactId 
	  ) lastexam ON c.SF_ContactId = lastexam.SF_ContactId
JOIN EMRIWarehouse.XRef.ARClassGroup ar ON r.ARClass = ar.ARClass
WHERE AppointmentStatus = 'Exam Complete'
	  AND COALESCE(c.SF_ContactId, '') <> ''
	  AND (@SFContactId = c.SF_ContactId OR @SFContactId IS NULL)
	  AND r.Account <> '10021'
GROUP BY CAST(DATEPART(MONTH, r.DOS) AS varchar(2)) + '/1/'+ CAST(DATEPART(YEAR, r.DOS) AS varchar(4))
		 , DATEPART(MONTH, r.DOS)  
		 , DATEPART(YEAR, r.DOS) 
		 , ar.ARGroup
		 --, r.ReferringDr
		 , r.Modality
		 --, c.ContactGroupCode
		 , c.SF_ContactId

INSERT INTO OUTBOUND.ScansByContactLastReferral (
		LatestReferral
		, SF_ContactId
		, OwnerId

	)

SELECT DISTINCT LatestExam
				, SF_ContactId
				, @OwnerId
FROM OUTBOUND.ScansByContact


UPDATE sc
SET TrailingSixMos = COALESCE(tt.TrailingSixMos, 0)/6
FROM OUTBOUND.ScansByContact sc
OUTER APPLY (
				SELECT SUM(ExamCount) AS TrailingSixMos
						, SF_ContactId
				FROM OUTBOUND.ScansByContact sc2
				WHERE sc2.DOSMonthYear >= CAST(DATEADD(MONTH, -7, sc.DOSMonthYear) as date) 
					AND sc2.DOSMonthYear < sc.DOSMonthYear
					AND sc.SF_ContactId = sc2.SF_ContactId
					AND sc.ARGroup = sc2.ARGroup
					AND sc.Modality = sc2.Modality
				GROUP BY SF_ContactId
			)tt

UPDATE sc
SET TrailingTwelveMos = COALESCE(tt.TrailingTwelveMos, 0)
FROM OUTBOUND.ScansByContact sc
OUTER APPLY (
				SELECT SUM(ExamCount) AS TrailingTwelveMos
						, SF_ContactId
				FROM OUTBOUND.ScansByContact sc2
				WHERE sc2.DOSMonthYear >= CAST(DATEADD(MONTH, -13, sc.DOSMonthYear) as date) 
					AND sc2.DOSMonthYear < sc.DOSMonthYear
					AND sc.SF_ContactId = sc2.SF_ContactId
					AND sc.ARGroup = sc2.ARGroup
					AND sc.Modality = sc2.Modality
				GROUP BY SF_ContactId
			)tt



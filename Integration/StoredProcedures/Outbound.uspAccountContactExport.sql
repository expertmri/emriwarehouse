USE [Integration]
GO
/****** Object:  StoredProcedure [Outbound].[uspAccountContactExport]    Script Date: 10/24/2017 9:07:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [Outbound].[uspAccountContactExport]
AS

SELECT	c.SourceSystemPK AS PrimaryKey
		, a.AccountNumber AS AccountFK	
		, a.AccountName
		, NULL AS AccountOriginator
		, c.Title
		, c.ContactName
		, c.JobTitle
		, c.Email
		, a.Phone
		, a.Fax
		, a.Website
		, a.Facility
		, a.Address1
		, a.Address2
		, a.City
		, a.StateName AS State
		, a.Zip
		, c.ContactType
FROM dbo.Accounts a
JOIN dbo.Contacts c ON a.AccountNumber = c.AccountNumber
WHERE a.IsExported = 0

GO

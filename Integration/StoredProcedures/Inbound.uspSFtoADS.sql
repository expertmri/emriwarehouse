USE [Integration]
GO
/****** Object:  StoredProcedure [INBOUND].[uspSFtoADS]    Script Date: 2/1/2018 11:38:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [INBOUND].[uspSFtoADS]
AS

BEGIN TRAN

UPDATE c
SET SF_ContactId = sf.Contact_ID__C
	, ModifiedDate = GETDATE()
FROM dbo.Contacts c
JOIN Inbound.SFtoADS sf ON sf.ADS_CONTACT_KEY__C = c.SourceSystemPk

UPDATE a
SET SF_AccountId = sf.Account_ID__C
	, ModifiedDate = GETDATE()
FROM dbo.Accounts a
JOIN Inbound.SFtoADS sf ON sf.ADS_Account_KEY__C = a.AccountNumber

INSERT INTO dbo.AccountContact (SF_AccountId, SF_ContactId)
SELECT DISTINCT	Account_ID__C
		, Contact_ID__C
FROM Inbound.SFtoADS sf
LEFT JOIN dbo.AccountContact ac ON sf.ACCOUNT_ID__C = ac.SF_AccountId AND sf.Contact_ID__C = ac.SF_ContactId
WHERE LTRIM(RTRIM(Contact_ID__C)) <> ''
AND ac.AccountContactId IS NULL
/*

--------NEED TO ADD UPDATE FOR ACCOUNT CONTACT TABLE
-------NEED FACILITY, ACCOUNTMANAGER, AND PARENTID

MERGE EMRIWarehouse.SF.Accounts AS dest
USING	(
			SELECT DISTINCT ACCOUNT_ID__C
				   , PRACTICE__C
				   , PHONE__C
				   , FAX__C
				   , WEBSITE__C
				   , ADDRESS1__C
				   , ADDRESS2__C
				   , CITY__C
				   , STATE__C
				   , ZIP__C
			FROM Inbound.SFtoADS
		) AS src
ON (dest.SF_AccountId = src.ACCOUNT_ID__C)
WHEN MATCHED THEN

UPDATE SET
	dest.AccountName = src.PRACTICE__C
	, dest.Phone = src.PHONE__C
	, dest.Fax = src.FAX__C
	, dest.Website = src.WEBSITE__C
	, dest.Address1 = src.ADDRESS1__C
	, dest.Address2 = src.ADDRESS2__C
	, dest.City = src.CITY__C
	, dest.StateName = src.STATE__C
	, dest.Zip = src.ZIP__C
	, ModifiedDate = GETDATE()


WHEN NOT MATCHED THEN
INSERT (
			SF_AccountId
			, AccountName
			, Phone
			, Fax
			, Website
			--, Facility
			, Address1
			, Address2
			, City
			, StateName
			, Zip
			, CreateDate
			--, ModifiedDate
			--, ParentId
			--, AccountManager
		)
VALUES (
			src.ACCOUNT_ID__C
			, src.PRACTICE__C
			, src.PHONE__C
			, src.FAX__C
			, src.WEBSITE__C
			, src.ADDRESS1__C
			, src.ADDRESS2__C
			, src.CITY__C
			, src.STATE__C
			, src.ZIP__C
			, GETDATE()
		)
;

MERGE EMRIWarehouse.SF.Contacts AS dest
USING (
		SELECT	DISTINCT FIRST_NAME__C 
				, LAST_NAME__C
				, MIDDLE_NAME__C
				, SALUTATION__C
				, SUFFIX__C
				, EMAIL__C
				, CONTACT_TYPE__C
				, CONTACT_ID__C
		FROM Inbound.SFtoADS
	) AS src
ON (dest.SF_ContactId = src.CONTACT_ID__C)
WHEN MATCHED THEN

UPDATE SET
      dest.FirstName = src.FIRST_NAME__C
      , dest.LastName = src.LAST_NAME__C
      , dest.MiddleName = src. MIDDLE_NAME__C
      , dest.Salutation = src.SALUTATION__C
      , dest.Suffix = src.SUFFIX__C
      , dest.JobTitle = null
      --, dest.Phone = src.PHONE__C
      --, dest.Fax = src.FAX__C
      , dest.Email = src.EMAIL__C
      , dest.ModifiedDate = GETDATE()
      , dest.ContactType = src.CONTACT_TYPE__C

WHEN NOT MATCHED THEN
INSERT	(
			SF_ContactId
			, FirstName
			, LastName
			, MiddleName
			, Salutation
			, Suffix
			, JobTitle
			--, Phone
			--, Fax
			, Email
			, CreateDate
			, ContactType 

		)
VALUES	(
			src.CONTACT_ID__C
			, src.FIRST_NAME__C
			, src.LAST_NAME__C
			, src. MIDDLE_NAME__C
			, src.SALUTATION__C
			, src.SUFFIX__C
			, null
			--, src.PHONE__C
			--, src.FAX__C
			, src.EMAIL__C
			, GETDATE()
			, src.CONTACT_TYPE__C
		)
;
*/
TRUNCATE TABLE Inbound.SFtoADS 

COMMIT


USE [Integration]
GO

/****** Object:  Trigger [dbo].[tr_AccountContact]    Script Date: 10/5/2018 11:58:41 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER TRIGGER [dbo].[tr_AccountContact]
   ON  [dbo].[AccountContact]
   AFTER DELETE,UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	IF EXISTS (SELECT * FROM inserted) AND EXISTS (SELECT * FROM deleted)
	
	BEGIN
		INSERT INTO Archive.AccountContact (
				AccountContactId
				, SF_AccountId
				, SF_ContactId
				, Activity
				, ModifiedUser
				, InsertDate
			)
		SELECT	deleted.AccountContactId
				, deleted.SF_AccountId
				, deleted.SF_ContactId
				, 'UPDATE'
				, COALESCE(deleted.ModifiedUser, 'Original Record')
				, GETDATE()
		FROM deleted
	END

	IF EXISTS(SELECT * FROM deleted) AND NOT EXISTS(SELECT * FROM inserted)

	BEGIN
		INSERT INTO Archive.AccountContact (
				AccountContactId
				, SF_AccountId
				, SF_ContactId
				, Activity
				, ModifiedUser
				, InsertDate
			)
		SELECT	deleted.AccountContactId
				, deleted.SF_AccountId
				, deleted.SF_ContactId
				, 'DELETE'
				, COALESCE(deleted.ModifiedUser, 'Original Record')
				, GETDATE()
		FROM deleted
	END

END
	
GO

ALTER TABLE [dbo].[AccountContact] ENABLE TRIGGER [tr_AccountContact]
GO



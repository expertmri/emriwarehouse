//Initiallising node modules
const express = require("express");
const bodyParser = require("body-parser");
const sql = require("mssql");
const morgan = require('morgan');
// const proxy = require('http-proxy-middleware');
const cors = require('cors')

//Config
const winston = require('./config/winston');
// const { routes } = require('./config.json');

const app = express();

//proxy
// for (route of routes) {
    // app.use(route.route,
        // proxy({
            // target: route.address,
            // pathRewrite: (path, req) => {
                // return path.split('/').slice(2).join('/'); // Could use replace, but take care of the leading '/'
            // }
        // })
    // );
// }

// Body Parser Middleware
app.use(bodyParser.json());
app.use(morgan('combined', { stream: winston.stream }));


// Authentication
function auth (err,req, res, next) {
    // console.log(req.headers);
    winston.error(`${err.status || 500} - ${err.message} - ${req.originalUrl} - ${req.method}`);

	// render the error page
	res.status(err.status || 500);

	//Enabling CORS Middleware
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,POST");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, contentType,Content-Type, Accept, Authorization, client-security-token, x-csrftoken");
	res.header("Access-Control-Allow-Credentials", "true");
	
	var authHeader = req.headers.authorization;
    if (!authHeader) {
        var err = new Error('You are not authenticated!');
        err.status = 401;
        next(err);
        return;
    }
    var auth = new Buffer(authHeader.split(' ')[1], 'base64').toString().split(':');
    var user = auth[0];
    var pass = auth[1];
    if (user == 'ZGJpZ5K3frgPmXSqdD6A7RbgNnKjv2XyVp99kF66ekw6tQqPyg' && pass == 'tB4GrGV524Ws4MCcvA8w3nV8TexJB5JapTGet2Kq6gGsq6sgSk') {
        next(); // authorized
    } else {
        var err = new Error('You are not authenticated!');
        err.status = 401;
        next(err);
    }
}

app.options('*', cors());
app.use(auth);

//Setting up server
 var server = app.listen(process.env.PORT || 8081, function () {
    var port = server.address().port;
    console.log("App now running on port", port);
 });

//Initializing connection string
var dbConfig = {
    user:  		'SalesForceAPI',
    password: 	'$tNG"5>XUde5!H`u',
    server: 	'172.31.0.8',
    database:	'WebAPI'
};

//Function to connect to database and execute query
var  executeQuery = function(query, res){
	 sql.close();
     sql.connect(dbConfig, function (err) {
         if (err) {   
                     console.log('Error while connecting database :- ' + err);
                     res.send(err);
                  }
                  else {
                         // create Request object
                         var request = new sql.Request();
                         // query to the database
                         request.query(query, function (err, result) {
							if (err) {
							console.log('Error while querying database :- ' + err);
							// res.send(err);
							res.send('Error with request');
							sql.close();
							}
							else {
							res.status(200);
							res.send('Success');
							sql.close();
							}
							});
                       }
      });           
}

//GET API
app.get("/api/user", function(req , res){
                // var query = 'select * from SalesForceRequest';
				// var query = 'select top 100 * from dbo.Appointments';
                // executeQuery (query, res);
				res.send('Hello World!');
				
});

//POST API
 app.post("/salesforce/syncdata", function(req , res){
					var postBody = req.body;
					var postData = JSON.stringify(postBody);
					var query = "EXEC dbo.uspSalesForceRequest @postArray = '" + postData + "'";
					executeQuery (query, res);
});

//This request requires CORS for browser ajax request to API
app.post("/webforms/schedreq", cors(), function(req , res, next){
					var postBody = req.body;
					var postData = JSON.stringify(postBody);
					var query = "EXEC dbo.uspWebFormRequest @postArray = '" + postData + "'";
					executeQuery (query, res);
});


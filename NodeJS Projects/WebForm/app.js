const express = require('../node_modules/express')
	, logger = require('../node_modules/morgan')
	, app = express()
	, sql = require("mssql")
	, proxy = require('../node_modules/http-proxy-middleware')
	, path    = require("path")
	, cors = require('cors');


app.use(logger('dev'))
// app.use(express.static(__dirname + '/static'))
app.use(express.static(__dirname))
app.use(cors())

// Config
// const { routes } = require('./config.json');

// for (route of routes) {
    // app.use(route.route,
        // proxy({
            // target: route.address,
            // pathRewrite: (path, req) => {
                // return path.split('/').slice(2).join('/'); // Could use replace, but take care of the leading '/'
            // }
        // })
    // );
// }

app.options('*', cors()) // include before other routes
app.get('/', function(req,res){
  res.sendFile(path.join(__dirname+'/index.html'));
  //__dirname : It will resolve to your project folder.
});



var server = app.listen(process.env.PORT || 8080, function () {
    var port = server.address().port;
    console.log("App now running on port", port);
 });